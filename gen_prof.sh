#!/bin/bash


proj_root="`dirname \"$0\"`" # relative
proj_root="`( cd \"$proj_root\" && pwd )`"  # absolutized and normalized
if [ -z "$proj_root" ] ; then
  # error; for some reason, the path is not accessible
  # to the script (e.g. permissions re-evaled after suid)
  exit 1  # fail
fi

cd $proj_root

cpu_pprof="cpu.pprof"
mem_pprof="mem.pprof"

if [ -f "$proj_root/$cpu_pprof" ] ; then
    go tool pprof -pdf $cpu_pprof > cpu.pdf
fi

if [ -f "$proj_root/$mem_pprof" ] ; then
    go tool pprof -pdf $mem_pprof > mem.pdf
fi