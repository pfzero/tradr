package admin

import (
	"bitbucket.org/pfzero/tradr/admin/auth"
	"bitbucket.org/pfzero/tradr/admin/config"
)

// Initialize initializes the admin
// panel
func Initialize() {
	auth.ConfigureAuth()
	config.Setup()
}
