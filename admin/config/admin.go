package config

import (
	"bitbucket.org/pfzero/tradr/admin/auth"
	"bitbucket.org/pfzero/tradr/admin/bindatafs"
	staticConfig "bitbucket.org/pfzero/tradr/cfg"
	"bitbucket.org/pfzero/tradr/store"
	"bitbucket.org/pfzero/tradr/traderstore"
	"github.com/qor/admin"
	"github.com/qor/qor"
)

// init listener function
type initHandler func()

// Admin is the module responsible for
// rendering the admin panel
var Admin *admin.Admin

var (
	// view path
	viewPath = "bitbucket.org/pfzero/tradr/static/views/qor"

	// init buffer is used to register internal
	// admin resources and call them
	initBuffer = []initHandler{}
)

// store an "init" listener
func onAdminInit(fn initHandler) {
	initBuffer = append(initBuffer, fn)
}

func handleInitBuffer() {
	for _, fn := range initBuffer {
		fn()
	}
}

func getAvailableTraderGUIDs(i interface{}, ctx *admin.Context) [][]string {
	traders, _ := traderstore.GetInstance().GetAllTraders()
	list := make([][]string, len(traders))
	if traders == nil {
		return list
	}
	for i := 0; i < len(traders); i++ {
		tStats := traders[i].GetStats()

		list[i] = []string{tStats.GUID, tStats.GUID}
	}
	return list
}

// Setup initializes admin module and its depending modules
func Setup() {
	Admin = admin.New(&qor.Config{DB: store.DB.Set("publish:draft_mode", true)})
	Admin.SetAssetFS(bindatafs.AssetFS)
	Admin.SetSiteName(staticConfig.GetConfig().GetString("appname"))
	Admin.RegisterViewPath(viewPath)
	Admin.AddMenu(&admin.Menu{Name: "Dashboard", Link: "/admin"})
	Admin.SetAuth(auth.AdminAuth{})
	handleInitBuffer()
}
