package config

import (
	"github.com/qor/worker"
)

// Worker is the admin worker resource
var Worker *worker.Worker

func init() {
	onAdminInit(initWorker)
}

func initWorker() {
	Worker = worker.New()

	Worker.RegisterJob(getSimpleMultiCoinWorker())
	Worker.RegisterJob(getBacktestWorker())
	Worker.RegisterJob(getMCTMonteCarloJob())
	Worker.RegisterJob(getMCTGeneratorsJob())

	// Register Job
	Admin.AddResource(Worker)
}
