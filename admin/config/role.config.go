package config

import (
	"bitbucket.org/pfzero/tradr/repo/model"
	"github.com/qor/admin"
)

var roleResource *admin.Resource

func init() {
	onAdminInit(func() {
		roleResource = Admin.AddResource(&model.Role{}, &admin.Config{Menu: []string{"Settings"}})
	})
}
