package config

import (
	"bitbucket.org/pfzero/tradr/repo/model"
	"github.com/qor/admin"
)

var tradeConditionRsc *admin.Resource

func init() {
	onAdminInit(initTradeConditionRsc)
}

func initTradeConditionRsc() {
	resourceMenu := []string{"Crypto Traders"}
	tradeConditionRsc = Admin.AddResource(&model.TradeCondition{}, &admin.Config{Menu: resourceMenu})
}
