package config

import (
	"fmt"
	"time"

	"bitbucket.org/pfzero/tradr/backtest/mcmontecarlo"
	"github.com/pkg/errors"
	"github.com/qor/worker"
)

type mcMonteCarloArg struct {
	mcmontecarlo.BuilderConfiguration

	FilterWithProfitLowerThan float64
	NumberOfResultsPerPair    int
	SortByWeightedProfit      bool
	SortByLastWindowProfit    bool
}

func getMCTMonteCarloJob() *worker.Job {
	mctMonteCarloCfgResource := Admin.NewResource(&mcMonteCarloArg{})

	return &worker.Job{
		Name: "Multi-Coin Trader Monte-Carlo analysis worker",
		Handler: func(arg interface{}, job worker.QorJobInterface) error {
			in, ok := arg.(*mcMonteCarloArg)

			if !ok {
				return errors.Errorf("Couldn't convert the given argument to BuilderConfiguration instance")
			}

			simulator, err := mcmontecarlo.NewMultiCoinMonteCarloSimulator(&in.BuilderConfiguration)

			if err != nil {
				return err
			}

			var totalDuration, avgDuration, runsCount time.Duration

			file := mcmontecarlo.NewExcelOutput("static/public/dl", &in.BuilderConfiguration)

			job.AddLog("Starting simulations")

			for {
				tStart := time.Now()
				pairResults, err := simulator.Next()
				tEnd := time.Now()

				if err != nil {
					return err
				}

				if pairResults == nil {
					break
				}

				currentDur := tEnd.Sub(tStart)
				totalDuration += currentDur
				runsCount++

				job.AddLog(fmt.Sprintf("Finished simulations for pair %s-%s@%s in %s; Got %d results",
					pairResults.FSym,
					pairResults.TSym,
					pairResults.Exchange,
					currentDur,
					pairResults.Len(),
				))

				pairResults = pairResults.Filter(func(pr *mcmontecarlo.ProbeResult) bool {
					cnt := in.BuilderConfiguration.WindowedTimeMonthsCount
					windowedResults := pr.WindowedResults

					for i := range windowedResults {
						wr := &windowedResults[i]
						if wr.InvestmentsMade < (cnt - 1) {
							return false
						}

						if in.FilterWithProfitLowerThan <= 0 {
							continue
						}

						if wr.GetProfit() < in.FilterWithProfitLowerThan {
							return false
						}
					}

					return true
				})

				if pairResults.Len() == 0 {
					job.AddLog(fmt.Sprintf("Simulation for pair: %s-%s@%s yielded 0 results after applying filters",
						pairResults.FSym,
						pairResults.TSym,
						pairResults.Exchange,
					))
					job.AddLog("Continuing")
					continue
				}

				var results *mcmontecarlo.ProbeResultList
				if in.SortByWeightedProfit {
					results = pairResults.SortDescByWeightedProfit().Batch(in.NumberOfResultsPerPair, 1)
				} else {
					results = pairResults.SortDescByLastWindowProfit().Batch(in.NumberOfResultsPerPair, 1)
				}

				if results != nil {
					results = results.SortDescByLastWindowProfit()
				} else {
					job.AddLog(fmt.Sprintf("Failed to run sim for pair: %s-%s@%s; Didn't get any results",
						pairResults.FSym,
						pairResults.TSym,
						pairResults.Exchange,
					))
					continue
				}

				file.AddResults(results)

				e := file.Save()

				if e != nil {
					job.AddLog(fmt.Sprintf("Bummer!! Failed to save excel file due to: %s", e.Error()))
					return e
				}

				progress := simulator.GetProgress()
				perc := progress * 100
				job.SetProgress(uint(perc))

				remainingPairs := simulator.GetRemainingPairsCount()

				job.AddLog(fmt.Sprintf("Remaining pairs to run: %d", remainingPairs))

				avgTime := totalDuration / runsCount
				estimatedRemainingDuration := avgTime * time.Duration(remainingPairs)
				estimatedFinishTime := time.Now().Add(estimatedRemainingDuration)

				job.AddLog(fmt.Sprintf("Estimated Remaining Duration: %s", estimatedRemainingDuration.String()))
				job.AddLog(fmt.Sprintf("Estimated finish time: %s", estimatedFinishTime))
				job.AddLog(fmt.Sprintf("Partial Results available for download at: /dl/%s", file.GetFileName()))
			}

			// just to be sure...
			e := file.Save()
			if e != nil {
				job.AddLog(fmt.Sprintf("Bummer!! Failed to save excel file due to: %s", e.Error()))
				return e
			}

			job.AddLog(fmt.Sprintf("Finished all %d pairs in %s", int(runsCount), totalDuration))
			job.AddLog(fmt.Sprintf("Avg duration per pair: %s", avgDuration))
			job.AddLog(fmt.Sprintf("Link To Download: /dl/%s", file.GetFileName()))

			return nil
		},
		Resource: mctMonteCarloCfgResource,
	}
}
