package config

import (
	"bitbucket.org/pfzero/tradr/repo/model"
	"github.com/qor/admin"
)

var traderTrendStepColResource *admin.Resource

func init() {
	onAdminInit(initCryptoPairTraderStepCol)
}

func initCryptoPairTraderStepCol() {
	resourceMenu := []string{"Crypto Traders"}
	traderTrendStepColResource = Admin.AddResource(&model.TradeTrendStepCol{}, &admin.Config{Menu: resourceMenu})
}
