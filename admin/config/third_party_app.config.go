package config

import (
	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/util/str"
	"github.com/qor/admin"
	"github.com/qor/qor"
	"github.com/qor/qor/resource"
)

var thirdPartyAppResource *admin.Resource

func init() {
	onAdminInit(initThirdPartyApp)
}

func initThirdPartyApp() {
	thirdPartyAppResource = Admin.AddResource(&model.ThirdPartyApp{}, &admin.Config{Menu: []string{"Account Management"}})
	thirdPartyAppResource.Meta(&admin.Meta{Name: "ConsumerToken", Type: "Hidden",
		FormattedValuer: func(interface{}, *qor.Context) interface{} { return "" },
		Valuer:          func(interface{}, *qor.Context) interface{} { return "" },
		Setter: func(record interface{}, metaValue *resource.MetaValue, context *qor.Context) {
			var thirdPartyApp *model.ThirdPartyApp
			var ok bool
			if thirdPartyApp, ok = record.(*model.ThirdPartyApp); !ok {
				return
			}
			if thirdPartyApp.ConsumerToken == "" {
				thirdPartyApp.ConsumerToken = str.GenerateToken()
			}
		},
	})
	thirdPartyAppResource.Meta(&admin.Meta{Name: "ConsumerSecret", Type: "Hidden",
		Valuer:          func(interface{}, *qor.Context) interface{} { return "" },
		FormattedValuer: func(interface{}, *qor.Context) interface{} { return "" },
		Setter: func(record interface{}, metaValue *resource.MetaValue, context *qor.Context) {
			var thirdPartyApp *model.ThirdPartyApp
			var ok bool
			if thirdPartyApp, ok = record.(*model.ThirdPartyApp); !ok {
				return
			}
			if thirdPartyApp.ConsumerSecret == "" {
				thirdPartyApp.ConsumerSecret = str.GenerateToken()
			}
		},
	})
	thirdPartyAppResource.Meta(&admin.Meta{Name: "DisplayedToken", Type: "Readonly",
		Valuer: func(record interface{}, ctx *qor.Context) interface{} {
			var thirdPartyApp *model.ThirdPartyApp
			var ok bool
			if thirdPartyApp, ok = record.(*model.ThirdPartyApp); !ok {
				return ""
			}
			return thirdPartyApp.ConsumerToken
		},
	})
	thirdPartyAppResource.ShowAttrs("Author", "Name", "DisplayedToken")
	thirdPartyAppResource.IndexAttrs("ID", "Author", "Name", "DisplayedToken")
}
