package config

import (
	"bitbucket.org/pfzero/tradr/repo/model"
	"github.com/qor/admin"
)

var portfolioResource *admin.Resource

func init() {
	onAdminInit(initPortfolioRsc)
}

func initPortfolioRsc() {
	resourceMenu := []string{"Crypto Traders"}
	portfolioResource = Admin.AddResource(&model.Portfolio{}, &admin.Config{Menu: resourceMenu})
}
