package config

import (
	"errors"
	"strconv"

	"bitbucket.org/pfzero/tradr/store/datastore/cryptomarketstore"

	"bitbucket.org/pfzero/tradr/repo/model"
	"github.com/qor/admin"
	"github.com/qor/qor"
)

var exchangeSymbolAliasRes *admin.Resource

func init() {
	onAdminInit(initExchangeSymbolAlias)
}

func initExchangeSymbolAlias() {
	exchangeSymbolAliasRes = Admin.AddResource(&model.ExchangeSymbolAlias{}, &admin.Config{Menu: []string{"Crypto Resources"}})

	oldSaveHandler := exchangeSymbolAliasRes.SaveHandler
	exchangeSymbolAliasRes.SaveHandler = func(v interface{}, ctx *qor.Context) error {

		alias, ok := v.(*model.ExchangeSymbolAlias)
		if !ok {
			return errors.New("Unexpected Error occured. Couldn't assert that the given value is a ExchangeSymbolAlias model instance")
		}
		err := oldSaveHandler(v, ctx)
		if err == nil {
			cryptomarketstore.SaveAliasToCache(*alias)
		}
		return err
	}

	oldDelHandler := exchangeSymbolAliasRes.DeleteHandler
	exchangeSymbolAliasRes.DeleteHandler = func(v interface{}, ctx *qor.Context) error {
		resourceID, err := strconv.ParseUint(ctx.ResourceID, 10, strconv.IntSize)
		populatedAlias, err := cryptomarketstore.GetAliasByIDFromDB(uint(resourceID))
		if err != nil {
			return err
		}

		err = oldDelHandler(v, ctx)
		if err == nil {
			cryptomarketstore.DeleteAliasFromCache(*populatedAlias)
		}
		return err
	}
}
