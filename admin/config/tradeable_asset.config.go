package config

import (
	"bitbucket.org/pfzero/tradr/repo/model"
	"github.com/qor/admin"
)

var tradeableAssetResource *admin.Resource

func init() {
	onAdminInit(initTradeableAssetResource)
}

func initTradeableAssetResource() {
	tradeableAssetResource = Admin.AddResource(&model.TradeableAsset{}, &admin.Config{Menu: []string{"Settings"}})
	tradeableAssetResource.ShowAttrs("Name", "Symbol")
	tradeableAssetResource.EditAttrs("Name")
}
