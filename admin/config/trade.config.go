package config

import (
	"fmt"
	"time"

	"bitbucket.org/pfzero/tradr/repo/modelutil"
	"bitbucket.org/pfzero/tradr/store"

	"bitbucket.org/pfzero/tradr/logs"

	"bitbucket.org/pfzero/tradr/traderstore"

	"bitbucket.org/pfzero/tradr/store/datastore"
	"bitbucket.org/pfzero/tradr/store/datastore/cryptomarketstore"

	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/repo/modelenum"
	"github.com/qor/admin"
	"github.com/qor/qor"
	"github.com/sirupsen/logrus"
)

var tradeResource *admin.Resource

func init() {
	onAdminInit(initTradeResource)
}

func initTradeResource() {
	m := &model.Trade{}

	tradeResource = Admin.AddResource(m, &admin.Config{Menu: []string{"Ledger"}})

	tradeResource.Meta(&admin.Meta{Name: "Type", Config: &admin.SelectOneConfig{Collection: modelenum.GetTradeTypes()}})

	tradeResource.Meta(&admin.Meta{Name: "Status", Config: &admin.SelectOneConfig{Collection: modelenum.GetTradeStates()}})

	tradeResource.Meta(&admin.Meta{Name: "TradeDecision", Config: &admin.SelectOneConfig{RemoteDataResource: tradeDecisionResource}})

	tradeResource.Meta(&admin.Meta{
		Name: "Title",
		Type: "string",
		Valuer: func(v interface{}, ctx *qor.Context) interface{} {
			if t, ok := v.(*model.Trade); ok {
				return fmt.Sprintf("Trade#%d", t.GetID())
			}
			return "Trade#Unknown"
		},
	})

	tradeResource.EditAttrs("FromAmountTrade", "ToAmountTrade", "TradeFinishTime", "Status")

	// oldSaveHandler := tradeResource.SaveHandler
	tradeResource.SaveHandler = func(v interface{}, ctx *qor.Context) error {
		trade, ok := v.(*model.Trade)

		if !ok {
			return fmt.Errorf("Couldn't save the trade. Error, couldn't be converted to model.Trade")
		}

		if trade.TradeFinishTime == nil || trade.TradeFinishTime.IsZero() {
			now := time.Now()
			trade.TradeFinishTime = &now
		}

		if trade.FromAmountTrade != 0 && trade.ToAmountTrade != 0 {
			trade.TradePrice = trade.ToAmountTrade / trade.FromAmountTrade
		}

		// saveErr := oldSaveHandler(v, ctx)
		saveErr := datastore.Save(store.DB, trade)
		if saveErr != nil {
			return saveErr
		}

		populatedTrade, err := cryptomarketstore.GetTradeByID(trade.GetID())
		if err != nil {
			logs.GetLogger().WithFields(logrus.Fields{
				"module": "admin.trade.config.go/trade_save_handler",
			}).Errorf("Couldn't fetch trade with id %d; Err: %s", populatedTrade.GetID(), err.Error())
			return err
		}

		if populatedTrade.TradeDecisionID == 0 {
			return nil
		}

		tradeDec, err := cryptomarketstore.GetTradeDecisionByID(populatedTrade.TradeDecisionID)
		if err != nil {
			logs.GetLogger().WithFields(logrus.Fields{
				"module": "admin.trade.config.go/trade_save_handler",
			}).Errorf("Couldn't fetch trade decision for trader with %d; Trade Dec ID: %d; Err: %s", populatedTrade.GetID(), populatedTrade.TradeDecisionID, err.Error())
			return nil
		}

		if tradeDec.TraderGUID == "" {
			return nil
		}

		trader, err := traderstore.GetInstance().GetTraderByGUID(tradeDec.TraderGUID)
		if err != nil {
			return nil
		}

		traderLedger := trader.GetLedger()
		traderLedger.Acquire()
		existingTrade := traderLedger.GetTradeList().GetByID(populatedTrade.GetID())
		traderLedger.Release()

		if existingTrade != nil {
			err := traderstore.GetInstance().RefreshTraderInstance(tradeDec.TraderGUID)
			if err != nil {
				logs.GetLogger().WithFields(logrus.Fields{
					"module": "admin.trade.config.go/trade_save_handler",
				}).Errorf("Couldn't refreshed trader with guid: %s; Err was: %s", tradeDec.TraderGUID, err.Error())
			}
			logs.GetLogger().WithFields(logrus.Fields{
				"module": "admin.trade.config.go/trade_save_handler",
			}).Infof("Refreshed trader with guid: %s", trader.GetStats().GUID)

			return nil
		}

		traderLedger.Acquire()
		modelutil.AddTradeToLedger(populatedTrade, traderLedger)
		traderLedger.Release()

		e := trader.HandleProcessedTrade(populatedTrade)
		if e != nil {
			logs.GetLogger().WithFields(logrus.Fields{
				"module": "admin.trade.config.go/trade_save_handler",
			}).Errorf(
				"Trader with guid: %s couldn handle the processed trade with ledger tx id: %s; Err was: %s",
				tradeDec.TraderGUID,
				populatedTrade.LedgerTxID,
				err.Error(),
			)
		}

		return e
	}

	oldDelHandler := tradeResource.DeleteHandler
	tradeResource.DeleteHandler = func(v interface{}, c *qor.Context) error {
		trade, ok := v.(*model.Trade)

		if !ok {
			return fmt.Errorf("Couldn't delete the trade. Error, couldn't be converted to model.Trade")
		}

		if trade.TradeDecisionID == 0 {
			return oldDelHandler(v, c)
		}

		tradeDec, err := cryptomarketstore.GetTradeDecisionByID(trade.TradeDecisionID)

		if err != nil {
			return err
		}

		if tradeDec.TraderGUID == "" {
			return oldDelHandler(v, c)
		}

		return fmt.Errorf("Currently, deletion of a trade that is linked to a trader is not supported; Please cancel the trade if possible")
	}
}
