package config

import (
	"fmt"

	"bitbucket.org/pfzero/tradr/traderstore"

	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/repo/modelenum"
	"github.com/pkg/errors"
	"github.com/qor/admin"
	"github.com/qor/qor"
)

var ledgerIOResource *admin.Resource

func init() {
	onAdminInit(initLedgerIOResource)
}

func initLedgerIOResource() {
	ledgerIOResource = Admin.AddResource(&model.LedgerIO{}, &admin.Config{Menu: []string{"Ledger"}})
	LedgerIOTypes := modelenum.GetLedgerIOTypes()
	ledgerIOResource.Meta(&admin.Meta{Name: "Type", Config: &admin.SelectOneConfig{Collection: LedgerIOTypes}})

	ledgerIOResource.Meta(&admin.Meta{
		Name: "TraderGUID",
		Config: &admin.SelectOneConfig{
			Collection: func(i interface{}, ctx *admin.Context) [][]string {
				existingOptions := getAvailableTraderGUIDs(i, ctx)
				ret := [][]string{[]string{"", ""}}
				return append(ret, existingOptions...)
			},
		},
	})

	oldSaveHandler := ledgerIOResource.SaveHandler
	ledgerIOResource.SaveHandler = func(v interface{}, c *qor.Context) error {
		ledgerIO, ok := v.(*model.LedgerIO)
		if !ok {
			return fmt.Errorf("Couldn't convert interface to model.LedgerIO")
		}
		saveErr := oldSaveHandler(v, c)
		if saveErr != nil {
			return saveErr
		}

		if ledgerIO.TraderGUID == "" {
			return nil
		}

		trader, err := traderstore.GetInstance().GetTraderByGUID(ledgerIO.TraderGUID)
		if err != nil {
			return nil
		}

		traderLedger := trader.GetLedger()
		defer traderLedger.Acquire().Release()

		traderLedgerIOList := traderLedger.GetLedgerIOList()

		found := traderLedgerIOList.Find(func(l *model.LedgerIO) bool {
			return l.GetID() == ledgerIO.GetID()
		})

		if found == nil {
			traderLedger.AddLedgerIO(ledgerIO)
			return nil
		}

		// else, try to reload the trader in this case;
		return traderstore.GetInstance().RefreshTraderInstance(ledgerIO.TraderGUID)
	}

	oldDelHandler := ledgerIOResource.DeleteHandler
	ledgerIOResource.DeleteHandler = func(v interface{}, c *qor.Context) error {
		ledgerIO, ok := v.(*model.LedgerIO)
		if !ok {
			return fmt.Errorf("Couldn't convert interface to model.LedgerIO")
		}
		if ledgerIO.TraderGUID == "" {
			return oldDelHandler(v, c)
		}

		return errors.New("Currently, deleting a ledger i/o linked with a trader is unsupported; Please create an opposite deposit/withdrawal to achieve the same side effect")
	}
}
