package config

import (
	"fmt"
	"time"

	"bitbucket.org/pfzero/tradr/repo/modelenum"

	"bitbucket.org/pfzero/tradr/repo/model"

	"bitbucket.org/pfzero/tradr/store/datastore/cryptomarketstore"

	"github.com/pkg/errors"

	"bitbucket.org/pfzero/tradr/backtest/traderbacktest"
	"github.com/qor/worker"
)

type simpleMultiCoinTraderCfg struct {
	FromTime                 time.Time
	ToTime                   time.Time
	InitialFiatAmountPerCoin float64
	CheckSnapData            bool
	CheckDbData              bool
	TraderConfig             traderbacktest.SimpleMultiCoinCfg
}

func getSimpleMultiCoinWorker() *worker.Job {
	argResource := Admin.NewResource(&simpleMultiCoinTraderCfg{})

	return &worker.Job{
		Name:     "Simple Multi Coin Trader Backtest",
		Resource: argResource,
		Handler: func(arg interface{}, job worker.QorJobInterface) error {

			in, ok := arg.(*simpleMultiCoinTraderCfg)
			if !ok {
				return errors.Errorf("Couldn't convert the given argument to simpleMultiCoinTraderCfg instance")
			}

			builtTrader, err := in.TraderConfig.BuildMultiCoinTrader()

			if err != nil {
				return err
			}

			exchange := builtTrader.GetStats().Exchange
			marketExchange := "Market"

			initialExchangeMarketSnap, err := cryptomarketstore.GetMarketSnapClosestTo(builtTrader.GetStats().Exchange, in.FromTime)
			if err != nil {
				return err
			}

			if exchange != marketExchange {
				initialMarketSnap, err := cryptomarketstore.GetMarketSnapClosestTo(marketExchange, in.FromTime)
				if err != nil {
					return err
				}

				available := initialMarketSnap.ListAvailable()[marketExchange]

				for sym := range available {
					if initialExchangeMarketSnap.HasAvailable(exchange, sym) {
						continue
					}

					p := initialMarketSnap.Get(sym, marketExchange)
					initialExchangeMarketSnap.Set(sym, exchange, p)
				}
			}

			basePortfolio := in.TraderConfig.BasePortfolio

			initialBalanceCells := []worker.TableCell{}
			for i := range basePortfolio.PortfolioAssets {
				tradeableAsset := basePortfolio.PortfolioAssets[i].TradeableAsset
				assetSym := tradeableAsset.Symbol
				assetPrice := initialExchangeMarketSnap.Get(assetSym, exchange)

				if assetPrice == nil {
					job.AddLog(fmt.Sprintf("Couldn't find symbol %s at time %s", assetSym, in.FromTime))
					continue
				}

				amt := in.InitialFiatAmountPerCoin / assetPrice.GetPrice()

				builtTrader.GetLedger().AddLedgerIO(&model.LedgerIO{
					Identity: model.Identity{
						CreatedAt: assetPrice.GetTimestamp(),
						UpdatedAt: assetPrice.GetTimestamp(),
					},
					Asset:      tradeableAsset,
					Amount:     amt,
					Type:       modelenum.Deposit.String(),
					TraderGUID: builtTrader.GetStats().GUID,
				})

				initialBalanceCells = append(initialBalanceCells, worker.TableCell{
					Value: fmt.Sprintf("%s: %f", assetSym, amt),
				})
			}

			job.AddResultsRow(initialBalanceCells...)

			report, backTestedTrader, err := traderbacktest.Run(builtTrader, in.FromTime, in.ToTime, in.CheckSnapData, in.CheckDbData)

			if err != nil {
				return err
			}

			tradeList := backTestedTrader.GetLedger().GetTradeList()
			investmentsCnt := tradeList.Filter(func(t *model.Trade) bool { return t.LinkedTradeID == 0 }).Len()
			profitsCnt := tradeList.Filter(func(t *model.Trade) bool { return t.LinkedTradeID > 0 }).Len()

			tradeReport := getFormattedTradeReport(report)

			for i := range tradeReport {
				job.AddLog(tradeReport[i])
			}

			job.AddResultsRow(
				worker.TableCell{
					Value: fmt.Sprintf("Investments Made: %d", investmentsCnt),
				},
				worker.TableCell{
					Value: fmt.Sprintf("Profits made: %d", profitsCnt),
				},
			)

			finalBalance := backTestedTrader.GetLedger().GetTotalBalance().GetRaw()
			finalBalanceCells := []worker.TableCell{}

			for sym, amt := range finalBalance {
				finalBalanceCells = append(finalBalanceCells, worker.TableCell{
					Value: fmt.Sprintf("%s: %f", sym, amt),
				})
			}

			job.AddResultsRow(finalBalanceCells...)

			return nil
		},
	}
}
