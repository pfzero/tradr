package config

import (
	"fmt"
	"time"

	"bitbucket.org/pfzero/tradr/backtest/traderbacktest"
	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/store/datastore/cryptomarketstore"
	"bitbucket.org/pfzero/tradr/traderstore"
	"github.com/qor/admin"
	"github.com/qor/worker"
)

type backTestTraderArg struct {
	FromTime   time.Time
	ToTime     time.Time
	TraderGUID string

	CheckAgainstSnapData bool
	CheckAgainstDB       bool

	worker.Schedule
}

func getFormattedTradeReport(report *traderbacktest.Report) []string {
	formatted := []string{}
	decisionReports := report.GetDecisionReports()

	for i := 0; i < len(decisionReports); i++ {
		accum := fmt.Sprintf(`Trade Decision #%d:`+"\n", i+1)
		decisionReport := decisionReports[i]

		if len(decisionReport.LiveDataProblems) > 0 {
			problematicTrades := decisionReport.LiveDataProblems
			for j := 0; j < len(problematicTrades); j++ {
				problematicTrade := problematicTrades[j]
				trade := problematicTrade.Trade
				accum += fmt.Sprintf(`Trade %.2f %s FOR %.2f %s @price %.2f is problematic
				
				Problem is %s
				`, trade.FromAmountTrade, trade.From.Symbol, trade.ToAmountTrade, trade.To.Symbol, trade.TradePrice, problematicTrade.Error.Error())
			}
		}

		if len(decisionReport.StoredDataProblems) > 0 {
			problematicTrades := decisionReport.StoredDataProblems
			for j := 0; j < len(problematicTrades); j++ {
				problematicTrade := problematicTrades[j]
				trade := problematicTrade.Trade
				accum += fmt.Sprintf(`Trade %.2f %s FOR %.2f %s @price %.2f is problematic
				
				Problem is %s
				`, trade.FromAmountTrade, trade.From.Symbol, trade.ToAmountTrade, trade.To.Symbol, trade.TradePrice, problematicTrade.Error.Error())
			}
		}

		trades := decisionReport.TradeDecision.Trades
		for j := 0; j < len(trades); j++ {
			trade := trades[j]
			if trade.LinkedTradeID != 0 {
				accum += fmt.Sprintf(
					`Trade#%d: Profit %.2f %s FOR %.2f %s @price %.2f at %s`+"\n",
					j+1,
					trade.FromAmountTrade,
					trade.From.Symbol,
					trade.ToAmountTrade,
					trade.To.Symbol,
					trade.TradePrice,
					trade.TradeFinishTime,
				)

			} else {
				accum += fmt.Sprintf(
					`Trade#%d: Trade %.2f %s FOR %.2f %s @price %.2f at %s`+"\n",
					j+1,
					trade.FromAmountTrade,
					trade.From.Symbol,
					trade.ToAmountTrade,
					trade.To.Symbol,
					trade.TradePrice,
					trade.TradeFinishTime,
				)
			}
		}
		formatted = append(formatted, accum)
	}
	return formatted
}

func getBacktestWorker() *worker.Job {

	backTestTraderArgRsc := Admin.NewResource(&backTestTraderArg{})
	backTestTraderArgRsc.Meta(&admin.Meta{
		Name: "TraderGUID",
		Config: &admin.SelectOneConfig{
			Collection: getAvailableTraderGUIDs,
		},
	})

	return &worker.Job{
		Name: "Backtest Trader",
		Handler: func(arg interface{}, job worker.QorJobInterface) error {
			job.AddLog("Started job for back-testing trader")
			in := arg.(*backTestTraderArg)
			job.AddLog(fmt.Sprintf("Argument: %+v", in))

			if in.ToTime.IsZero() {
				in.ToTime = time.Now()
			}

			trader, err := traderstore.GetInstance().GetTraderForBacktesting(in.FromTime, in.TraderGUID)
			if err != nil {
				return err
			}

			initialBalance := trader.GetLedger().GetTotalBalance().GetRaw()
			startGrandTotal := 0.0

			for sym, amt := range initialBalance {
				var p float64
				if sym == "USD" {
					p = 1
				} else {
					price, e := cryptomarketstore.GetPriceClosestToBySymAndExchange(sym, "Market", in.FromTime)
					if e != nil {
						job.AddLog(fmt.Sprintf("Couldn't find price for %s@Market", sym))
						continue
					}
					p = price.GetPrice()
				}
				startGrandTotal += amt * p
			}

			report, backTestedTrader, err := traderbacktest.Run(trader, in.FromTime, in.ToTime, in.CheckAgainstSnapData, in.CheckAgainstDB)

			if err != nil {
				job.SetStatus(worker.JobStatusException)
				return err
			}

			job.AddLog("Trader backtests ran. Preparing results...")

			formattedReport := []string{}
			if report != nil {
				formattedReport = getFormattedTradeReport(report)
			}

			for _, v := range formattedReport {
				job.AddLog(v)
			}

			job.AddLog(fmt.Sprintf("Start With Total of: $%.2f", startGrandTotal))

			initialBalanceCells := []worker.TableCell{}
			for k, v := range initialBalance {
				cell := worker.TableCell{Value: fmt.Sprintf("%s : %f", k, v)}
				initialBalanceCells = append(initialBalanceCells, cell)
			}
			job.AddResultsRow(initialBalanceCells...)

			ledger := backTestedTrader.GetLedger()
			defer ledger.Acquire().Release()
			rawBalance := ledger.GetTotalBalance().GetRaw()

			cells := []worker.TableCell{}
			for k, v := range rawBalance {
				cell := &worker.TableCell{
					Value: fmt.Sprintf(`%s : %f`, k, v),
				}
				cells = append(cells, *cell)
			}

			job.AddResultsRow(cells...)

			finalGrandTotal := 0.0
			for sym, amt := range rawBalance {
				var p float64
				if sym == "USD" {
					p = 1
				} else {
					price, e := cryptomarketstore.GetPriceClosestToBySymAndExchange(sym, "Market", in.ToTime)
					if e != nil {
						job.AddLog(fmt.Sprintf("Couldn't find price for %s@Market", sym))
						continue
					}
					p = price.GetPrice()
				}
				finalGrandTotal += amt * p
			}

			job.AddLog(fmt.Sprintf("Final Balance: %v", rawBalance))
			job.AddLog(fmt.Sprintf("Final grand total: $%.2f", finalGrandTotal))

			tradeList := ledger.GetTradeList()
			investmentsCnt := tradeList.Filter(func(t *model.Trade) bool { return t.LinkedTradeID == 0 }).Len()
			profitsCnt := tradeList.Filter(func(t *model.Trade) bool { return t.LinkedTradeID > 0 }).Len()

			ethFinalPrice := 0.0
			ethInitialPrice := 0.0

			ethFinalPriceModel, err := cryptomarketstore.GetPriceClosestToBySymAndExchange("ETH", "Market", in.ToTime)
			if err != nil {
				job.AddLog(fmt.Sprintf("Couldn't find price for eth@Market at %s", in.ToTime.String()))
			} else {
				ethFinalPrice = ethFinalPriceModel.GetPrice()
			}
			ethInitialPriceModel, err := cryptomarketstore.GetPriceClosestToBySymAndExchange("ETH", "Market", in.FromTime)
			if err != nil {
				job.AddLog(fmt.Sprintf("Couldn't find price for eth@Market at %s", in.FromTime.String()))
			} else {
				ethInitialPrice = ethInitialPriceModel.GetPrice()
			}

			finalFiatHodl := 0.0
			for sym, amt := range initialBalance {
				var price float64
				if sym == "USD" {
					price = 1
				} else {
					p, e := cryptomarketstore.GetPriceClosestToBySymAndExchange(sym, "Market", in.ToTime)
					if e != nil {
						job.AddLog(fmt.Sprintf("Couldn't find price for %s@Market at %s", sym, in.ToTime.String()))
					} else {
						price = p.GetPrice()
					}
				}

				finalFiatHodl += amt * price
			}

			initialETH := 0.0
			for sym, amt := range initialBalance {
				var price float64
				if "ETH" == sym {
					initialETH += amt
					continue
				}

				if sym == "USD" {
					price = 1 / ethInitialPrice
				} else {
					p, e := cryptomarketstore.ConvertPriceClosestToBySymAndExchange(sym, "ETH", "Market", in.FromTime)
					if e != nil {
						job.AddLog(fmt.Sprintf("Couldn't get the price of %s:ETH@Market at %s", sym, in.FromTime.String()))
					} else {
						price = p.GetPrice()
					}
				}

				initialETH += amt * price
			}

			finalETHHodl := 0.0
			for sym, amt := range initialBalance {
				var price float64
				if "ETH" == sym {
					finalETHHodl += amt
					continue
				}

				if sym == "USD" {
					price = 1 / ethFinalPrice
				} else {
					p, e := cryptomarketstore.ConvertPriceClosestToBySymAndExchange(sym, "ETH", "Market", in.ToTime)
					if e != nil {
						job.AddLog(fmt.Sprintf("Couldn't get the price of %s:ETH@Market at %s", sym, in.ToTime.String()))
					} else {
						price = p.GetPrice()
					}
				}

				finalETHHodl += amt * price
			}

			finalETH := 0.0

			for sym, amt := range rawBalance {
				if sym == "ETH" {
					finalETH += amt
					continue
				}

				var price float64
				if sym == "USD" {
					price = 1 / ethFinalPrice
				} else {
					p, e := cryptomarketstore.ConvertPriceClosestToBySymAndExchange(sym, "ETH", "Market", in.ToTime)
					if e != nil {
						job.AddLog(fmt.Sprintf("Couldn't get the price of %s:ETH@Market at %s", sym, in.ToTime.String()))
					} else {
						price = p.GetPrice()
					}
				}

				finalETH += price * amt
			}

			job.AddResultsRow(
				worker.TableCell{Value: fmt.Sprintf("Investments Count: %d", investmentsCnt)},
				worker.TableCell{Value: fmt.Sprintf("Profits Count: %d", profitsCnt)},
				worker.TableCell{Value: fmt.Sprintf("Initial Fiat: $%f", startGrandTotal)},
				worker.TableCell{Value: fmt.Sprintf("Final Fiat HODL: $%f", finalFiatHodl)},
				worker.TableCell{Value: fmt.Sprintf("Final Fiat: $%f", finalGrandTotal)},
				worker.TableCell{Value: fmt.Sprintf("Initial ETH: %f ETH", initialETH)},
				worker.TableCell{Value: fmt.Sprintf("Final ETH HODL: %f ETH", finalETHHodl)},
				worker.TableCell{Value: fmt.Sprintf("Final ETH: %f ETH", finalETH)},
			)

			job.AddLog("Trader backtesting finished.")
			return nil
		},
		Resource: backTestTraderArgRsc,
	}
}
