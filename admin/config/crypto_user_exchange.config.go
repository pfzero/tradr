package config

import (
	"bitbucket.org/pfzero/tradr/repo/model"
	"github.com/qor/admin"
)

var cryptoUserExchange *admin.Resource

func init() {
	onAdminInit(initCryptoUserExchange)
}

func initCryptoUserExchange() {
	cryptoUserExchange = Admin.AddResource(&model.UserCryptoExchange{}, &admin.Config{Menu: []string{"Crypto Resources"}})
}
