package config

import (
	"bitbucket.org/pfzero/tradr/repo/model"
	"github.com/qor/admin"
	"github.com/qor/qor"
	"github.com/qor/qor/resource"
	"github.com/qor/validations"
	"golang.org/x/crypto/bcrypt"
)

var userResource *admin.Resource

func init() {
	onAdminInit(initUser)
}

func initUser() {
	userResource = Admin.AddResource(&model.User{}, &admin.Config{Menu: []string{"Account Management"}})
	userResource.Meta(&admin.Meta{Name: "Gender", Config: &admin.SelectOneConfig{Collection: []string{"Male", "Female", "Unknown"}}})
	userResource.Meta(&admin.Meta{Name: "Role", Config: &admin.SelectOneConfig{RemoteDataResource: roleResource}})
	userResource.Meta(&admin.Meta{Name: "Password",
		Type:            "password",
		FormattedValuer: func(interface{}, *qor.Context) interface{} { return "" },
		Setter: func(resource interface{}, metaValue *resource.MetaValue, context *qor.Context) {
			values := metaValue.Value.([]string)
			if len(values) > 0 {
				if newPassword := values[0]; newPassword != "" {
					bcryptPassword, err := bcrypt.GenerateFromPassword([]byte(newPassword), bcrypt.DefaultCost)
					if err != nil {
						context.DB.AddError(validations.NewError(userResource, "Password", "Can't encrpt password"))
						return
					}
					u := resource.(*model.User)
					u.Password = string(bcryptPassword)
				}
			}
		},
	})
	userResource.Meta(&admin.Meta{Name: "Confirmed", Valuer: func(user interface{}, ctx *qor.Context) interface{} {
		if user.(*model.User).ID == 0 {
			return true
		}
		return user.(*model.User).Confirmed
	}})

	userResource.Filter(&admin.Filter{
		Name: "Role",
		Config: &admin.SelectOneConfig{
			RemoteDataResource: roleResource,
		},
	})

	userResource.IndexAttrs("ID", "Email", "Name", "Gender", "Role")
	userResource.ShowAttrs(
		&admin.Section{
			Title: "Basic Information",
			Rows: [][]string{
				{"Name"},
				{"Email", "Password"},
				{"Gender", "Role"},
				{"Confirmed"},
			},
		},
		"Addresses",
	)
	userResource.EditAttrs(userResource.ShowAttrs())
}
