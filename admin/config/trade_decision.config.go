package config

import (
	"fmt"

	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/repo/modelenum"
	"github.com/qor/admin"
	"github.com/qor/qor"
)

var tradeDecisionResource *admin.Resource

func init() {
	onAdminInit(initTradeDecisionResource)
}

func initTradeDecisionResource() {
	tradeDecisionResource = Admin.AddResource(&model.TradeDecision{}, &admin.Config{Menu: []string{"Ledger"}})

	tradeDecisionResource.Meta(&admin.Meta{
		Name:   "NotifyStatus",
		Config: &admin.SelectOneConfig{Collection: modelenum.GetNotificationStates()},
	})

	tradeDecisionResource.Meta(&admin.Meta{
		Name: "TraderGUID",
		Config: &admin.SelectOneConfig{
			Collection: getAvailableTraderGUIDs,
		},
	})

	tradeDecisionResource.Meta(&admin.Meta{
		Name: "Title",
		Type: "string",
		Valuer: func(v interface{}, ctx *qor.Context) interface{} {
			if t, ok := v.(*model.TradeDecision); ok {
				return fmt.Sprintf("TradeDecision#%d", t.GetID())
			}
			return "TradeDecision#Unknown"
		},
	})

	tradeDecisionResource.IndexAttrs("ID", "Beneficiary", "TraderGUID", "Notify", "NotifyStatus", "AutoTrade", "Title")
}
