package config

import (
	"errors"
	"fmt"

	"bitbucket.org/pfzero/tradr/tools/genmct"
	"bitbucket.org/pfzero/tradr/trade/strategies/multicointrade"
	"bitbucket.org/pfzero/tradr/traderstore"
	"github.com/qor/admin"
	"github.com/qor/worker"
)

type mctGenArgs struct {
	TradersConfigString string
}

func getMCTGeneratorsJob() *worker.Job {
	argResource := Admin.NewResource(&mctGenArgs{})
	argResource.Meta(&admin.Meta{Name: "TradersConfigString", Type: "text"})

	return &worker.Job{
		Name:     "Multi Coin Trader Generator",
		Resource: argResource,
		Handler: func(arg interface{}, job worker.QorJobInterface) error {
			in, ok := arg.(*mctGenArgs)
			if !ok {
				return errors.New("Couldn't convert the given argument to required data-structure")
			}

			createdTraders, err := genmct.GenerateConfigs(in.TradersConfigString)

			for i := range createdTraders {
				trader := &createdTraders[i]
				traderGUID := multicointrade.GetTraderIdentifier().GetTraderInstanceGUID(trader.GetID())
				traderstore.GetInstance().RefreshTraderInstance(traderGUID)
			}

			job.AddResultsRow(worker.TableCell{Value: fmt.Sprintf("Created Count: %d", len(createdTraders))})

			return err
		},
	}
}
