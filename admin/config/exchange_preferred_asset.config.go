package config

import (
	"errors"
	"strconv"

	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/store/datastore/cryptomarketstore"
	"github.com/qor/admin"
	"github.com/qor/qor"
)

var exchangePreferredAssetRes *admin.Resource

func init() {
	onAdminInit(initExchangePreferredAssetRes)
}

func initExchangePreferredAssetRes() {
	exchangePreferredAssetRes = Admin.AddResource(&model.ExchangePreferredAsset{}, &admin.Config{Menu: []string{"Crypto Resources"}})

	oldSaveHandler := exchangePreferredAssetRes.SaveHandler
	exchangePreferredAssetRes.SaveHandler = func(v interface{}, ctx *qor.Context) error {

		preferredAsset, ok := v.(*model.ExchangePreferredAsset)
		if !ok {
			return errors.New("Unexpected Error occured. Couldn't assert that the given value is a ExchangePreferredAsset model instance")
		}
		err := oldSaveHandler(v, ctx)
		if err == nil {
			cryptomarketstore.SavePreferredAssetToCache(*preferredAsset)
		}
		return err
	}

	oldDelHandler := exchangePreferredAssetRes.DeleteHandler
	exchangePreferredAssetRes.DeleteHandler = func(v interface{}, ctx *qor.Context) error {
		resourceID, err := strconv.ParseUint(ctx.ResourceID, 10, strconv.IntSize)
		if err != nil {
			return err
		}

		populatedAsset, err := cryptomarketstore.GetPreferredAssetByIDFromDB(uint(resourceID))

		if err != nil {
			return err
		}

		err = oldDelHandler(v, ctx)
		if err == nil {
			cryptomarketstore.DeletePreferredAssetFromCache(*populatedAsset)
		}
		return err
	}
}
