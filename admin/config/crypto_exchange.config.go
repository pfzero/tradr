package config

import (
	"bitbucket.org/pfzero/tradr/repo/model"
	"github.com/qor/admin"
)

var cryptoExchange *admin.Resource

func init() {
	onAdminInit(initCryptoExchangeResource)
}

func initCryptoExchangeResource() {
	cryptoExchange = Admin.AddResource(&model.CryptoExchange{}, &admin.Config{Menu: []string{"Crypto Resources"}})
}
