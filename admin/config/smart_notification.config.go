package config

import (
	"errors"

	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/repo/modelenum"
	"bitbucket.org/pfzero/tradr/store/datastore/cryptomarketstore"
	"bitbucket.org/pfzero/tradr/trade/strategies/multicointrade"
	"bitbucket.org/pfzero/tradr/trade/strategies/smartnotification"
	"bitbucket.org/pfzero/tradr/traderstore"
	"github.com/qor/admin"
	"github.com/qor/qor"
)

var smartNotificationResource *admin.Resource

func setSmartNotificationState(sn *model.SmartNotification, state string) error {
	err := cryptomarketstore.UpdateSmartNotifierState(sn.GetID(), state)
	if err != nil {
		return err
	}

	traderGUID := smartnotification.GetTraderIdentifier().GetTraderInstanceGUID(sn.GetID())
	traderstore.GetInstance().RefreshTraderInstance(traderGUID)
	return nil
}

func init() {
	onAdminInit(func() {
		smartNotificationResource = Admin.AddResource(&model.SmartNotification{}, &admin.Config{Menu: []string{"Crypto Traders"}})

		smartNotificationResource.Meta(
			&admin.Meta{Name: "State", Config: &admin.SelectOneConfig{Collection: modelenum.GetTradeBotStates()}},
		)

		smartNotificationResource.Action(&admin.Action{
			Name: "Start",
			Handler: func(action *admin.ActionArgument) error {
				for _, record := range action.FindSelectedRecords() {
					sn := record.(*model.SmartNotification)
					err := setSmartNotificationState(sn, modelenum.TradeBotRunningState.String())
					return err
				}
				return nil
			},
			Modes: []string{"show", "menu_item"},
		})

		smartNotificationResource.Action(&admin.Action{
			Name: "Pause",
			Handler: func(action *admin.ActionArgument) error {
				for _, record := range action.FindSelectedRecords() {
					sn := record.(*model.SmartNotification)
					err := setSmartNotificationState(sn, modelenum.TradeBotStoppedState.String())
					return err

				}
				return nil
			},
			Modes: []string{"show", "menu_item"},
		})

		oldDelHandler := smartNotificationResource.DeleteHandler
		smartNotificationResource.DeleteHandler = func(v interface{}, ctx *qor.Context) error {
			err := oldDelHandler(v, ctx)
			if err != nil {
				return err
			}

			snc, ok := v.(*model.SmartNotification)
			if !ok {
				return errors.New("Couldn't convert saved model to model.SmartNotification")
			}

			traderGUID := multicointrade.GetTraderIdentifier().GetTraderInstanceGUID(snc.GetID())
			traderstore.GetInstance().RemoveTraderInstance(traderGUID)

			return nil
		}
	})
}
