package config

import (
	"errors"
	"fmt"

	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/repo/modelenum"
	"bitbucket.org/pfzero/tradr/store"
	"bitbucket.org/pfzero/tradr/store/datastore/cryptomarketstore"
	"bitbucket.org/pfzero/tradr/trade/strategies/multicointrade"
	"bitbucket.org/pfzero/tradr/traderstore"
	"github.com/qor/admin"
	"github.com/qor/qor"
)

var multiCoinTradeResource *admin.Resource

func setMultiCoinTraderState(mct *model.MultiCoinTradeCfg, state string) error {

	err := cryptomarketstore.UpdateMultiCoinTraderState(mct.GetID(), state)
	if err != nil {
		return err
	}

	traderGUID := multicointrade.GetTraderIdentifier().GetTraderInstanceGUID(mct.GetID())
	traderstore.GetInstance().RefreshTraderInstance(traderGUID)
	return nil
}

func init() {
	onAdminInit(func() {
		multiCoinTradeResource = Admin.AddResource(&model.MultiCoinTradeCfg{}, &admin.Config{Menu: []string{"Crypto Traders"}})

		multiCoinTradeResource.Meta(
			&admin.Meta{Name: "State", Config: &admin.SelectOneConfig{Collection: modelenum.GetTradeBotStates()}},
		)

		multiCoinTradeResource.Action(&admin.Action{
			Name: "Start",
			Handler: func(action *admin.ActionArgument) error {
				for _, record := range action.FindSelectedRecords() {
					mct := record.(*model.MultiCoinTradeCfg)
					err := setMultiCoinTraderState(mct, modelenum.TradeBotRunningState.String())
					if err != nil {
						return err
					}
				}
				return nil
			},
			Visible: func(record interface{}, context *admin.Context) bool {
				return true
			},
			Modes: []string{"show", "menu_item", "batch"},
		})

		multiCoinTradeResource.Action(&admin.Action{
			Name: "Pause",
			Handler: func(action *admin.ActionArgument) error {
				for _, record := range action.FindSelectedRecords() {
					mct := record.(*model.MultiCoinTradeCfg)
					err := setMultiCoinTraderState(mct, modelenum.TradeBotStoppedState.String())
					if err != nil {
						return err
					}
				}
				return nil
			},
			Visible: func(record interface{}, context *admin.Context) bool {
				return true
			},
			Modes: []string{"show", "menu_item", "batch"},
		})

		oldSaveHandler := multiCoinTradeResource.SaveHandler
		multiCoinTradeResource.SaveHandler = func(v interface{}, ctx *qor.Context) error {
			err := oldSaveHandler(v, ctx)
			if err != nil {
				return err
			}

			multiCoinTradeCfg, ok := v.(*model.MultiCoinTradeCfg)
			if !ok {
				return errors.New("Couldn't convert saved model to model.MultiCoinTradeCfg")
			}

			traderGUID := multicointrade.GetTraderIdentifier().GetTraderInstanceGUID(multiCoinTradeCfg.GetID())
			err = traderstore.GetInstance().RefreshTraderInstance(traderGUID)

			if err != nil {
				dbErr := store.DB.Delete(multiCoinTradeCfg).Error
				if dbErr != nil {
					return fmt.Errorf("Failed to add multi coin trader to trader manager; Err was: %s; Also got error when deleting the config instance; Err was %s",
						err.Error(), dbErr.Error(),
					)
				}

				return fmt.Errorf("Failed to add multi coin trader to trader manager; Err was %s; Trader Config was successfully deleted", err.Error())
			}

			return nil
		}

		oldDeleteHandler := multiCoinTradeResource.DeleteHandler
		multiCoinTradeResource.DeleteHandler = func(v interface{}, ctx *qor.Context) error {
			err := oldDeleteHandler(v, ctx)
			if err != nil {
				return err
			}

			multiCoinTradeCfg, ok := v.(*model.MultiCoinTradeCfg)
			if !ok {
				return errors.New("Couldn't convert saved model to model.MultiCoinTradeCfg")
			}

			traderGUID := multicointrade.GetTraderIdentifier().GetTraderInstanceGUID(multiCoinTradeCfg.GetID())
			traderstore.GetInstance().RemoveTraderInstance(traderGUID)

			return nil
		}
	})
}
