package config

import (
	"errors"
	"fmt"

	"bitbucket.org/pfzero/tradr/store"

	"bitbucket.org/pfzero/tradr/daemon/marketdaemon"
	"bitbucket.org/pfzero/tradr/repo/model"
	"github.com/qor/admin"
	"github.com/qor/qor"
)

var trackedCryptoCoinResource *admin.Resource

func init() {
	onAdminInit(initTrackedCryptoCoinResource)
}

func initTrackedCryptoCoinResource() {
	trackedCryptoCoinResource = Admin.AddResource(&model.CryptoTrackedCoin{}, &admin.Config{Menu: []string{"Crypto Resources"}})

	trackedCryptoCoinResource.ShowAttrs(
		"Name", "Coin", "RefCurrency", "Exchange",
	)
	trackedCryptoCoinResource.EditAttrs("Name")

	oldSaveHandler := trackedCryptoCoinResource.SaveHandler
	trackedCryptoCoinResource.SaveHandler = func(v interface{}, ctx *qor.Context) error {
		err := oldSaveHandler(v, ctx)
		if err != nil {
			return err
		}

		trackedCryptoCoin, ok := v.(*model.CryptoTrackedCoin)
		if !ok {
			return errors.New("Couldn't convert saved model to model.CryptoTrackedCoin")
		}

		trackedCryptoCoinErr := marketdaemon.GetInstance().AddCoin(trackedCryptoCoin)

		if trackedCryptoCoinErr == nil {
			return nil
		}

		err = store.DB.Delete(trackedCryptoCoin).Error

		if err != nil {
			return fmt.Errorf("Failed to add coin: %s; Also failed to delete tracked coin: %s", trackedCryptoCoinErr.Error(), err.Error())
		}

		return errors.New(trackedCryptoCoinErr.Error())
	}

	// oldDeleteHandler := trackedCryptoCoinResource.DeleteHandler
	trackedCryptoCoinResource.DeleteHandler = func(v interface{}, ctx *qor.Context) error {
		return errors.New("Operation unsupported at the moment")
	}
}
