// +build !bindatafs

package bindatafs

import (
	"fmt"
	"os"
)

var _bindata = map[string]interface{}{}

// Asset is blank implementation of Asset fn;
func Asset(name string) ([]byte, error) {
	return nil, fmt.Errorf("Can't find asset %s", name)
}

// AssetDir returns the list of files within the given path
// this is a blank implementation
func AssetDir(path string) ([]string, error) {
	return nil, fmt.Errorf("Dir %s not found", path)
}

// AssetInfo returns the info of the asset found at the given path;
// this is a blank implementation
func AssetInfo(path string) (os.FileInfo, error) {
	return nil, fmt.Errorf("Asset %s not found", path)
}
