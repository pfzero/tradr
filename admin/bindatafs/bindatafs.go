package bindatafs

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/jteeuwen/go-bindata"
	"github.com/qor/assetfs"
)

// BindataFS represents binary data
// filesystem
type BindataFS struct {
	Path            string
	ViewPaths       []string
	AssetFileSystem assetfs.Interface
}

// AssetFS represents the virtual asset fs
var AssetFS *BindataFS

func init() {
	AssetFS = &BindataFS{AssetFileSystem: &assetfs.AssetFileSystem{}, Path: "static/bindatafs"}
}

// RegisterPath registers a new path to be added
func (assetFS *BindataFS) RegisterPath(path string) error {
	assetFS.ViewPaths = append(assetFS.ViewPaths, path)
	return assetFS.AssetFileSystem.RegisterPath(path)
}

// PrependPath prepends the given path
func (assetFS *BindataFS) PrependPath(path string) error {
	assetFS.ViewPaths = append([]string{path}, assetFS.ViewPaths...)
	return assetFS.AssetFileSystem.PrependPath(path)
}

// NameSpace creates a new namespace
func (assetFS *BindataFS) NameSpace(namespace string) assetfs.Interface {
	return assetFS.AssetFileSystem.NameSpace(namespace)
}

// Asset registers a new asset
func (assetFS *BindataFS) Asset(name string) ([]byte, error) {
	name = strings.TrimPrefix(name, "/")
	if len(_bindata) > 0 {
		return Asset(name)
	}
	return assetFS.AssetFileSystem.Asset(name)
}

// Glob searches for the given pattern within the list of binary files
func (assetFS *BindataFS) Glob(pattern string) (matches []string, err error) {
	if len(_bindata) > 0 {
		for key := range _bindata {
			if ok, err := filepath.Match(pattern, key); ok && err == nil {
				matches = append(matches, key)
			}
		}
		return matches, nil
	}

	return assetFS.AssetFileSystem.Glob(pattern)
}

// Compile compiles the bindatafs
func (assetFS *BindataFS) Compile() error {
	os.RemoveAll(filepath.Join(assetFS.Path, "templates"))

	sources := assetFS.ViewPaths
	assetFS.copyFiles(filepath.Join(assetFS.Path, "templates"), sources...)

	config := bindata.NewConfig()
	config.Input = []bindata.InputConfig{
		{
			Path:      filepath.Join(assetFS.Path, "templates"),
			Recursive: true,
		},
	}
	config.Package = "bindatafs"
	config.Tags = "bindatafs"
	config.Output = "admin/bindatafs/templates_bindatafs.go"
	config.Prefix = filepath.Join(assetFS.Path, "templates")
	config.NoMetadata = true
	err := bindata.Translate(config)
	if err != nil {
		os.RemoveAll(assetFS.Path)
		return err
	}
	err = os.RemoveAll(filepath.Join(assetFS.Path))
	return err
}

func (assetFS *BindataFS) copyFiles(dest string, sources ...string) {
	for i := len(sources) - 1; i >= 0; i-- {
		viewPath := sources[i]
		filepath.Walk(viewPath, func(path string, info os.FileInfo, err error) error {
			if err == nil {
				var relativePath = strings.TrimPrefix(path, viewPath)

				if info.IsDir() {
					err = os.MkdirAll(filepath.Join(dest, relativePath), os.ModePerm)
				} else if info.Mode().IsRegular() {
					if source, err := ioutil.ReadFile(path); err == nil {
						err = ioutil.WriteFile(filepath.Join(dest, relativePath), source, os.ModePerm)
					}
				}
			}
			return err
		})
	}
}
