package auth

import (
	"encoding/base64"
	"net/http"

	"bitbucket.org/pfzero/tradr/logs"
	"github.com/sirupsen/logrus"

	"github.com/gorilla/sessions"
	"gopkg.in/authboss.v1"
)

const sessionCookieName = "tradr_tok"

var sessionStore *sessions.CookieStore

type sessionStorer struct {
	w http.ResponseWriter
	r *http.Request
}

func init() {
	sessionStoreKey, _ := base64.StdEncoding.DecodeString(`EGxRGrqkogymr5EHAX9V3RAzaZzj9_heLXM4M6DZDuGsEd-nfw8veekXWDY11pfsWXtQsJMzZxRm2zpjGg9dJQ==`)
	sessionStore = sessions.NewCookieStore(sessionStoreKey)
}

func newSessionStorer(w http.ResponseWriter, r *http.Request) authboss.ClientStorer {
	return &sessionStorer{w, r}
}

func (s sessionStorer) Get(key string) (string, bool) {
	session, err := sessionStore.Get(s.r, sessionCookieName)
	if err != nil {
		logs.GetLogger().WithFields(logrus.Fields{
			"module": "admin.auth.session_storer",
		}).Warnf("Couldn't get key %s from session storage; Err was: %s", key, err.Error())

		return "", false
	}

	strInf, ok := session.Values[key]
	if !ok {
		return "", false
	}

	str, ok := strInf.(string)
	if !ok {
		return "", false
	}

	return str, true
}

func (s sessionStorer) Put(key, value string) {
	session, err := sessionStore.Get(s.r, sessionCookieName)
	if err != nil {
		logs.GetLogger().WithFields(logrus.Fields{
			"module": "admin.auth.session_storer",
		}).Warnf("Couldn't put key %s with value %s in session storage; Err was: %s", key, value, err.Error())

		return
	}

	session.Values[key] = value
	session.Save(s.r, s.w)
}

func (s sessionStorer) Del(key string) {
	session, err := sessionStore.Get(s.r, sessionCookieName)
	if err != nil {
		logs.GetLogger().WithFields(logrus.Fields{
			"module": "admin.auth.session_storer",
		}).Warnf("Couldn't delete key %s from session storage; Err was: %s", key, err.Error())

		return
	}

	delete(session.Values, key)
	session.Save(s.r, s.w)
}
