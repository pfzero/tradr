package auth

import (
	"encoding/base64"
	"net/http"
	"time"

	"bitbucket.org/pfzero/tradr/logs"

	"github.com/gorilla/securecookie"
	"github.com/sirupsen/logrus"
	"gopkg.in/authboss.v1"
)

var cookieStore *securecookie.SecureCookie

func init() {
	cookieStoreKey, _ := base64.StdEncoding.DecodeString(`MLxWpWgQifDJ33zoCjz77rH5HR3ifJ8kmT3JqfUu_CEKbGXkwCXK_RoyKCnwoEd1-WnRP2cFcDZxxycpQLp3EQ==`)
	cookieStore = securecookie.New(cookieStoreKey, nil)
}

type cookieStorer struct {
	w http.ResponseWriter
	r *http.Request
}

func newCookieStorer(w http.ResponseWriter, r *http.Request) authboss.ClientStorer {
	return &cookieStorer{w, r}
}

// Get key from cookie
func (s cookieStorer) Get(key string) (string, bool) {
	cookie, err := s.r.Cookie(key)
	if err != nil {
		return "", false
	}

	var value string
	err = cookieStore.Decode(key, cookie.Value, &value)
	if err != nil {
		return "", false
	}

	return value, true
}

// Put adds a new <k,v> to cookie
func (s cookieStorer) Put(key, value string) {
	encoded, err := cookieStore.Encode(key, value)
	if err != nil {
		logs.GetLogger().WithFields(logrus.Fields{
			"module": "admin.auth.cookie_storer",
		}).Warnf("Couldn't store cookie within cookie store; Err: %s", err.Error())
	}

	cookie := &http.Cookie{
		Expires: time.Now().UTC().AddDate(1, 0, 0),
		Name:    key,
		Value:   encoded,
		Path:    "/",
	}
	http.SetCookie(s.w, cookie)
}

// Del removes a key from cookie
func (s cookieStorer) Del(key string) {
	cookie := &http.Cookie{
		MaxAge: -1,
		Name:   key,
		Path:   "/",
	}
	http.SetCookie(s.w, cookie)
}
