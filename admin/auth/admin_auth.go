package auth

import (
	"bitbucket.org/pfzero/tradr/repo/model"
	"github.com/qor/admin"
	"github.com/qor/qor"
)

// AdminAuth is used as an integration point
// between auth module and the existing admin module
type AdminAuth struct {
}

// LoginURL returns the login url to authenticate
// to admin panel
func (AdminAuth) LoginURL(c *admin.Context) string {
	return "/auth/login?redir=/admin"
}

// LogoutURL returns the logout url needed for
// logging out the user
func (AdminAuth) LogoutURL(c *admin.Context) string {
	return "/auth/logout?redir=/auth/login"
}

// GetCurrentUser returns the currently logged user
func (AdminAuth) GetCurrentUser(c *admin.Context) qor.CurrentUser {
	userInter, err := Auth.CurrentUser(c.Writer, c.Request)
	if userInter != nil && err == nil {
		return userInter.(*model.User)
	}
	return nil
}
