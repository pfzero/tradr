package auth

import (
	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/store"
	"gopkg.in/authboss.v1"
)

type dBStorer struct {
}

// Create creates a new user in database
func (s dBStorer) Create(key string, attr authboss.Attributes) error {
	var user model.User
	if err := attr.Bind(&user, true); err != nil {
		return err
	}

	if err := store.DB.Save(&user).Error; err != nil {
		return err
	}
	return nil
}

// Put modifies an existing user from database based on his email addr
func (s dBStorer) Put(key string, attr authboss.Attributes) error {
	var user model.User
	if err := store.DB.Where("email = ?", key).First(&user).Error; err != nil {
		return authboss.ErrUserNotFound
	}

	if err := attr.Bind(&user, true); err != nil {
		return err
	}

	if err := store.DB.Save(&user).Error; err != nil {
		return err
	}
	return nil
}

// Get returns the user from database based on his email addr
func (s dBStorer) Get(key string) (result interface{}, err error) {
	var user model.User
	if err := store.DB.Where("email = ?", key).First(&user).Error; err != nil {
		return nil, authboss.ErrUserNotFound
	}
	return &user, nil
}

// ConfirmUser confirms user based on the given token
func (s dBStorer) ConfirmUser(tok string) (result interface{}, err error) {
	var user model.User
	if err := store.DB.Where("confirm_token = ?", tok).First(&user).Error; err != nil {
		return nil, authboss.ErrUserNotFound
	}
	return &user, nil
}

// RecoverUser fetches the user based on the given recover_token parameter
func (s dBStorer) RecoverUser(rec string) (result interface{}, err error) {
	var user model.User
	if err := store.DB.Where("recover_token = ?", rec).First(&user).Error; err != nil {
		return nil, authboss.ErrUserNotFound
	}
	return &user, nil
}
