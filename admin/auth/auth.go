package auth

import (
	"html/template"
	"net/http"
	"regexp"

	"bitbucket.org/pfzero/tradr/logs"

	"github.com/gorilla/csrf"
	"golang.org/x/crypto/bcrypt"
	authboss "gopkg.in/authboss.v1"

	// auth implements password based user logins
	_ "gopkg.in/authboss.v1/auth"
)

var (
	// Auth is the exported authentication module
	// it must be used to handle authentication logic
	// by mounting its router on server start
	Auth = authboss.New()
)

func layoutData(w http.ResponseWriter, r *http.Request) authboss.HTMLData {
	return authboss.HTMLData{
		"Result": authboss.HTMLData{},
	}
}

// ConfigureAuth configures the authentication
// module
func ConfigureAuth() {
	Auth.MountPath = "/auth"
	Auth.XSRFName = "gorilla.csrf.Token"
	Auth.XSRFMaker = func(_ http.ResponseWriter, r *http.Request) string {
		return csrf.Token(r)
	}
	Auth.CookieStoreMaker = newCookieStorer
	Auth.SessionStoreMaker = newSessionStorer
	Auth.BCryptCost = bcrypt.DefaultCost
	Auth.LogWriter = logs.GetLogger().Writer()
	Auth.Storer = &dBStorer{}
	Auth.ViewsPath = "static/views/auth"
	Auth.Layout = template.Must(template.ParseFiles("static/views/layouts/auth.tmpl"))
	Auth.LayoutDataMaker = layoutData
	Auth.Policies = []authboss.Validator{
		authboss.Rules{
			FieldName:       "email",
			Required:        true,
			AllowWhitespace: false,
			MustMatch:       regexp.MustCompile(`^[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}$`),
			MatchError:      "Please input a valid email address",
		},
		authboss.Rules{
			FieldName:       "password",
			Required:        true,
			MinLength:       6,
			MaxLength:       12,
			AllowWhitespace: false,
		},
	}

	if err := Auth.Init(); err != nil {
		panic(err)
	}
}
