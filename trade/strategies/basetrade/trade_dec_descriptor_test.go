package basetrade_test

import (
	"reflect"
	"testing"
	"time"

	"bitbucket.org/pfzero/tradr/repo/model"

	"bitbucket.org/pfzero/tradr/trade/strategies/basetrade"
)

func TestNewTradeDecisionDescriptor(t *testing.T) {
	tests := []struct {
		name string
		want *basetrade.TradeDecisionDescriptor
	}{
		{
			name: "Simple Constructor",
			want: &basetrade.TradeDecisionDescriptor{TradeCondition: &model.TradeCondition{}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := basetrade.NewTradeDecisionDescriptor(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewTradeDecisionDescriptor() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTradeDecisionDescriptor_SetPair(t *testing.T) {
	type args struct {
		fsym string
		tsym string
	}
	tests := []struct {
		name string
		tdd  *basetrade.TradeDecisionDescriptor
		args args
		want *basetrade.TradeDecisionDescriptor
	}{
		// TODO: Add test cases.
		{
			name: "Pair Setter",
			tdd:  &basetrade.TradeDecisionDescriptor{},
			args: args{fsym: "TST1", tsym: "TST2"},
			want: &basetrade.TradeDecisionDescriptor{FromSym: "TST1", ToSym: "TST2"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tdd.SetPair(tt.args.fsym, tt.args.tsym); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TradeDecisionDescriptor.SetPair() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTradeDecisionDescriptor_SetExchange(t *testing.T) {
	type args struct {
		exchange string
	}
	tests := []struct {
		name string
		tdd  *basetrade.TradeDecisionDescriptor
		args args
		want *basetrade.TradeDecisionDescriptor
	}{
		{
			name: "Exchange Setter",
			tdd:  &basetrade.TradeDecisionDescriptor{},
			args: args{exchange: "TestGround"},
			want: &basetrade.TradeDecisionDescriptor{Exchange: "TestGround"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tdd.SetExchange(tt.args.exchange); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TradeDecisionDescriptor.SetExchange() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTradeDecisionDescriptor_SetRefContextTime(t *testing.T) {
	type args struct {
		t time.Time
	}
	tests := []struct {
		name string
		tdd  *basetrade.TradeDecisionDescriptor
		args args
		want *basetrade.TradeDecisionDescriptor
	}{
		{
			name: "Ref Context Time Setter",
			tdd:  &basetrade.TradeDecisionDescriptor{},
			args: args{t: time.Date(2018, 4, 12, 0, 0, 0, 0, time.Local)},
			want: &basetrade.TradeDecisionDescriptor{RefContextTime: time.Date(2018, 4, 12, 0, 0, 0, 0, time.Local)},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tdd.SetRefContextTime(tt.args.t); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TradeDecisionDescriptor.SetRefContextTime() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTradeDecisionDescriptor_SetRefPrice(t *testing.T) {
	type args struct {
		p float64
	}
	tests := []struct {
		name string
		tdd  *basetrade.TradeDecisionDescriptor
		args args
		want *basetrade.TradeDecisionDescriptor
	}{
		{
			name: "Ref Price Setter",
			tdd:  &basetrade.TradeDecisionDescriptor{},
			args: args{p: 10},
			want: &basetrade.TradeDecisionDescriptor{RefPrice: 10},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tdd.SetRefPrice(tt.args.p); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TradeDecisionDescriptor.SetRefPrice() = %v, want %v", got, tt.want)
			}
		})
	}
}
