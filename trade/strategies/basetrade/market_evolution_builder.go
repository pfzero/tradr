package basetrade

import (
	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"
	"bitbucket.org/pfzero/tradr/util/tradeutil"
)

// BuildMarketEvolutionReport build a market evolution reporter
func BuildMarketEvolutionReport(cfg *TradeDecisionDescriptor, refCtx, ctx tradeutil.TradeContext) *tradeutil.MarketEvolutionReport {
	cListID := &tradeutil.CandlestickListID{
		Exchange: cfg.Exchange,
		FSym:     cfg.FromSym,
		TSym:     cfg.ToSym,
	}

	if variation := getTrendNoise(cfg); variation != nil {
		cListID.Variation = *variation
	}

	return tradeutil.NewMarketEvolutionReport(cListID, &refCtx, &ctx)
}

// getTrendNoise returns the first trend parameters from the decision pipeline
// currently it doesn't support parsing all the steps from the pipeline
func getTrendNoise(cfg *TradeDecisionDescriptor) *cryptomarketutil.Variation {
	v := &cryptomarketutil.Variation{
		Up:   cfg.TradeCondition.NoiseUp,
		Down: cfg.TradeCondition.NoiseDown,
	}
	if !v.IsZero() {
		return v
	}
	return nil
}
