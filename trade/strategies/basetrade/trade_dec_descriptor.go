package basetrade

import (
	"time"

	"bitbucket.org/pfzero/tradr/repo/model"
)

// TradeDecisionDescriptor represents the context
// within which the trade decision will be built; It augments
// model.TradeCondition
type TradeDecisionDescriptor struct {
	FromSym        string
	ToSym          string
	Exchange       string
	RefContextTime time.Time
	RefPrice       float64
	*model.TradeCondition
}

// NewTradeDecisionDescriptor is the constructor of the TradeConditionDescriptor
func NewTradeDecisionDescriptor() *TradeDecisionDescriptor {
	return &TradeDecisionDescriptor{TradeCondition: &model.TradeCondition{}}
}

// SetPair sets the pair for which the trade condition applies
func (tdd *TradeDecisionDescriptor) SetPair(fsym, tsym string) *TradeDecisionDescriptor {
	tdd.FromSym = fsym
	tdd.ToSym = tsym
	return tdd
}

// SetExchange sets the exchange where the data will come from
func (tdd *TradeDecisionDescriptor) SetExchange(exchange string) *TradeDecisionDescriptor {
	tdd.Exchange = exchange
	return tdd
}

// SetRefContextTime sets the ref context time
func (tdd *TradeDecisionDescriptor) SetRefContextTime(t time.Time) *TradeDecisionDescriptor {
	tdd.RefContextTime = t
	return tdd
}

// SetRefPrice sets the ref price of the trade condition descriptor
func (tdd *TradeDecisionDescriptor) SetRefPrice(p float64) *TradeDecisionDescriptor {
	tdd.RefPrice = p
	return tdd
}
