package basetrade_test

import (
	"testing"
	"time"

	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"
	"bitbucket.org/pfzero/tradr/util/tradedec/testutil"

	"bitbucket.org/pfzero/tradr/trade/strategies/basetrade"
)

const fsym = "TST"
const tsym = "USD"
const exchange = "Testground"
const fillInterval = 30 * time.Second

const day = 24 * time.Hour
const week = 7 * day
const month = 30 * day
const year = 365 * day

type testpair struct {
	desc        *basetrade.TradeDecisionDescriptor
	refPt       testutil.Point
	histoPoints []testutil.Point
	points      []testutil.Point
}

type testdataprovider func() *testpair

func TestTrees(t *testing.T) {
	tests := []struct {
		name       string
		providerFn testdataprovider
	}{
		{
			name:       "Wait Duration",
			providerFn: waitDurationProvider,
		},
		{
			name:       "Wait Time",
			providerFn: waitTimeProvider,
		},
		{
			name:       "Deadline Duration",
			providerFn: deadlineDurationProvider,
		},
		{
			name:       "Deadline Time",
			providerFn: deadlineTimeProvider,
		},
		{
			name:       "Price Variation Up",
			providerFn: priceVariationUpProvider,
		},
		{
			name:       "Price Variation Down",
			providerFn: priceVariationDownProvider,
		},
		{
			name:       "Price Variation",
			providerFn: priceVariationProvider,
		},
		{
			name:       "Price Variation Symmetric",
			providerFn: priceVariationSymmetricProvider,
		},
		{
			name:       "Price Variation with Ref Price",
			providerFn: priceVariationRef,
		},
		{
			name:       "Trend Changed",
			providerFn: trendChangeProvider,
		},
		{
			name:       "Candle Up Formed",
			providerFn: trendCandleUpProvider,
		},
		{
			name:       "Candle Down Formed",
			providerFn: trendCandleDownProvider,
		},
		{
			name:       "Symmetric Candle Formed",
			providerFn: trendSymmetricCandleProvider,
		},
		{
			name:       "Trend Formed Up Provider",
			providerFn: trendFormedUpProvider,
		},
		{
			name:       "Trend Formed Down Provider",
			providerFn: trendFormedDownProvider,
		},
		{
			name:       "Complex Scenario 1",
			providerFn: complexScenario_1,
		},
		{
			name:       "Complex Scenario 2",
			providerFn: complexScenario_2,
		},
		{
			name:       "Complex Scenario 3",
			providerFn: complexScenario_3,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			data := tt.providerFn()
			tree, err := basetrade.BuildDecisionTree(data.desc)
			if err != nil {
				t.Errorf("basetrade.BuildTradeCondition(); Got error: %s", err.Error())
				return
			}
			tableTest := prepareTableTest(0)
			tableTest.HistoPrices = data.histoPoints
			tableTest.Prices = data.points
			refCtx := tableTest.MakeRefCtx(&data.refPt)

			tree.SetRefCtx(*refCtx)
			tableTest.Fn = testutil.TreeExecFn(tree)
			failurePoints := tableTest.Run()
			if len(failurePoints) > 0 {
				t.Errorf(`Expected no failure points but got %d`, len(failurePoints))
				logFailurePoints(t, failurePoints)
			}
		})
	}
}

func prepareTableTest(fillVariation float64) *testutil.TableTest {
	tt := &testutil.TableTest{
		Coin: fsym, Exchange: exchange, Fill: true, FillNoise: cryptomarketutil.GetSymmetricVariationFrom(fillVariation),
		FillInterval: fillInterval,
	}
	if fillVariation == 0 {
		tt.Fill = false
	}
	return tt
}

func logFailurePoints(t *testing.T, results []testutil.FailurePoint) {
	for i := 0; i < len(results); i++ {
		result := results[i]
		if result.Expected {
			t.Errorf(`Expected to signal for this point:: Price - %.2f :: Timestamp: %s`, result.GetPrice(), result.GetTimestamp())
		} else {
			t.Errorf(`Expected not to signal for this point:: Price - %.2f :: Timestamp: %s`, result.GetPrice(), result.GetTimestamp())
		}
	}
}

// All the test caases below

// waitDurationProvider waits for a certain time before starting
// to signal
func waitDurationProvider() *testpair {

	histoPts := `1->2->3->4->5->6->7->8->9->10`
	pts := `11->12->13->(14)->(16)`
	histoStart := time.Date(2018, 4, 1, 0, 0, 0, 0, time.Local)
	histoStop := time.Date(2018, 4, 10, 0, 0, 0, 0, time.Local)
	pointsStop := time.Date(2018, 4, 12, 0, 0, 0, 0, time.Local)
	refTime := time.Date(2018, 4, 6, 0, 0, 0, 0, time.Local)
	desc := basetrade.NewTradeDecisionDescriptor()

	desc.
		SetWaitDuration(5 * day)

	histoPoints := testutil.ParsePointSequence(histoPts, histoStart, histoStop)
	points := testutil.ParsePointSequence(pts, histoStop, pointsStop)
	refPt := testutil.Point{Price: 10, Time: refTime, Signal: false}
	return &testpair{desc, refPt, histoPoints, points}
}

func waitTimeProvider() *testpair {
	histoPts := `1->2->3->4->5->6->7->8->9->10`
	pts := `11->12->(13)->(14)`
	histoStart := time.Date(2018, 4, 1, 0, 0, 0, 0, time.Local)
	histoStop := time.Date(2018, 4, 10, 0, 0, 0, 0, time.Local)
	pointsStop := time.Date(2018, 4, 12, 0, 0, 0, 0, time.Local)
	waitUntil := time.Date(2018, 4, 10, 23, 59, 0, 0, time.Local)
	desc := basetrade.NewTradeDecisionDescriptor()

	desc.
		SetWaitTime(waitUntil)

	histoPoints := testutil.ParsePointSequence(histoPts, histoStart, histoStop)
	points := testutil.ParsePointSequence(pts, histoStop, pointsStop)
	refPt := testutil.Point{Price: 10, Time: time.Time{}, Signal: false}
	return &testpair{desc, refPt, histoPoints, points}
}

func deadlineDurationProvider() *testpair {
	histoPts := `1->2->3->4->5->6->7->8->9->10`
	pts := `11->12->13->(14)->(15)`
	histoStart := time.Date(2018, 4, 1, 0, 0, 0, 0, time.Local)
	histoStop := time.Date(2018, 4, 10, 0, 0, 0, 0, time.Local)
	pointsStop := time.Date(2018, 4, 12, 0, 0, 0, 0, time.Local)
	refTime := time.Date(2018, 4, 6, 0, 0, 0, 0, time.Local)
	desc := basetrade.NewTradeDecisionDescriptor()

	desc.
		SetWaitDuration(30 * day).
		SetDeadlineDuration(5 * day)

	histoPoints := testutil.ParsePointSequence(histoPts, histoStart, histoStop)
	points := testutil.ParsePointSequence(pts, histoStop, pointsStop)
	refPt := testutil.Point{Price: 10, Time: refTime, Signal: false}
	return &testpair{desc, refPt, histoPoints, points}
}

func deadlineTimeProvider() *testpair {
	histoPts := `1->2->3->4->5->6->7->8->9->10`
	pts := `11->12->(13)->(14)`
	histoStart := time.Date(2018, 4, 1, 0, 0, 0, 0, time.Local)
	histoStop := time.Date(2018, 4, 10, 0, 0, 0, 0, time.Local)
	pointsStop := time.Date(2018, 4, 12, 0, 0, 0, 0, time.Local)
	waitUntil := time.Date(2018, 4, 10, 23, 59, 0, 0, time.Local)
	desc := basetrade.NewTradeDecisionDescriptor()

	desc.
		SetWaitDuration(30 * day).
		SetDeadlineTime(waitUntil)

	histoPoints := testutil.ParsePointSequence(histoPts, histoStart, histoStop)
	points := testutil.ParsePointSequence(pts, histoStop, pointsStop)
	refPt := testutil.Point{Price: 10, Time: time.Now(), Signal: false}
	return &testpair{desc, refPt, histoPoints, points}
}

func priceVariationUpProvider() *testpair {
	histoPts := `1->2->3->4->5->6->7->8->9->10`
	pts := `11->12->(13)->(14)`
	histoStart := time.Date(2018, 4, 1, 0, 0, 0, 0, time.Local)
	histoStop := time.Date(2018, 4, 10, 0, 0, 0, 0, time.Local)
	pointsStop := time.Date(2018, 4, 12, 0, 0, 0, 0, time.Local)
	variation := 0.25

	desc := basetrade.NewTradeDecisionDescriptor()

	desc.
		SetPair(fsym, tsym).
		SetExchange(exchange).
		SetPriceVariation(variation)

	histoPoints := testutil.ParsePointSequence(histoPts, histoStart, histoStop)
	points := testutil.ParsePointSequence(pts, histoStop, pointsStop)
	refPt := testutil.Point{Price: 10, Time: time.Now(), Signal: false}
	return &testpair{desc, refPt, histoPoints, points}
}

func priceVariationDownProvider() *testpair {
	histoPts := `1->2->3->4->5->6->7->8->9->10`
	pts := `11->12->13->14->15->13->11->(8.99)->(8)->(7)`
	histoStart := time.Date(2018, 4, 1, 0, 0, 0, 0, time.Local)
	histoStop := time.Date(2018, 4, 10, 0, 0, 0, 0, time.Local)
	pointsStop := time.Date(2018, 4, 12, 0, 0, 0, 0, time.Local)
	variation := -0.25

	desc := basetrade.NewTradeDecisionDescriptor()

	desc.
		SetPair(fsym, tsym).
		SetExchange(exchange).
		SetPriceVariation(variation)

	histoPoints := testutil.ParsePointSequence(histoPts, histoStart, histoStop)
	points := testutil.ParsePointSequence(pts, histoStop, pointsStop)
	refPt := testutil.Point{Price: 12, Time: time.Now(), Signal: false}
	return &testpair{desc, refPt, histoPoints, points}
}

func priceVariationProvider() *testpair {
	histoPts := `1->2->3->4->5->6->7->8->9->10`
	pts := `11->12->13->14->(15.01)->(16)->(18)->11->(8.99)->(8)->(7)`
	histoStart := time.Date(2018, 4, 1, 0, 0, 0, 0, time.Local)
	histoStop := time.Date(2018, 4, 10, 0, 0, 0, 0, time.Local)
	pointsStop := time.Date(2018, 4, 12, 0, 0, 0, 0, time.Local)
	up := 0.25
	down := -0.25

	desc := basetrade.NewTradeDecisionDescriptor()

	desc.
		SetPair(fsym, tsym).
		SetExchange(exchange).
		SetPriceVariation(up).
		SetPriceVariation(down)

	histoPoints := testutil.ParsePointSequence(histoPts, histoStart, histoStop)
	points := testutil.ParsePointSequence(pts, histoStop, pointsStop)
	refPt := testutil.Point{Price: 12, Time: histoStop, Signal: false}
	return &testpair{desc, refPt, histoPoints, points}
}

func priceVariationSymmetricProvider() *testpair {
	histoPts := `1->2->3->4->5->6->7->8->9->10`
	pts := `11->12->13->14->(15.01)->(16)->(18)->11->(9.59)->(9)->(7)`
	histoStart := time.Date(2018, 4, 1, 0, 0, 0, 0, time.Local)
	histoStop := time.Date(2018, 4, 10, 0, 0, 0, 0, time.Local)
	pointsStop := time.Date(2018, 4, 12, 0, 0, 0, 0, time.Local)
	variation := 0.25
	// expectedDown := -0.2

	desc := basetrade.NewTradeDecisionDescriptor()

	desc.
		SetPair(fsym, tsym).
		SetExchange(exchange).
		SetSymmetricPriceVariation(variation)

	histoPoints := testutil.ParsePointSequence(histoPts, histoStart, histoStop)
	points := testutil.ParsePointSequence(pts, histoStop, pointsStop)
	refPt := testutil.Point{Price: 12, Time: histoStop, Signal: false}
	return &testpair{desc, refPt, histoPoints, points}
}

func priceVariationRef() *testpair {
	histoPts := `1->2->3->4->5->6->7->8->9->10`
	pts := `11->12->13->14->(15.01)->(16)->(18)->11->(9.59)->(9)->(7)`
	histoStart := time.Date(2018, 4, 1, 0, 0, 0, 0, time.Local)
	histoStop := time.Date(2018, 4, 10, 0, 0, 0, 0, time.Local)
	pointsStop := time.Date(2018, 4, 12, 0, 0, 0, 0, time.Local)
	variation := 0.25
	// expectedDown := -0.2

	desc := basetrade.NewTradeDecisionDescriptor()

	desc.
		SetPair(fsym, tsym).
		SetExchange(exchange).
		SetRefPrice(12).
		SetSymmetricPriceVariation(variation)

	histoPoints := testutil.ParsePointSequence(histoPts, histoStart, histoStop)
	points := testutil.ParsePointSequence(pts, histoStop, pointsStop)
	refPt := testutil.Point{Price: 0, Time: histoStop, Signal: false}
	return &testpair{desc, refPt, histoPoints, points}
}

func trendChangeProvider() *testpair {
	histoPts := `1->2->3->4->5->6->7->8->9->10`
	pts := `12->(11.69)->11->10->(10.25)->11->15->(14.63)->14`
	histoStart := time.Date(2018, 4, 1, 0, 0, 0, 0, time.Local)
	histoStop := time.Date(2018, 4, 10, 0, 0, 0, 0, time.Local)
	pointsStop := time.Date(2018, 4, 12, 0, 0, 0, 0, time.Local)
	noiseUp := 0.025
	// expectedDown := -0.02439

	desc := basetrade.NewTradeDecisionDescriptor()

	desc.
		SetPair(fsym, tsym).
		SetExchange(exchange).
		SetNoise(noiseUp).
		WaitTrendChange()

	histoPoints := testutil.ParsePointSequence(histoPts, histoStart, histoStop)
	points := testutil.ParsePointSequence(pts, histoStop, pointsStop)
	refPt := testutil.Point{Price: 0, Time: histoStop, Signal: false}
	return &testpair{desc, refPt, histoPoints, points}
}

func trendCandleUpProvider() *testpair {
	histoPts := `1->2->3->4->5->6->7->8->9->10`
	pts := `(12)->(11.11)->11.01->10->10.25->11.02->(13.51)->(12.5)->11.09`
	histoStart := time.Date(2018, 4, 1, 0, 0, 0, 0, time.Local)
	histoStop := time.Date(2018, 4, 10, 0, 0, 0, 0, time.Local)
	pointsStop := time.Date(2018, 4, 12, 0, 0, 0, 0, time.Local)
	noiseUp := 0.08
	candleUp := 0.25
	// expectedDown := -0.074(074)

	desc := basetrade.NewTradeDecisionDescriptor()

	desc.
		SetPair(fsym, tsym).
		SetExchange(exchange).
		SetNoise(noiseUp).
		WaitForCandle(candleUp)

	histoPoints := testutil.ParsePointSequence(histoPts, histoStart, histoStop)
	points := testutil.ParsePointSequence(pts, histoStop, pointsStop)
	refPt := testutil.Point{Price: 0, Time: histoStart, Signal: false}
	return &testpair{desc, refPt, histoPoints, points}
}

func trendCandleDownProvider() *testpair {
	histoPts := `1->2->3->4->5->6->7->8->9->10`
	pts := `20->18->16.1->(13.51)->(13)->(14.05)->15->18->15->(12)->(10)->(10.8)->11`
	histoStart := time.Date(2018, 4, 1, 0, 0, 0, 0, time.Local)
	histoStop := time.Date(2018, 4, 10, 0, 0, 0, 0, time.Local)
	pointsStop := time.Date(2018, 4, 12, 0, 0, 0, 0, time.Local)
	noiseUp := 0.08
	// expectedDown := -0.074(074)

	candleDown := -0.25

	desc := basetrade.NewTradeDecisionDescriptor()

	desc.
		SetPair(fsym, tsym).
		SetExchange(exchange).
		SetNoise(noiseUp).
		WaitForCandle(candleDown)

	histoPoints := testutil.ParsePointSequence(histoPts, histoStart, histoStop)
	points := testutil.ParsePointSequence(pts, histoStop, pointsStop)
	refPt := testutil.Point{Price: 0, Time: histoStart, Signal: false}
	return &testpair{desc, refPt, histoPoints, points}
}

func trendSymmetricCandleProvider() *testpair {
	histoPts := `1->2->3->4->5->6->7->8->9->10`
	pts := `
	(20)->(18.51)->18->16.1->(13.51)->(13)->(14.05)->
	15->16->15->14->15->16->
	(24)->(25)->(26)->(24.07)->24->23->24->25->26->
	20->(16)->(15)->(12)->(10)->(10.8)->11`
	histoStart := time.Date(2018, 4, 1, 0, 0, 0, 0, time.Local)
	histoStop := time.Date(2018, 4, 10, 0, 0, 0, 0, time.Local)
	pointsStop := time.Date(2018, 4, 12, 0, 0, 0, 0, time.Local)
	noiseUp := 0.08
	// expectedDown := -0.074(074)

	candleUp := 0.25
	// expectedCandleDown = -0.2

	desc := basetrade.NewTradeDecisionDescriptor()

	desc.
		SetPair(fsym, tsym).
		SetExchange(exchange).
		SetNoise(noiseUp).
		WaitForSymmetricCandle(candleUp)

	histoPoints := testutil.ParsePointSequence(histoPts, histoStart, histoStop)
	points := testutil.ParsePointSequence(pts, histoStop, pointsStop)
	refPt := testutil.Point{Price: 0, Time: histoStart, Signal: false}
	return &testpair{desc, refPt, histoPoints, points}
}

func trendFormedUpProvider() *testpair {
	histoPts := `1->2->3->4->5->6->7->8->9->10`
	pts := `
	(12)->(12.11)->(11.11)->(10)->(9.7)->9.6->9->8->
	9->(10.1)->(11)->(12)->(11)->9.7->9.6->9.5`
	histoStart := time.Date(2018, 4, 1, 0, 0, 0, 0, time.Local)
	histoStop := time.Date(2018, 4, 10, 0, 0, 0, 0, time.Local)
	pointsStop := time.Date(2018, 4, 12, 0, 0, 0, 0, time.Local)
	formedUp := 0.25
	// expectedDown := -0.2

	desc := basetrade.NewTradeDecisionDescriptor()

	desc.
		SetPair(fsym, tsym).
		SetExchange(exchange).
		WaitTrendFormation(formedUp)

	histoPoints := testutil.ParsePointSequence(histoPts, histoStart, histoStop)
	points := testutil.ParsePointSequence(pts, histoStop, pointsStop)
	refPt := testutil.Point{Price: 0, Time: histoStart, Signal: false}
	return &testpair{desc, refPt, histoPoints, points}
}

func trendFormedDownProvider() *testpair {
	histoPts := `1->2->3->4->5->6->7->8->9->10`
	pts := `
	12->11.11->10->9.7->(9.59)->(9)->(8)->
	(9)->9.9->10->11->12->11->9.7->(9.6)->(9.5)`
	histoStart := time.Date(2018, 4, 1, 0, 0, 0, 0, time.Local)
	histoStop := time.Date(2018, 4, 10, 0, 0, 0, 0, time.Local)
	pointsStop := time.Date(2018, 4, 12, 0, 0, 0, 0, time.Local)
	formedDown := -0.2
	// expectedUp := 0.25

	desc := basetrade.NewTradeDecisionDescriptor()

	desc.
		SetPair(fsym, tsym).
		SetExchange(exchange).
		WaitTrendFormation(formedDown)

	histoPoints := testutil.ParsePointSequence(histoPts, histoStart, histoStop)
	points := testutil.ParsePointSequence(pts, histoStop, pointsStop)
	refPt := testutil.Point{Price: 0, Time: histoStart, Signal: false}
	return &testpair{desc, refPt, histoPoints, points}
}

func complexScenario_1() *testpair {
	histoPts := `1->2->3->4->5->6->7->8->9->10`
	pts := `
	11->12->13->14->15->14->15->14->15->10->15->12->
	15->16->(14.87)->(16.06)->(14.93)->14->13->(14.05)`
	histoStart := time.Date(2018, 1, 1, 0, 0, 0, 0, time.Local)
	histoStop := time.Date(2018, 1, 10, 0, 0, 0, 0, time.Local)
	pointsStop := time.Date(2018, 4, 12, 0, 0, 0, 0, time.Local)
	noiseUp := 0.08
	noiseDown := -0.07

	desc := basetrade.NewTradeDecisionDescriptor()
	desc.
		SetPair(fsym, tsym).SetExchange(exchange).
		SetWaitDuration(2 * month).
		SetNoise(noiseUp).
		SetNoise(noiseDown).
		WaitTrendChange()

	histoPoints := testutil.ParsePointSequence(histoPts, histoStart, histoStop)
	points := testutil.ParsePointSequence(pts, histoStop, pointsStop)
	refPt := testutil.Point{Price: 15, Time: histoStop, Signal: false}
	return &testpair{desc, refPt, histoPoints, points}
}

func complexScenario_2() *testpair {
	histoPts := `1->2->3->4->5->6->7->8->9->10->5`
	pts := `
	6->7->5->6->7->8->9->(8.33)->7->5.62->
	7.02->5->4->5->7->8->(7.39)->7->6->5->4->
	4->4->4->4->4->4->4->4->4->(4)->(4)->(4)`
	histoStart := time.Date(2018, 1, 1, 0, 0, 0, 0, time.Local)
	histoStop := time.Date(2018, 1, 10, 0, 0, 0, 0, time.Local)
	pointsStop := time.Date(2018, 4, 12, 0, 0, 0, 0, time.Local)
	noiseUp := 0.08

	desc := basetrade.NewTradeDecisionDescriptor()
	desc.
		SetPair(fsym, tsym).SetExchange(exchange).
		SetDeadlineDuration(3 * month).
		SetNoise(noiseUp).
		WaitTrendChange().
		WaitTrendFormation(0.6)

	histoPoints := testutil.ParsePointSequence(histoPts, histoStart, histoStop)
	points := testutil.ParsePointSequence(pts, histoStop, pointsStop)
	refPt := testutil.Point{Price: 15, Time: histoStart, Signal: false}
	return &testpair{desc, refPt, histoPoints, points}
}

func complexScenario_3() *testpair {
	histoPts := `1->2->3->4->5->6->7->8->9->10->5`
	pts := `
	6->7->5->6->7->8->9->8.33->7->5.62->
	7.02->5->4->5->7->8->7.39->7->6->5->4->
	3->5->2->5->8->9->(8.33)->7->6->8->7->10->8->12->11.5->
	12->13->17->18->(16.65)`
	histoStart := time.Date(2018, 1, 1, 0, 0, 0, 0, time.Local)
	histoStop := time.Date(2018, 1, 10, 0, 0, 0, 0, time.Local)
	pointsStop := time.Date(2018, 4, 12, 0, 0, 0, 0, time.Local)
	noiseUp := 0.08

	desc := basetrade.NewTradeDecisionDescriptor()
	desc.
		SetPair(fsym, tsym).SetExchange(exchange).
		SetWaitDuration(2 * month).
		SetNoise(noiseUp).
		WaitTrendChange().
		WaitForCandle(0.6)

	histoPoints := testutil.ParsePointSequence(histoPts, histoStart, histoStop)
	points := testutil.ParsePointSequence(pts, histoStop, pointsStop)
	refPt := testutil.Point{Price: 15, Time: histoStart, Signal: false}
	return &testpair{desc, refPt, histoPoints, points}
}
