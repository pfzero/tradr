package basetrade

import (
	"errors"
	"fmt"
	"time"

	"bitbucket.org/pfzero/tradr/service/exchange"

	"bitbucket.org/pfzero/tradr/cfg"
	"bitbucket.org/pfzero/tradr/daemon/marketdaemon"

	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"
	"bitbucket.org/pfzero/tradr/util/dur"
	"bitbucket.org/pfzero/tradr/util/tradedec"
	"bitbucket.org/pfzero/tradr/util/tradeutil"
)

// BuildDecisionTreeWithCtx builds the decision tree and assembles the reference context
// as well
func BuildDecisionTreeWithCtx(desc *TradeDecisionDescriptor) (*tradedec.DecisionTree, error) {
	var refCtxTime time.Time
	if desc.RefContextTime.IsZero() {
		refCtxTime = time.Now()
	} else {
		refCtxTime = desc.RefContextTime
	}
	refCtx, err := makeRefCtx(desc.Exchange, refCtxTime)
	if err != nil {
		return nil, err
	}
	dt, err := buildDecisionTree(desc)
	if err != nil {
		return nil, err
	}

	dt.SetRefCtx(*refCtx)

	return dt, nil
}

// BuildDecisionTree builds the decision tree
func BuildDecisionTree(desc *TradeDecisionDescriptor) (*tradedec.DecisionTree, error) {
	return buildDecisionTree(desc)
}

// makeRefCtx creates a trading context with the market indexes
// close to the given time
func makeRefCtx(exchangeName string, closeTo time.Time) (*tradeutil.TradeContext, error) {
	marketSnap, err := marketdaemon.GetInstance().GetMarketSnapClosestTo(exchangeName, closeTo)
	if err != nil {
		return nil, err
	}

	candlesticksLim := cfg.GetConfig().GetInt("daemons.trader.candlesticks_hold")

	fiatCurrencies := exchange.GetCurrencies()

	ctxManager := tradeutil.NewTradeContextManager(candlesticksLim, fiatCurrencies...)
	ctx := ctxManager.MakeContext(marketSnap)
	return ctx, nil
}

func buildDecisionTree(desc *TradeDecisionDescriptor) (*tradedec.DecisionTree, error) {

	dt := tradedec.CreateDecTree()
	err := buildTimeBasedOps(desc, dt)
	err = buildPriceVarOps(desc, dt)
	err = buildTrendBasedOps(desc, dt)
	err = buildTrendFormationOps(desc, dt)
	if err != nil {
		return nil, err
	}

	if !desc.TradeCondition.HasDeadline {
		return dt, nil
	}

	if desc.TradeCondition.DeadlineDate == nil && desc.TradeCondition.DeadlineDuration == "" {
		return nil, errors.New(`Can't set deadline date to this trade condition without a given deadline duration or deadline date`)
	}

	if desc.TradeCondition.DeadlineDuration != "" {
		parsed, err := dur.ParseBigDuration(desc.TradeCondition.DeadlineDuration)
		if err != nil {
			return nil, err
		}
		dt.Or(tradedec.Elapsed(parsed))
		return dt, nil
	}
	dt.Or(tradedec.AfterTime(*desc.TradeCondition.DeadlineDate))
	return dt, nil

}

func buildTimeBasedOps(desc *TradeDecisionDescriptor, dec *tradedec.DecisionTree) error {
	if desc.TradeCondition.HasWaitDuration {
		if desc.TradeCondition.WaitDuration != "" {
			parsed, err := dur.ParseBigDuration(desc.TradeCondition.WaitDuration)
			if err != nil {
				return err
			}
			dec.Elapsed(parsed)
		} else if desc.TradeCondition.WaitUntil != nil {
			dec.AfterTime(*desc.TradeCondition.WaitUntil)
		} else {
			return errors.New("TradeConditionBuilder:: Cannot build wait duration op without a WaitDuration string or WaitUntil date")
		}
	}
	return nil
}

func buildPriceVarOps(desc *TradeDecisionDescriptor, dec *tradedec.DecisionTree) error {
	fSym := desc.FromSym
	tSym := desc.ToSym
	exchange := desc.Exchange

	if !desc.TradeCondition.HasPriceVariation {
		return nil
	}

	var decisionUpOp tradedec.DecisionNode
	var decisionDownOp tradedec.DecisionNode
	variation := cryptomarketutil.Variation{
		Up:   desc.TradeCondition.PriceIncreasePercent,
		Down: desc.TradeCondition.PriceDecreasePercent,
	}

	symmetricVar := &variation
	if desc.TradeCondition.SymmetricPriceVariation {
		symmetricVar = cryptomarketutil.FillSymmetricVariation(variation)
	}

	if symmetricVar.Up != 0 {
		if desc.RefPrice == 0 {
			decisionUpOp = tradedec.PriceVariation(fSym, tSym, exchange, symmetricVar.Up)
		} else {
			decisionUpOp = tradedec.PriceVariationFrom(fSym, tSym, exchange, symmetricVar.Up, desc.RefPrice)
		}
	}

	if symmetricVar.Down != 0 {
		if desc.RefPrice == 0 {
			decisionDownOp = tradedec.PriceVariation(fSym, tSym, exchange, symmetricVar.Down)
		} else {
			decisionDownOp = tradedec.PriceVariationFrom(fSym, tSym, exchange, symmetricVar.Down, desc.RefPrice)
		}
	}

	var operator tradedec.DecisionNode
	if decisionUpOp != nil && decisionDownOp != nil {
		operator = tradedec.Or(decisionUpOp, decisionDownOp)
	} else if decisionUpOp != nil {
		operator = tradedec.And(decisionUpOp)
	} else if decisionDownOp != nil {
		operator = tradedec.And(decisionDownOp)
	}

	dec.And(operator)
	return nil
}

func buildTrendBasedOps(desc *TradeDecisionDescriptor, dec *tradedec.DecisionTree) error {

	// nothing to do in this case
	if desc.TradeCondition.NoiseDown == 0 && desc.TradeCondition.NoiseUp == 0 {
		if desc.TradeCondition.ListenCandlestick || desc.TradeCondition.ListenTrend {
			return fmt.Errorf(`Cannot listen to trend or candlestick formation without noise levels`)
		}
		return nil
	}

	// Nothing to do in this case
	if !desc.TradeCondition.ListenTrend && !desc.TradeCondition.ListenCandlestick {
		return nil
	}

	candlestickListID := &tradeutil.CandlestickListID{
		FSym:     desc.FromSym,
		TSym:     desc.ToSym,
		Exchange: desc.Exchange,
	}
	variation := cryptomarketutil.Variation{
		Up:   desc.TradeCondition.NoiseUp,
		Down: desc.TradeCondition.NoiseDown,
	}

	symmetricVar := cryptomarketutil.FillSymmetricVariation(variation)

	candlestickListID.Variation = *symmetricVar
	if desc.TradeCondition.ListenTrend {
		var trendChangeOp tradedec.DecisionNode = tradedec.TrendChange(*candlestickListID)
		dec.And(trendChangeOp)
		if desc.TradeCondition.ListenTrendFormation {
			if desc.TradeCondition.TrendFormationDown != 0 {
				dec.And(tradedec.TrendType(*candlestickListID, true))
			} else {
				dec.And(tradedec.TrendType(*candlestickListID, false))
			}
		}
	}

	// check if we must listen for semnificative candlesticks
	// as well
	if !desc.TradeCondition.ListenCandlestick {
		return nil
	}

	var candlestickUpOp tradedec.DecisionNode
	var candlestickDownOp tradedec.DecisionNode
	cVar := &cryptomarketutil.Variation{
		Up:   desc.TradeCondition.TrendUp,
		Down: desc.TradeCondition.TrendDown,
	}

	if desc.TradeCondition.SymmetricTrend {
		cVar = cryptomarketutil.FillSymmetricVariation(*cVar)
	}

	if cVar.Up != 0 {
		amortizedUp := cryptomarketutil.GetAmortizedChance(cVar.Up, symmetricVar.Down)
		candlestickUpOp = tradedec.TrendPassed(*candlestickListID, amortizedUp)
	}

	if cVar.Down != 0 {
		amortizedDown := cryptomarketutil.GetAmortizedChance(cVar.Down, symmetricVar.Up)
		candlestickDownOp = tradedec.TrendPassed(*candlestickListID, amortizedDown)
	}

	if candlestickUpOp != nil && candlestickDownOp != nil {
		dec.And(tradedec.Or(candlestickUpOp, candlestickDownOp))
	} else if candlestickUpOp != nil {
		dec.And(candlestickUpOp)
	} else if candlestickDownOp != nil {
		dec.And(candlestickDownOp)
	}

	return nil
}

func buildTrendFormationOps(desc *TradeDecisionDescriptor, dec *tradedec.DecisionTree) error {
	if !desc.TradeCondition.ListenTrendFormation {
		return nil
	}

	if desc.TradeCondition.TrendFormationUp == 0 && desc.TradeCondition.TrendFormationDown == 0 {
		return errors.New(`Cannot listen for trend formation if both TrendFormationUp and TrendFormationDown are zero`)
	}

	// operator := tradedec.And()
	var tradeFormedUp, tradeFormedDown tradedec.DecisionNode

	if desc.TradeCondition.TrendFormationUp > 0 {
		tradeFormedUp = tradedec.TrendFormed(desc.FromSym, desc.ToSym, desc.Exchange, desc.TradeCondition.TrendFormationUp)
	}
	if desc.TradeCondition.TrendFormationDown < 0 {
		tradeFormedUp = tradedec.TrendFormed(desc.FromSym, desc.ToSym, desc.Exchange, desc.TradeCondition.TrendFormationDown)
	}

	if tradeFormedUp != nil && tradeFormedDown != nil {
		dec.And(tradedec.Or(tradeFormedUp, tradeFormedDown))
		return nil
	}
	if tradeFormedUp != nil {
		dec.And(tradeFormedUp)
		return nil
	}
	dec.And(tradeFormedDown)
	return nil
}
