package basetrade

import (
	"bitbucket.org/pfzero/tradr/repo/contracts"
	"bitbucket.org/pfzero/tradr/repo/modelenum"
	"bitbucket.org/pfzero/tradr/repo/modelutil"
	"bitbucket.org/pfzero/tradr/util/str"
)

// BaseTrader represents the base
// trader
type BaseTrader struct {
	isBackTestMode bool

	stats          *contracts.TraderStats
	ledger         *modelutil.Ledger
	stateListeners map[string]contracts.StateChangeSubscriber
}

// NewBaseTrader represents the base trader constructor
func NewBaseTrader() *BaseTrader {
	return &BaseTrader{
		stats: &contracts.TraderStats{
			State: modelenum.TradeBotNewState.String(),
		},
		ledger:         modelutil.NewLedger(),
		stateListeners: make(map[string]contracts.StateChangeSubscriber),
	}
}

func (bt *BaseTrader) broadcastStateChange(oldState, newSetate string) {
	for _, fn := range bt.stateListeners {
		fn(bt.stats.GUID, oldState, newSetate)
	}
}

// SubStateChange creates a new subscriber to trader's state change
// and returns subscriber's unique id
func (bt *BaseTrader) SubStateChange(fn contracts.StateChangeSubscriber) string {
	hashID := str.GenerateToken()
	bt.stateListeners[hashID] = fn
	return hashID
}

// UnsubStateChange removes the listener with the given id
func (bt *BaseTrader) UnsubStateChange(hashID string) {
	_, ok := bt.stateListeners[hashID]
	if !ok {
		return
	}
	delete(bt.stateListeners, hashID)
}

// SetState sets the state of the trader
func (bt *BaseTrader) SetState(state string) {
	if bt.stats.State == state {
		return
	}

	oldState := bt.stats.State
	bt.stats.State = state

	if !bt.isBackTestMode {
		bt.broadcastStateChange(oldState, state)
	}
}

// SetBackTestMode sets the trader to run in backtesting mode
func (bt *BaseTrader) SetBackTestMode(flag bool) {
	bt.isBackTestMode = flag
}

// SetStats sets the stats of the trader
func (bt *BaseTrader) SetStats(stats *contracts.TraderStats) {
	bt.stats = stats
}

// GetStats returns the stats of the trader
func (bt *BaseTrader) GetStats() contracts.TraderStats {
	return *bt.stats
}

// GetLedger returns the ledger of the trader
func (bt *BaseTrader) GetLedger() *modelutil.Ledger {
	return bt.ledger
}
