package multicointrade

import (
	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/repo/modelenum"
	"bitbucket.org/pfzero/tradr/repo/modelutil"
	"bitbucket.org/pfzero/tradr/trade/strategies/basetrade"
	"bitbucket.org/pfzero/tradr/trade/tradeunit"
	"bitbucket.org/pfzero/tradr/util/tradeutil"
)

const (
	usd = "USD"
)

type pairInvestment struct {
	fsym          *model.TradeableAsset
	tsym          *model.TradeableAsset
	exchange      *model.CryptoExchange
	minFiatAmount float64
	trendSteps    *model.TradeTrendStepCol
	cdCond        *model.TradeCondition
	invCond       *model.TradeCondition
	ledger        *modelutil.Ledger
}

func (pi *pairInvestment) getInvestmentCooldown() tradeunit.ConditionFactory {
	return func(ctx tradeutil.TradeContext) (tradeunit.Condition, error) {
		pi.ledger.Acquire()
		unprofitedInvestments := pi.ledger.GetUnProfitedInvestments(pi.fsym.Symbol, pi.tsym.Symbol)
		pi.ledger.Release()

		if unprofitedInvestments.Len() == 0 {
			return func(ctx tradeutil.TradeContext) (bool, error) { return true, nil }, nil
		}

		lastInvestment := unprofitedInvestments.GetByIndex(unprofitedInvestments.Len() - 1)

		decTreeDescriptor := basetrade.NewTradeDecisionDescriptor()
		decTreeDescriptor.SetPair(pi.tsym.Symbol, pi.fsym.Symbol).SetExchange(pi.exchange.Name)
		decTreeDescriptor.SetRefContextTime(lastInvestment.GetTradeRelevantTime())
		decTreeDescriptor.TradeCondition = pi.cdCond
		decTree, err := basetrade.BuildDecisionTreeWithCtx(decTreeDescriptor)
		if err != nil {
			return nil, err
		}

		return func(ctx tradeutil.TradeContext) (bool, error) {
			pi.ledger.Acquire()
			isLastInvUnProfited := pi.ledger.IsLastInvestmentPending(pi.fsym.Symbol, pi.tsym.Symbol)
			pi.ledger.Release()
			if !isLastInvUnProfited {
				return true, nil
			}

			flag, err := decTree.Exec(ctx)

			if err != nil {
				return false, err
			}
			return flag, nil
		}, nil
	}
}

func (pi *pairInvestment) getInvestmentUnit() tradeunit.UnitFactory {
	return func(ctx tradeutil.TradeContext) (*tradeunit.TradeUnit, error) {
		invTreeDescriptor := basetrade.NewTradeDecisionDescriptor().
			SetPair(pi.tsym.Symbol, pi.fsym.Symbol).
			SetExchange(pi.exchange.Name)
		invTreeDescriptor.TradeCondition = pi.invCond
		invDecision, err := basetrade.BuildDecisionTree(invTreeDescriptor)
		if err != nil {
			// again...
			return nil, err
		}
		invDecision.SetRefCtx(ctx)

		investmentUnitBuilder := tradeunit.NewBuilder()
		investmentUnitBuilder.
			ExecOnce().
			SetConditionFactory(func(ctx tradeutil.TradeContext) (tradeunit.Condition, error) {
				return func(ctx tradeutil.TradeContext) (bool, error) {
					flag, err := invDecision.Exec(ctx)
					if err != nil {
						// ...
						return false, err
					}
					return flag, nil
				}, nil
			})

		invUnit := investmentUnitBuilder.RegisterTradeFactory(func(ctx tradeutil.TradeContext) (*model.Trade, error) {
			return pi.getNextInvestment(ctx)
		})
		return invUnit, nil
	}
}

func (pi *pairInvestment) getNextInvestment(ctx tradeutil.TradeContext) (*model.Trade, error) {
	defer pi.ledger.Acquire().Release()

	investmentStreak := pi.ledger.GetInvestmentStreak(pi.fsym.Symbol, pi.tsym.Symbol)
	percentage := pi.trendSteps.GetTrendLevelAmount(investmentStreak.Len() + 1)
	if percentage == 0 {
		return nil, nil
	}

	assetInvestmentBalance := pi.ledger.GetAssetInvestmentBalance(pi.fsym.Symbol)
	if assetInvestmentBalance == nil {
		return nil, nil
	}

	amount := assetInvestmentBalance.GetBalanceFor(pi.fsym.Symbol) * percentage
	amount = amount - amountDeviation*amount
	if amount <= 0 {
		return nil, nil
	}

	var fiatAmt float64
	if pi.fsym.Symbol == usd {
		fiatAmt = amount
	} else {
		price, err := ctx.GetPrice(pi.fsym.Symbol, usd, pi.exchange.Name)
		if err != nil {
			return nil, err
		}
		fiatAmt = price.GetPrice() * amount
	}

	if fiatAmt < pi.minFiatAmount {
		return nil, nil
	}

	price, err := ctx.GetPrice(pi.fsym.Symbol, pi.tsym.Symbol, pi.exchange.Name)
	if err != nil {
		return nil, err
	}
	toAmount := amount * price.GetPrice()

	trade := &model.Trade{
		Identity: model.Identity{
			CreatedAt: ctx.GetTimestamp(),
			UpdatedAt: ctx.GetTimestamp(),
		},
		Exchange:         *pi.exchange,
		ExchangeID:       pi.exchange.GetID(),
		From:             *pi.fsym,
		FromID:           pi.fsym.GetID(),
		To:               *pi.tsym,
		ToID:             pi.tsym.GetID(),
		Type:             modelenum.TradeTypeMarket.String(),
		FromAmountIntent: amount,
		ToAmountIntent:   toAmount,
		PriceIntent:      price.GetPrice(),
	}
	pi.ledger.StageTrade(trade)

	return trade, nil
}

func (pi *pairInvestment) getUnit() *tradeunit.TradeUnit {
	builder := tradeunit.NewBuilder()
	unit := builder.
		ExecCycle().
		SetConditionFactory(pi.getInvestmentCooldown()).
		RegisterUnitFactory(pi.getInvestmentUnit())
	return unit
}
