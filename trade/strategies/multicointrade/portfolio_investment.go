package multicointrade

import (
	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/repo/modelutil"
	"bitbucket.org/pfzero/tradr/trade/tradeunit"
)

type portfolioInvestment struct {
	basePool   *model.Portfolio
	targetPool *model.Portfolio
	exchange   *model.CryptoExchange
	trendSteps *model.TradeTrendStepCol

	cdCond  *model.TradeCondition
	invCond *model.TradeCondition
	ledger  *modelutil.Ledger
}

func (pfi *portfolioInvestment) getPortfolioInvestmentUnit() *tradeunit.TradeUnit {
	units := []*tradeunit.TradeUnit{}
	for i := 0; i < len(pfi.basePool.PortfolioAssets); i++ {
		baseAsset := pfi.basePool.PortfolioAssets[i]
		for j := 0; j < len(pfi.targetPool.PortfolioAssets); j++ {
			targetAsset := pfi.targetPool.PortfolioAssets[j]
			if targetAsset.TradeableAsset.Symbol == baseAsset.TradeableAsset.Symbol {
				continue
			}

			unitBuilder := &pairInvestment{
				fsym:       &baseAsset.TradeableAsset,
				tsym:       &targetAsset.TradeableAsset,
				exchange:   pfi.exchange,
				trendSteps: pfi.trendSteps,
				cdCond:     pfi.cdCond,
				invCond:    pfi.invCond,
				ledger:     pfi.ledger,
			}
			unit := unitBuilder.getUnit()
			units = append(units, unit)
		}
	}

	return tradeunit.All(tradeunit.Parallel, units...)
}
