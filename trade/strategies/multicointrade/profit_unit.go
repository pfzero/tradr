package multicointrade

import (
	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/repo/modelenum"
	"bitbucket.org/pfzero/tradr/repo/modelutil"
	"bitbucket.org/pfzero/tradr/trade/strategies/basetrade"
	"bitbucket.org/pfzero/tradr/trade/tradeunit"
	"bitbucket.org/pfzero/tradr/util/tradeutil"
)

type pairProfit struct {
	investment     *model.Trade
	profitDecision *model.TradeCondition
	ledger         *modelutil.Ledger
}

func (pp *pairProfit) getConditionFactory() tradeunit.ConditionFactory {
	return func(ctx tradeutil.TradeContext) (tradeunit.Condition, error) {
		profitTreeDescriptor := basetrade.NewTradeDecisionDescriptor().
			SetPair(pp.investment.To.Symbol, pp.investment.From.Symbol).
			SetExchange(pp.investment.Exchange.Name).
			SetRefContextTime(pp.investment.GetTradeRelevantTime())

		profitTreeDescriptor.TradeCondition = pp.profitDecision

		profitDecision, err := basetrade.BuildDecisionTreeWithCtx(profitTreeDescriptor)
		if err != nil {
			return nil, err
		}

		return func(ctx tradeutil.TradeContext) (bool, error) {
			flag, err := profitDecision.Exec(ctx)
			if err != nil {
				return false, err
			}
			return flag, nil
		}, nil
	}
}

func (pp *pairProfit) getProfitTrade(ctx tradeutil.TradeContext) (*model.Trade, error) {
	defer pp.ledger.Acquire().Release()

	boughtAmount := pp.ledger.GetTradeBackAmount(pp.investment)

	// this means that the investment amount was withdrawn
	if boughtAmount == 0 {
		return nil, nil
	}

	price, e := ctx.GetPrice(pp.investment.To.Symbol, pp.investment.From.Symbol, pp.investment.Exchange.Name)
	if e != nil {
		return nil, e
	}
	boughtAmount = boughtAmount - amountDeviation*boughtAmount
	buyBackAmount := boughtAmount * price.GetPrice()

	t := &model.Trade{
		Identity: model.Identity{
			CreatedAt: ctx.GetTimestamp(),
			UpdatedAt: ctx.GetTimestamp(),
		},
		Exchange:         pp.investment.Exchange,
		ExchangeID:       pp.investment.Exchange.GetID(),
		From:             pp.investment.To,
		FromID:           pp.investment.To.GetID(),
		To:               pp.investment.From,
		ToID:             pp.investment.From.GetID(),
		LinkedTradeID:    pp.investment.GetID(),
		LinkedTrade:      pp.investment,
		Status:           modelenum.NewTrade.String(),
		Type:             modelenum.TradeTypeMarket.String(),
		FromAmountIntent: boughtAmount,
		ToAmountIntent:   buyBackAmount,
		PriceIntent:      price.GetPrice(),
	}

	pp.ledger.StageTrade(t)

	return t, nil
}

func (pp *pairProfit) getUnit() *tradeunit.TradeUnit {
	builder := tradeunit.NewBuilder()
	unit := builder.
		ExecOnce().
		SetConditionFactory(pp.getConditionFactory()).
		RegisterTradeFactory(func(ctx tradeutil.TradeContext) (*model.Trade, error) {
			return pp.getProfitTrade(ctx)
		})

	return unit
}
