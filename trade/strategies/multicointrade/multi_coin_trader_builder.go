package multicointrade

import (
	"sync"
	"time"

	"bitbucket.org/pfzero/tradr/repo/contracts"
	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/store/datastore/cryptomarketstore"
)

var multiCoinTraderBuilderOnce sync.Once
var multiCoinTraderBuilder *MultiCoinTraderBuilder

// MultiCoinTraderBuilder is the data structure responsible
// for building multi coin traders
type MultiCoinTraderBuilder struct {
	activeListeners map[string]string
}

// GetInstance returns the singleton instance
// of the CoinPairTraderBuilder
func GetInstance() *MultiCoinTraderBuilder {
	multiCoinTraderBuilderOnce.Do(func() {
		multiCoinTraderBuilder = &MultiCoinTraderBuilder{
			activeListeners: make(map[string]string),
		}
	})
	return multiCoinTraderBuilder
}

// GetAllTraders returns all multi coin traders
func (mctb *MultiCoinTraderBuilder) GetAllTraders() ([]contracts.Trader, error) {
	mcts, err := cryptomarketstore.GetAllMultiCoinTraders()
	if err != nil {
		return nil, err
	}
	built, err := mctb.buildTraders(mcts...)
	if err != nil {
		return nil, err
	}

	for i := 0; i < len(built); i++ {
		trader := built[i]
		if trader.GetStats().IsTraderActive() {
			mctb.subStateChange(trader)
		}
	}
	return mctb.getAsTraders(built...), nil
}

// GetActiveTraders returns the list of active traders
func (mctb *MultiCoinTraderBuilder) GetActiveTraders() ([]contracts.Trader, error) {
	mcts, err := cryptomarketstore.GetActiveMultiCoinTraders()
	if err != nil {
		return nil, err
	}

	built, err := mctb.buildTraders(mcts...)
	if err != nil {
		return nil, err
	}
	for i := 0; i < len(built); i++ {
		trader := built[i]
		if trader.GetStats().IsTraderActive() {
			mctb.subStateChange(trader)
		}
	}

	return mctb.getAsTraders(built...), nil
}

// GetTraderByGUID retunrs the traders by guid
func (mctb *MultiCoinTraderBuilder) GetTraderByGUID(guid string) (contracts.Trader, error) {
	trader, err := mctb.getTraderByGUID(guid)
	if err != nil {
		return nil, err
	}
	if trader.GetStats().IsTraderActive() {
		mctb.subStateChange(trader)
	}
	return trader, nil
}

// GetActiveTradersForExchange is used only to implement the TradeStorer interface; it does nothing
func (mctb *MultiCoinTraderBuilder) GetActiveTradersForExchange(eName string) ([]contracts.Trader, error) {
	return nil, nil
}

// GetAllTradersForExchange returns all the traders for the given exchange; Placeholder method present
// only to implement the contracts.TradeStorer interface;
func (mctb *MultiCoinTraderBuilder) GetAllTradersForExchange(eName string) ([]contracts.Trader, error) {
	return nil, nil
}

// GetTraderForBacktesting returns the multi coin trader prepared for backtesting
func (mctb *MultiCoinTraderBuilder) GetTraderForBacktesting(startTime time.Time, guid string) (contracts.Trader, error) {
	id, err := traderIdentifier.GetIDFromGUID(guid)
	if err != nil {
		return nil, err
	}
	mct, err := cryptomarketstore.GetMultiCoinTraderByID(id)
	if err != nil {
		return nil, err
	}
	built, err := mctb.buildTraders(*mct)
	if err != nil {
		return nil, err
	}
	trader := built[0]
	return trader, nil
}

// Name returns the name of the builder
func (mctb *MultiCoinTraderBuilder) Name() string {
	return traderIdentifier.GetName()
}

func (mctb *MultiCoinTraderBuilder) buildTraders(mcts ...model.MultiCoinTradeCfg) ([]*MultiCoinTrader, error) {
	traders := []*MultiCoinTrader{}
	for i := 0; i < len(mcts); i++ {
		mct := &mcts[i]
		trader, err := NewMultiCoinTrader(mct)
		if err != nil {
			return nil, err
		}
		traders = append(traders, trader)
	}
	return traders, nil
}

func (mctb *MultiCoinTraderBuilder) subStateChange(t *MultiCoinTrader) {
	traderStats := t.GetStats()
	_, ok := mctb.activeListeners[traderStats.GUID]
	// check if already subscribed
	if ok {
		return
	}

	subID := t.SubStateChange(func(guid, oldState, newState string) {

		id, e := traderIdentifier.GetIDFromGUID(guid)

		if e != nil {
			return
		}

		// log these errors somehow... as well
		_ = cryptomarketstore.UpdateMultiCoinTraderState(id, newState)

		if !traderStats.IsTraderActive() {
			// unsubscribe and delete subscription
			t.UnsubStateChange(mctb.activeListeners[traderStats.GUID])
			delete(mctb.activeListeners, traderStats.GUID)
		}
	})
	mctb.activeListeners[traderStats.GUID] = subID
}

func (mctb *MultiCoinTraderBuilder) getAsTraders(mcts ...*MultiCoinTrader) []contracts.Trader {
	traders := make([]contracts.Trader, len(mcts))
	for i := 0; i < len(mcts); i++ {
		traders[i] = mcts[i]
	}
	return traders
}

func (mctb *MultiCoinTraderBuilder) getTraderByGUID(guid string) (*MultiCoinTrader, error) {
	id, err := traderIdentifier.GetIDFromGUID(guid)
	if err != nil {
		return nil, err
	}
	mct, err := cryptomarketstore.GetMultiCoinTraderByID(id)
	if err != nil {
		return nil, err
	}
	built, err := mctb.buildTraders(*mct)
	if err != nil {
		return nil, err
	}
	trader := built[0]
	return trader, nil
}
