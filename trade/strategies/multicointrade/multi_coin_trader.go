package multicointrade

import (
	"bitbucket.org/pfzero/tradr/repo/modelenum"

	"bitbucket.org/pfzero/tradr/repo/contracts"

	"bitbucket.org/pfzero/tradr/trade/strategies/basetrade"

	"bitbucket.org/pfzero/tradr/util/tradeutil"

	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/trade/tradeunit"
)

const amountDeviation = 0.0001
const traderName = "MultiCoinTrader"

var traderIdentifier = tradeutil.NewTraderID(traderName)

// GetTraderIdentifier returns the trader identifier
func GetTraderIdentifier() *tradeutil.TraderID {
	return traderIdentifier
}

// MultiCoinTrader is a strategy that
// makes given a base portfolio and a target
// portfolio, it tries to make profits in the
// base portfolio by investing in the target
// portfolio
type MultiCoinTrader struct {
	basetrade.BaseTrader

	// main units that provide with trades
	investmentUnit *tradeunit.TradeUnit
	profitUnit     *tradeunit.TradeUnit

	// configuration object
	cfg *model.MultiCoinTradeCfg

	initialized bool
}

// NewMultiCoinTrader represents the multi-coin trader constructor
func NewMultiCoinTrader(cfg *model.MultiCoinTradeCfg) (*MultiCoinTrader, error) {
	baseTrader := basetrade.NewBaseTrader()
	baseTrader.SetStats(&contracts.TraderStats{
		State:    cfg.State,
		GUID:     traderIdentifier.GetTraderInstanceGUID(cfg.GetID()),
		Exchange: cfg.Exchange.Name,
	})

	investmentUnitBuilder := &portfolioInvestment{
		basePool:   &cfg.BasePortfolio,
		targetPool: &cfg.TargetPortfolio,
		exchange:   &cfg.Exchange,
		trendSteps: &cfg.TradeTrendStepCol,
		cdCond:     &cfg.InvCooldownCondition,
		invCond:    &cfg.InvCondition,
		ledger:     baseTrader.GetLedger(),
	}

	profitUnit := tradeunit.All(tradeunit.Parallel)

	return &MultiCoinTrader{
		BaseTrader:     *baseTrader,
		investmentUnit: investmentUnitBuilder.getPortfolioInvestmentUnit(),
		profitUnit:     profitUnit,
		cfg:            cfg,
	}, nil
}

// MakeDecision is used to make a new trade decision (real time)
func (mct *MultiCoinTrader) MakeDecision(ctx tradeutil.TradeContext) *contracts.TraderResponse {
	if !mct.initialized {
		mct.initializeProfitUnits()
		mct.initialized = true
	}

	profits, err := mct.profitUnit.Exec(ctx)
	if err != nil {
		return &contracts.TraderResponse{
			Decision: nil,
			Err:      &contracts.TraderErr{Err: err, IsCritical: false},
		}
	}

	investments, err := mct.investmentUnit.Exec(ctx)

	if err != nil {
		return &contracts.TraderResponse{
			Decision: nil,
			Err:      &contracts.TraderErr{Err: err, IsCritical: false},
		}
	}

	aggregate := []model.Trade{}
	if investments != nil && len(investments) > 0 {
		aggregate = append(aggregate, investments...)
	}
	if profits != nil && len(profits) > 0 {
		aggregate = append(aggregate, profits...)
	}

	if len(aggregate) == 0 {
		return nil
	}

	dec := mct.buildTradeDecision(aggregate...)
	return &contracts.TraderResponse{
		Decision: dec,
	}
}

// HandleProcessedTrade handles a processed trade
func (mct *MultiCoinTrader) HandleProcessedTrade(t *model.Trade) error {
	if t.LinkedTradeID == 0 {
		if t.IsCompleteTrade() {
			mct.addProfitTrader(t)
		}
	} else {
		if t.IsCanceledTrade() {
			l := mct.GetLedger()
			defer l.Acquire().Release()

			investments := l.GetInvestments(t.To.Symbol, t.From.Symbol)
			investment := investments.
				Find(func(investment *model.Trade) bool {
					return investment.GetID() == t.LinkedTradeID
				})

			if investment != nil {
				mct.addProfitTrader(investment)
			}
		}
	}

	return nil
}

func (mct *MultiCoinTrader) initializeProfitUnits() {
	ledger := mct.GetLedger()
	defer ledger.Acquire().Release()

	unprofitedTrades := []model.Trade{}
	for _, baseAsset := range mct.cfg.BasePortfolio.PortfolioAssets {

		for _, targetAsset := range mct.cfg.TargetPortfolio.PortfolioAssets {
			if targetAsset.TradeableAsset.Symbol == baseAsset.TradeableAsset.Symbol {
				continue
			}
			unprofited := ledger.
				GetUnProfitedInvestments(baseAsset.TradeableAsset.Symbol, targetAsset.TradeableAsset.Symbol).
				FilterByStatus(modelenum.CompletedTrade).
				GetAll()

			unprofitedTrades = append(unprofitedTrades, unprofited...)
		}
	}

	for i := 0; i < len(unprofitedTrades); i++ {
		unprofited := &unprofitedTrades[i]
		mct.addProfitTrader(unprofited)
	}
}

func (mct *MultiCoinTrader) addProfitTrader(t *model.Trade) {
	profitUnit := &pairProfit{
		investment:     t,
		profitDecision: &mct.cfg.ProfitCondition,
		ledger:         mct.GetLedger(),
	}
	mct.profitUnit.AddChildren(profitUnit.getUnit())
}

func (mct *MultiCoinTrader) populateTrade(t *model.Trade) {
	t.Status = modelenum.NewTrade.String()
	t.Owner = mct.cfg.Beneficiary
	t.OwnerID = mct.cfg.BeneficiaryID
}

func (mct *MultiCoinTrader) buildTradeDecision(trades ...model.Trade) *model.TradeDecision {
	if trades == nil && len(trades) == 0 {
		return nil
	}

	for i := 0; i < len(trades); i++ {
		mct.populateTrade(&trades[i])
	}

	return &model.TradeDecision{
		AutoTrade:     mct.cfg.AutoTrade,
		Beneficiary:   mct.cfg.Beneficiary,
		BeneficiaryID: mct.cfg.BeneficiaryID,
		Notify:        mct.cfg.Notify,
		TraderGUID:    mct.GetStats().GUID,
		Trades:        trades,
	}
}
