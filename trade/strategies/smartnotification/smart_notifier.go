package smartnotification

import (
	"fmt"

	"bitbucket.org/pfzero/tradr/logs"
	"github.com/sirupsen/logrus"

	"bitbucket.org/pfzero/tradr/trade/strategies/basetrade"

	"bitbucket.org/pfzero/tradr/util/tradedec"
	"bitbucket.org/pfzero/tradr/util/tradeutil"

	"bitbucket.org/pfzero/tradr/repo/contracts"
	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/repo/modelenum"
)

const traderName = "SmartNotifier"

var traderIdentifier = tradeutil.NewTraderID(traderName)

// GetTraderIdentifier returns the trader identifier
func GetTraderIdentifier() *tradeutil.TraderID {
	return traderIdentifier
}

// SmartNotifier is the implementation of the smart
// notifications
// it implements the trader interface
type SmartNotifier struct {
	basetrade.BaseTrader
	cfg                     *model.SmartNotification
	tradeDecisionDescriptor *basetrade.TradeDecisionDescriptor
	tradeCondition          *tradedec.DecisionTree
}

// NewSmartNotifier creates a new smart notifier based on the given configuration
func NewSmartNotifier(cfg *model.SmartNotification) (*SmartNotifier, error) {
	if cfg == nil {
		return nil, fmt.Errorf(`%s::NewSmartNotifier - The given config object was nil`, traderName)
	}
	tradeDecisionDescriptor := &basetrade.TradeDecisionDescriptor{
		FromSym:        cfg.PairFrom.Symbol,
		ToSym:          cfg.PairTo.Symbol,
		Exchange:       cfg.Exchange.Name,
		RefContextTime: cfg.CreatedAt,
		RefPrice:       cfg.RefPrice,
		TradeCondition: &cfg.TradeCondition,
	}

	if cfg.RefContextTime != nil && !cfg.RefContextTime.IsZero() {
		tradeDecisionDescriptor.RefContextTime = *cfg.RefContextTime
	}

	tradeCondition, err := basetrade.BuildDecisionTreeWithCtx(tradeDecisionDescriptor)
	if err != nil {
		return nil, err
	}

	baseTrader := basetrade.NewBaseTrader()
	baseTrader.SetStats(
		&contracts.TraderStats{
			GUID:     traderIdentifier.GetTraderInstanceGUID(cfg.GetID()),
			State:    cfg.State,
			Exchange: cfg.Exchange.Name,
		},
	)

	margin := 0.001 * cfg.TradeAmount

	lio := &model.LedgerIO{
		Identity: model.Identity{
			ID:        1,
			CreatedAt: cfg.CreatedAt,
			UpdatedAt: cfg.UpdatedAt,
		},
		Type:       modelenum.Deposit.String(),
		Asset:      cfg.PairFrom,
		AssetID:    cfg.PairFromID,
		Exchange:   cfg.Exchange,
		ExchangeID: cfg.ExchangeID,
		Owner:      cfg.Beneficiary,
		OwnerID:    cfg.BeneficiaryID,
		Amount:     cfg.TradeAmount + margin, // just enough so it doesn't overflow
	}
	baseTrader.GetLedger().AddLedgerIO(lio)

	return &SmartNotifier{
		BaseTrader: *baseTrader,
		cfg:        cfg,
		tradeDecisionDescriptor: tradeDecisionDescriptor,
		tradeCondition:          tradeCondition,
	}, nil
}

// MakeDecision is used to determine trader's reaction to
// price changes; If the trader generates a trade decision
// based on current market price, the trade decision will then
// be processed further
func (sm *SmartNotifier) MakeDecision(ctx tradeutil.TradeContext) *contracts.TraderResponse {

	l := sm.GetLedger()
	defer l.Acquire().Release()

	tradesLen := l.GetTradeList().Len()
	if tradesLen > 0 {
		sm.SetState(modelenum.TradeBotCompletedState.String())
		return nil
	}

	investments := l.GetInvestments(sm.cfg.PairFrom.Symbol, sm.cfg.PairTo.Symbol)

	if investments.Len() > 0 {
		return nil
	}

	signal, err := sm.tradeCondition.Exec(ctx)

	if err != nil {
		return nil
	}

	if !signal {
		return nil
	}

	dec, err := sm.buildTradeDecision(ctx)
	if err != nil {
		logs.GetLogger().WithFields(logrus.Fields{
			"module": "traders.smart_notifier",
			"action": "build_trade_decision",
		}).Error(err)

		sm.SetState(modelenum.TradeBotErroredState.String())
		return nil
	}

	return &contracts.TraderResponse{
		Decision: dec,
	}
}

// HandleProcessedTrade is the method needed for handling
// successfull/canceled trades
func (sm *SmartNotifier) HandleProcessedTrade(t *model.Trade) error {
	if t.IsCompleteTrade() {
		sm.SetState(modelenum.TradeBotCompletedState.String())
	}

	return nil
}

func (sm *SmartNotifier) buildTrade(ctx tradeutil.TradeContext) (*model.Trade, error) {
	cfg := sm.cfg
	price, e := ctx.GetPrice(cfg.PairFrom.Symbol, cfg.PairTo.Symbol, cfg.Exchange.Name)
	if e != nil {
		return nil, e
	}
	buyAmount := cfg.TradeAmount * price.GetPrice()
	trade := &model.Trade{
		OwnerID:          cfg.Beneficiary.GetID(),
		Owner:            cfg.Beneficiary,
		FromID:           cfg.PairFrom.GetID(),
		From:             cfg.PairFrom,
		ToID:             cfg.PairTo.GetID(),
		To:               cfg.PairTo,
		ExchangeID:       cfg.Exchange.GetID(),
		Exchange:         cfg.Exchange,
		Status:           modelenum.NewTrade.String(),
		Type:             modelenum.TradeTypeMarket.String(),
		FromAmountIntent: cfg.TradeAmount,
		ToAmountIntent:   buyAmount,
		PriceIntent:      price.GetPrice(),
	}
	return trade, nil
}

func (sm *SmartNotifier) buildTradeDecision(ctx tradeutil.TradeContext) (*model.TradeDecision, error) {
	cfg := sm.cfg
	trade, e := sm.buildTrade(ctx)
	if e != nil {
		return nil, e
	}

	decision := &model.TradeDecision{
		BeneficiaryID: cfg.Beneficiary.GetID(),
		Beneficiary:   cfg.Beneficiary,
		Trades:        []model.Trade{*trade},
		TraderGUID:    sm.GetStats().GUID,
		Notify:        true,
		AutoTrade:     cfg.AutoTrade,
	}
	return decision, nil
}
