package smartnotification

import (
	"sync"
	"time"

	"bitbucket.org/pfzero/tradr/logs"
	"github.com/sirupsen/logrus"

	"bitbucket.org/pfzero/tradr/repo/contracts"
	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/store/datastore/cryptomarketstore"
)

var smartNotifierBuilderOnce sync.Once
var smartNotifierBuilder *SmartNotifierBuilder

// SmartNotifierBuilder is the data structure responsible
// for building smart notifications
type SmartNotifierBuilder struct {
	activeTraderListeners map[string]string
}

// GetInstance returns the singleton instance
// of the CoinPairTraderBuilder
func GetInstance() *SmartNotifierBuilder {
	smartNotifierBuilderOnce.Do(func() {
		smartNotifierBuilder = &SmartNotifierBuilder{
			activeTraderListeners: make(map[string]string),
		}
	})
	return smartNotifierBuilder
}

// GetAllTraders returns all smart traders
func (snb *SmartNotifierBuilder) GetAllTraders() ([]contracts.Trader, error) {
	sns, err := cryptomarketstore.GetAllSmartNotifiers()
	if err != nil {
		return nil, err
	}
	built, err := snb.buildTraders(sns...)
	if err != nil {
		return nil, err
	}

	for i := 0; i < len(built); i++ {
		trader := built[i]
		if trader.GetStats().IsTraderActive() {
			snb.subStateChange(trader)
		}
	}

	return snb.convertSmartNotifiersToTrader(built...), nil
}

// GetActiveTraders returns the list of active traders
func (snb *SmartNotifierBuilder) GetActiveTraders() ([]contracts.Trader, error) {
	sns, err := cryptomarketstore.GetActiveSmartNotifiers()
	if err != nil {
		return nil, err
	}

	built, err := snb.buildTraders(sns...)
	if err != nil {
		return nil, err
	}

	for i := 0; i < len(built); i++ {
		trader := built[i]
		if trader.GetStats().IsTraderActive() {
			snb.subStateChange(trader)
		}
	}

	return snb.convertSmartNotifiersToTrader(built...), nil
}

// GetActiveTradersForExchange is used only to implement the TradeStorer interface; it does nothing
func (snb *SmartNotifierBuilder) GetActiveTradersForExchange(eName string) ([]contracts.Trader, error) {
	return nil, nil
}

// GetAllTradersForExchange returns all the traders for the given exchange; Placeholder method present
// only to implement the contracts.TradeStorer interface;
func (snb *SmartNotifierBuilder) GetAllTradersForExchange(eName string) ([]contracts.Trader, error) {
	return nil, nil
}

// GetTraderByGUID retunrs the traders by guid
func (snb *SmartNotifierBuilder) GetTraderByGUID(guid string) (contracts.Trader, error) {
	trader, err := snb.getTraderByGUID(guid)
	if err != nil {
		return nil, err
	}
	if trader.GetStats().IsTraderActive() {
		snb.subStateChange(trader)
	}
	return trader, nil
}

// GetTraderForBacktesting returns the smart notifier prepared for backtesting
func (snb *SmartNotifierBuilder) GetTraderForBacktesting(startTime time.Time, guid string) (contracts.Trader, error) {
	id, err := traderIdentifier.GetIDFromGUID(guid)
	if err != nil {
		return nil, err
	}
	sn, err := cryptomarketstore.GetSmartNotifierByID(id)
	sn.RefContextTime = &startTime
	if err != nil {
		return nil, err
	}
	built, err := snb.buildTraders(*sn)
	if err != nil {
		return nil, err
	}
	trader := built[0]
	return trader, nil
}

// Name returns the name of the builder
func (snb *SmartNotifierBuilder) Name() string {
	return traderIdentifier.GetName()
}

func (snb *SmartNotifierBuilder) subStateChange(t *SmartNotifier) {
	traderStats := t.GetStats()

	_, ok := snb.activeTraderListeners[traderStats.GUID]
	// check if already subscribed
	if ok {
		return
	}

	subID := t.SubStateChange(func(guid, oldState, newState string) {

		id, e := traderIdentifier.GetIDFromGUID(guid)

		if e != nil {
			logs.GetLogger().WithFields(logrus.Fields{
				"module": "smart_notification.smart_notifier_builder.sm_subscriber",
			}).Error(e)
			return
		}

		// log these errors somehow... as well
		e = cryptomarketstore.UpdateSmartNotifierState(id, newState)
		if e != nil {
			logs.GetLogger().WithFields(logrus.Fields{
				"module": "smart_notification.smart_notifier_builder.sm_subscriber",
			}).Error(e)
		}

		if !traderStats.IsTraderActive() {
			// unsubscribe and delete subscription
			t.UnsubStateChange(snb.activeTraderListeners[traderStats.GUID])
			delete(snb.activeTraderListeners, traderStats.GUID)
		}
	})

	snb.activeTraderListeners[traderStats.GUID] = subID
}

func (snb *SmartNotifierBuilder) buildTraders(sns ...model.SmartNotification) ([]*SmartNotifier, error) {
	traders := []*SmartNotifier{}
	for i := 0; i < len(sns); i++ {
		sn := &sns[i]
		trader, err := NewSmartNotifier(sn)
		if err != nil {
			return nil, err
		}
		traders = append(traders, trader)
	}
	return traders, nil
}

func (snb *SmartNotifierBuilder) convertSmartNotifiersToTrader(sns ...*SmartNotifier) []contracts.Trader {
	traders := make([]contracts.Trader, len(sns))
	for i := 0; i < len(sns); i++ {
		traders[i] = sns[i]
	}
	return traders
}

func (snb *SmartNotifierBuilder) getTraderByGUID(guid string) (*SmartNotifier, error) {
	id, err := traderIdentifier.GetIDFromGUID(guid)
	if err != nil {
		return nil, err
	}
	sn, err := cryptomarketstore.GetSmartNotifierByID(id)
	if err != nil {
		return nil, err
	}
	built, err := snb.buildTraders(*sn)
	if err != nil {
		return nil, err
	}
	trader := built[0]
	return trader, nil
}
