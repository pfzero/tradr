# Smart Notification

A smart notification is just like any other trader but it makes only 1 decision based on the given configuration.

To create a smart notification, you need to define a decision pipeline first. Afterwards, configure the `SmartNotification` by configuring the cryptoassets pair to monitor - eg. `ETH-USD`.

So, to create a `SmartNotification`:

1. Go to `admin UI` -> `Crypto Traders`
2. `Trade Decision Steps` -> `+ button bottom-right` -> Setup the `Step`
   * For Test, you can just configure it to execute after 10 minutes;
   * `Check Has Wait Duration` -> under `Wait Duration` field set value to `10m`; Please note that `10M` = 10 Months whereas `10m` = 10 minutes (careful!).
   * Or, you can just configure `Wait Until` field which is a date.
   * Then click `Add` button;
3. Go to `Trade Decision Pipelines` -> `+ button bottom right`
   * Setup a name and hit Add button
4. GO back to the decision step created at pt. 2, click on that decision step
   * A side panel should open to edit the values;
   * Now link the decision step to the decision pipeline configured at pt. 3; At the bottom of the section you have a drop-down for `Trade Decision Pipeline`
5. Now you setup your `Smart Notification`. Go to `Smart Notifications` page.
6. There create a new `Smart Notification` (again, `+ at bottom right`);
7. At `Beneficiary` insert the user who'll receive the smart notification;
8. Leave `AutoTrade` unchecked - not supported for now;
9. Set `TradeAmount` as a number
10. Set `Exchange` field from the list of available exchanges;
11. Set `PairFrom` && `PairTo` from the list of available tradeable assets; Please note that currently the only fiat currency supported is USD.
12. Select `Decision Pipeline`. Select the decision pipeline created above at point 3;
13. Leave `State` as-is. Hit `Add` button.
14. Now you should have a smart notification created in the table from `Smart Notifications` page.
15. On the row of your created `Smart Notification`, at the end of the row, you should have a small menu with 3 dots.
16. Hit that menu and from the list of options hit: `Start`.
17. You're finished. Normally, after 10 minutes the selected user should receive an email informing him on the trade.
18. You can also stop the trader anytime by hiting `Pause` instead of `Start` at pt.

Right now the smart notifications and the whole system has some bugs that will be resolved in the future...
