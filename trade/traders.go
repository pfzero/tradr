package trade

import (
	"bitbucket.org/pfzero/tradr/repo/contracts"
	// blank import this for now; just to enable the compiler
	"bitbucket.org/pfzero/tradr/trade/strategies/multicointrade"
	"bitbucket.org/pfzero/tradr/trade/strategies/smartnotification"
)

// GetTraderProviders returns the trade instance providers
func GetTraderProviders() []contracts.TraderStorer {
	providers := []contracts.TraderStorer{}
	providers = append(providers, smartnotification.GetInstance(), multicointrade.GetInstance())
	return providers
}
