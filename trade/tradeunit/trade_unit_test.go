package tradeunit

import (
	"errors"
	"reflect"
	"testing"

	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/util/tradeutil"
)

func blankTradeFactory(ctx tradeutil.TradeContext) (*model.Trade, error) {
	return &model.Trade{}, nil
}

func TestFactoryFromCondition(t *testing.T) {
	type args struct {
		cond Condition
	}
	type want struct {
		condRes    bool
		condResErr bool
	}
	tests := []struct {
		name string
		args args
		want want
	}{
		{
			name: "Simple Factory from Simple Condition",
			args: args{cond: func(ctx tradeutil.TradeContext) (bool, error) { return false, nil }},
			want: want{condRes: false, condResErr: false},
		},
		{
			name: "Factory from Truthy Condition",
			args: args{cond: func(ctx tradeutil.TradeContext) (bool, error) { return true, nil }},
			want: want{condRes: true, condResErr: false},
		},
		{
			name: "Factory from err condition",
			args: args{cond: func(ctx tradeutil.TradeContext) (bool, error) { return false, errors.New("Unexpected Error") }},
			want: want{condRes: false, condResErr: true},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotFactory := FactoryFromCondition(tt.args.cond)
			cond, _ := gotFactory(tradeutil.TradeContext{})

			f, e := cond(tradeutil.TradeContext{})
			if (e != nil) != tt.want.condResErr {
				t.Errorf("FactoryFromCondition() wrapped condition error = %v, want %v", e, tt.want.condResErr)
			}

			if f != tt.want.condRes {
				t.Errorf("FactoryFromCondition() wrapped condition result = %t, want %t", f, tt.want.condRes)
			}
		})
	}
}

func TestTradeUnit_Exec(t *testing.T) {

	type want struct {
		tradesLen  int
		err        error
		isComplete bool
	}

	tests := []struct {
		name  string
		tu    func() *TradeUnit
		wants []want
	}{
		{
			name: "Exec Once unit",
			tu: func() *TradeUnit {
				return &TradeUnit{
					execMode:     execModeUnit,
					repeatMode:   repeatModeOnce,
					condFactory:  TruthyCondFactory,
					tradeFactory: blankTradeFactory,
				}
			},
			wants: []want{
				want{tradesLen: 1, err: nil, isComplete: true},
				want{tradesLen: 0, err: nil, isComplete: true},
				want{tradesLen: 0, err: nil, isComplete: true},
				want{tradesLen: 0, err: nil, isComplete: true},
			},
		},
		{
			name: "Exec cycle unit",
			tu: func() *TradeUnit {
				return &TradeUnit{
					execMode:     execModeUnit,
					repeatMode:   repeatModeCycle,
					condFactory:  TruthyCondFactory,
					tradeFactory: blankTradeFactory,
				}
			},
			wants: []want{
				want{tradesLen: 1, err: nil, isComplete: false},
				want{tradesLen: 1, err: nil, isComplete: false},
				want{tradesLen: 1, err: nil, isComplete: false},
				want{tradesLen: 1, err: nil, isComplete: false},
			},
		},
		{
			name: "Exec group of once",
			tu: func() *TradeUnit {

				return &TradeUnit{
					execMode:         execModeGroup,
					groupRuntimeMode: Parallel,

					subUnits: []*TradeUnit{
						&TradeUnit{
							execMode:     execModeUnit,
							repeatMode:   repeatModeOnce,
							condFactory:  TruthyCondFactory,
							tradeFactory: blankTradeFactory,
						},
						&TradeUnit{
							execMode:     execModeUnit,
							repeatMode:   repeatModeOnce,
							condFactory:  TruthyCondFactory,
							tradeFactory: blankTradeFactory,
						},
					},
				}
			},
			wants: []want{
				want{tradesLen: 2, err: nil, isComplete: true},
				want{tradesLen: 0, err: nil, isComplete: true},
				want{tradesLen: 0, err: nil, isComplete: true},
				want{tradesLen: 0, err: nil, isComplete: true},
			},
		},
		{
			name: "Exec group of cycle",
			tu: func() *TradeUnit {

				return &TradeUnit{
					execMode:         execModeGroup,
					groupRuntimeMode: Parallel,

					subUnits: []*TradeUnit{
						&TradeUnit{
							execMode:     execModeUnit,
							repeatMode:   repeatModeCycle,
							condFactory:  TruthyCondFactory,
							tradeFactory: blankTradeFactory,
						},
						&TradeUnit{
							execMode:     execModeUnit,
							repeatMode:   repeatModeCycle,
							condFactory:  TruthyCondFactory,
							tradeFactory: blankTradeFactory,
						},
					},
				}
			},
			wants: []want{
				want{tradesLen: 2, err: nil, isComplete: false},
				want{tradesLen: 2, err: nil, isComplete: false},
				want{tradesLen: 2, err: nil, isComplete: false},
				want{tradesLen: 2, err: nil, isComplete: false},
			},
		},
		{
			name: "Investment Group",
			tu: func() *TradeUnit {

				return &TradeUnit{
					execMode:         execModeGroup,
					groupRuntimeMode: Parallel,

					subUnits: []*TradeUnit{
						&TradeUnit{
							execMode:    execModeUnit,
							repeatMode:  repeatModeCycle,
							condFactory: TruthyCondFactory,
							unitFactory: func(ctx tradeutil.TradeContext) (*TradeUnit, error) {
								return &TradeUnit{
									execMode:     execModeUnit,
									repeatMode:   repeatModeOnce,
									condFactory:  TruthyCondFactory,
									tradeFactory: blankTradeFactory,
								}, nil
							},
						},
						&TradeUnit{
							execMode:     execModeUnit,
							repeatMode:   repeatModeOnce,
							condFactory:  TruthyCondFactory,
							tradeFactory: blankTradeFactory,
						},
					},
				}
			},
			wants: []want{
				want{tradesLen: 2, err: nil, isComplete: false},
				want{tradesLen: 1, err: nil, isComplete: false},
				want{tradesLen: 1, err: nil, isComplete: false},
				want{tradesLen: 1, err: nil, isComplete: false},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tu := tt.tu()
			for _, want := range tt.wants {
				trades, err := tu.Exec(tradeutil.TradeContext{})

				if !reflect.DeepEqual(want.err, err) {
					t.Errorf("Wanted error: %v, instead got: %v", want.err, err)
				}

				if want.tradesLen != len(trades) {
					t.Errorf("Wanted %d trades to be made. Instead got %d", want.tradesLen, len(trades))
				}

				if tu.IsComplete() != want.isComplete {
					t.Errorf("TradeUnit.Exec() wanted IsComplete to be %t. Instead got %t", want.isComplete, tu.IsComplete())
				}
			}
		})
	}
}

func TestTradeUnit_IsComplete(t *testing.T) {
	tests := []struct {
		name string
		tu   *TradeUnit
		want bool
	}{
		{
			name: "Empty Group Mode",
			tu:   &TradeUnit{execMode: execModeGroup, subUnits: []*TradeUnit{}},
			want: true,
		},
		{
			name: "Non-empty Group Mode",
			tu:   &TradeUnit{execMode: execModeGroup, subUnits: []*TradeUnit{&TradeUnit{}}},
			want: false,
		},
		{
			name: "Once not-complete trade unit",
			tu: &TradeUnit{
				execMode:          execModeUnit,
				repeatMode:        repeatModeOnce,
				unitCycleFinished: false,
			},
			want: false,
		},
		{
			name: "Once complete trade unit",
			tu: &TradeUnit{
				execMode:          execModeUnit,
				repeatMode:        repeatModeOnce,
				unitCycleFinished: true,
			},
			want: true,
		},
		{
			name: "Cyclic completed-cycle trade unit",
			tu: &TradeUnit{
				execMode:          execModeUnit,
				repeatMode:        repeatModeCycle,
				unitCycleFinished: true,
			},
			want: false,
		},
		{
			name: "Cyclic non-completed-cycle trade unit",
			tu: &TradeUnit{
				execMode:          execModeUnit,
				repeatMode:        repeatModeCycle,
				unitCycleFinished: false,
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tu.IsComplete(); got != tt.want {
				t.Errorf("TradeUnit.IsComplete() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTradeUnit_AddChildren(t *testing.T) {
	type args struct {
		subUnits []*TradeUnit
	}

	tests := []struct {
		name            string
		tu              *TradeUnit
		args            args
		wantSubUnitsLen int
	}{
		{
			name:            "Add Children",
			tu:              &TradeUnit{subUnits: []*TradeUnit{}},
			args:            args{subUnits: []*TradeUnit{&TradeUnit{}, &TradeUnit{}}},
			wantSubUnitsLen: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.tu.AddChildren(tt.args.subUnits...)
			if len(got.subUnits) != tt.wantSubUnitsLen {
				t.Errorf("TradeUnit.AddChildren() Wanted %d sub-units. Got %d", tt.wantSubUnitsLen, len(got.subUnits))
			}
		})
	}
}

func TestTradeUnit_execUnit(t *testing.T) {

	type want struct {
		tradesLen int
		err       error
	}
	tests := []struct {
		name string
		tu   func() *TradeUnit
		want want
	}{
		{
			name: "Once Unit",
			tu: func() *TradeUnit {
				return &TradeUnit{
					repeatMode:   repeatModeOnce,
					condFactory:  TruthyCondFactory,
					tradeFactory: blankTradeFactory,
					execMode:     execModeUnit,
				}
			},
			want: want{
				tradesLen: 1,
			},
		},
		{
			name: "Cycle Unit",
			tu: func() *TradeUnit {
				return &TradeUnit{
					repeatMode:   repeatModeCycle,
					condFactory:  TruthyCondFactory,
					tradeFactory: blankTradeFactory,
					execMode:     execModeUnit,
				}
			},
			want: want{
				tradesLen: 1,
			},
		},
		{
			name: "Cycle Unit with Unit Builder",
			tu: func() *TradeUnit {
				return &TradeUnit{
					repeatMode:  repeatModeCycle,
					execMode:    execModeUnit,
					condFactory: TruthyCondFactory,
					unitFactory: func(ctx tradeutil.TradeContext) (*TradeUnit, error) {
						return &TradeUnit{
							repeatMode:  repeatModeOnce,
							execMode:    execModeUnit,
							condFactory: TruthyCondFactory,
							tradeFactory: func(ctx tradeutil.TradeContext) (*model.Trade, error) {
								return &model.Trade{}, nil
							},
						}, nil
					},
				}
			},
			want: want{
				tradesLen: 1,
			},
		},
		{
			name: "Unit with falsy condition",
			tu: func() *TradeUnit {
				return &TradeUnit{
					repeatMode: repeatModeCycle,
					execMode:   execModeUnit,
					cond:       func(ctx tradeutil.TradeContext) (bool, error) { return false, nil },
					unitFactory: func(ctx tradeutil.TradeContext) (*TradeUnit, error) {
						return &TradeUnit{
							repeatMode:  repeatModeOnce,
							execMode:    execModeUnit,
							condFactory: TruthyCondFactory,
							tradeFactory: func(ctx tradeutil.TradeContext) (*model.Trade, error) {
								return &model.Trade{}, nil
							},
						}, nil
					},
				}
			},
			want: want{
				tradesLen: 0,
			},
		},
		{
			name: "Unit with err cond factory",
			tu: func() *TradeUnit {
				return &TradeUnit{
					repeatMode: repeatModeCycle,
					execMode:   execModeUnit,
					condFactory: func(ctx tradeutil.TradeContext) (Condition, error) {
						return func(ctx tradeutil.TradeContext) (bool, error) { return false, nil }, errors.New("Error happened")
					},
					tradeFactory: blankTradeFactory,
				}
			},
			want: want{
				tradesLen: 0,
				err:       errors.New("Error happened"),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			built := tt.tu()
			want := tt.want
			trades, err := built.execUnit(tradeutil.TradeContext{})
			if want.err != nil && !reflect.DeepEqual(want.err, err) {
				t.Errorf("Wanted error to be %v but got %v", want.err, err)
			}
			if want.err == nil && err != nil {
				t.Errorf("Unexpected error returned from trade unit: %v", err)
			}
			if len(trades) != want.tradesLen {
				t.Errorf("Wanted %d trades; Got %d", want.tradesLen, len(trades))
			}
		})
	}
}

func TestTradeUnit_execGroup(t *testing.T) {
	type want struct {
		tradesLen int
		err       error
	}

	tests := []struct {
		name  string
		tu    func() *TradeUnit
		wants []want
	}{
		{
			name: "Exec Parallel group of once",
			tu: func() *TradeUnit {
				return &TradeUnit{
					groupRuntimeMode: Parallel,
					execMode:         execModeGroup,
					subUnits: []*TradeUnit{
						&TradeUnit{
							repeatMode:   repeatModeOnce,
							condMet:      true,
							tradeFactory: blankTradeFactory,
						},
						&TradeUnit{
							repeatMode:   repeatModeOnce,
							condMet:      true,
							tradeFactory: blankTradeFactory,
						},
						&TradeUnit{
							repeatMode:   repeatModeOnce,
							condMet:      true,
							tradeFactory: blankTradeFactory,
						},
					},
				}
			},
			wants: []want{
				want{tradesLen: 3},
				want{tradesLen: 0},
				want{tradesLen: 0},
				want{tradesLen: 0},
			},
		},
		{
			name: "Exec Sequential group of cyclic trade units",
			tu: func() *TradeUnit {
				return &TradeUnit{
					groupRuntimeMode: Sequential,
					execMode:         execModeGroup,
					subUnits: []*TradeUnit{
						&TradeUnit{
							repeatMode:   repeatModeCycle,
							condFactory:  TruthyCondFactory,
							tradeFactory: blankTradeFactory,
						},
						&TradeUnit{
							repeatMode:   repeatModeCycle,
							condFactory:  TruthyCondFactory,
							tradeFactory: blankTradeFactory,
						},
						&TradeUnit{
							repeatMode:   repeatModeCycle,
							condFactory:  TruthyCondFactory,
							tradeFactory: blankTradeFactory,
						},
					},
				}
			},
			wants: []want{
				want{tradesLen: 3},
				want{tradesLen: 3},
				want{tradesLen: 3},
				want{tradesLen: 3},
			},
		},
		{
			name: "Exec Sequential group of mixed trade units",
			tu: func() *TradeUnit {
				return &TradeUnit{
					groupRuntimeMode: Sequential,
					execMode:         execModeGroup,
					subUnits: []*TradeUnit{
						&TradeUnit{
							repeatMode:   repeatModeCycle,
							condFactory:  TruthyCondFactory,
							tradeFactory: blankTradeFactory,
						},
						&TradeUnit{
							repeatMode:   repeatModeOnce,
							condFactory:  TruthyCondFactory,
							tradeFactory: blankTradeFactory,
						},
						&TradeUnit{
							execMode: execModeGroup,
							subUnits: []*TradeUnit{
								&TradeUnit{
									repeatMode:   repeatModeOnce,
									condFactory:  TruthyCondFactory,
									tradeFactory: blankTradeFactory,
								},
								&TradeUnit{
									repeatMode:   repeatModeOnce,
									condFactory:  TruthyCondFactory,
									tradeFactory: blankTradeFactory,
								},
							},
						},
					},
				}
			},
			wants: []want{
				want{tradesLen: 4},
				want{tradesLen: 1},
				want{tradesLen: 1},
				want{tradesLen: 1},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tu := tt.tu()
			for _, want := range tt.wants {
				trades, err := tu.execGroup(tradeutil.TradeContext{})
				if want.err != nil && !reflect.DeepEqual(want.err, err) {
					t.Errorf("Wanted error to be %v, instead got %v", want.err, err)
				}
				if len(trades) != want.tradesLen {
					t.Errorf("Wanted %d number of trades to be made; Instead got %d", want.tradesLen, len(trades))
				}
			}
		})
	}
}

func TestTradeUnit_execGroupParallel(t *testing.T) {
	type want struct {
		tradesLen int
		err       error
	}

	tests := []struct {
		name  string
		tu    func() *TradeUnit
		wants []want
	}{
		{
			name: "Exec Parallel group of once",
			tu: func() *TradeUnit {
				return &TradeUnit{
					groupRuntimeMode: Parallel,
					execMode:         execModeGroup,
					subUnits: []*TradeUnit{
						&TradeUnit{
							repeatMode:   repeatModeOnce,
							condMet:      true,
							tradeFactory: blankTradeFactory,
						},
						&TradeUnit{
							repeatMode:   repeatModeOnce,
							condMet:      true,
							tradeFactory: blankTradeFactory,
						},
						&TradeUnit{
							repeatMode:   repeatModeOnce,
							condMet:      true,
							tradeFactory: blankTradeFactory,
						},
					},
				}
			},
			wants: []want{
				want{tradesLen: 3},
				want{tradesLen: 0},
				want{tradesLen: 0},
				want{tradesLen: 0},
			},
		},
		{
			name: "Exec Parallel group of cyclic trade units",
			tu: func() *TradeUnit {
				return &TradeUnit{
					groupRuntimeMode: Parallel,
					execMode:         execModeGroup,
					subUnits: []*TradeUnit{
						&TradeUnit{
							repeatMode:   repeatModeCycle,
							condFactory:  TruthyCondFactory,
							tradeFactory: blankTradeFactory,
						},
						&TradeUnit{
							repeatMode:   repeatModeCycle,
							condFactory:  TruthyCondFactory,
							tradeFactory: blankTradeFactory,
						},
						&TradeUnit{
							repeatMode:   repeatModeCycle,
							condFactory:  TruthyCondFactory,
							tradeFactory: blankTradeFactory,
						},
					},
				}
			},
			wants: []want{
				want{tradesLen: 3},
				want{tradesLen: 3},
				want{tradesLen: 3},
				want{tradesLen: 3},
			},
		},
		{
			name: "Exec Parallel group of mixed trade units",
			tu: func() *TradeUnit {
				return &TradeUnit{
					groupRuntimeMode: Parallel,
					execMode:         execModeGroup,
					subUnits: []*TradeUnit{
						&TradeUnit{
							repeatMode:   repeatModeCycle,
							condFactory:  TruthyCondFactory,
							tradeFactory: blankTradeFactory,
						},
						&TradeUnit{
							repeatMode:   repeatModeOnce,
							condFactory:  TruthyCondFactory,
							tradeFactory: blankTradeFactory,
						},
						&TradeUnit{
							execMode: execModeGroup,
							subUnits: []*TradeUnit{
								&TradeUnit{
									repeatMode:   repeatModeOnce,
									condFactory:  TruthyCondFactory,
									tradeFactory: blankTradeFactory,
								},
								&TradeUnit{
									repeatMode:   repeatModeOnce,
									condFactory:  TruthyCondFactory,
									tradeFactory: blankTradeFactory,
								},
							},
						},
					},
				}
			},
			wants: []want{
				want{tradesLen: 4},
				want{tradesLen: 1},
				want{tradesLen: 1},
				want{tradesLen: 1},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tu := tt.tu()
			for _, want := range tt.wants {
				trades, err := tu.execGroupParallel(tradeutil.TradeContext{})
				if want.err != nil && !reflect.DeepEqual(want.err, err) {
					t.Errorf("Wanted error to be %v, instead got %v", want.err, err)
				}
				if len(trades) != want.tradesLen {
					t.Errorf("Wanted %d number of trades to be made; Instead got %d", want.tradesLen, len(trades))
				}
			}
		})
	}
}

func TestTradeUnit_execGroupSequential(t *testing.T) {
	type want struct {
		tradesLen int
		err       error
	}

	tests := []struct {
		name  string
		tu    func() *TradeUnit
		wants []want
	}{
		{
			name: "Exec Sequential group of once",
			tu: func() *TradeUnit {
				return &TradeUnit{
					groupRuntimeMode: Sequential,
					execMode:         execModeGroup,
					subUnits: []*TradeUnit{
						&TradeUnit{
							repeatMode:   repeatModeOnce,
							condMet:      true,
							tradeFactory: blankTradeFactory,
						},
						&TradeUnit{
							repeatMode:   repeatModeOnce,
							condMet:      true,
							tradeFactory: blankTradeFactory,
						},
						&TradeUnit{
							repeatMode:   repeatModeOnce,
							condMet:      true,
							tradeFactory: blankTradeFactory,
						},
					},
				}
			},
			wants: []want{
				want{tradesLen: 3},
				want{tradesLen: 0},
				want{tradesLen: 0},
				want{tradesLen: 0},
			},
		},
		{
			name: "Exec Sequential group of cyclic trade units",
			tu: func() *TradeUnit {
				return &TradeUnit{
					groupRuntimeMode: Sequential,
					execMode:         execModeGroup,
					subUnits: []*TradeUnit{
						&TradeUnit{
							repeatMode:   repeatModeCycle,
							condFactory:  TruthyCondFactory,
							tradeFactory: blankTradeFactory,
						},
						&TradeUnit{
							repeatMode:   repeatModeCycle,
							condFactory:  TruthyCondFactory,
							tradeFactory: blankTradeFactory,
						},
						&TradeUnit{
							repeatMode:   repeatModeCycle,
							condFactory:  TruthyCondFactory,
							tradeFactory: blankTradeFactory,
						},
					},
				}
			},
			wants: []want{
				want{tradesLen: 3},
				want{tradesLen: 3},
				want{tradesLen: 3},
				want{tradesLen: 3},
			},
		},
		{
			name: "Exec Sequential group of mixed trade units",
			tu: func() *TradeUnit {
				return &TradeUnit{
					groupRuntimeMode: Sequential,
					execMode:         execModeGroup,
					subUnits: []*TradeUnit{
						&TradeUnit{
							repeatMode:   repeatModeCycle,
							condFactory:  TruthyCondFactory,
							tradeFactory: blankTradeFactory,
						},
						&TradeUnit{
							repeatMode:   repeatModeOnce,
							condFactory:  TruthyCondFactory,
							tradeFactory: blankTradeFactory,
						},
						&TradeUnit{
							execMode: execModeGroup,
							subUnits: []*TradeUnit{
								&TradeUnit{
									repeatMode:   repeatModeOnce,
									condFactory:  TruthyCondFactory,
									tradeFactory: blankTradeFactory,
								},
								&TradeUnit{
									repeatMode:   repeatModeOnce,
									condFactory:  TruthyCondFactory,
									tradeFactory: blankTradeFactory,
								},
							},
						},
					},
				}
			},
			wants: []want{
				want{tradesLen: 4},
				want{tradesLen: 1},
				want{tradesLen: 1},
				want{tradesLen: 1},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tu := tt.tu()
			for _, want := range tt.wants {
				trades, err := tu.execGroupSequential(tradeutil.TradeContext{})
				if want.err != nil && !reflect.DeepEqual(want.err, err) {
					t.Errorf("Wanted error to be %v, instead got %v", want.err, err)
				}
				if len(trades) != want.tradesLen {
					t.Errorf("Wanted %d number of trades to be made; Instead got %d", want.tradesLen, len(trades))
				}
			}
		})
	}
}

func TestTradeUnit_cleanupUnitCycle(t *testing.T) {
	type want struct {
		condMet           bool
		nilCond           bool
		nilBuiltUnit      bool
		unitCycleFinished bool
	}

	tests := []struct {
		name string
		tu   func() *TradeUnit
		want want
	}{
		{
			name: "Nothing to do",
			tu: func() *TradeUnit {
				return &TradeUnit{
					repeatMode:        repeatModeOnce,
					unitCycleFinished: true,
					builtUnit:         &TradeUnit{},
					condMet:           true,
					cond:              func(ctx tradeutil.TradeContext) (bool, error) { return true, nil },
				}
			},
			want: want{
				condMet:           true,
				nilCond:           false,
				nilBuiltUnit:      false,
				unitCycleFinished: true,
			},
		},
		{
			name: "clean up state",
			tu: func() *TradeUnit {
				return &TradeUnit{
					repeatMode:        repeatModeCycle,
					unitCycleFinished: true,
					builtUnit:         &TradeUnit{},
					condMet:           true,
					condFactory:       TruthyCondFactory,
					cond:              func(ctx tradeutil.TradeContext) (bool, error) { return true, nil },
				}
			},
			want: want{
				condMet:           false,
				nilCond:           true,
				nilBuiltUnit:      true,
				unitCycleFinished: false,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.tu().cleanupUnitCycle()
			if got.condMet != tt.want.condMet {
				t.Errorf("TradeUnit.cleanupUnitCycle() didn't set the correct condMet value; Got %t, want %t", got.condMet, tt.want.condMet)
			}
			if tt.want.nilCond && got.cond != nil {
				t.Error("TradeUnit.cleanupUnitCycle() should've set the condition to nil but got non-nil condition")
			}
			if !tt.want.nilCond && got.cond == nil {
				t.Error("TradeUnit.cleanupUnitCycle() should've had non-nil condition but instead got nil")
			}

			if tt.want.nilBuiltUnit && got.builtUnit != nil {
				t.Error("TradeUnit.cleanupUnitCycle() should've cleaned up the built unit but instead got non-nil built unit")
			}
			if !tt.want.nilBuiltUnit && got.builtUnit == nil {
				t.Error("TradeUnit.cleanupUnitCycle() should've had non-nil built unit but instead got nil")
			}
			if tt.want.unitCycleFinished != got.unitCycleFinished {
				t.Errorf("TradeUnit.cleanupUnitCycle() unitCycleFinished must be %t, got %t", tt.want.unitCycleFinished, got.unitCycleFinished)
			}
		})
	}
}

func TestTradeUnit_cleanupGroupCycle(t *testing.T) {

	tests := []struct {
		name              string
		tu                func() *TradeUnit
		remainingSubUnits int
	}{
		{
			name: "Nothing removable",
			tu: func() *TradeUnit {
				return &TradeUnit{
					subUnits: []*TradeUnit{
						&TradeUnit{unitCycleFinished: true, repeatMode: repeatModeCycle},
						&TradeUnit{unitCycleFinished: false, repeatMode: repeatModeOnce},
						&TradeUnit{execMode: execModeGroup, subUnits: []*TradeUnit{&TradeUnit{}}},
					},
				}
			},
			remainingSubUnits: 3,
		},
		{
			name: "All removable",
			tu: func() *TradeUnit {
				return &TradeUnit{
					subUnits: []*TradeUnit{
						&TradeUnit{unitCycleFinished: true, repeatMode: repeatModeOnce},
						&TradeUnit{execMode: execModeGroup},
					},
				}
			},
			remainingSubUnits: 0,
		},
		{
			name: "Some removable",
			tu: func() *TradeUnit {
				return &TradeUnit{
					subUnits: []*TradeUnit{
						&TradeUnit{unitCycleFinished: true, repeatMode: repeatModeOnce},
						&TradeUnit{unitCycleFinished: true, repeatMode: repeatModeCycle},
						&TradeUnit{execMode: execModeGroup, subUnits: []*TradeUnit{&TradeUnit{}}},
						&TradeUnit{execMode: execModeGroup},
					},
				}
			},
			remainingSubUnits: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.tu().cleanupGroupCycle()
			if len(got.subUnits) != tt.remainingSubUnits {
				t.Errorf("TradeUnit.cleanupGroupCycle wanted %d remaining sub-units; instead got %d", tt.remainingSubUnits, len(got.subUnits))
			}
		})
	}
}

func TestTradeUnit_resolveCondition(t *testing.T) {

	type want struct {
		condMet   bool
		isCondNil bool
		wantErr   bool
		errResult string
	}
	tests := []struct {
		name  string
		tu    func() *TradeUnit
		wants []want
	}{
		{
			name: "Truthy Cond Factory",
			tu: func() *TradeUnit {
				return &TradeUnit{
					condFactory: TruthyCondFactory,
				}
			},
			wants: []want{
				want{condMet: true, isCondNil: false, wantErr: false},
				want{condMet: true, isCondNil: false, wantErr: false},
				want{condMet: true, isCondNil: false, wantErr: false},
				want{condMet: true, isCondNil: false, wantErr: false},
			},
		},
		{
			name: "Falsy Cond Factory",
			tu: func() *TradeUnit {
				return &TradeUnit{
					condFactory: FactoryFromCondition(func(ctx tradeutil.TradeContext) (bool, error) {
						return false, nil
					}),
				}
			},
			wants: []want{
				want{condMet: false, isCondNil: false, wantErr: false},
				want{condMet: false, isCondNil: false, wantErr: false},
				want{condMet: false, isCondNil: false, wantErr: false},
				want{condMet: false, isCondNil: false, wantErr: false},
			},
		},
		{
			name: "Cond Factory is called once only",
			tu: func() *TradeUnit {
				cnt := 1
				return &TradeUnit{
					condFactory: func(ctx tradeutil.TradeContext) (Condition, error) {
						cnt++
						if cnt%2 == 0 {
							return func(ctx tradeutil.TradeContext) (bool, error) {
								return true, nil
							}, nil
						}

						return func(ctx tradeutil.TradeContext) (bool, error) {
							return false, nil
						}, nil
					},
				}
			},
			wants: []want{
				want{condMet: true, isCondNil: false, wantErr: false},
				want{condMet: true, isCondNil: false, wantErr: false},
				want{condMet: true, isCondNil: false, wantErr: false},
				want{condMet: true, isCondNil: false, wantErr: false},
			},
		},
		{
			name: "Cond Factory returns error",
			tu: func() *TradeUnit {
				return &TradeUnit{
					condFactory: func(ctx tradeutil.TradeContext) (Condition, error) {
						return func(ctx tradeutil.TradeContext) (bool, error) { return true, nil }, errors.New("Error returned")
					},
				}
			},
			wants: []want{
				want{condMet: false, isCondNil: true, wantErr: true, errResult: "Error returned"},
				want{condMet: false, isCondNil: true, wantErr: true, errResult: "Error returned"},
				want{condMet: false, isCondNil: true, wantErr: true, errResult: "Error returned"},
				want{condMet: false, isCondNil: true, wantErr: true, errResult: "Error returned"},
			},
		},
		{
			name: "Cond truthy",
			tu: func() *TradeUnit {
				return &TradeUnit{
					cond: func(ctx tradeutil.TradeContext) (bool, error) {
						return true, nil
					},
				}
			},
			wants: []want{
				want{condMet: true, isCondNil: false, wantErr: false},
				want{condMet: true, isCondNil: false, wantErr: false},
				want{condMet: true, isCondNil: false, wantErr: false},
				want{condMet: true, isCondNil: false, wantErr: false},
			},
		},
		{
			name: "Cond intermittent",
			tu: func() *TradeUnit {
				return &TradeUnit{
					condFactory: func(ctx tradeutil.TradeContext) (Condition, error) {
						cnt := 0
						return func(ctx tradeutil.TradeContext) (bool, error) {
							cnt++
							return cnt%2 == 0, nil
						}, nil
					},
				}
			},
			wants: []want{
				want{condMet: false, isCondNil: false, wantErr: false},
				want{condMet: true, isCondNil: false, wantErr: false},
				want{condMet: false, isCondNil: false, wantErr: false},
				want{condMet: true, isCondNil: false, wantErr: false},
			},
		},
		{
			name: "Cond error",
			tu: func() *TradeUnit {
				return &TradeUnit{
					condFactory: func(ctx tradeutil.TradeContext) (Condition, error) {
						cnt := 1
						return func(ctx tradeutil.TradeContext) (bool, error) {
							cnt++
							if cnt%2 == 0 {
								return true, nil
							}
							return false, errors.New("Error happened")
						}, nil
					},
				}
			},
			wants: []want{
				want{condMet: true, isCondNil: false, wantErr: false},
				want{condMet: false, isCondNil: false, wantErr: true, errResult: "Error happened"},
				want{condMet: true, isCondNil: false, wantErr: false},
				want{condMet: false, isCondNil: false, wantErr: true, errResult: "Error happened"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tu := tt.tu()
			for _, want := range tt.wants {
				err := tu.resolveCondition(tradeutil.TradeContext{})
				if (err != nil) != want.wantErr {
					t.Errorf("TradeUnit.resolveCondition() error = %v, wantErr %v", err, want.wantErr)
				}
				if err != nil && err.Error() != want.errResult {
					t.Errorf("TradeUnit.resolveCondition() gave different error value; Wanted %s, Got %s", want.errResult, err.Error())
				}
				if tu.condMet != want.condMet {
					t.Errorf("TradeUnit.resolveCondition() wanted condition met to be %t, but got %t", want.condMet, tu.condMet)
				}
				if want.isCondNil && tu.cond != nil {
					t.Error("TradeUnit.resolveCondition() wanted nil condition, bot instead condition is non-nil")
				}
				if !want.isCondNil && tu.cond == nil {
					t.Error("TradeUnit.resolveCondition() not wanted nil condition, but it's nil")
				}
			}
		})
	}
}
