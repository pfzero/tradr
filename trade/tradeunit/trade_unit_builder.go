package tradeunit

// Builder is a helper structure
// for creating and orchestrating trade units
type Builder struct {
	tu *TradeUnit
}

// NewBuilder returns the a new Trade Unit Builder
func NewBuilder() *Builder {
	return &Builder{tu: &TradeUnit{
		execMode: execModeUnit,
	}}
}

// All creates a new trade unit that runs all the given units; When all of them
// finish, the trade unit finishes as well
func All(concurrencyMode uint8, units ...*TradeUnit) *TradeUnit {
	tu := &TradeUnit{
		execMode:         execModeGroup,
		groupRuntimeMode: concurrencyMode,
	}
	tu.AddChildren(units...)
	return tu
}

// SetConditionFactory sets the given condition factory;
func (b *Builder) SetConditionFactory(cf ConditionFactory) *Builder {
	b.tu.condFactory = cf
	return b
}

// ExecOnce sets the trade unit's condition and
// flow mode
func (b *Builder) ExecOnce() *Builder {
	b.tu.repeatMode = repeatModeOnce
	return b
}

// ExecCycle is similar to Once but it sets the flow mode
// to cycle
func (b *Builder) ExecCycle() *Builder {
	b.tu.repeatMode = repeatModeCycle
	return b
}

// RegisterUnitFactory registers a new UnitFactory
func (b *Builder) RegisterUnitFactory(unitFactory UnitFactory) *TradeUnit {
	b.tu.unitFactory = unitFactory
	return b.tu
}

// RegisterTradeFactory registers a new trade maker; Please note that either trade factory
// or unit factory can be functional. Trade factory has higher precedence than unit factory and
// thus, if non-nil, the unit factory will be ignored
func (b *Builder) RegisterTradeFactory(tradeFactory TradeFactory) *TradeUnit {
	b.tu.tradeFactory = tradeFactory
	return b.tu
}
