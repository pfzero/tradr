package tradeunit

import (
	"testing"

	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/util/tradeutil"
)

func truthyCond(ctx tradeutil.TradeContext) (bool, error)              { return true, nil }
func nilUnitFactory(ctx tradeutil.TradeContext) (*TradeUnit, error)    { return nil, nil }
func nilTradeFactory(ctx tradeutil.TradeContext) (*model.Trade, error) { return nil, nil }

func TestNewBuilder(t *testing.T) {
	tests := []struct {
		name string
	}{
		{
			name: "Simple Builder",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewBuilder()
			if got == nil {
				t.Error("NewBuilder() should have returned a &Builder{} but instead got nil")
			}
			if got.tu == nil {
				t.Error("NewBuilder() should have trade unit initialized but got nil")
			}
		})
	}
}

func TestAll(t *testing.T) {
	type args struct {
		groupRuntimeMode uint8
		units            []*TradeUnit
	}
	type want struct {
		length           int
		groupRuntimeMode uint8
	}
	tests := []struct {
		name string
		args args
		want want
	}{
		// TODO: Add test cases.
		{
			name: "All Parallel",
			args: args{groupRuntimeMode: Parallel, units: []*TradeUnit{&TradeUnit{}, &TradeUnit{}}},
			want: want{length: 2, groupRuntimeMode: Parallel},
		},
		{
			name: "All Sequential",
			args: args{groupRuntimeMode: Sequential, units: []*TradeUnit{&TradeUnit{}, &TradeUnit{}, &TradeUnit{}}},
			want: want{length: 3, groupRuntimeMode: Sequential},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := All(tt.args.groupRuntimeMode, tt.args.units...)

			if len(got.subUnits) != tt.want.length {
				t.Errorf("All() made %d sub-units; Wanted %d", len(got.subUnits), tt.want.length)
			}
			if got.groupRuntimeMode != tt.want.groupRuntimeMode {
				t.Errorf("All() made trade unit with concurrency mode %d; Wanted %d", got.groupRuntimeMode, tt.want.groupRuntimeMode)
			}
		})
	}
}

func TestBuilder_SetConditionFactory(t *testing.T) {
	type args struct {
		cf ConditionFactory
	}
	type want struct {
		wantNilCondFactory bool
		condResult         bool
	}

	tests := []struct {
		name string
		b    *Builder
		args args
		want want
	}{
		{
			name: "Truthy Cond",
			b:    NewBuilder(),
			args: args{cf: TruthyCondFactory},
			want: want{wantNilCondFactory: false, condResult: true},
		},
		{
			name: "Falsy Cond",
			b:    NewBuilder(),
			args: args{cf: func(ctx tradeutil.TradeContext) (Condition, error) {
				return func(ctx tradeutil.TradeContext) (bool, error) { return false, nil }, nil
			}},
			want: want{wantNilCondFactory: false, condResult: false},
		},
		{
			name: "Nil Cond",
			b:    NewBuilder(),
			args: args{cf: nil},
			want: want{wantNilCondFactory: true, condResult: false},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.b.SetConditionFactory(tt.args.cf)
			if tt.want.wantNilCondFactory {
				if got.tu.condFactory != nil {
					t.Errorf("Builder.SetConditionFactory() Wanted nil condition factory but got non-nil condition")
				}
				return
			}

			if got.tu.condFactory == nil {
				t.Errorf("Builder.SetConditionFactory() Wanted non-nil condition factory but got nil")
			}

			cond, err := got.tu.condFactory(tradeutil.TradeContext{})
			if err != nil {
				t.Errorf("Builder.SetConditionFactory() not expected condition factory to return error")
			}
			flag, err := cond(tradeutil.TradeContext{})

			if tt.want.condResult != flag {
				t.Errorf(
					`Builder.SetConditionFactory() Wanted the result of the condition returned by condition factory to be %t but got %t`,
					tt.want.condResult,
					flag,
				)
			}
		})
	}
}

func TestBuilder_ExecOnce(t *testing.T) {

	type want struct {
		repeatMode uint8
	}

	tests := []struct {
		name string
		b    *Builder
		want want
	}{
		{
			name: "Constructor ExecOnce",
			b:    NewBuilder(),
			want: want{repeatMode: repeatModeOnce},
		},
		{
			name: "Barebone ExecOnce",
			b:    &Builder{tu: &TradeUnit{}},
			want: want{repeatMode: repeatModeOnce},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.b.ExecOnce()
			if got.tu.repeatMode != tt.want.repeatMode {
				t.Errorf("Builder.ExecOnce() should've assigned repeat mode to 'once'. Got %d but wanted %d", got.tu.repeatMode, tt.want.repeatMode)
			}
		})
	}
}

func TestBuilder_ExecCycle(t *testing.T) {
	type want struct {
		repeatMode uint8
	}

	tests := []struct {
		name string
		b    *Builder
		want want
	}{
		{
			name: "Constructor ExecCycle",
			b:    NewBuilder(),
			want: want{repeatMode: repeatModeCycle},
		},
		{
			name: "Barebone ExecCycle",
			b:    &Builder{tu: &TradeUnit{}},
			want: want{repeatMode: repeatModeCycle},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.b.ExecCycle()
			if got.tu.repeatMode != tt.want.repeatMode {
				t.Errorf("Builder.ExecCycle() should've assigned repeat mode to 'cycle'. Got %d but wanted %d", got.tu.repeatMode, tt.want.repeatMode)
			}
		})
	}
}

func TestBuilder_RegisterUnitFactory(t *testing.T) {
	type args struct {
		unitFactory UnitFactory
	}

	tests := []struct {
		name    string
		b       *Builder
		args    args
		wantNil bool
	}{
		{
			name:    "Constructor RegisterUnitFactory",
			b:       NewBuilder(),
			args:    args{unitFactory: nil},
			wantNil: true,
		},
		{
			name:    "Constructor RegisterUnitFactory",
			b:       NewBuilder(),
			args:    args{unitFactory: nilUnitFactory},
			wantNil: false,
		},
		{
			name:    "Barebone RegisterUnitFactory",
			b:       &Builder{tu: &TradeUnit{}},
			args:    args{unitFactory: nilUnitFactory},
			wantNil: false,
		},
		{
			name:    "Barebone RegisterUnitFactory",
			b:       &Builder{tu: &TradeUnit{}},
			args:    args{unitFactory: nil},
			wantNil: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.b.RegisterUnitFactory(tt.args.unitFactory)
			if tt.wantNil && got.unitFactory != nil {
				t.Error("unit factory should've been nil")
			}
			if !tt.wantNil && got.unitFactory == nil {
				t.Error("Unit factory shouldn't have been nil")
			}
		})
	}
}

func TestBuilder_RegisterTradeFactory(t *testing.T) {
	type args struct {
		tradeFactory TradeFactory
	}
	tests := []struct {
		name    string
		b       *Builder
		args    args
		wantNil bool
	}{
		{
			name:    "Constructor RegisterTradeFactory",
			b:       NewBuilder(),
			args:    args{tradeFactory: nil},
			wantNil: true,
		},
		{
			name:    "Constructor RegisterTradeFactory",
			b:       NewBuilder(),
			args:    args{tradeFactory: blankTradeFactory},
			wantNil: false,
		},
		{
			name:    "Barebone RegisterTradeFactory",
			b:       &Builder{tu: &TradeUnit{}},
			args:    args{tradeFactory: blankTradeFactory},
			wantNil: false,
		},
		{
			name:    "Barebone RegisterTradeFactory",
			b:       &Builder{tu: &TradeUnit{}},
			args:    args{tradeFactory: nil},
			wantNil: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.b.RegisterTradeFactory(tt.args.tradeFactory)
			if tt.wantNil && got.tradeFactory != nil {
				t.Error("trade factory should have been nil")
			}
			if !tt.wantNil && got.tradeFactory == nil {
				t.Error("trade factory shouldn't have been nil")
			}
		})
	}
}
