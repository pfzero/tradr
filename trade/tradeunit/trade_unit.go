package tradeunit

import (
	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/util/tradeutil"
)

const (
	// repeatModeOnce makes the trade unit to
	// execute the whole cycle only once
	repeatModeOnce uint8 = iota
	// repeatModeCycle makes the trade unit to
	// repeat the unit everytime it finishes
	repeatModeCycle
)

const (
	execModeUnit uint8 = iota
	execModeGroup
)

const (
	// Sequential sets the execution mode to
	// sequential - when executing the trade unit
	// as a group of other trade units
	Sequential uint8 = iota
	// Parallel sets the execution mode of the group to parallel
	Parallel
)

// Condition is a function that resolves to a boolean value; Used
// by trade unit to do something after the condition resolved
type Condition func(ctx tradeutil.TradeContext) (bool, error)

// ConditionFactory is a factory function that returns a condition;
// ConditionFactory is evaluated only once at the beginning of each cycle
type ConditionFactory func(ctx tradeutil.TradeContext) (Condition, error)

// UnitFactory is a function that is used to construct a new trade unit;
// UnitFactory is evaluated once after the condition is met
type UnitFactory func(ctx tradeutil.TradeContext) (*TradeUnit, error)

// TradeFactory is used to construct a trade object;
type TradeFactory func(ctx tradeutil.TradeContext) (*model.Trade, error)

// FactoryFromCondition is a helper function that wraps a condition into a
// condition factory
func FactoryFromCondition(cond Condition) ConditionFactory {
	return func(ctx tradeutil.TradeContext) (Condition, error) {
		return cond, nil
	}
}

// TruthyCondFactory is a helper condition factory used to always return true
func TruthyCondFactory(ctx tradeutil.TradeContext) (Condition, error) {
	return func(ctx tradeutil.TradeContext) (bool, error) { return true, nil }, nil
}

// TradeUnit is a data-structure
// that helps creating trading units and
// grouping them
type TradeUnit struct {
	// repeatMode sets the cycling mode of the
	// trade unit; Once or Cycle;
	repeatMode uint8
	// execMode sets the execution mode of the
	// trade unit; Unit or Group;
	execMode uint8
	// groupRuntimeMode sets the execution of the
	// group trade unit to either Parallel or Sequential
	groupRuntimeMode uint8

	// CondFactory is used to build a new condition
	condFactory ConditionFactory
	// cond is used to be evaluated at every execution
	// of the trade unit; When the condition is met,
	// either a trade gets created, or, a new trade sub-unit
	// is created
	cond    Condition
	condMet bool

	// unitFactory is used to create a new trade sub-unit; This
	// trade sub-unit will be evaluated after the current trade unit
	// will be completed (the condition will be met);
	unitFactory UnitFactory
	builtUnit   *TradeUnit

	// tradeFactory is a function used to create a new trade; This is the
	// ultimate goal of every trade unit - to create trades
	tradeFactory TradeFactory

	// unitCycleFinished specifies wether the current cycle
	// has finished
	unitCycleFinished bool

	// subUnits holds the list of sub-units that needs to be evaluated when
	// this trade unit runs in group mode;
	subUnits []*TradeUnit
}

// Exec executes the trade unit
func (tu *TradeUnit) Exec(ctx tradeutil.TradeContext) ([]model.Trade, error) {
	trades := []model.Trade{}
	if tu.execMode == execModeUnit {
		unitTrades, err := tu.execUnit(ctx)
		if tu.unitCycleFinished {
			tu.cleanupUnitCycle()
		}
		if err != nil {
			return nil, err
		}
		trades = append(trades, unitTrades...)
	}

	if tu.execMode == execModeGroup {
		groupTrades, err := tu.execGroup(ctx)
		tu.cleanupGroupCycle()
		if err != nil {
			return nil, err
		}
		trades = append(trades, groupTrades...)
	}

	return trades, nil
}

// IsComplete checks wether the current trade unit completed and has
// nothing else to do;
func (tu *TradeUnit) IsComplete() bool {
	if tu.execMode == execModeGroup {
		if len(tu.subUnits) == 0 {
			return true
		}
		return false
	}

	if tu.repeatMode == repeatModeOnce {
		return tu.unitCycleFinished
	}

	return false
}

// AddChildren adds the given children to this trade unit; The added sub units
// are relevant only when this trade units has its execution mode set to group;
func (tu *TradeUnit) AddChildren(subUnits ...*TradeUnit) *TradeUnit {
	tu.subUnits = append(tu.subUnits, subUnits...)
	return tu
}

func (tu *TradeUnit) execUnit(ctx tradeutil.TradeContext) ([]model.Trade, error) {

	// check if the cycle already ended and we had to execute
	// it only once
	if tu.repeatMode == repeatModeOnce && tu.unitCycleFinished {
		return nil, nil
	}

	// check for condition result
	if !tu.condMet {
		err := tu.resolveCondition(ctx)
		if err != nil {
			return nil, err
		}
	}

	if !tu.condMet {
		return nil, nil
	}

	// check for sub-unit and sub-unit factory
	if tu.builtUnit == nil && tu.unitFactory != nil {
		builtUnit, err := tu.unitFactory(ctx)
		if err != nil {
			return nil, err
		}
		tu.builtUnit = builtUnit
	}

	if tu.builtUnit != nil {
		trades, err := tu.builtUnit.Exec(ctx)
		if tu.builtUnit.IsComplete() {
			tu.unitCycleFinished = true
		}
		if err != nil {
			return nil, err
		}
		return trades, nil
	}

	trade, err := tu.tradeFactory(ctx)
	tu.unitCycleFinished = true

	if err != nil {
		return nil, err
	}
	if trade == nil {
		return nil, nil
	}

	return []model.Trade{*trade}, nil
}

func (tu *TradeUnit) execGroup(ctx tradeutil.TradeContext) ([]model.Trade, error) {
	if tu.groupRuntimeMode == Parallel {
		return tu.execGroupParallel(ctx)
	}
	return tu.execGroupSequential(ctx)
}

func (tu *TradeUnit) execGroupParallel(ctx tradeutil.TradeContext) ([]model.Trade, error) {
	cnt := len(tu.subUnits)

	type resp struct {
		trades []model.Trade
		err    error
	}

	ch := make(chan *resp, cnt)

	for i := range tu.subUnits {
		go func(idx int) {
			subUnit := tu.subUnits[idx]
			trades, err := subUnit.Exec(ctx)
			resp := &resp{
				trades: trades,
				err:    err,
			}
			ch <- resp
		}(i)
	}

	trades := []model.Trade{}
	errors := []error{}

	for i := 0; i < cnt; i++ {
		resp := <-ch
		if resp.err != nil {
			errors = append(errors, resp.err)
			continue
		}
		if resp.trades != nil && len(resp.trades) > 0 {
			trades = append(trades, resp.trades...)
		}
	}

	if len(errors) > 0 {
		return nil, errors[len(errors)-1]
	}

	if len(trades) > 0 {
		return trades, nil
	}

	return nil, nil
}

func (tu *TradeUnit) execGroupSequential(ctx tradeutil.TradeContext) ([]model.Trade, error) {
	retTrades := []model.Trade{}
	retErrs := []error{}
	for _, unit := range tu.subUnits {
		trades, err := unit.Exec(ctx)
		if err != nil {
			retErrs = append(retErrs, err)
			continue
		}
		if trades != nil && len(trades) > 0 {
			retTrades = append(retTrades, trades...)
		}
	}
	if len(retErrs) > 0 {
		return nil, retErrs[len(retErrs)-1]
	}

	if len(retTrades) > 0 {
		return retTrades, nil
	}

	return nil, nil
}

func (tu *TradeUnit) cleanupUnitCycle() *TradeUnit {
	// we finished
	if tu.repeatMode == repeatModeOnce {
		return tu
	}

	if tu.condFactory != nil {
		tu.cond = nil
	}
	tu.condMet = false
	tu.builtUnit = nil
	tu.unitCycleFinished = false

	return tu
}

func (tu *TradeUnit) cleanupGroupCycle() *TradeUnit {
	i := 0
	l := len(tu.subUnits)
	for i < l {
		unit := tu.subUnits[i]
		if !unit.IsComplete() {
			i++
			continue
		}
		tu.subUnits = append(tu.subUnits[:i], tu.subUnits[i+1:]...)
		l = len(tu.subUnits)
	}

	return tu
}

func (tu *TradeUnit) resolveCondition(ctx tradeutil.TradeContext) error {
	if tu.cond == nil && tu.condFactory != nil {
		cond, err := tu.condFactory(ctx)
		if err != nil {
			return err
		}
		tu.cond = cond
	}

	f, e := tu.cond(ctx)
	if e != nil {
		tu.condMet = false
		return e
	}
	tu.condMet = f
	return nil
}
