package dur

import (
	"errors"
	"time"

	"github.com/hako/durafmt"
)

const (
	// Day = 1 day time duration
	Day = 24 * time.Hour
	// Week = 7 days
	Week = 7 * Day
	// Month = 30 days (don't measure exact month duration)
	Month = 30 * Day
	// Year = 12 months
	Year = 12 * Month
)

var errLeadingInt = errors.New("bigDuration: bad [0-9]*") // never printed

// leadingInt consumes the leading [0-9]* from s.
func leadingInt(s string) (x int64, rem string, err error) {
	i := 0
	for ; i < len(s); i++ {
		c := s[i]
		if c < '0' || c > '9' {
			break
		}
		if x > (1<<63-1)/10 {
			// overflow
			return 0, "", errLeadingInt
		}
		x = x*10 + int64(c) - '0'
		if x < 0 {
			// overflow
			return 0, "", errLeadingInt
		}
	}
	return x, s[i:], nil
}

// leadingFraction consumes the leading [0-9]* from s.
// It is used only for fractions, so does not return an error on overflow,
// it just stops accumulating precision.
func leadingFraction(s string) (x int64, scale float64, rem string) {
	i := 0
	scale = 1
	overflow := false
	for ; i < len(s); i++ {
		c := s[i]
		if c < '0' || c > '9' {
			break
		}
		if overflow {
			continue
		}
		if x > (1<<63-1)/10 {
			// It's possible for overflow to give a positive number, so take care.
			overflow = true
			continue
		}
		y := x*10 + int64(c) - '0'
		if y < 0 {
			overflow = true
			continue
		}
		x = y
		scale *= 10
	}
	return x, scale, s[i:]
}

func splitDuration(originalDuration string) (bigDurStr, durStr string) {
	lastBigDurIdx := -1
	for i := 0; i < len(originalDuration); i++ {
		w := string(originalDuration[i])
		_, isBigDurSymbol := bigUnitMap[w]
		_, isDurSymbol := unitMap[w]
		if isBigDurSymbol {
			lastBigDurIdx = i
		}
		if isDurSymbol {
			break
		}
	}

	return originalDuration[:lastBigDurIdx+1], originalDuration[lastBigDurIdx+1:]
}

var unitMap = map[string]int64{
	"ns": int64(time.Nanosecond),
	"us": int64(time.Microsecond),
	"µs": int64(time.Microsecond), // U+00B5 = micro symbol
	"μs": int64(time.Microsecond), // U+03BC = Greek letter mu
	"ms": int64(time.Millisecond),
	"s":  int64(time.Second),
	"m":  int64(time.Minute),
	"h":  int64(time.Hour),
}

var bigUnitMap = map[string]int64{
	"D": int64(Day),
	"W": int64(Week),
	"M": int64(Month),
	"Y": int64(Year),
}

// ParseBigDuration parses a big duration of the form:
// "1Y5M1W3D" plus the string supported by time.Duration
func ParseBigDuration(bigDurStr string) (time.Duration, error) {

	// [-+]?([0-9]*(\.[0-9]*)?[a-z]+)+
	orig := bigDurStr
	var d int64
	neg := false

	// Consume [-+]?
	if bigDurStr != "" {
		c := bigDurStr[0]
		if c == '-' || c == '+' {
			neg = c == '-'
			bigDurStr = bigDurStr[1:]
		}
	}

	// Special case: if all that is left is "0", this is zero.
	if bigDurStr == "0" {
		return 0, nil
	}
	if bigDurStr == "" {
		return 0, errors.New("bigDuration: invalid duration " + orig)
	}

	s, dur := splitDuration(bigDurStr)

	if s == "" {
		return time.ParseDuration(dur)
	}

	for s != "" {
		var (
			v, f  int64       // integers before, after decimal point
			scale float64 = 1 // value = v + f/scale
		)

		var err error

		// The next character must be [0-9.]
		if !(s[0] == '.' || '0' <= s[0] && s[0] <= '9') {
			return 0, errors.New("bigDuration: invalid duration " + orig)
		}

		// Consume [0-9]*
		pl := len(s)
		v, s, err = leadingInt(s)
		if err != nil {
			return 0, errors.New("bigDuration: invalid duration " + orig)
		}
		pre := pl != len(s) // whether we consumed anything before a period

		// Consume (\.[0-9]*)?
		post := false
		if s != "" && s[0] == '.' {
			s = s[1:]
			pl := len(s)
			f, scale, s = leadingFraction(s)
			post = pl != len(s)
		}

		if !pre && !post {
			// no digits (e.g. ".s" or "-.s")
			return 0, errors.New("bigDuration: invalid duration " + orig)
		}

		// Consume unit.
		i := 0
		for ; i < len(s); i++ {
			c := s[i]
			if c == '.' || '0' <= c && c <= '9' {
				break
			}
		}
		if i == 0 {
			return 0, errors.New("bigDuration: missing unit in duration " + orig)
		}
		u := s[:i]
		s = s[i:]
		unit, ok := bigUnitMap[u]
		if !ok {
			return 0, errors.New("bigDuration: unknown unit " + u + " in duration " + orig)
		}
		if v > (1<<63-1)/unit {
			// overflow
			return 0, errors.New("bigDuration: invalid duration " + orig)
		}
		v *= unit
		if f > 0 {
			// float64 is needed to be nanosecond accurate for fractions of hours.
			// v >= 0 && (f*unit/scale) <= 3.6e+12 (ns/h, h is the largest unit)
			v += int64(float64(f) * (float64(unit) / scale))
			if v < 0 {
				// overflow
				return 0, errors.New("bigDuration: invalid duration " + orig)
			}
		}
		d += v
		if d < 0 {
			// overflow
			return 0, errors.New("bigDuration: invalid duration " + orig)
		}
	}

	var normalDur time.Duration
	var err error
	if dur != "" {
		normalDur, err = time.ParseDuration(dur)
		if err != nil {
			return 0, err
		}
	}

	if neg {
		d = -d
		normalDur = -normalDur
	}

	return time.Duration(d) + normalDur, nil
}

// PrettyString takes a single duration and returns
// the duration as a pretty-formated string
func PrettyString(d time.Duration) string {
	return durafmt.Parse(d).String()
}
