package tradeutil

import (
	"math"
	"time"

	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"
	"github.com/hako/durafmt"
)

// TrendSummary encodes the information needed to provide
// a short summary on trend
type TrendSummary struct {
	// Noise defined for trend formation
	Noise cryptomarketutil.Variation

	// latest 3 relevant points on the trend
	// lines
	// PrevCandlestickFrom represents the HEAD-1
	// candlestick's PriceFrom value
	PrevCandlestickFrom float64
	// HeadCandlestickFrom represents the HEAD candlestick's
	// PriceFrom value; Please note that this value is the same
	// as HEAD-1 PriceTo value;
	HeadCandlestickFrom float64
	// HeadCandlestickTo represents the HEAD candlestick's
	// PriceTo value;
	HeadCandlestickTo float64

	// CurPrice represents the latest price of the market; If CurPrice
	// is different from HeadCandlestickTo than this means that CurPrice
	// is within the noisy area of the trend formation;
	CurPrice float64
}

// PrevCandlestickVariation returns the previous candlestick variation
func (ts *TrendSummary) PrevCandlestickVariation() float64 {
	return cryptomarketutil.GetPriceVariation(ts.PrevCandlestickFrom, ts.HeadCandlestickFrom)
}

// HeadCandlestickVariation returns the variation of the head candlestick
func (ts *TrendSummary) HeadCandlestickVariation() float64 {
	return cryptomarketutil.GetPriceVariation(ts.HeadCandlestickFrom, ts.HeadCandlestickTo)
}

// CurrentVariation returns the current variation
func (ts *TrendSummary) CurrentVariation() float64 {
	return cryptomarketutil.GetPriceVariation(ts.HeadCandlestickTo, ts.CurPrice)
}

// MarketEvolutionReport represents the data structure
// that encodes the "change" of the market between 2 points
// in time;
type MarketEvolutionReport struct {
	FSym        string
	TSym        string
	Exchange    string
	Noise       *cryptomarketutil.Variation
	FromContext *TradeContext
	ToContext   *TradeContext
}

// NewMarketEvolutionReport creates a new
// MarketEvolutionReport based on the given parameters
func NewMarketEvolutionReport(
	cListID *CandlestickListID,
	fromCtx, toCtx *TradeContext,
) *MarketEvolutionReport {
	return &MarketEvolutionReport{
		FSym:        cListID.FSym,
		TSym:        cListID.TSym,
		Exchange:    cListID.Exchange,
		Noise:       &cListID.Variation,
		FromContext: fromCtx,
		ToContext:   toCtx,
	}
}

// GetPriceFrom returns the market evolution `from` price
func (mer *MarketEvolutionReport) GetPriceFrom() float64 {
	fPrice, err := mer.FromContext.GetPrice(mer.FSym, mer.TSym, mer.Exchange)
	if err != nil {
		return math.NaN()
	}
	return fPrice.GetPrice()
}

// GetPriceTo returns the market evolution `to` price
func (mer *MarketEvolutionReport) GetPriceTo() float64 {
	tPrice, err := mer.ToContext.GetPrice(mer.FSym, mer.TSym, mer.Exchange)
	if err != nil {
		return math.NaN()
	}
	return tPrice.GetPrice()
}

// PriceVariation returns the price variation between the 2 market
// snapshots
func (mer *MarketEvolutionReport) PriceVariation() float64 {
	fPrice, fPriceErr := mer.FromContext.GetPrice(mer.FSym, mer.TSym, mer.Exchange)
	tPrice, tPriceErr := mer.ToContext.GetPrice(mer.FSym, mer.TSym, mer.Exchange)
	if fPriceErr != nil || tPriceErr != nil {
		return math.NaN()
	}

	return cryptomarketutil.GetPriceVariation(fPrice.GetPrice(), tPrice.GetPrice())
}

// Elapsed returns the elapsed time between the 2 market snapshots
func (mer *MarketEvolutionReport) Elapsed() time.Duration {
	fTimestamp := mer.FromContext.GetTimestamp()
	tTimestamp := mer.ToContext.GetTimestamp()
	return tTimestamp.Sub(fTimestamp)
}

// ElapsedString returns the humanized string version of the time elapsed
// between 2 market snapshots
func (mer *MarketEvolutionReport) ElapsedString() string {
	return durafmt.Parse(mer.Elapsed()).String()
}

// GetTrendInfo returns a summary of the trend for the price
func (mer *MarketEvolutionReport) GetTrendInfo() *TrendSummary {

	// nothing to do in this case
	if mer.Noise.IsZero() {
		return nil
	}

	cListID := &CandlestickListID{
		FSym:      mer.FSym,
		TSym:      mer.TSym,
		Exchange:  mer.Exchange,
		Variation: *mer.Noise,
	}

	candlestickList, err := mer.ToContext.GetCandlestickList(*cListID)
	if err != nil {
		return nil
	}

	consolidatedCandlestick := candlestickList.GetLastConsolidatedCandlestick()
	curCandlestick := candlestickList.GetLastCandlestick()
	curPrice, _ := mer.ToContext.GetPrice(mer.FSym, mer.TSym, mer.Exchange)

	return &TrendSummary{
		Noise:               *mer.Noise,
		PrevCandlestickFrom: consolidatedCandlestick.PriceFrom.GetPrice(),
		HeadCandlestickFrom: consolidatedCandlestick.PriceTo.GetPrice(),
		HeadCandlestickTo:   curCandlestick.PriceTo.GetPrice(),
		CurPrice:            curPrice.GetPrice(),
	}
}
