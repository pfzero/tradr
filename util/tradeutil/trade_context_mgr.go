package tradeutil

import (
	"errors"
	"strings"
	"sync"
	"time"

	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"
)

const schedulerCheckStoreInterval = 1 * time.Minute

// TradeContextManager is the data structure responsible
// for creating trade contexts
type TradeContextManager struct {
	histoMarketSnapsLim  int
	histoMarketSnapsLock sync.Mutex
	histoMarketSnaps     []cryptomarketutil.MarketSnapshot

	candlesticksLim int

	fiatCurrencies map[string]struct{}

	candlestickListBucketLock sync.Mutex
	candlestickListBucket     map[string]*cryptomarketutil.CandlestickList

	candleListRespLock       sync.Mutex
	candlestickListResponses map[string]*cryptomarketutil.CandlestickListResp

	lastUsedTimeL sync.Mutex
	lastUsedTime  map[string]time.Time

	lastCtxL sync.Mutex
	lastCtx  *TradeContext
}

// NewTradeContextManagerWithCleanup is the TradeContextManager constructor that also enables garbage collection
// of unused candlestick lists - should be used in real-time trader
func NewTradeContextManagerWithCleanup(candlesticksLim int, cleanupCandlesAfter time.Duration, fiatCurrencies ...string) *TradeContextManager {
	tcm := NewTradeContextManager(candlesticksLim, fiatCurrencies...)
	startCandleCleanupScheduler(cleanupCandlesAfter, tcm)
	return tcm
}

// NewTradeContextManager is the TradeContextManager constructor
func NewTradeContextManager(candlesticksLim int, fiatCurrencies ...string) *TradeContextManager {
	currencies := make(map[string]struct{})

	for i := range fiatCurrencies {
		currency := fiatCurrencies[i]
		currencies[strings.ToUpper(currency)] = struct{}{}
	}

	tcm := &TradeContextManager{
		histoMarketSnapsLim: -1, // no limit
		histoMarketSnaps:    []cryptomarketutil.MarketSnapshot{},

		candlesticksLim:          candlesticksLim,
		fiatCurrencies:           currencies,
		candlestickListBucket:    make(map[string]*cryptomarketutil.CandlestickList),
		candlestickListResponses: make(map[string]*cryptomarketutil.CandlestickListResp),
		lastUsedTime:             make(map[string]time.Time),
	}

	return tcm
}

// SetSnapsLim sets the limit of market snapshots held
func (tcm *TradeContextManager) SetSnapsLim(lim int) *TradeContextManager {
	tcm.histoMarketSnapsLim = lim
	return tcm
}

// FeedHistoMarketSnaps adds historical price buckets in order to create
// candlestick lists
func (tcm *TradeContextManager) FeedHistoMarketSnaps(snaps []cryptomarketutil.MarketSnapshot) {
	tcm.histoMarketSnaps = snaps
}

// GetHistoMarketSnaps returns the historical price buckets
func (tcm *TradeContextManager) GetHistoMarketSnaps() []cryptomarketutil.MarketSnapshot {
	tcm.histoMarketSnapsLock.Lock()
	defer tcm.histoMarketSnapsLock.Unlock()

	cp := make([]cryptomarketutil.MarketSnapshot, len(tcm.histoMarketSnaps))
	copy(cp, tcm.histoMarketSnaps)

	return cp
}

// GetLastContext returns the latest context
func (tcm *TradeContextManager) GetLastContext() *TradeContext {
	tcm.lastCtxL.Lock()
	defer tcm.lastCtxL.Unlock()

	return tcm.lastCtx
}

// MakeContext creates a new trading context
func (tcm *TradeContextManager) MakeContext(marketSnapshot *cryptomarketutil.MarketSnapshot) *TradeContext {
	ctx := NewTradeContext(marketSnapshot, tcm)

	tcm.lastCtxL.Lock()
	tcm.lastCtx = ctx
	tcm.lastCtxL.Unlock()

	tcm.candleListRespLock.Lock()
	tcm.candlestickListBucketLock.Lock()
	// add prices to existing candlesticks
	for hashID, cl := range tcm.candlestickListBucket {
		candlestickListID, _ := CandlestickListIDFromHash(hashID)
		price, _ := ctx.GetPrice(candlestickListID.FSym, candlestickListID.TSym, candlestickListID.Exchange)

		if price == nil {
			continue
		}

		resp := cl.AddPrice(price)
		tcm.candlestickListResponses[hashID] = resp
	}
	tcm.candleListRespLock.Unlock()
	tcm.candlestickListBucketLock.Unlock()

	return ctx
}

// MakeContextAndStore creates a new context and also stores the market snapshot
func (tcm *TradeContextManager) MakeContextAndStore(snap *cryptomarketutil.MarketSnapshot) *TradeContext {
	tcm.addSnaps(*snap)
	return tcm.MakeContext(snap)
}

// GetCandlestickList creates a new candlestick list if doesn't exist
// and returns it
func (tcm *TradeContextManager) GetCandlestickList(candlestickListID CandlestickListID) (
	*cryptomarketutil.CandlestickList, error,
) {
	// mark this as being accessed just now
	tcm.lastUsedTimeL.Lock()
	tcm.lastUsedTime[candlestickListID.Hash()] = time.Now()
	tcm.lastUsedTimeL.Unlock()

	tcm.candlestickListBucketLock.Lock()
	cl, ok := tcm.candlestickListBucket[candlestickListID.Hash()]
	tcm.candlestickListBucketLock.Unlock()

	if ok {
		return cl, nil
	}

	e := tcm.setCandlestickList(candlestickListID)
	if e != nil {
		return nil, e
	}

	tcm.candlestickListBucketLock.Lock()
	cl, ok = tcm.candlestickListBucket[candlestickListID.Hash()]
	tcm.candlestickListBucketLock.Unlock()

	if ok {
		return cl, nil
	}
	return nil, errors.New("Couldn't find the requested candlestick list")
}

// GetCandlestickListResp returns the latest response from the given candlestick
// after the latest price relevant to that candlestick list was added
func (tcm *TradeContextManager) GetCandlestickListResp(candlestickListID CandlestickListID) *cryptomarketutil.CandlestickListResp {
	// mark this as being accessed just now
	tcm.lastUsedTimeL.Lock()
	tcm.lastUsedTime[candlestickListID.Hash()] = time.Now()
	tcm.lastUsedTimeL.Unlock()

	tcm.candleListRespLock.Lock()
	resp, ok := tcm.candlestickListResponses[candlestickListID.Hash()]
	tcm.candleListRespLock.Unlock()

	if ok {
		return resp
	}

	err := tcm.setCandlestickList(candlestickListID)

	if err != nil {
		return nil
	}

	return tcm.GetCandlestickListResp(candlestickListID)
}

// isFiatCurrency identifies if the given coin is a fiat currency
func (tcm *TradeContextManager) isFiatCurrency(sym string) bool {
	_, ok := tcm.fiatCurrencies[strings.ToUpper(sym)]
	return ok
}

//
func (tcm *TradeContextManager) getPrice(
	marketSnapshot *cryptomarketutil.MarketSnapshot,
	candlestickListID *CandlestickListID,
) cryptomarketutil.Pricer {

	var price cryptomarketutil.Pricer
	var e error
	isFSymFiat := tcm.isFiatCurrency(candlestickListID.FSym)
	isTSymFiat := tcm.isFiatCurrency(candlestickListID.TSym)

	if isFSymFiat {
		p := marketSnapshot.Get(candlestickListID.TSym, candlestickListID.Exchange)
		if p == nil {
			return nil
		}

		// since our price is represented relative to a fiat (USD)
		// currency, in this case, we must invert the price
		price = &cryptomarketutil.PriceConversion{
			FSym:      candlestickListID.FSym,
			TSym:      candlestickListID.TSym,
			Exchange:  candlestickListID.Exchange,
			Timestamp: p.GetTimestamp(),
			FromPrice: 1,
			ToPrice:   p.GetPrice(),
		}
	}

	if isTSymFiat {
		p := marketSnapshot.Get(candlestickListID.TSym, candlestickListID.Exchange)
		price = p
	}

	if !(isFSymFiat || isTSymFiat) {
		price, e = marketSnapshot.Convert(candlestickListID.FSym, candlestickListID.TSym, candlestickListID.Exchange)
		// the only error we can get is that the pair wasn't found
		if e != nil {
			return nil
		}
	}

	return price
}

// setCandlestickList creates a new candleStickList and feeds it
// with historical prices
func (tcm *TradeContextManager) setCandlestickList(candlestickListID CandlestickListID) error {

	cl := cryptomarketutil.NewCandlestickList(candlestickListID.Variation, tcm.candlesticksLim)

	if tcm.isFiatCurrency(candlestickListID.FSym) && tcm.isFiatCurrency(candlestickListID.TSym) {
		return errors.New("Candlesticklist between 2 fiat currencies isn't supported")
	}

	tcm.histoMarketSnapsLock.Lock()
	cp := make([]cryptomarketutil.MarketSnapshot, len(tcm.histoMarketSnaps))
	copy(cp, tcm.histoMarketSnaps)
	tcm.histoMarketSnapsLock.Unlock()

	var lastResp *cryptomarketutil.CandlestickListResp

	for i := range cp {
		ctx := NewTradeContext(&cp[i], tcm)
		p, e := ctx.GetPrice(candlestickListID.FSym, candlestickListID.TSym, candlestickListID.Exchange)
		if e != nil {
			continue
		}

		lastResp = cl.AddPrice(p)
	}

	lastCtx := tcm.GetLastContext()
	if !lastCtx.GetTimestamp().Equal(cp[len(cp)-1].GetTimestamp()) {
		p, e := lastCtx.GetPrice(candlestickListID.FSym, candlestickListID.TSym, candlestickListID.Exchange)
		if e == nil {
			lastResp = cl.AddPrice(p)
		}
	}

	tcm.candlestickListBucketLock.Lock()
	tcm.candlestickListBucket[candlestickListID.Hash()] = cl
	tcm.candlestickListBucketLock.Unlock()

	tcm.candleListRespLock.Lock()
	tcm.candlestickListResponses[candlestickListID.Hash()] = lastResp
	tcm.candleListRespLock.Unlock()

	return nil
}

// addSnaps adds the given snaps to the list of snaps and also takes into consideration
// market snapshots limits
func (tcm *TradeContextManager) addSnaps(snaps ...cryptomarketutil.MarketSnapshot) {
	tcm.histoMarketSnapsLock.Lock()
	defer tcm.histoMarketSnapsLock.Unlock()

	if tcm.histoMarketSnapsLim == -1 {
		tcm.histoMarketSnaps = append(tcm.histoMarketSnaps, snaps...)
		return
	}

	surplus := len(snaps) + len(tcm.histoMarketSnaps) - tcm.histoMarketSnapsLim

	if surplus <= 0 {
		tcm.histoMarketSnaps = append(tcm.histoMarketSnaps, snaps...)
		return
	}

	tcm.histoMarketSnaps = tcm.histoMarketSnaps[surplus:]
	tcm.histoMarketSnaps = append(tcm.histoMarketSnaps, snaps...)

	return
}
