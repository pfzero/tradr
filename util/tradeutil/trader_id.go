package tradeutil

import (
	"fmt"
	"strconv"
	"strings"
)

const sep = "#"

// TraderID represents the data structure
// that holds a trader builder
type TraderID struct {
	name string
}

// NewTraderID is the constructor for TraderID data structure
func NewTraderID(name string) *TraderID {
	return &TraderID{name: name}
}

// NewTraderIDFromGUID returns a trader identifier based on the
// given trader guid
func NewTraderIDFromGUID(guid string) (*TraderID, error) {
	parts := strings.Split(guid, sep)
	if len(parts) == 0 {
		return nil, fmt.Errorf(`GUID: "%s" doesn't represent a trader guid`, guid)
	}
	traderName := parts[0]
	traderIdentifier := NewTraderID(traderName)
	return traderIdentifier, nil
}

// GetName returns the name of the trader
func (tid *TraderID) GetName() string {
	return tid.name
}

// GetTraderInstanceGUID returns a specific trader instance guid
func (tid *TraderID) GetTraderInstanceGUID(instanceID uint) string {
	return fmt.Sprintf(`%s%s%d`, tid.GetName(), sep, instanceID)
}

// GetIDFromGUID returns the trader's id from the given guid
func (tid *TraderID) GetIDFromGUID(guid string) (uint, error) {
	parts := strings.Split(guid, sep)
	traderName := parts[0]

	if traderName != tid.GetName() {
		return 0, fmt.Errorf(`Trader instance with guid: "%s" not found within trader "%s"`, guid, tid.GetName())
	}

	strID := parts[1]
	parsedID, e := strconv.ParseUint(strID, 10, 64)
	if e != nil {
		return 0, e
	}
	return uint(parsedID), nil
}

// GetTraderIDFromGUID returns the trader's id based on the given guid
func GetTraderIDFromGUID(guid string) (uint, error) {
	traderIdentifier, e := NewTraderIDFromGUID(guid)
	if e != nil {
		return 0, e
	}
	return traderIdentifier.GetIDFromGUID(guid)
}
