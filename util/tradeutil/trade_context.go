package tradeutil

import (
	"errors"
	"fmt"
	"time"

	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"
)

// TradeContext is the data structure
// that holds the latest list of prices for all
// the coins and also a set of tools (candlestickList) that
// are handy for traders
type TradeContext struct {
	marketSnapshot *cryptomarketutil.MarketSnapshot
	tcm            *TradeContextManager
}

// NewTradeContext is the constructor of the TradeContext
func NewTradeContext(ms *cryptomarketutil.MarketSnapshot, tcm *TradeContextManager) *TradeContext {
	return &TradeContext{
		marketSnapshot: ms,
		tcm:            tcm,
	}
}

// GetPrice returns the price of the first given currency symbol
// relative to the second
func (tc *TradeContext) GetPrice(fsym, tsym, exchange string) (cryptomarketutil.Pricer, error) {

	if tc.tcm.isFiatCurrency(fsym) && tc.tcm.isFiatCurrency(tsym) {
		return nil, errors.New(`Conversion from fiat to fiat is not supported`)
	}

	result := &cryptomarketutil.PriceConversion{
		FSym:     fsym,
		TSym:     tsym,
		Exchange: exchange,
	}

	if tc.tcm.isFiatCurrency(fsym) {
		tSymPrice := tc.marketSnapshot.Get(tsym, exchange)
		if tSymPrice == nil {
			return nil, fmt.Errorf(`Cannot get price of "%s":"%s"`, tsym, exchange)
		}
		result.Timestamp = tSymPrice.GetTimestamp()
		result.FromPrice = 1
		result.ToPrice = tSymPrice.GetPrice()
		return result, nil
	}

	if tc.tcm.isFiatCurrency(tsym) {
		fSymPrice := tc.marketSnapshot.Get(fsym, exchange)
		if fSymPrice == nil {
			return nil, fmt.Errorf(`Cannot get price of "%s":"%s"`, fsym, exchange)
		}
		return fSymPrice, nil
	}

	return tc.marketSnapshot.Convert(fsym, tsym, exchange)
}

// GetLastCandlestick returns the latest candlestick formed for the
// given candlestick list
func (tc *TradeContext) GetLastCandlestick(candlestickListID CandlestickListID) (
	*cryptomarketutil.Candlestick, error,
) {
	candlestickList, e := tc.tcm.GetCandlestickList(candlestickListID)
	if e != nil {
		return nil, e
	}
	return candlestickList.GetLastCandlestick(), nil
}

// GetCandlestickList returns the candlestickList for the given id
func (tc *TradeContext) GetCandlestickList(candlestickListID CandlestickListID) (
	*cryptomarketutil.CandlestickList, error,
) {
	candlestickList, e := tc.tcm.GetCandlestickList(candlestickListID)
	if e != nil {
		return nil, e
	}
	return candlestickList, nil
}

// GetLastCandlestickResp returns the latest response of the given candlestick list
func (tc *TradeContext) GetLastCandlestickResp(candlestickListID CandlestickListID) *cryptomarketutil.CandlestickListResp {
	return tc.tcm.GetCandlestickListResp(candlestickListID)
}

// GetTimestamp return the timestamp of the trade context
func (tc *TradeContext) GetTimestamp() time.Time {
	return tc.marketSnapshot.GetTimestamp()
}

// IsZero checks wether the current context is zeroed
func (tc *TradeContext) IsZero() bool {
	return tc.marketSnapshot == nil
}
