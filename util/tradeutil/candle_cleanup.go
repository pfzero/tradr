package tradeutil

import (
	"time"
)

// startCandleCleanupScheduler is the poor-man's garbage collector of unused candlestickLists;
// it runs at intervals of time and checks which of the candlestickLists were used recently
func startCandleCleanupScheduler(cleanupCycle time.Duration, tcm *TradeContextManager) {

	if cleanupCycle <= 0 {
		return
	}

	ticker := time.NewTicker(cleanupCycle)

	go func() {
		for {
			<-ticker.C

			tcm.candlestickListBucketLock.Lock()
			tcm.lastUsedTimeL.Lock()
			tcm.candleListRespLock.Lock()
			for candleID := range tcm.candlestickListBucket {
				lastUsed, ok := tcm.lastUsedTime[candleID]

				if !ok {
					// never used...
					delete(tcm.candlestickListBucket, candleID)
					delete(tcm.candlestickListResponses, candleID)
					continue
				}

				if time.Now().Add(-cleanupCycle).After(lastUsed) {
					delete(tcm.candlestickListBucket, candleID)
					delete(tcm.candlestickListResponses, candleID)
					delete(tcm.lastUsedTime, candleID)
				}
			}

			tcm.candlestickListBucketLock.Unlock()
			tcm.lastUsedTimeL.Unlock()
			tcm.candleListRespLock.Unlock()
		}
	}()
}
