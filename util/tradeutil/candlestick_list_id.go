package tradeutil

import (
	"errors"
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"
)

// CandlestickListID represents the unique identifier
// for a single candlestick list
type CandlestickListID struct {
	FSym      string
	TSym      string
	Exchange  string
	Variation cryptomarketutil.Variation
}

// Hash returns the candlestickList id as a string
func (cli *CandlestickListID) Hash() string {
	return fmt.Sprintf("%s:%s:%s~%f:%f", cli.FSym, cli.TSym, cli.Exchange, cli.Variation.Up, cli.Variation.Down)
}

// CandlestickListIDFromHash returns the candlestickList id data structure
// from the given hash string
func CandlestickListIDFromHash(hash string) (*CandlestickListID, error) {
	id := &CandlestickListID{}
	noiseSep := "~"
	atomicSep := ":"
	parts := strings.Split(hash, noiseSep)
	if len(parts) != 2 {
		return nil, fmt.Errorf(`The given hash: "%s" is not correct. The accepted form is: "fsym:tsym:exchange-upNoise:downNoise"`, hash)
	}

	leftParts := strings.Split(parts[0], atomicSep)
	if len(leftParts) != 3 {
		return nil, errors.New(`The left side of the hash must be of form: "fsym:tsym:exchange"`)
	}
	id.FSym = leftParts[0]
	id.TSym = leftParts[1]
	id.Exchange = leftParts[2]

	rightParts := strings.Split(parts[1], atomicSep)
	if len(rightParts) != 2 {
		return nil, errors.New(`The right side of the hash must be of form: "upNoise:downNoise"`)
	}
	upNoise, err := strconv.ParseFloat(rightParts[0], 64)
	downNoise, err := strconv.ParseFloat(rightParts[1], 64)
	if err != nil {
		return nil, fmt.Errorf(`Error converting string to f64: "%s"`, err)
	}
	id.Variation.Up = upNoise
	id.Variation.Down = downNoise
	return id, nil
}
