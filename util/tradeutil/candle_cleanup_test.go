package tradeutil

import (
	"math"
	"testing"
	"time"

	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"
)

func Test_startCandleCleanupScheduler(t *testing.T) {
	type args struct {
		cleanupCycle time.Duration
		tcm          *TradeContextManager
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "It should remove unused candles",
			args: args{
				cleanupCycle: time.Millisecond,
				tcm: func() *TradeContextManager {
					tcm := NewTradeContextManager(math.MaxInt32, "USD")
					tcm.candlestickListBucket["SomeCandleID"] = &cryptomarketutil.CandlestickList{}
					tcm.candlestickListResponses["SomeCandleID"] = &cryptomarketutil.CandlestickListResp{}
					tcm.lastUsedTime["SomeCandleID"] = time.Date(2018, time.January, 1, 1, 1, 1, 1, time.Local)
					return tcm
				}(),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tcm := tt.args.tcm
			tcm.candlestickListBucketLock.Lock()
			tcm.candleListRespLock.Lock()
			tcm.lastUsedTimeL.Lock()

			if _, ok := tcm.candlestickListBucket["SomeCandleID"]; !ok {
				t.Error("Expected SameCandleID to be found in candlestickListBucket but the key wasn't found")
			}
			if _, ok := tcm.candlestickListResponses["SomeCandleID"]; !ok {
				t.Error("Expected SameCandleID to be found in candlestickListResponses but the key wasn't found")
			}
			if _, ok := tcm.lastUsedTime["SomeCandleID"]; !ok {
				t.Error("Expected SameCandleID to be found in lastUsedTime but the key wasn't found")
			}

			tcm.candlestickListBucketLock.Unlock()
			tcm.candleListRespLock.Unlock()
			tcm.lastUsedTimeL.Unlock()

			startCandleCleanupScheduler(tt.args.cleanupCycle, tt.args.tcm)
			time.Sleep(2 * time.Millisecond)

			tcm.candlestickListBucketLock.Lock()
			tcm.candleListRespLock.Lock()
			tcm.lastUsedTimeL.Lock()
			defer tcm.candlestickListBucketLock.Unlock()
			defer tcm.candleListRespLock.Unlock()
			defer tcm.lastUsedTimeL.Unlock()

			if _, ok := tcm.candlestickListBucket["SomeCandleID"]; ok {
				t.Error("Expected SameCandleID to be removed from candlestickListBucket but it's still in place")
			}
			if _, ok := tcm.candlestickListResponses["SomeCandleID"]; ok {
				t.Error("Expected SameCandleID to be removed from candlestickListResponses but it's still in place")
			}
			if _, ok := tcm.lastUsedTime["SomeCandleID"]; ok {
				t.Error("Expected SameCandleID to be removed from lastUsedTime but it's still in place")
			}
		})
	}
}
