package cryptomarketutil

import (
	"reflect"
	"testing"
	"time"
)

func TestMarketSnapshotList_GetAsTimestampedEntries(t *testing.T) {
	tests := []struct {
		name string
		msl  MarketSnapshotList
		want []Timestamped
	}{
		{
			name: "It should transform list to timestamped values",
			msl: MarketSnapshotList{
				MarketSnapshot{prices: map[string]map[string]Pricer{
					"Market": map[string]Pricer{
						"ETH": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 1, 0, 0, 0, 0, time.Local),
							Price:     3,
						},
					},
				}},
			},
			want: []Timestamped{
				&MarketSnapshot{prices: map[string]map[string]Pricer{
					"Market": map[string]Pricer{
						"ETH": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 1, 0, 0, 0, 0, time.Local),
							Price:     3,
						},
					},
				}},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.msl.GetAsTimestampedEntries(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("MarketSnapshotList.GetAsTimestampedEntries() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMarketSnapshotList_GetFirstSnapIndexForSymbols(t *testing.T) {
	type args struct {
		exchange string
		symbols  []string
	}
	tests := []struct {
		name string
		msl  MarketSnapshotList
		args args
		want int
	}{
		{
			name: "It should return the index of the first market snapshot that contains the list of symbols",
			msl: MarketSnapshotList{
				MarketSnapshot{prices: map[string]map[string]Pricer{
					"Market": map[string]Pricer{
						"ETH": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 1, 0, 0, 0, 0, time.Local),
							Price:     3,
						},
					},
				}},
				MarketSnapshot{prices: map[string]map[string]Pricer{
					"Market": map[string]Pricer{
						"ETH": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 2, 0, 0, 0, 0, time.Local),
							Price:     3,
						},
						"BTC": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 2, 0, 0, 0, 0, time.Local),
							Price:     300,
						},
					},
				}},
				MarketSnapshot{prices: map[string]map[string]Pricer{
					"Market": map[string]Pricer{
						"ETH": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 3, 0, 0, 0, 0, time.Local),
							Price:     3,
						},
						"BTC": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 3, 0, 0, 0, 0, time.Local),
							Price:     300,
						},
					},
				}},
				MarketSnapshot{prices: map[string]map[string]Pricer{
					"Market": map[string]Pricer{
						"ETH": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 4, 0, 0, 0, 0, time.Local),
							Price:     3,
						},
						"BTC": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 4, 0, 0, 0, 0, time.Local),
							Price:     300,
						},
						"XRP": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 4, 0, 0, 0, 0, time.Local),
							Price:     0.3,
						},
					},
				}},
				MarketSnapshot{prices: map[string]map[string]Pricer{
					"Market": map[string]Pricer{
						"ETH": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 5, 0, 0, 0, 0, time.Local),
							Price:     3,
						},
						"BTC": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 5, 0, 0, 0, 0, time.Local),
							Price:     300,
						},
						"XRP": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 5, 0, 0, 0, 0, time.Local),
							Price:     0.3,
						},
					},
				}},
			},
			args: args{
				exchange: "Market",
				symbols:  []string{"ETH", "BTC", "XRP"},
			},
			want: 3,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.msl.GetFirstSnapIndexForSymbols(tt.args.exchange, tt.args.symbols...); got != tt.want {
				t.Errorf("MarketSnapshotList.GetFirstSnapIndexForSymbols() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestMarketSnapshotList_FillSnaps(t *testing.T) {
	type args struct {
		src          MarketSnapshotList
		destExchange string
		srcExchange  string
	}
	tests := []struct {
		name string
		msl  MarketSnapshotList
		args args
		want MarketSnapshotList
	}{
		{
			name: "It should fill market snaps from src exchange",
			msl: MarketSnapshotList{
				MarketSnapshot{prices: map[string]map[string]Pricer{
					"DestExchange": map[string]Pricer{
						"ETH": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 3, 0, 0, 0, 0, time.Local),
							Price:     5,
						},
					},
				}},
				MarketSnapshot{prices: map[string]map[string]Pricer{
					"DestExchange": map[string]Pricer{
						"ETH": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 4, 0, 0, 0, 0, time.Local),
							Price:     8,
						},
					},
				}},
				MarketSnapshot{prices: map[string]map[string]Pricer{
					"DestExchange": map[string]Pricer{
						"ETH": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 5, 0, 0, 0, 0, time.Local),
							Price:     9,
						},
						"BTC": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 5, 0, 0, 0, 0, time.Local),
							Price:     900,
						},
						"XRP": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 5, 0, 0, 0, 0, time.Local),
							Price:     0.9,
						},
					},
				}},
			},
			args: args{
				src: MarketSnapshotList{
					MarketSnapshot{prices: map[string]map[string]Pricer{
						"Market": map[string]Pricer{
							"ETH": &SimplePrice{
								Timestamp: time.Date(2018, time.January, 1, 0, 0, 0, 0, time.Local),
								Price:     3,
							},
						},
					}},
					MarketSnapshot{prices: map[string]map[string]Pricer{
						"Market": map[string]Pricer{
							"ETH": &SimplePrice{
								Timestamp: time.Date(2018, time.January, 2, 0, 0, 0, 0, time.Local),
								Price:     3,
							},
							"BTC": &SimplePrice{
								Timestamp: time.Date(2018, time.January, 2, 0, 0, 0, 0, time.Local),
								Price:     300,
							},
						},
					}},
					MarketSnapshot{prices: map[string]map[string]Pricer{
						"Market": map[string]Pricer{
							"ETH": &SimplePrice{
								Timestamp: time.Date(2018, time.January, 3, 0, 0, 0, 0, time.Local),
								Price:     3,
							},
							"BTC": &SimplePrice{
								Timestamp: time.Date(2018, time.January, 3, 0, 0, 0, 0, time.Local),
								Price:     300,
							},
						},
					}},
					MarketSnapshot{prices: map[string]map[string]Pricer{
						"Market": map[string]Pricer{
							"ETH": &SimplePrice{
								Timestamp: time.Date(2018, time.January, 4, 0, 0, 0, 0, time.Local),
								Price:     3,
							},
							"BTC": &SimplePrice{
								Timestamp: time.Date(2018, time.January, 4, 0, 0, 0, 0, time.Local),
								Price:     300,
							},
							"XRP": &SimplePrice{
								Timestamp: time.Date(2018, time.January, 4, 0, 0, 0, 0, time.Local),
								Price:     0.3,
							},
						},
					}},
					MarketSnapshot{prices: map[string]map[string]Pricer{
						"Market": map[string]Pricer{
							"ETH": &SimplePrice{
								Timestamp: time.Date(2018, time.January, 5, 0, 0, 0, 0, time.Local),
								Price:     3,
							},
							"BTC": &SimplePrice{
								Timestamp: time.Date(2018, time.January, 5, 0, 0, 0, 0, time.Local),
								Price:     300,
							},
							"XRP": &SimplePrice{
								Timestamp: time.Date(2018, time.January, 5, 0, 0, 0, 0, time.Local),
								Price:     0.3,
							},
						},
					}},
				},
				destExchange: "DestExchange",
				srcExchange:  "Market",
			},
			want: MarketSnapshotList{
				MarketSnapshot{prices: map[string]map[string]Pricer{
					"DestExchange": map[string]Pricer{
						"ETH": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 1, 0, 0, 0, 0, time.Local),
							Price:     3,
						},
					},
				}},
				MarketSnapshot{prices: map[string]map[string]Pricer{
					"DestExchange": map[string]Pricer{
						"ETH": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 2, 0, 0, 0, 0, time.Local),
							Price:     3,
						},
						"BTC": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 2, 0, 0, 0, 0, time.Local),
							Price:     300,
						},
					},
				}},
				MarketSnapshot{prices: map[string]map[string]Pricer{
					"DestExchange": map[string]Pricer{
						"ETH": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 3, 0, 0, 0, 0, time.Local),
							Price:     5,
						},
						"BTC": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 3, 0, 0, 0, 0, time.Local),
							Price:     300,
						},
					},
				}},
				MarketSnapshot{prices: map[string]map[string]Pricer{
					"DestExchange": map[string]Pricer{
						"ETH": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 4, 0, 0, 0, 0, time.Local),
							Price:     8,
						},
						"BTC": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 4, 0, 0, 0, 0, time.Local),
							Price:     300,
						},
						"XRP": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 4, 0, 0, 0, 0, time.Local),
							Price:     0.3,
						},
					},
				}},
				MarketSnapshot{prices: map[string]map[string]Pricer{
					"DestExchange": map[string]Pricer{
						"ETH": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 5, 0, 0, 0, 0, time.Local),
							Price:     9,
						},
						"BTC": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 5, 0, 0, 0, 0, time.Local),
							Price:     900,
						},
						"XRP": &SimplePrice{
							Timestamp: time.Date(2018, time.January, 5, 0, 0, 0, 0, time.Local),
							Price:     0.9,
						},
					},
				}},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.msl.FillSnaps(tt.args.src, tt.args.destExchange, tt.args.srcExchange); !reflect.DeepEqual(got, tt.want) {
				t.Error("MarketSnapshotList.FillSnaps() failed")

				for i := range tt.want {
					wantSN := tt.want[i]
					gotSN := got[i]
					if !reflect.DeepEqual(wantSN, gotSN) {
						t.Errorf("Got differences between market snaps got and want @ %s", wantSN.GetTimestamp())
						if !wantSN.GetTimestamp().Equal(gotSN.GetTimestamp()) {
							t.Errorf("Got timestamp: %s; Want timestamp: %s", gotSN.GetTimestamp(), wantSN.GetTimestamp())
						}

						for exchange, prices := range wantSN.ListAvailable() {
							for sym := range prices {
								wantPrice := wantSN.Get(sym, exchange)
								gotPrice := gotSN.Get(sym, exchange)

								if wantPrice != nil && gotPrice == nil {
									t.Errorf("Got price for exchange %s and sym %s at time %s nil",
										exchange, sym, wantPrice.GetTimestamp())
									continue
								}

								if !wantPrice.GetTimestamp().Equal(gotPrice.GetTimestamp()) {
									t.Errorf("Price for exchange %s and sym %s have different timestamps; Got %s; Want %s",
										exchange, sym, gotPrice.GetTimestamp(), wantPrice.GetTimestamp())
								}

								if wantPrice.GetPrice() != gotPrice.GetPrice() {
									t.Errorf("Price for exchange %s and sym %s have different values; Got %f; Want %f",
										exchange, sym, gotPrice.GetPrice(), wantPrice.GetPrice())
								}
							}
						}
					}
				}
			}
		})
	}
}

func TestMarketSnapshotList_InjectFakeSnaps(t *testing.T) {
	type args struct {
		factor int
	}
	tests := []struct {
		name string
		msl  MarketSnapshotList
		args args
		want MarketSnapshotList
	}{
		{
			name: "It should generate fake prices for market snaps",
			msl: MarketSnapshotList{
				MarketSnapshot{
					prices: map[string]map[string]Pricer{
						"Market": map[string]Pricer{
							"ETH": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 0, 0, 0, 0, time.Local),
								Price:     250,
							},
							"BTC": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 0, 0, 0, 0, time.Local),
								Price:     2500,
							},
						},
						"SomeExchange": map[string]Pricer{
							"ETH": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 0, 0, 0, 0, time.Local),
								Price:     400,
							},
							"BTC": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 0, 0, 0, 0, time.Local),
								Price:     4000,
							},
						},
					},
				},
				MarketSnapshot{
					prices: map[string]map[string]Pricer{
						"Market": map[string]Pricer{
							"ETH": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 1, 0, 0, 0, time.Local),
								Price:     290,
							},
							"BTC": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 1, 0, 0, 0, time.Local),
								Price:     2900,
							},
						},
						"SomeExchange": map[string]Pricer{
							"ETH": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 1, 0, 0, 0, time.Local),
								Price:     440,
							},
							"BTC": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 1, 0, 0, 0, time.Local),
								Price:     4400,
							},
						},
					},
				},
			},
			args: args{factor: 3},
			want: MarketSnapshotList{
				MarketSnapshot{
					prices: map[string]map[string]Pricer{
						"Market": map[string]Pricer{
							"ETH": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 0, 0, 0, 0, time.Local),
								Price:     250,
							},
							"BTC": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 0, 0, 0, 0, time.Local),
								Price:     2500,
							},
						},
						"SomeExchange": map[string]Pricer{
							"ETH": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 0, 0, 0, 0, time.Local),
								Price:     400,
							},
							"BTC": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 0, 0, 0, 0, time.Local),
								Price:     4000,
							},
						},
					},
				},
				MarketSnapshot{
					prices: map[string]map[string]Pricer{
						"Market": map[string]Pricer{
							"ETH": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 0, 15, 0, 0, time.Local),
								Price:     260,
							},
							"BTC": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 0, 15, 0, 0, time.Local),
								Price:     2600,
							},
						},
						"SomeExchange": map[string]Pricer{
							"ETH": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 0, 15, 0, 0, time.Local),
								Price:     410,
							},
							"BTC": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 0, 15, 0, 0, time.Local),
								Price:     4100,
							},
						},
					},
				},
				MarketSnapshot{
					prices: map[string]map[string]Pricer{
						"Market": map[string]Pricer{
							"ETH": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 0, 30, 0, 0, time.Local),
								Price:     270,
							},
							"BTC": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 0, 30, 0, 0, time.Local),
								Price:     2700,
							},
						},
						"SomeExchange": map[string]Pricer{
							"ETH": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 0, 30, 0, 0, time.Local),
								Price:     420,
							},
							"BTC": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 0, 30, 0, 0, time.Local),
								Price:     4200,
							},
						},
					},
				},
				MarketSnapshot{
					prices: map[string]map[string]Pricer{
						"Market": map[string]Pricer{
							"ETH": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 0, 45, 0, 0, time.Local),
								Price:     280,
							},
							"BTC": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 0, 45, 0, 0, time.Local),
								Price:     2800,
							},
						},
						"SomeExchange": map[string]Pricer{
							"ETH": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 0, 45, 0, 0, time.Local),
								Price:     430,
							},
							"BTC": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 0, 45, 0, 0, time.Local),
								Price:     4300,
							},
						},
					},
				},
				MarketSnapshot{
					prices: map[string]map[string]Pricer{
						"Market": map[string]Pricer{
							"ETH": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 1, 0, 0, 0, time.Local),
								Price:     290,
							},
							"BTC": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 1, 0, 0, 0, time.Local),
								Price:     2900,
							},
						},
						"SomeExchange": map[string]Pricer{
							"ETH": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 1, 0, 0, 0, time.Local),
								Price:     440,
							},
							"BTC": &SimplePrice{
								Timestamp: time.Date(2018, time.June, 1, 1, 0, 0, 0, time.Local),
								Price:     4400,
							},
						},
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.msl.InjectFakeSnaps(tt.args.factor); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("MarketSnapshotList.InjectFakeSnaps() = %v, want %v", got, tt.want)
			}
		})
	}
}
