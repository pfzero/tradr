package cryptomarketutil

func getNextPriceListIdx(nextIndexes map[int]int, priceLists map[int][]Pricer) int {
	var foundListID int
	var closestPrice Pricer

	for listID, nextIndex := range nextIndexes {
		price := priceLists[listID][nextIndex]
		if closestPrice == nil {
			foundListID = listID
			closestPrice = price
			continue
		}

		if price.GetTimestamp().Before(closestPrice.GetTimestamp()) {
			closestPrice = price
			foundListID = listID
		}
	}

	return foundListID
}

func groupPrices(priceLists map[int][]Pricer) []map[int]Pricer {

	snaps := []map[int]Pricer{}
	nextIndexes := map[int]int{}
	candidatePrices := map[int]Pricer{}

	for listIdx := range priceLists {
		nextIndexes[listIdx] = 0
		candidatePrices[listIdx] = nil
	}

	needsBreak := false
	for {
		// check if no index overflows
		for listIdx, idx := range nextIndexes {
			if idx >= len(priceLists[listIdx]) {
				needsBreak = true
			}
		}

		if needsBreak {
			break
		}

		// snap which'll contain the prices from this
		// iteration
		snap := make(map[int]Pricer)

		// get the list that has the closest price
		nextListIdx := getNextPriceListIdx(nextIndexes, priceLists)
		// fetch the closest price of the found list; this will go
		// into our snap
		closestPrice := priceLists[nextListIdx][nextIndexes[nextListIdx]]
		nextIndexes[nextListIdx]++
		candidatePrices[nextListIdx] = closestPrice

		for listIdx := range nextIndexes {
			if listIdx == nextListIdx {
				continue
			}

			potentialPrice := priceLists[listIdx][nextIndexes[listIdx]]

			if potentialPrice.GetTimestamp().After(closestPrice.GetTimestamp()) {
				continue
			}

			nextIndexes[listIdx]++
			candidatePrices[listIdx] = potentialPrice
		}

		for listIdx := range candidatePrices {
			candidatePrice := candidatePrices[listIdx]
			if candidatePrice != nil {
				snap[listIdx] = candidatePrice
			}
		}

		snaps = append(snaps, snap)
	}

	// check for remaining prices
	remaining := map[int][]Pricer{}
	for listID, idx := range nextIndexes {
		if idx < len(priceLists[listID]) {
			remaining[listID] = priceLists[listID][idx:]
		}
	}

	if len(remaining) == 0 {
		return snaps
	}

	groupedRemaining := groupPrices(remaining)
	lastSnap := snaps[len(snaps)-1]

	for i := 0; i < len(groupedRemaining); i++ {
		group := groupedRemaining[i]
		for listIdx := range lastSnap {
			price := lastSnap[listIdx]
			if _, ok := group[listIdx]; ok {
				continue
			}

			group[listIdx] = price
		}
	}

	return append(snaps, groupedRemaining...)
}

// SyncPriceGroups syncs the groups of chronologically sorted
// prices; It takes a variable number of lists of prices and returns
// the prices synchronized within price groups; Each price group
// will contain all the prices that are older or have the same timestamp;
func SyncPriceGroups(groups [][]Pricer) []map[int]Pricer {
	if len(groups) == 0 {
		return nil
	}

	if len(groups) == 1 {
		snaps := make([]map[int]Pricer, len(groups[0]))
		for idx := range groups[0] {
			snap := make(map[int]Pricer)
			snap[0] = groups[0][idx]
			snaps[idx] = snap
		}
		return snaps
	}

	priceLists := make(map[int][]Pricer)

	for i := 0; i < len(groups); i++ {
		priceLists[i] = groups[i]
	}

	return groupPrices(priceLists)
}
