package cryptomarketutil

import (
	"sort"
	"sync"
)

// ExchangePriorityAsset represents a prioritized
// asset
type ExchangePriorityAsset interface {
	GetExchange() string
	GetSymbol() string
	GetPriority() int
}

// ExchangePriorityAssetList represents a sortable list
// of exchange priority assets
type ExchangePriorityAssetList []ExchangePriorityAsset

// Len returns the length of the ds
func (epal ExchangePriorityAssetList) Len() int {
	return len(epal)
}

// Swap swaps the given 2 keys
func (epal ExchangePriorityAssetList) Swap(i, j int) {
	epal[i], epal[j] = epal[j], epal[i]
}

// Less returns wether one asset has smaller priority than
// the other
func (epal ExchangePriorityAssetList) Less(i, j int) bool {
	return epal[i].GetPriority() < epal[j].GetPriority()
}

// ExchangePreferredAssets is a store containing a list of prioritized
// central assets for each exchange; Those assets are used for finding
// consistent cross-pair paths between any 2 assets; FOr example, if on
// an exchange there are the pairs: XRP-ETH and ETH-USDT and also
// XRP-BTC and BTC-USDT; Given a priority of {ETH: 1}, {BTC: 2},
// cross-trades will always do the conversion from XRP-USDT via ETH;
type ExchangePreferredAssets struct {
	assets map[string]ExchangePriorityAssetList

	l sync.Mutex
}

// NewExchangePreferredAssets returns an instance of exchange preferred
// assets
func NewExchangePreferredAssets() *ExchangePreferredAssets {
	return &ExchangePreferredAssets{assets: make(map[string]ExchangePriorityAssetList)}
}

// GetPreferredAssetsForExchange returns the preferred assets for each exchange
func (epa *ExchangePreferredAssets) GetPreferredAssetsForExchange(exchange string) ExchangePriorityAssetList {
	epa.l.Lock()
	defer epa.l.Unlock()

	existing, ok := epa.assets[exchange]

	if !ok {
		return nil
	}

	c := make(ExchangePriorityAssetList, len(existing))
	copy(c, existing)
	return c
}

// Save adds a new exchange preferred asset
func (epa *ExchangePreferredAssets) Save(ea ExchangePriorityAsset) *ExchangePreferredAssets {
	if ea == nil {
		return epa
	}
	epa.l.Lock()
	defer epa.l.Unlock()

	epa.ensureExchange(ea.GetExchange())
	assets := epa.assets[ea.GetExchange()]
	updated := false
	for i := 0; i < len(assets); i++ {
		asset := assets[i]
		if asset.GetSymbol() == ea.GetSymbol() {
			assets[i] = ea
			updated = true
			break
		}
	}

	if updated {
		sort.Stable(assets)
		return epa
	}

	// use insertion sort for adding a new asset entry
	// it should perform roughly in O(n) + O(n) while resizing
	// the slice; ~O(N)
	return epa.insert(ea)
}

// Del removes an exchange priority asset from the list of assets
func (epa *ExchangePreferredAssets) Del(ea ExchangePriorityAsset) *ExchangePreferredAssets {
	if ea == nil {
		return epa
	}
	epa.l.Lock()
	defer epa.l.Unlock()

	epa.ensureExchange(ea.GetExchange())
	assets := epa.assets[ea.GetExchange()]
	for i := 0; i < len(assets); i++ {
		asset := assets[i]
		if asset.GetSymbol() != ea.GetSymbol() {
			continue
		}
		afterRemove := append(assets[:i], assets[i+1:]...)
		epa.assets[ea.GetExchange()] = afterRemove
	}
	return epa
}

func (epa *ExchangePreferredAssets) ensureExchange(e string) *ExchangePreferredAssets {
	_, ok := epa.assets[e]
	if ok {
		return epa
	}

	epa.assets[e] = ExchangePriorityAssetList{}
	return epa
}

// use simple insertion sort for adding a new entry
func (epa *ExchangePreferredAssets) insert(ea ExchangePriorityAsset) *ExchangePreferredAssets {
	// insert using insertion sort
	added := false
	assets := epa.assets[ea.GetExchange()]

	for i := 0; i < len(assets); i++ {
		asset := assets[i]
		if asset.GetPriority() >= ea.GetPriority() {
			added = true
			assetsAfter := append(assets[:i], append(ExchangePriorityAssetList{ea}, assets[i:]...)...)
			epa.assets[ea.GetExchange()] = assetsAfter
			break
		}
	}

	if added {
		return epa
	}

	// then we have to insert at the end of the list
	assetsAfter := append(assets, ea)
	epa.assets[ea.GetExchange()] = assetsAfter

	return epa
}
