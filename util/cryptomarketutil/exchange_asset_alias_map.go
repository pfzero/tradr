package cryptomarketutil

import "sync"

// ExchangeAssetAlias is a thread-safe data-structure that holds
// per-exchange asset aliases
type ExchangeAssetAlias struct {
	aliases map[string]map[string]string
	l       sync.Mutex
}

// NewExchangeAssetAlias returns a new ExchangeAssetAlias
func NewExchangeAssetAlias() *ExchangeAssetAlias {
	return &ExchangeAssetAlias{
		aliases: make(map[string]map[string]string),
	}
}

// Save adds / updates an existing alias
func (eaa *ExchangeAssetAlias) Save(exchange, asset, alias string) *ExchangeAssetAlias {
	defer eaa.acquire().release()
	eaa.ensureExchangeNS(exchange)
	eaa.aliases[exchange][asset] = alias
	return eaa
}

// Del removes an alias for the given exchange and asset
func (eaa *ExchangeAssetAlias) Del(exchange, asset string) *ExchangeAssetAlias {
	defer eaa.acquire().release()
	eaa.ensureExchangeNS(exchange)
	delete(eaa.aliases[exchange], asset)
	return eaa
}

// GetAliasForAsset is a safe method that checks wether there's an alias defined for the
// given asset on the given exchange. If an alias is found, it's returned; otherwise
// the asset symbol is returned
func (eaa *ExchangeAssetAlias) GetAliasForAsset(exchange, asset string) string {
	defer eaa.acquire().release()
	eaa.ensureExchangeNS(exchange)
	alias, ok := eaa.aliases[exchange][asset]
	if ok && alias != "" {
		return alias
	}
	return asset
}

func (eaa *ExchangeAssetAlias) ensureExchangeNS(exchange string) *ExchangeAssetAlias {
	_, hasExchange := eaa.aliases[exchange]
	if !hasExchange {
		eaa.aliases[exchange] = make(map[string]string)
	}
	return eaa
}

func (eaa *ExchangeAssetAlias) acquire() *ExchangeAssetAlias {
	eaa.l.Lock()
	return eaa
}

func (eaa *ExchangeAssetAlias) release() *ExchangeAssetAlias {
	eaa.l.Unlock()
	return eaa
}
