package cryptomarketutil

import (
	"fmt"
	"reflect"
	"testing"
)

func Test_impossibleTradeRoute(t *testing.T) {
	type args struct {
		e    string
		fsym string
		tsym string
	}
	tests := []struct {
		name string
		args args
		want error
	}{
		{
			name: "It should create error based on given params",
			args: args{e: "Market", fsym: "Inexistent", tsym: "impossible"},
			want: fmt.Errorf(`Impossible to trade from "%s" to "%s" on exchange "%s"`, "Inexistent", "impossible", "Market"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := impossibleTradeRoute(tt.args.e, tt.args.fsym, tt.args.tsym); err.Error() != tt.want.Error() {
				t.Errorf("impossibleTradeRoute() error = %s, wantErr %s", err.Error(), tt.want.Error())
			}
		})
	}
}

func TestNewExchangeTradeRoutes(t *testing.T) {
	type args struct {
		exchangeName    string
		exchangeAliases *ExchangeAssetAlias
		preferredAssets *ExchangePreferredAssets
	}
	tests := []struct {
		name string
		args args
		want *ExchangeTradeRoutes
	}{
		{
			name: "It should return a new instance",
			args: args{
				exchangeName:    "Market",
				exchangeAliases: NewExchangeAssetAlias(),
				preferredAssets: NewExchangePreferredAssets(),
			},
			want: &ExchangeTradeRoutes{
				eName:           "Market",
				exchangeAliases: NewExchangeAssetAlias(),
				preferredAssets: NewExchangePreferredAssets(),
				supportedPairs:  make(map[string]map[string]string),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewExchangeTradeRoutes(tt.args.exchangeName, tt.args.exchangeAliases, tt.args.preferredAssets); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewExchangeTradeRoutes() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestExchangeTradeRoutes_SetPairs(t *testing.T) {
	type args struct {
		pairs map[string]map[string]string
	}
	tests := []struct {
		name string
		etr  *ExchangeTradeRoutes
		args args
		want *ExchangeTradeRoutes
	}{
		{
			name: "It should set the list of supported pairs",
			etr:  NewExchangeTradeRoutes("Market", NewExchangeAssetAlias(), NewExchangePreferredAssets()),
			args: args{
				pairs: map[string]map[string]string{
					"ETH": map[string]string{
						"USD": "ETHUSD",
					},
				},
			},
			want: func() *ExchangeTradeRoutes {
				etr := NewExchangeTradeRoutes("Market", NewExchangeAssetAlias(), NewExchangePreferredAssets())
				etr.supportedPairs["ETH"] = map[string]string{
					"USD": "ETHUSD",
				}
				return etr
			}(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.etr.SetPairs(tt.args.pairs); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ExchangeTradeRoutes.SetPairs() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestExchangeTradeRoutes_AddPair(t *testing.T) {
	type args struct {
		fsym   string
		tsym   string
		symbol string
	}
	tests := []struct {
		name string
		etr  *ExchangeTradeRoutes
		args args
		want *ExchangeTradeRoutes
	}{
		{
			name: "It should add a supported pair",
			etr:  NewExchangeTradeRoutes("Market", NewExchangeAssetAlias(), NewExchangePreferredAssets()),
			args: args{fsym: "XETH", tsym: "XUSDT", symbol: "XETHXUSDT"},
			want: func() *ExchangeTradeRoutes {
				etr := NewExchangeTradeRoutes("Market", NewExchangeAssetAlias(), NewExchangePreferredAssets())
				etr.supportedPairs["XETH"] = map[string]string{
					"XUSDT": "XETHXUSDT",
				}
				return etr
			}(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.etr.AddPair(tt.args.fsym, tt.args.tsym, tt.args.symbol); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ExchangeTradeRoutes.AddPair() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestExchangeTradeRoutes_GetExchangeTradeRoute(t *testing.T) {
	type args struct {
		fsym string
		tsym string
	}
	tests := []struct {
		name    string
		etr     *ExchangeTradeRoutes
		args    args
		want    []TradeRoute
		wantErr bool
	}{
		{
			name: "It should return the direct (Side-Sell) trade route if it's possible",
			etr: func() *ExchangeTradeRoutes {
				aliases := NewExchangeAssetAlias()
				aliases.Save("Market", "ETH", "XETH")
				aliases.Save("Market", "BTC", "XZBT")
				etr := NewExchangeTradeRoutes("Market", aliases, NewExchangePreferredAssets())
				etr.AddPair("XETH", "XZBT", "XETHXZBT")
				return etr
			}(),
			args: args{fsym: "ETH", tsym: "BTC"},
			want: []TradeRoute{
				TradeRoute{Base: "ETH", Quoted: "BTC", ExchangeBase: "XETH", ExchangeQuoted: "XZBT", ExchangeSymbol: "XETHXZBT", Side: SideSell},
			},
			wantErr: false,
		},
		{
			name: "It should return the direct (Side-Buy) trade route if it's possible",
			etr: func() *ExchangeTradeRoutes {
				aliases := NewExchangeAssetAlias()
				aliases.Save("Market", "ETH", "XETH")
				aliases.Save("Market", "BTC", "XZBT")
				etr := NewExchangeTradeRoutes("Market", aliases, NewExchangePreferredAssets())
				etr.AddPair("XETH", "XZBT", "XETHXZBT")
				return etr
			}(),
			args: args{fsym: "BTC", tsym: "ETH"},
			want: []TradeRoute{
				TradeRoute{Base: "ETH", Quoted: "BTC", ExchangeBase: "XETH", ExchangeQuoted: "XZBT", ExchangeSymbol: "XETHXZBT", Side: SideBuy},
			},
			wantErr: false,
		},
		{
			name: "It should return indirect trade route via preferred assets",
			etr: func() *ExchangeTradeRoutes {
				aliases := NewExchangeAssetAlias()
				aliases.Save("Market", "ETH", "XETH")
				aliases.Save("Market", "BTC", "XZBT")
				aliases.Save("Market", "USD", "USDT")
				epa := NewExchangePreferredAssets()

				epa.Save(&TestEPA{exchange: "Market", symbol: "USD", priority: 1})
				etr := NewExchangeTradeRoutes("Market", aliases, epa)
				etr.AddPair("XETH", "USDT", "XETHUSDT")
				etr.AddPair("XZBT", "USDT", "XZBTUSDT")
				return etr
			}(),

			args: args{fsym: "BTC", tsym: "ETH"},
			want: []TradeRoute{
				TradeRoute{Base: "BTC", Quoted: "USD", ExchangeBase: "XZBT", ExchangeQuoted: "USDT", ExchangeSymbol: "XZBTUSDT", Side: SideSell},
				TradeRoute{Base: "ETH", Quoted: "USD", ExchangeBase: "XETH", ExchangeQuoted: "USDT", ExchangeSymbol: "XETHUSDT", Side: SideBuy},
			},
			wantErr: false,
		},
		{
			name: "It should take into account preferred assets priority",
			etr: func() *ExchangeTradeRoutes {
				aliases := NewExchangeAssetAlias()
				aliases.Save("Market", "ETH", "XETH")
				aliases.Save("Market", "BTC", "XZBT")
				aliases.Save("Market", "USD", "USDT")
				aliases.Save("Market", "TSQR", "XTSQR")

				epa := NewExchangePreferredAssets()
				epa.Save(&TestEPA{exchange: "Market", symbol: "USD", priority: 2})
				epa.Save(&TestEPA{exchange: "Market", symbol: "TSQR", priority: 1})

				etr := NewExchangeTradeRoutes("Market", aliases, epa)
				etr.AddPair("XETH", "USDT", "XETHUSDT")
				etr.AddPair("XZBT", "USDT", "XZBTUSDT")
				etr.AddPair("XTSQR", "XETH", "XTSQRXETH")
				etr.AddPair("XZBT", "XTSQR", "XZBTXTSQR")

				return etr
			}(),

			args: args{fsym: "ETH", tsym: "BTC"},
			want: []TradeRoute{
				TradeRoute{Base: "TSQR", Quoted: "ETH", ExchangeBase: "XTSQR", ExchangeQuoted: "XETH", ExchangeSymbol: "XTSQRXETH", Side: SideBuy},
				TradeRoute{Base: "BTC", Quoted: "TSQR", ExchangeBase: "XZBT", ExchangeQuoted: "XTSQR", ExchangeSymbol: "XZBTXTSQR", Side: SideBuy},
			},
			wantErr: false,
		},
		{
			name: "It should return error if there are more than 2 hops to make the trade",
			etr: func() *ExchangeTradeRoutes {
				aliases := NewExchangeAssetAlias()
				aliases.Save("Market", "ETH", "XETH")
				aliases.Save("Market", "BTC", "XZBT")
				aliases.Save("Market", "USD", "USDT")
				aliases.Save("Market", "TSQR", "XTSQR")

				epa := NewExchangePreferredAssets()
				epa.Save(&TestEPA{exchange: "Market", symbol: "USD", priority: 2})
				epa.Save(&TestEPA{exchange: "Market", symbol: "TSQR", priority: 1})

				etr := NewExchangeTradeRoutes("Market", aliases, epa)
				etr.AddPair("XETH", "USDT", "XETHUSDT")
				etr.AddPair("XZBT", "USDT", "XZBTUSDT")
				etr.AddPair("XZBT", "XTSQR", "XTSQRXETH")
				// so we have: ETH -> USDT -> BTC -> TSQR
				return etr
			}(),

			args:    args{fsym: "ETH", tsym: "TSQR"},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.etr.GetExchangeTradeRoute(tt.args.fsym, tt.args.tsym)
			if (err != nil) != tt.wantErr {
				t.Errorf("ExchangeTradeRoutes.GetExchangeTradeRoute() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ExchangeTradeRoutes.GetExchangeTradeRoute() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestExchangeTradeRoutes_getCopyOfPairs(t *testing.T) {
	tests := []struct {
		name string
		etr  *ExchangeTradeRoutes
		want map[string]map[string]string
	}{
		{
			name: "It should return a copy of the list of supported pairs",
			etr: func() *ExchangeTradeRoutes {
				etr := NewExchangeTradeRoutes("Market", NewExchangeAssetAlias(), NewExchangePreferredAssets())
				etr.supportedPairs["XETH"] = map[string]string{
					"XZBT": "XETHXZBT",
				}
				return etr
			}(),
			want: map[string]map[string]string{
				"XETH": map[string]string{
					"XZBT": "XETHXZBT",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.etr.getCopyOfPairs(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ExchangeTradeRoutes.getCopyOfPairs() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestExchangeTradeRoutes_set(t *testing.T) {
	type args struct {
		fsym   string
		tsym   string
		symbol string
	}
	tests := []struct {
		name string
		etr  *ExchangeTradeRoutes
		args args
		want *ExchangeTradeRoutes
	}{
		{
			name: "It should set the supported pair",
			etr:  NewExchangeTradeRoutes("Market", NewExchangeAssetAlias(), NewExchangePreferredAssets()),
			args: args{fsym: "XETH", tsym: "XZBT", symbol: "XETHXZBT"},
			want: func() *ExchangeTradeRoutes {
				etr := NewExchangeTradeRoutes("Market", NewExchangeAssetAlias(), NewExchangePreferredAssets())
				etr.supportedPairs["XETH"] = map[string]string{
					"XZBT": "XETHXZBT",
				}
				return etr
			}(),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.etr.set(tt.args.fsym, tt.args.tsym, tt.args.symbol); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ExchangeTradeRoutes.set() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestExchangeTradeRoutes_getTradeRoute(t *testing.T) {
	type args struct {
		pairs map[string]map[string]string
		fsym  string
		tsym  string
	}
	tests := []struct {
		name  string
		etr   *ExchangeTradeRoutes
		args  args
		want  *TradeRoute
		want1 bool
	}{
		{
			name: "It should return trade instructions between the 2 assets by giving a Sell side if the path is direct",
			etr:  NewExchangeTradeRoutes("Market", NewExchangeAssetAlias(), NewExchangePreferredAssets()),
			args: args{
				pairs: map[string]map[string]string{
					"ETH": map[string]string{
						"XBT": "ETHXBT",
					},
				},
				fsym: "ETH",
				tsym: "XBT",
			},
			want:  &TradeRoute{Base: "ETH", Quoted: "XBT", ExchangeBase: "ETH", ExchangeQuoted: "XBT", ExchangeSymbol: "ETHXBT", Side: SideSell},
			want1: true,
		},
		{
			name: "It should return trade instructions between the 2 assets by giving a Buy side if the path is inverse",
			etr:  NewExchangeTradeRoutes("Market", NewExchangeAssetAlias(), NewExchangePreferredAssets()),
			args: args{
				pairs: map[string]map[string]string{
					"ETH": map[string]string{
						"BTC": "ETHBTC",
					},
				},
				fsym: "BTC",
				tsym: "ETH",
			},
			want:  &TradeRoute{Base: "ETH", Quoted: "BTC", ExchangeBase: "ETH", ExchangeQuoted: "BTC", ExchangeSymbol: "ETHBTC", Side: SideBuy},
			want1: true,
		},
		{
			name: "It should take into account asset aliases",
			etr: func() *ExchangeTradeRoutes {
				aliases := NewExchangeAssetAlias()
				aliases.Save("Market", "ETH", "XETH")
				aliases.Save("Market", "BTC", "XZBT")
				etr := NewExchangeTradeRoutes("Market", aliases, NewExchangePreferredAssets())
				return etr
			}(),
			args: args{
				pairs: map[string]map[string]string{
					"XETH": map[string]string{
						"XZBT": "XETHXZBT",
					},
				},
				fsym: "BTC",
				tsym: "ETH",
			},
			want:  &TradeRoute{Base: "ETH", Quoted: "BTC", ExchangeBase: "XETH", ExchangeQuoted: "XZBT", ExchangeSymbol: "XETHXZBT", Side: SideBuy},
			want1: true,
		},
		{
			name: "It should return nil and false if direct trade route is not possible between the 2 assets",
			etr: func() *ExchangeTradeRoutes {
				aliases := NewExchangeAssetAlias()
				aliases.Save("Market", "ETH", "XETH")
				aliases.Save("Market", "BTC", "XZBT")
				etr := NewExchangeTradeRoutes("Market", aliases, NewExchangePreferredAssets())
				return etr
			}(),
			args: args{
				pairs: map[string]map[string]string{
					"XETH": map[string]string{
						"XZBT": "XETHXZBT",
					},
				},
				fsym: "XMR",
				tsym: "ETH",
			},
			want:  nil,
			want1: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := tt.etr.getTradeRoute(tt.args.pairs, tt.args.fsym, tt.args.tsym)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ExchangeTradeRoutes.getTradeRoute() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("ExchangeTradeRoutes.getTradeRoute() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
