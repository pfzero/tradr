package cryptomarketutil

import (
	"reflect"
	"testing"
	"time"
)

var priceConversionTimestamp = time.Date(2017, time.January, 1, 0, 0, 0, 0, time.Local)

func TestPriceConversion_GetTimestamp(t *testing.T) {
	tests := []struct {
		name string
		pbcr *PriceConversion
		want time.Time
	}{
		{
			name: "It should return the timestamp of the price conversion",
			pbcr: &PriceConversion{
				FromPrice: 1,
				ToPrice:   2,
				Timestamp: priceConversionTimestamp,
			},
			want: priceConversionTimestamp,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.pbcr.GetTimestamp(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("PriceConversion.GetTimestamp() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPriceConversion_GetPrice(t *testing.T) {
	tests := []struct {
		name string
		pbcr *PriceConversion
		want float64
	}{
		{
			name: "It should return the conversion result",
			pbcr: &PriceConversion{
				FSym:      "LOL",
				FromPrice: 10,
				TSym:      "WTF",
				ToPrice:   2,
				Timestamp: priceConversionTimestamp,
			},
			want: 5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.pbcr.GetPrice(); got != tt.want {
				t.Errorf("PriceConversion.GetPrice() = %v, want %v", got, tt.want)
			}
		})
	}
}
