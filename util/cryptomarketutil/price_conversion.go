package cryptomarketutil

import "time"

// PriceConversion represents a simple conversion
// between 2 coins
type PriceConversion struct {
	Timestamp time.Time
	FSym      string
	TSym      string
	Exchange  string
	FromPrice float64
	ToPrice   float64
}

// GetTimestamp returns the timestamp of the conversion
func (pbcr *PriceConversion) GetTimestamp() time.Time {
	return pbcr.Timestamp
}

// GetPrice returns the conversion price of the 2 coins
func (pbcr *PriceConversion) GetPrice() float64 {
	return pbcr.FromPrice / pbcr.ToPrice
}
