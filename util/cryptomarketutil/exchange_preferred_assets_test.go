package cryptomarketutil

import (
	"reflect"
	"testing"

	"github.com/kylelemons/godebug/pretty"
)

// TestEpa is a simple data structure
// that implements the ExchangePriorityAsset and
// used for testing
type TestEPA struct {
	exchange string
	symbol   string
	priority int
}

func (tepa *TestEPA) GetExchange() string {
	return tepa.exchange
}

func (tepa *TestEPA) GetSymbol() string {
	return tepa.symbol
}

func (tepa *TestEPA) GetPriority() int {
	return tepa.priority
}

func TestNewExchangePreferredAssets(t *testing.T) {
	tests := []struct {
		name string
		want *ExchangePreferredAssets
	}{
		{
			name: "It should return a new instance of ExchangePreferredAssets",
			want: &ExchangePreferredAssets{
				assets: map[string]ExchangePriorityAssetList{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewExchangePreferredAssets(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewExchangePreferredAssets() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestExchangePreferredAssets_GetPreferredAssetsForExchange(t *testing.T) {
	type args struct {
		exchange string
	}
	tests := []struct {
		name string
		epa  *ExchangePreferredAssets
		args args
		want ExchangePriorityAssetList
	}{
		{
			name: "It should return the exchange's priority assets",
			epa: &ExchangePreferredAssets{
				assets: map[string]ExchangePriorityAssetList{
					"Market": ExchangePriorityAssetList{
						&TestEPA{exchange: "Market", symbol: "MIOTA", priority: 1},
						&TestEPA{exchange: "Market", symbol: "ETH", priority: 2},
						&TestEPA{exchange: "Market", symbol: "BTC", priority: 3},
					},
				},
			},
			args: args{exchange: "Market"},
			want: ExchangePriorityAssetList{
				&TestEPA{exchange: "Market", symbol: "MIOTA", priority: 1},
				&TestEPA{exchange: "Market", symbol: "ETH", priority: 2},
				&TestEPA{exchange: "Market", symbol: "BTC", priority: 3},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.epa.GetPreferredAssetsForExchange(tt.args.exchange); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ExchangePreferredAssets.GetPreferredAssetsForExchange() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestExchangePreferredAssets_Save(t *testing.T) {
	type args struct {
		ea ExchangePriorityAsset
	}
	tests := []struct {
		name string
		epa  *ExchangePreferredAssets
		args args
		want *ExchangePreferredAssets
	}{
		{
			name: "It should insert the exchange preferred asset",
			epa:  NewExchangePreferredAssets(),
			args: args{
				ea: &TestEPA{exchange: "Market", symbol: "MIOTA", priority: 1},
			},
			want: &ExchangePreferredAssets{
				assets: map[string]ExchangePriorityAssetList{
					"Market": ExchangePriorityAssetList{
						&TestEPA{exchange: "Market", symbol: "MIOTA", priority: 1},
					},
				},
			},
		},
		{
			name: "It should use insertion sort when inserting a new priority asset",
			epa: func() *ExchangePreferredAssets {
				epa := NewExchangePreferredAssets()
				epa.assets = map[string]ExchangePriorityAssetList{
					"Market": ExchangePriorityAssetList{
						&TestEPA{exchange: "Market", symbol: "MIOTA", priority: 1},
						&TestEPA{exchange: "Market", symbol: "ETH", priority: 3},
						&TestEPA{exchange: "Market", symbol: "BTC", priority: 5},
						&TestEPA{exchange: "Market", symbol: "XTH", priority: 7},
					},
				}
				return epa
			}(),
			args: args{
				ea: &TestEPA{exchange: "Market", symbol: "XTQ", priority: 4},
			},
			want: &ExchangePreferredAssets{
				assets: map[string]ExchangePriorityAssetList{
					"Market": ExchangePriorityAssetList{
						&TestEPA{exchange: "Market", symbol: "MIOTA", priority: 1},
						&TestEPA{exchange: "Market", symbol: "ETH", priority: 3},
						&TestEPA{exchange: "Market", symbol: "XTQ", priority: 4},
						&TestEPA{exchange: "Market", symbol: "BTC", priority: 5},
						&TestEPA{exchange: "Market", symbol: "XTH", priority: 7},
					},
				},
			},
		},
		{
			name: "It should use update the priority of the symbol and re-sort the list",
			epa: func() *ExchangePreferredAssets {
				epa := NewExchangePreferredAssets()
				epa.assets = map[string]ExchangePriorityAssetList{
					"Market": ExchangePriorityAssetList{
						&TestEPA{exchange: "Market", symbol: "MIOTA", priority: 1},
						&TestEPA{exchange: "Market", symbol: "ETH", priority: 3},
						&TestEPA{exchange: "Market", symbol: "BTC", priority: 5},
						&TestEPA{exchange: "Market", symbol: "XTH", priority: 7},
					},
				}
				return epa
			}(),
			args: args{
				ea: &TestEPA{exchange: "Market", symbol: "BTC", priority: 2},
			},
			want: &ExchangePreferredAssets{
				assets: map[string]ExchangePriorityAssetList{
					"Market": ExchangePriorityAssetList{
						&TestEPA{exchange: "Market", symbol: "MIOTA", priority: 1},
						&TestEPA{exchange: "Market", symbol: "BTC", priority: 2},
						&TestEPA{exchange: "Market", symbol: "ETH", priority: 3},
						&TestEPA{exchange: "Market", symbol: "XTH", priority: 7},
					},
				},
			},
		},
		{
			name: "It should shift the list if the newly added asset has the lowest priority",
			epa: func() *ExchangePreferredAssets {
				epa := NewExchangePreferredAssets()
				epa.assets = map[string]ExchangePriorityAssetList{
					"Market": ExchangePriorityAssetList{
						&TestEPA{exchange: "Market", symbol: "MIOTA", priority: 1},
						&TestEPA{exchange: "Market", symbol: "ETH", priority: 3},
						&TestEPA{exchange: "Market", symbol: "BTC", priority: 5},
						&TestEPA{exchange: "Market", symbol: "XTH", priority: 7},
					},
				}
				return epa
			}(),
			args: args{
				ea: &TestEPA{exchange: "Market", symbol: "XTQ", priority: 0},
			},
			want: &ExchangePreferredAssets{
				assets: map[string]ExchangePriorityAssetList{
					"Market": ExchangePriorityAssetList{
						&TestEPA{exchange: "Market", symbol: "XTQ", priority: 0},
						&TestEPA{exchange: "Market", symbol: "MIOTA", priority: 1},
						&TestEPA{exchange: "Market", symbol: "ETH", priority: 3},
						&TestEPA{exchange: "Market", symbol: "BTC", priority: 5},
						&TestEPA{exchange: "Market", symbol: "XTH", priority: 7},
					},
				},
			},
		},
		{
			name: "It should push in the list if the newly added asset has the highest priority",
			epa: func() *ExchangePreferredAssets {
				epa := NewExchangePreferredAssets()
				epa.assets = map[string]ExchangePriorityAssetList{
					"Market": ExchangePriorityAssetList{
						&TestEPA{exchange: "Market", symbol: "MIOTA", priority: 1},
						&TestEPA{exchange: "Market", symbol: "ETH", priority: 3},
						&TestEPA{exchange: "Market", symbol: "BTC", priority: 5},
						&TestEPA{exchange: "Market", symbol: "XTH", priority: 7},
					},
				}
				return epa
			}(),
			args: args{
				ea: &TestEPA{exchange: "Market", symbol: "XTQ", priority: 9},
			},
			want: &ExchangePreferredAssets{
				assets: map[string]ExchangePriorityAssetList{
					"Market": ExchangePriorityAssetList{
						&TestEPA{exchange: "Market", symbol: "MIOTA", priority: 1},
						&TestEPA{exchange: "Market", symbol: "ETH", priority: 3},
						&TestEPA{exchange: "Market", symbol: "BTC", priority: 5},
						&TestEPA{exchange: "Market", symbol: "XTH", priority: 7},
						&TestEPA{exchange: "Market", symbol: "XTQ", priority: 9},
					},
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.epa.Save(tt.args.ea); !reflect.DeepEqual(got, tt.want) {
				t.Error("ExschangePreferredAssets.Del(); Got diff between got and want")
				t.Error(pretty.Compare(got, tt.want))
			}
		})
	}
}

func TestExchangePreferredAssets_Del(t *testing.T) {
	type args struct {
		ea ExchangePriorityAsset
	}
	tests := []struct {
		name string
		epa  *ExchangePreferredAssets
		args args
		want *ExchangePreferredAssets
	}{
		{
			name: "It should remove the priority asset from the list",
			epa: func() *ExchangePreferredAssets {
				epa := NewExchangePreferredAssets()
				epa.assets = map[string]ExchangePriorityAssetList{
					"Market": ExchangePriorityAssetList{
						&TestEPA{exchange: "Market", symbol: "MIOTA", priority: 1},
						&TestEPA{exchange: "Market", symbol: "ETH", priority: 3},
						&TestEPA{exchange: "Market", symbol: "BTC", priority: 5},
						&TestEPA{exchange: "Market", symbol: "XTH", priority: 7},
					},
				}
				return epa
			}(),
			args: args{
				ea: &TestEPA{
					exchange: "Market", symbol: "MIOTA",
				},
			},
			want: func() *ExchangePreferredAssets {
				epa := NewExchangePreferredAssets()
				epa.assets = map[string]ExchangePriorityAssetList{
					"Market": ExchangePriorityAssetList{
						&TestEPA{exchange: "Market", symbol: "ETH", priority: 3},
						&TestEPA{exchange: "Market", symbol: "BTC", priority: 5},
						&TestEPA{exchange: "Market", symbol: "XTH", priority: 7},
					},
				}
				return epa
			}(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.epa.Del(tt.args.ea); !reflect.DeepEqual(got, tt.want) {
				t.Error("ExschangePreferredAssets.Del(); Got diff between got and want")
				t.Error(pretty.Compare(got, tt.want))
			}
		})
	}
}

func TestExchangePreferredAssets_ensureExchange(t *testing.T) {
	type args struct {
		e string
	}
	tests := []struct {
		name string
		epa  *ExchangePreferredAssets
		args args
		want *ExchangePreferredAssets
	}{
		{
			name: "It should simply ensure that the given exchange has a list available",
			epa:  NewExchangePreferredAssets(),
			args: args{e: "Market"},
			want: &ExchangePreferredAssets{
				assets: map[string]ExchangePriorityAssetList{
					"Market": ExchangePriorityAssetList{},
				},
			},
		},
		{
			name: "It should not re-create the namespace if already exists",
			epa: &ExchangePreferredAssets{
				assets: map[string]ExchangePriorityAssetList{
					"Market": ExchangePriorityAssetList{
						&TestEPA{exchange: "Market", symbol: "ADA", priority: 1},
					},
				},
			},
			args: args{e: "Market"},
			want: &ExchangePreferredAssets{
				assets: map[string]ExchangePriorityAssetList{
					"Market": ExchangePriorityAssetList{
						&TestEPA{exchange: "Market", symbol: "ADA", priority: 1},
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.epa.ensureExchange(tt.args.e); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ExchangePreferredAssets.ensureExchange() = %v, want %v", got, tt.want)
			}
		})
	}
}
