package cryptomarketutil

import (
	"math"

	"bitbucket.org/pfzero/tradr/util/num"
)

// PriceListIterator represents the function needed
// to filter / find elements in PriceList
type PriceListIterator func(idx int, p Pricer) bool

// PriceList contains a list of prices and is
// also able to filter based on a given noise
type PriceList struct {
	prices            []Pricer
	limit             int
	distilledBufLimit int
	distilledBuf      []Pricer
	bounds            *Variation
}

// NewPriceList creates a new PriceList
func NewPriceList(lim, distilledBufLimit int, bounds *Variation) *PriceList {
	return &PriceList{
		prices:            []Pricer{},
		limit:             lim,
		distilledBufLimit: distilledBufLimit,
		distilledBuf:      []Pricer{},
		bounds:            FillSymmetricVariation(*bounds),
	}
}

// GetAll returns the list of prices
func (p *PriceList) GetAll() []Pricer {
	return p.prices
}

// AsFloats returns the prices as a list of floating point numbers
func (p *PriceList) AsFloats() num.FloatsList {
	arr := num.FloatsList{}

	for _, price := range p.prices {
		arr = append(arr, price.GetPrice())
	}

	return arr
}

// AsPercVars transforms the list of prices into a list of
// price percentage variations
func (p *PriceList) AsPercVars() num.FloatsList {
	arr := num.FloatsList{}
	for idx, price := range p.prices {
		if idx == 0 {
			continue
		}
		v := GetPriceVariation(p.prices[idx-1].GetPrice(), price.GetPrice())
		arr = append(arr, v)
	}
	return arr
}

// AsVars returns the list of prices as a list of price variations
func (p *PriceList) AsVars() num.FloatsList {
	arr := num.FloatsList{}
	for idx, price := range p.prices {
		if idx == 0 {
			continue
		}
		v := price.GetPrice() - p.prices[idx-1].GetPrice()
		arr = append(arr, v)
	}
	return arr
}

// Filter is used to filter the list based on the given fn iterator
func (p *PriceList) Filter(fn PriceListIterator) *PriceList {
	accum := []Pricer{}
	for i := 0; i < len(p.prices); i++ {
		if fn(i, p.prices[i]) {
			accum = append(accum, p.prices[i])
		}
	}

	PriceList := NewPriceList(p.limit, p.distilledBufLimit, p.bounds)

	PriceList.prices = accum
	return PriceList
}

// Find is used to find a value within the PriceList
func (p *PriceList) Find(fn PriceListIterator) Pricer {
	var found Pricer
	for i := 0; i < len(p.prices); i++ {
		if fn(i, p.prices[i]) {
			found = p.prices[i]
			break
		}
	}
	return found
}

// Min finds the minimum price in price list
func (p *PriceList) Min() Pricer {
	var min Pricer
	for i := 0; i < len(p.prices); i++ {
		if min == nil || min.GetPrice() > p.prices[i].GetPrice() {
			min = p.prices[i]
		}
	}
	return min
}

// Max returns the max price within the price list
func (p *PriceList) Max() Pricer {
	var max Pricer
	for i := 0; i < len(p.prices); i++ {
		if max == nil || max.GetPrice() < p.prices[i].GetPrice() {
			max = p.prices[i]
		}
	}
	return max
}

// Avg returns the average price of the list
func (p *PriceList) Avg() float64 {
	var avg float64
	floats := p.AsFloats()
	for i := 0; i < len(floats); i++ {
		avg += floats[i]
	}

	return avg / float64(len(floats))
}

// Add adds a new price to the list of prices
func (p *PriceList) Add(price Pricer) *PriceList {
	return p.add(price)
}

// AddDistilled adds the price by checking if it can be adde directly to the list
// of existing prices; Otherwise it tries to distill the cumulated buffer of out-of-bounds
// prices and merge it to the price list
func (p *PriceList) AddDistilled(price Pricer) *PriceList {
	if !p.canAdd(price) {
		p.distilledBuf = append(p.distilledBuf, price)
		if len(p.distilledBuf) >= p.distilledBufLimit {
			p.procDistilled()
		}

		return p
	}

	if len(p.distilledBuf) > 0 {
		p.distilledBuf = []Pricer{}
	}

	return p.Add(price)
}

// Seed seeds the list of prices
func (p *PriceList) Seed(seed []Pricer) *PriceList {
	dif := len(seed) - p.limit
	if dif > 0 {
		p.prices = seed[dif:]
	} else {
		p.prices = seed
	}
	return p
}

// Distill distilles the list of prices
func (p *PriceList) Distill() *PriceList {
	PriceList := NewPriceList(p.limit, p.distilledBufLimit, p.bounds)
	for i := 0; i < len(p.prices); i++ {
		PriceList.AddDistilled(p.prices[i])
	}
	return PriceList
}

func (p *PriceList) canAdd(price Pricer) bool {
	if len(p.prices) == 0 {
		return true
	}
	last := p.prices[len(p.prices)-1]
	maxAccepted := last.GetPrice() + (p.bounds.GetUpVariation() * last.GetPrice())
	minAccepted := last.GetPrice() + (p.bounds.GetDownVariation() * last.GetPrice())

	return price.GetPrice() >= minAccepted && price.GetPrice() <= maxAccepted
}

func (p *PriceList) procDistilled() {

	buf := p.distilledBuf
	p.distilledBuf = []Pricer{}

	l := NewPriceList(p.limit, 0, &Variation{})
	avg := l.Seed(buf).Avg()
	closestIDX := -1

	for i, distilledPrice := range buf {
		if closestIDX == -1 {
			closestIDX = i
		}

		delta := math.Abs(distilledPrice.GetPrice() - avg)
		currentDelta := math.Abs(buf[closestIDX].GetPrice() - avg)

		if delta < currentDelta {
			closestIDX = i
		}
	}

	accepted := []Pricer{}

	for i := closestIDX; i >= 0; i-- {
		currentPrice := buf[i]
		if i == closestIDX {
			accepted = []Pricer{currentPrice}
			continue
		}

		lastPrice := accepted[0].GetPrice()
		crtPrice := currentPrice.GetPrice()

		maxAccepted := lastPrice + (p.bounds.GetUpVariation() * lastPrice)
		minAccepted := lastPrice + (p.bounds.GetDownVariation() * lastPrice)

		if crtPrice >= minAccepted && crtPrice <= maxAccepted {
			accepted = append([]Pricer{currentPrice}, accepted...)
		}
	}

	p.add(accepted...)

	if closestIDX == len(buf)-1 {
		return
	}

	accepted = []Pricer{}

	for i := closestIDX + 1; i < len(buf); i++ {
		currentPrice := buf[i]
		var lastPrice Pricer
		if len(accepted) == 0 {
			lastPrice = buf[closestIDX]
		} else {
			lastPrice = accepted[len(accepted)-1]
		}
		lastP := lastPrice.GetPrice()
		crtPrice := currentPrice.GetPrice()

		maxAccepted := lastP + (p.bounds.GetUpVariation() * lastP)
		minAccepted := lastP + (p.bounds.GetDownVariation() * lastP)

		if crtPrice >= minAccepted && crtPrice <= maxAccepted {
			accepted = append(accepted, currentPrice)
		}
	}

	p.add(accepted...)
}

func (p *PriceList) add(prices ...Pricer) *PriceList {
	if len(prices) >= p.limit {
		return p.Seed(prices)
	}

	surplus := len(prices) + len(p.prices) - p.limit
	if surplus > 0 {
		p.prices = p.prices[surplus:]
	}
	p.prices = append(p.prices, prices...)
	return p
}
