package cryptomarketutil

// Candlestick is a data structure that encodes
// a price variation
type Candlestick struct {
	PriceFrom Pricer
	PriceTo   Pricer
}

// NewCandlestick is the constructor for the candlestick
func NewCandlestick(priceFrom, priceTo Pricer) *Candlestick {
	return &Candlestick{PriceFrom: priceFrom, PriceTo: priceTo}
}

// GetFixedVariation returns the fixed variation of the candlestick
func (c *Candlestick) GetFixedVariation() float64 {
	return c.PriceTo.GetPrice() - c.PriceFrom.GetPrice()
}

// GetPercVariation returns the percent variation of the candlestick
func (c *Candlestick) GetPercVariation() float64 {
	return c.GetFixedVariation() / c.PriceFrom.GetPrice()
}

// IsDecreasing checks wether the candlestick is decreasing
func (c *Candlestick) IsDecreasing() bool {
	return c.GetFixedVariation() < 0
}

// IsIncreasing checks wether the candlestick is increasing
func (c *Candlestick) IsIncreasing() bool {
	return !c.IsDecreasing()
}

// IsPriceCompatible checks wether the given price is compatible
// and can be added to this candlestick (ie. it offers continuity to this
// candlestick or is within the existing price range defined by this candlestick)
func (c *Candlestick) IsPriceCompatible(price Pricer) bool {
	if price.GetPrice() == c.PriceTo.GetPrice() {
		return true
	}

	if c.IsDecreasing() && price.GetPrice() < c.PriceTo.GetPrice() {
		return true
	}
	if c.IsIncreasing() && price.GetPrice() > c.PriceTo.GetPrice() {
		return true
	}
	return false
}

// AddPrice tries to add a new price and modify the range of this
// candlestick
func (c *Candlestick) AddPrice(price Pricer) bool {
	isCompatible := c.IsPriceCompatible(price)
	if isCompatible {
		c.PriceTo = price
	}
	return isCompatible
}

// IsIdentical checks wether 2 candlesticks are identical
func (c *Candlestick) IsIdentical(candlestick *Candlestick) bool {
	if candlestick == nil {
		return false
	}
	isIdentical := true
	isIdentical = c.PriceFrom.GetTimestamp().Equal(candlestick.PriceFrom.GetTimestamp())
	isIdentical = isIdentical && c.PriceTo.GetTimestamp().Equal(candlestick.PriceTo.GetTimestamp())
	isIdentical = isIdentical && c.PriceFrom.GetPrice() == candlestick.PriceFrom.GetPrice()
	isIdentical = isIdentical && c.PriceTo.GetPrice() == candlestick.PriceTo.GetPrice()

	return isIdentical
}

// IsZero checks wether the candlestick is zeroed
func (c *Candlestick) IsZero() bool {
	if c.PriceFrom == nil || c.PriceTo == nil {
		return true
	}

	return c.PriceFrom.GetPrice() == 0 && c.PriceFrom.GetTimestamp().IsZero()
}
