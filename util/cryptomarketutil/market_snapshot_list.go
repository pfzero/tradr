package cryptomarketutil

import (
	"time"
)

// MarketSnapshotList represents a list of market snapshots
type MarketSnapshotList []MarketSnapshot

// GetAsTimestampedEntries transforms the list of market snapshots to a
// list of timestamped values
func (msl MarketSnapshotList) GetAsTimestampedEntries() []Timestamped {
	cp := make(MarketSnapshotList, len(msl))
	copy(cp, msl)
	res := make([]Timestamped, len(msl))

	for i := range cp {
		res[i] = &cp[i]
	}
	return res
}

// GetFirstSnapIndexForSymbols returns the first market snapshot index where
// all the symbols are present; The rule here is that if market snapshot at index N
// has all the symbols available, then all the following market snapshots will have all
// the symbols available;
// Complexity: len(symbols)*log(len(MarketSnapshotList[exchange])); Since the number of searched
// symbols is << number of market snapshots (on average), the algo will do roughly in log(N) where
// N = no of market snapshots per exchange; ie. len(MarketSnapshotList[exchange])
func (msl MarketSnapshotList) GetFirstSnapIndexForSymbols(exchange string, symbols ...string) int {
	first := msl[0]
	last := msl[len(msl)-1]

	if !last.HasAvailable(exchange, symbols...) {
		return -1
	}

	if first.HasAvailable(exchange, symbols...) {
		return 0
	}

	// ok, we are somewhere in between; let's find out with binary search algo..
	return msl.getFirstSnapIndexForSymbols(exchange, 0, len(msl)-1, symbols...)
}

func (msl MarketSnapshotList) getFirstSnapIndexForSymbols(exchange string, lo, hi int, symbols ...string) int {
	mi := (lo + hi) / 2
	middle := msl[mi]
	midAvailable := middle.HasAvailable(exchange, symbols...)

	// it means that we reached the scenario where
	// lo=k; hi={k,k+1}; So if lo has the requested symbols,
	// return lo, otherwise return hi
	if mi == lo {
		if midAvailable {
			return mi
		}
		return hi
	}

	if midAvailable {
		return msl.getFirstSnapIndexForSymbols(exchange, lo, mi, symbols...)
	}

	return msl.getFirstSnapIndexForSymbols(exchange, mi, hi, symbols...)
}

// FillSnaps fills the current market snapshot list with data from another source market snapshot list
// from another exchange
func (msl MarketSnapshotList) FillSnaps(src MarketSnapshotList, destExchange, srcExchange string) MarketSnapshotList {

	if len(msl) == 0 || len(src) == 0 {
		return msl
	}

	if destExchange == srcExchange {
		return msl
	}

	available := make(map[string]struct{})
	lastDestSnap := msl[len(msl)-1]

	// the snapshot contains only one exchange, so everything is fine
	for e, list := range lastDestSnap.ListAvailable() {
		if e != destExchange {
			continue
		}
		available = list
		break
	}

	ret := MarketSnapshotList{}

	currentSrcIDx := 0
	currentDestIdx := 0

	currentSrcSnap := &src[currentSrcIDx]
	currentDestSnap := &msl[currentDestIdx]

	for currentDestSnap.GetTimestamp().After(currentSrcSnap.GetTimestamp()) {
		snap := NewMarketSnapshot()
		for sym := range available {
			price := currentSrcSnap.Get(sym, srcExchange)
			if price != nil {
				snap.Set(sym, destExchange, price)
			}
		}

		ret = append(ret, *snap)

		currentSrcIDx++
		currentSrcSnap = &src[currentSrcIDx]
	}

	if currentSrcSnap.GetTimestamp().After(currentDestSnap.GetTimestamp()) {
		currentSrcIDx--
		currentSrcSnap = &src[currentSrcIDx]
	}

	for {
		for sym := range available {
			destP := currentDestSnap.Get(sym, destExchange)
			srcP := currentSrcSnap.Get(sym, srcExchange)
			if destP == nil && srcP != nil {
				currentDestSnap.Set(sym, destExchange, srcP)
			}
		}

		ret = append(ret, *currentDestSnap)

		currentDestIdx++
		if currentDestIdx == len(msl) {
			break
		}
		currentDestSnap = &msl[currentDestIdx]

		for currentSrcSnap.GetTimestamp().Before(currentDestSnap.GetTimestamp()) {
			currentSrcIDx++
			if currentSrcIDx == len(src) {
				break
			}
			currentSrcSnap = &src[currentSrcIDx]
		}
	}

	return ret
}

// InjectFakeSnaps increases the numbers of market snapshots by the given factor; It generates
// "fake" market snapshots that linearly scale in timestamp and value between the 2 points
func (msl MarketSnapshotList) InjectFakeSnaps(factor int) MarketSnapshotList {

	if len(msl) < 2 {
		return msl
	}

	generated := MarketSnapshotList{}

	for i := 0; i < len(msl)-1; i++ {
		current := msl[i]
		next := msl[i+1]

		generated = append(generated, current)

		for currentFactor := 1; currentFactor <= factor; currentFactor++ {
			currentMS := NewMarketSnapshot()

			available := current.ListAvailable()
			for exchange, prices := range available {
				for sym := range prices {
					currentPrice := current.Get(sym, exchange)
					nextPrice := next.Get(sym, exchange)

					deltaTs := nextPrice.GetTimestamp().Sub(currentPrice.GetTimestamp())
					deltaPrice := nextPrice.GetPrice() - currentPrice.GetPrice()

					durSegment := deltaTs / time.Duration(factor+1)
					priceSegment := deltaPrice / float64(factor+1)

					currentFactorPrice := currentPrice.GetPrice() + (priceSegment * float64(currentFactor))
					currentFactorTs := currentPrice.GetTimestamp().Add(time.Duration(currentFactor) * durSegment)

					currentMS.Set(sym, exchange, &SimplePrice{
						Price:     currentFactorPrice,
						Timestamp: currentFactorTs,
					})
				}
			}

			generated = append(generated, *currentMS)
		}
	}

	generated = append(generated, msl[len(msl)-1])

	return generated
}
