package cryptomarketutil

import (
	"math"
)

// CandlestickList represents a list of candlesticks
// defined by noise factor
type CandlestickList struct {
	noise *Variation
	// candlesticks contains the list of candlesticks
	// that aren't noisy;
	candlesticks    []Candlestick
	candlesticksLim int

	min Pricer
	max Pricer
}

func parseLimit(lim int) int {
	if lim == -1 {
		return math.MaxInt32
	}

	return lim
}

// NewCandlestickListWithoutNoise is the CandlestickList constructor
// without providing a noise level
func NewCandlestickListWithoutNoise(lim int) *CandlestickList {
	noiseless := &Variation{}
	return &CandlestickList{noise: noiseless, candlesticksLim: parseLimit(lim)}
}

// NewCandlestickList creates a new CandlestickList given variation levels and
// candle limit
func NewCandlestickList(noiseBounds Variation, lim int) *CandlestickList {
	bounds := FillSymmetricVariation(noiseBounds)

	return &CandlestickList{
		noise:           bounds,
		candlesticksLim: parseLimit(lim),
	}
}

// SetNoiseBounds sets the noise bounds for this candlesticklist
func (candlestickList *CandlestickList) SetNoiseBounds(noise Variation) {
	candlestickList.noise = FillSymmetricVariation(noise)
}

// GetNoiseBounds returns the noise bounds of this candlesticklist
func (candlestickList *CandlestickList) GetNoiseBounds() *Variation {
	return candlestickList.noise
}

// GetCandlesticks returns the list of candlesticks
func (candlestickList *CandlestickList) GetCandlesticks() []Candlestick {
	return candlestickList.candlesticks
}

// GetCandlestick returns the candlestick at the given index
func (candlestickList *CandlestickList) GetCandlestick(idx int) *Candlestick {
	return &candlestickList.candlesticks[idx]
}

// Len returns the number of candlesticks currently stored in this candlesticklist
func (candlestickList *CandlestickList) Len() int {
	return len(candlestickList.candlesticks)
}

// Limit reutrns the limit of this candlesticklist
func (candlestickList *CandlestickList) Limit() int {
	return candlestickList.candlesticksLim
}

// GetLastCandlestick returns the latest candlestick within the list of candlesticks
func (candlestickList *CandlestickList) GetLastCandlestick() *Candlestick {
	l := len(candlestickList.candlesticks)
	if l > 0 {
		return &candlestickList.candlesticks[l-1]
	}
	return nil
}

// GetLastConsolidatedCandlestick returns the latest candlestick that is actually freezed;
// Since the last existing candlestick within the list of candlesticks can be modified by
// adding new prices to this CandlestickList, the previous one is consolidated and guarantees
// that it won't change
func (candlestickList *CandlestickList) GetLastConsolidatedCandlestick() *Candlestick {
	l := len(candlestickList.candlesticks)
	if l > 1 {
		return &candlestickList.candlesticks[l-2]
	}
	return nil
}

// RemoveCandlestick removes the candlestick at the given index
func (candlestickList *CandlestickList) RemoveCandlestick(index int) *CandlestickList {
	tmp := []Candlestick{}
	firstSticks := candlestickList.candlesticks[:index]
	remaining := candlestickList.candlesticks[index+1:]
	tmp = append(tmp, firstSticks...)
	tmp = append(tmp, remaining...)
	candlestickList.candlesticks = tmp
	return candlestickList
}

// FilterCandlesticks filters the list of candles based on the given function
func (candlestickList *CandlestickList) FilterCandlesticks(fn func(c *Candlestick) bool) []Candlestick {
	accum := []Candlestick{}
	for _, el := range candlestickList.candlesticks {
		if fn(&el) {
			accum = append(accum, el)
		}
	}
	return accum
}

// GetIncreasingCandlesticks returns the list of candlest that have a positive variation
func (candlestickList *CandlestickList) GetIncreasingCandlesticks() []Candlestick {
	return candlestickList.FilterCandlesticks(func(c *Candlestick) bool { return c.IsIncreasing() })
}

// GetDecreasingCandlesticks returns the list of candlest that have a negative variation
func (candlestickList *CandlestickList) GetDecreasingCandlesticks() []Candlestick {
	return candlestickList.FilterCandlesticks(func(c *Candlestick) bool { return c.IsDecreasing() })
}

// AddPrice tries to add the new price to the latest candlestick; if it doesn't succeed in doing so,
// it checks for a new candlestick that has a bigger variation than the configured noise bounds
func (candlestickList *CandlestickList) AddPrice(price Pricer) *CandlestickListResp {
	lastCandlestick := candlestickList.GetLastCandlestick()
	if lastCandlestick == nil {
		return candlestickList.addFirstCandle(price)
	}
	added := lastCandlestick.AddPrice(price)

	if added {
		return &CandlestickListResp{
			IsBearish:   lastCandlestick.IsDecreasing(),
			IsBullish:   lastCandlestick.IsIncreasing(),
			IsNoisy:     candlestickList.isCandlestickNoisy(lastCandlestick),
			Candlestick: lastCandlestick,
		}
	}

	return candlestickList.addNewTrendPrice(price)
}

func (candlestickList *CandlestickList) isCandlestickNoisy(candlestick *Candlestick) bool {
	var noiseLevel float64
	if candlestick.IsDecreasing() {
		noiseLevel = candlestickList.noise.Down
	} else {
		noiseLevel = candlestickList.noise.Up
	}

	return math.Abs(candlestick.GetPercVariation()) < math.Abs(noiseLevel)
}

func (candlestickList *CandlestickList) addFirstCandle(price Pricer) *CandlestickListResp {

	if candlestickList.min == nil {
		candlestickList.min = price
	} else if candlestickList.min.GetPrice() > price.GetPrice() {
		candlestickList.min = price
	}

	if candlestickList.max == nil {
		candlestickList.max = price
	} else if candlestickList.max.GetPrice() < price.GetPrice() {
		candlestickList.max = price
	}

	firstCandle := candlestickList.getMinMaxCandle()
	isFirstCandleNoisy := candlestickList.isCandlestickNoisy(firstCandle)

	if !isFirstCandleNoisy {
		candlestickList.min = nil
		candlestickList.max = nil
		candlestickList.candlesticks = append(candlestickList.candlesticks, *firstCandle)
	}

	return &CandlestickListResp{
		IsNoisy:          isFirstCandleNoisy,
		IsNewCandlestick: !isFirstCandleNoisy,
		Candlestick:      firstCandle,
	}
}

func (candlestickList *CandlestickList) getMinMaxCandle() *Candlestick {
	var candle *Candlestick
	if candlestickList.min.GetTimestamp().Before(candlestickList.max.GetTimestamp()) {
		candle = NewCandlestick(candlestickList.min, candlestickList.max)
	} else {
		candle = NewCandlestick(candlestickList.max, candlestickList.min)
	}
	return candle
}

func (candlestickList *CandlestickList) addNewTrendPrice(price Pricer) *CandlestickListResp {
	newCandlestick := NewCandlestick(candlestickList.GetLastCandlestick().PriceTo, price)
	isNoisy := candlestickList.isCandlestickNoisy(newCandlestick)

	if !isNoisy {
		candlestickList.candlesticks = append(candlestickList.candlesticks, *newCandlestick)
	}

	// check the limit of candlesticks
	if len(candlestickList.candlesticks) > candlestickList.candlesticksLim {
		dif := len(candlestickList.candlesticks) - candlestickList.candlesticksLim
		candlestickList.candlesticks = candlestickList.candlesticks[dif:]
	}

	return &CandlestickListResp{
		IsBearish:        newCandlestick.IsDecreasing(),
		IsBullish:        newCandlestick.IsIncreasing(),
		IsNoisy:          isNoisy,
		IsNewCandlestick: !isNoisy,
		Candlestick:      newCandlestick,
	}
}
