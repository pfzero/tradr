package cryptomarketutil

import (
	"reflect"
	"testing"
	"time"
)

var candlestickPriceTime = time.Date(2018, 1, 1, 1, 1, 1, 1, time.Local)

func TestNewCandlestick(t *testing.T) {
	type args struct {
		priceFrom Pricer
		priceTo   Pricer
	}
	tests := []struct {
		name string
		args args
		want *Candlestick
	}{
		{
			name: "It should create a new candlestick",
			args: args{
				priceFrom: &testPricer{p: 10, t: candlestickPriceTime},
				priceTo:   &testPricer{p: 12, t: candlestickPriceTime},
			},
			want: &Candlestick{
				PriceFrom: &testPricer{p: 10, t: candlestickPriceTime},
				PriceTo:   &testPricer{p: 12, t: candlestickPriceTime},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewCandlestick(tt.args.priceFrom, tt.args.priceTo); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewCandlestick() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCandlestick_GetFixedVariation(t *testing.T) {
	tests := []struct {
		name string
		c    *Candlestick
		want float64
	}{
		{
			name: "It should get the fixed variation of the candlestick",
			c: &Candlestick{
				PriceFrom: &testPricer{p: 10, t: candlestickPriceTime},
				PriceTo:   &testPricer{p: 12, t: candlestickPriceTime},
			},
			want: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.c.GetFixedVariation(); got != tt.want {
				t.Errorf("Candlestick.GetFixedVariation() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCandlestick_GetPercVariation(t *testing.T) {
	tests := []struct {
		name string
		c    *Candlestick
		want float64
	}{
		{
			name: "It should get the percentage variation of the candlestick - positive",
			c: &Candlestick{
				PriceFrom: &testPricer{p: 10, t: candlestickPriceTime},
				PriceTo:   &testPricer{p: 12, t: candlestickPriceTime},
			},
			want: 0.2,
		},
		{
			name: "It should get the percentage variation of the candlestick - negative",
			c: &Candlestick{
				PriceFrom: &testPricer{p: 10, t: candlestickPriceTime},
				PriceTo:   &testPricer{p: 7.5, t: candlestickPriceTime},
			},
			want: -0.25,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.c.GetPercVariation(); got != tt.want {
				t.Errorf("Candlestick.GetPercVariation() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCandlestick_IsDecreasing(t *testing.T) {
	tests := []struct {
		name string
		c    *Candlestick
		want bool
	}{
		{
			name: "It should return true if the candlestick is decreasing",
			c: &Candlestick{
				PriceFrom: &testPricer{p: 10, t: candlestickPriceTime},
				PriceTo:   &testPricer{p: 9.99, t: candlestickPriceTime},
			},
			want: true,
		},
		{
			name: "It should return false if the candlestick is increasing",
			c: &Candlestick{
				PriceFrom: &testPricer{p: 10, t: candlestickPriceTime},
				PriceTo:   &testPricer{p: 10.01, t: candlestickPriceTime},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.c.IsDecreasing(); got != tt.want {
				t.Errorf("Candlestick.IsDecreasing() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCandlestick_IsIncreasing(t *testing.T) {
	tests := []struct {
		name string
		c    *Candlestick
		want bool
	}{
		{
			name: "It should return true if the candlestick is increasing",
			c: &Candlestick{
				PriceFrom: &testPricer{p: 10, t: candlestickPriceTime},
				PriceTo:   &testPricer{p: 10.01, t: candlestickPriceTime},
			},
			want: true,
		},
		{
			name: "It should return true if the candlestick is increasing (even if prices are equal)",
			c: &Candlestick{
				PriceFrom: &testPricer{p: 10, t: candlestickPriceTime},
				PriceTo:   &testPricer{p: 10, t: candlestickPriceTime},
			},
			want: true,
		},
		{
			name: "It should return false if the candlestick is decreasing",
			c: &Candlestick{
				PriceFrom: &testPricer{p: 10, t: candlestickPriceTime},
				PriceTo:   &testPricer{p: 9.9, t: candlestickPriceTime},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.c.IsIncreasing(); got != tt.want {
				t.Errorf("Candlestick.IsIncreasing() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCandlestick_IsPriceCompatible(t *testing.T) {
	type args struct {
		price Pricer
	}
	tests := []struct {
		name string
		c    *Candlestick
		args args
		want bool
	}{
		{
			name: "It should return true if the price is compatible with the candlesticks' increasing trend",
			c:    NewCandlestick(&testPricer{p: 1, t: candlestickPriceTime}, &testPricer{p: 2, t: candlestickPriceTime}),
			args: args{
				price: &testPricer{p: 3, t: candlestickPriceTime},
			},
			want: true,
		},
		{
			name: "It should return false if the price is incompatible with the candlesticks' increasing trend",
			c:    NewCandlestick(&testPricer{p: 1, t: candlestickPriceTime}, &testPricer{p: 2, t: candlestickPriceTime}),
			args: args{
				price: &testPricer{p: 1, t: candlestickPriceTime},
			},
			want: false,
		},
		{
			name: "It should return true if the price is compatible with the candlesticks' decreasing trend",
			c:    NewCandlestick(&testPricer{p: 1, t: candlestickPriceTime}, &testPricer{p: 0.5, t: candlestickPriceTime}),
			args: args{
				price: &testPricer{p: 0.4, t: candlestickPriceTime},
			},
			want: true,
		},
		{
			name: "It should return false if the price is incompatible with the candlesticks' decreasing trend",
			c:    NewCandlestick(&testPricer{p: 1, t: candlestickPriceTime}, &testPricer{p: 0.5, t: candlestickPriceTime}),
			args: args{
				price: &testPricer{p: 0.8, t: candlestickPriceTime},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.c.IsPriceCompatible(tt.args.price); got != tt.want {
				t.Errorf("Candlestick.IsPriceCompatible() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCandlestick_AddPrice(t *testing.T) {
	type args struct {
		price Pricer
	}
	tests := []struct {
		name string
		c    *Candlestick
		args args
		want bool
	}{
		{
			name: "It should return true if the price given was successfully added",
			c: &Candlestick{
				PriceFrom: &testPricer{p: 10, t: candlestickPriceTime},
				PriceTo:   &testPricer{p: 11, t: candlestickPriceTime},
			},
			args: args{
				price: &testPricer{p: 11.01, t: candlestickPriceTime},
			},
			want: true,
		},
		{
			name: "It should return false if the price given couldn't be added",
			c: &Candlestick{
				PriceFrom: &testPricer{p: 10, t: candlestickPriceTime},
				PriceTo:   &testPricer{p: 11, t: candlestickPriceTime},
			},
			args: args{
				price: &testPricer{p: 10.59, t: candlestickPriceTime},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.c.AddPrice(tt.args.price); got != tt.want {
				t.Errorf("Candlestick.AddPrice() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCandlestick_IsIdentical(t *testing.T) {
	type args struct {
		candlestick *Candlestick
	}
	tests := []struct {
		name string
		c    *Candlestick
		args args
		want bool
	}{
		{
			name: "It should return true if 2 candlesticks are the same",
			c: &Candlestick{
				PriceFrom: &testPricer{p: 10, t: candlestickPriceTime},
				PriceTo:   &testPricer{p: 11, t: candlestickPriceTime},
			},
			args: args{
				candlestick: &Candlestick{
					PriceFrom: &testPricer{p: 10, t: candlestickPriceTime},
					PriceTo:   &testPricer{p: 11, t: candlestickPriceTime},
				},
			},
			want: true,
		},
		{
			name: "It should return false if 2 candlesticks have different prices",
			c: &Candlestick{
				PriceFrom: &testPricer{p: 10, t: candlestickPriceTime},
				PriceTo:   &testPricer{p: 11, t: candlestickPriceTime},
			},
			args: args{
				candlestick: &Candlestick{
					PriceFrom: &testPricer{p: 10, t: candlestickPriceTime},
					PriceTo:   &testPricer{p: 12, t: candlestickPriceTime},
				},
			},
			want: false,
		},
		{
			name: "It should return false if 2 candlesticks have different timestamps for prices",
			c: &Candlestick{
				PriceFrom: &testPricer{p: 10, t: candlestickPriceTime},
				PriceTo:   &testPricer{p: 11, t: candlestickPriceTime},
			},
			args: args{
				candlestick: &Candlestick{
					PriceFrom: &testPricer{p: 10, t: candlestickPriceTime},
					PriceTo:   &testPricer{p: 11, t: time.Now()},
				},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.c.IsIdentical(tt.args.candlestick); got != tt.want {
				t.Errorf("Candlestick.IsIdentical() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCandlestick_IsZero(t *testing.T) {
	tests := []struct {
		name string
		c    *Candlestick
		want bool
	}{
		{
			name: "It should return true if the candlestick's prices are nil",
			c:    &Candlestick{},
			want: true,
		},
		{
			name: "It should return true if the candlestick's priceFrom is zero",
			c:    NewCandlestick(&testPricer{p: 0, t: time.Time{}}, &testPricer{p: 11, t: candlestickPriceTime}),
			want: true,
		},
		{
			name: "It should return false if the candlestick's priceFrom is non-zero",
			c:    NewCandlestick(&testPricer{p: 10, t: candlestickPriceTime}, &testPricer{p: 11, t: candlestickPriceTime}),
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.c.IsZero(); got != tt.want {
				t.Errorf("Candlestick.IsZero() = %v, want %v", got, tt.want)
			}
		})
	}
}
