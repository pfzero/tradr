package cryptomarketutil

import (
	"reflect"
	"testing"
	"time"

	"github.com/kylelemons/godebug/pretty"
)

type testPricer struct {
	p float64
	t time.Time
}

func (tp *testPricer) GetPrice() float64 {
	return tp.p
}

func (tp *testPricer) GetTimestamp() time.Time {
	return tp.t
}

func TestSyncPriceGroups(t *testing.T) {
	type args struct {
		groups [][]Pricer
	}
	n := time.Now()

	tg1 := time.Date(2015, time.January, 1, 0, 0, 0, 0, time.Local)
	tg2 := time.Date(2016, time.January, 1, 0, 0, 0, 0, time.Local)
	tg3 := time.Date(2017, time.January, 1, 0, 0, 0, 0, time.Local)
	tg4 := time.Date(2018, time.January, 1, 0, 0, 0, 0, time.Local)

	tests := []struct {
		name string
		args args
		want []map[int]Pricer
	}{
		{
			name: "It should return nil if the given group is nil",
			args: args{
				groups: nil,
			},
			want: nil,
		},
		{
			name: "It should return only the group of prices if only 1 group is given",
			args: func() args {
				return args{
					groups: [][]Pricer{
						[]Pricer{
							&testPricer{p: 1, t: n},
							&testPricer{p: 2, t: n},
							&testPricer{p: 3, t: n},
							&testPricer{p: 4, t: n},
						},
					},
				}
			}(),
			want: []map[int]Pricer{
				map[int]Pricer{0: &testPricer{p: 1, t: n}},
				map[int]Pricer{0: &testPricer{p: 2, t: n}},
				map[int]Pricer{0: &testPricer{p: 3, t: n}},
				map[int]Pricer{0: &testPricer{p: 4, t: n}},
			},
		},
		{
			name: "It should group prices based on their timestamp",
			args: func() args {
				return args{
					groups: [][]Pricer{
						[]Pricer{
							&testPricer{p: 1, t: tg1},
							&testPricer{p: 2, t: tg1},
							&testPricer{p: 3, t: tg1},
							&testPricer{p: 4, t: tg2},
							&testPricer{p: 5, t: tg2},
							&testPricer{p: 6, t: tg2},
							&testPricer{p: 7, t: tg3},
							&testPricer{p: 8, t: tg3},
							&testPricer{p: 9, t: tg3},
							&testPricer{p: 10, t: tg4},
							&testPricer{p: 11, t: tg4},
							&testPricer{p: 12, t: tg4},
							&testPricer{p: 11, t: n},
							&testPricer{p: 10, t: n},
							&testPricer{p: 9, t: n},
						},
						[]Pricer{
							&testPricer{p: 400, t: tg2},
							&testPricer{p: 500, t: tg2},
							&testPricer{p: 600, t: tg2},
							&testPricer{p: 700, t: tg3},
							&testPricer{p: 800, t: tg3},
							&testPricer{p: 900, t: tg3},
							&testPricer{p: 1000, t: tg4},
							&testPricer{p: 1100, t: tg4},
							&testPricer{p: 1200, t: tg4},
						},
						[]Pricer{
							&testPricer{p: 111, t: tg1},
							&testPricer{p: 311, t: tg1},
							&testPricer{p: 711, t: tg3},
							&testPricer{p: 811, t: tg3},
							&testPricer{p: 1011, t: tg4},
							&testPricer{p: 1111, t: tg4},
							&testPricer{p: 911, t: n},
						},
					},
				}
			}(),
			want: []map[int]Pricer{
				map[int]Pricer{
					0: &testPricer{p: 1, t: tg1},
					2: &testPricer{p: 111, t: tg1},
				},
				map[int]Pricer{
					0: &testPricer{p: 2, t: tg1},
					2: &testPricer{p: 311, t: tg1},
				},
				map[int]Pricer{
					0: &testPricer{p: 3, t: tg1},
					2: &testPricer{p: 311, t: tg1},
				},
				map[int]Pricer{
					0: &testPricer{p: 4, t: tg2},
					1: &testPricer{p: 400, t: tg2},
					2: &testPricer{p: 311, t: tg1},
				},
				map[int]Pricer{
					0: &testPricer{p: 5, t: tg2},
					1: &testPricer{p: 500, t: tg2},
					2: &testPricer{p: 311, t: tg1},
				},
				map[int]Pricer{
					0: &testPricer{p: 6, t: tg2},
					1: &testPricer{p: 600, t: tg2},
					2: &testPricer{p: 311, t: tg1},
				},
				map[int]Pricer{
					0: &testPricer{p: 7, t: tg3},
					1: &testPricer{p: 700, t: tg3},
					2: &testPricer{p: 711, t: tg3},
				},
				map[int]Pricer{
					0: &testPricer{p: 8, t: tg3},
					1: &testPricer{p: 800, t: tg3},
					2: &testPricer{p: 811, t: tg3},
				},
				map[int]Pricer{
					0: &testPricer{p: 9, t: tg3},
					1: &testPricer{p: 900, t: tg3},
					2: &testPricer{p: 811, t: tg3},
				},
				map[int]Pricer{
					0: &testPricer{p: 10, t: tg4},
					1: &testPricer{p: 1000, t: tg4},
					2: &testPricer{p: 1011, t: tg4},
				},
				map[int]Pricer{
					0: &testPricer{p: 11, t: tg4},
					1: &testPricer{p: 1100, t: tg4},
					2: &testPricer{p: 1111, t: tg4},
				},
				map[int]Pricer{
					0: &testPricer{p: 12, t: tg4},
					1: &testPricer{p: 1200, t: tg4},
					2: &testPricer{p: 1111, t: tg4},
				},
				map[int]Pricer{
					0: &testPricer{p: 11, t: n},
					1: &testPricer{p: 1200, t: tg4},
					2: &testPricer{p: 911, t: n},
				},
				map[int]Pricer{
					0: &testPricer{p: 10, t: n},
					1: &testPricer{p: 1200, t: tg4},
					2: &testPricer{p: 911, t: n},
				},
				map[int]Pricer{
					0: &testPricer{p: 9, t: n},
					1: &testPricer{p: 1200, t: tg4},
					2: &testPricer{p: 911, t: n},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := SyncPriceGroups(tt.args.groups); !reflect.DeepEqual(got, tt.want) {
				t.Error("SyncPriceGroups() got diff between got and want")
				parsedGot := parseSyncRespForComparison(got)
				parsedWant := parseSyncRespForComparison(tt.want)
				t.Error(pretty.Compare(parsedGot, parsedWant))
			}
		})
	}
}

func parseSyncRespForComparison(results []map[int]Pricer) []map[int]float64 {
	ret := make([]map[int]float64, len(results))

	for idx, el := range results {
		m := make(map[int]float64)
		for i, p := range el {
			m[i] = p.GetPrice()
		}
		ret[idx] = m
	}
	return ret
}
