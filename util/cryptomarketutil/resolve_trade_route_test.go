package cryptomarketutil

import (
	"reflect"
	"testing"
)

func TestNewPricedTradeRoute(t *testing.T) {
	type args struct {
		tr TradeRoute
		p  float64
	}
	tests := []struct {
		name string
		args args
		want *PricedTradeRoute
	}{
		{
			name: "It should create a new PricedTradeRoute",
			args: args{
				tr: TradeRoute{Base: "ETH", Quoted: "USD", Side: SideBuy},
				p:  200,
			},
			want: &PricedTradeRoute{
				TradeRoute: TradeRoute{Base: "ETH", Quoted: "USD", Side: SideBuy},
				Price:      200,
				Amount:     0,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewPricedTradeRoute(tt.args.tr, tt.args.p); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewPricedTradeRoute() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestResolveTradeRoute(t *testing.T) {
	type args struct {
		trs []PricedTradeRoute
	}
	tests := []struct {
		name string
		args args
		want []PricedTradeRoute
	}{
		{
			name: "It should specify how much to trade per each trade route: Buy-Buy",
			args: args{
				trs: []PricedTradeRoute{
					PricedTradeRoute{
						TradeRoute: TradeRoute{Base: "BCH", Quoted: "ETH", Side: SideBuy},
						Price:      2,
					},
					PricedTradeRoute{
						TradeRoute: TradeRoute{Base: "BTC", Quoted: "BCH", Side: SideBuy},
						Price:      2,
					},
				},
			},
			want: []PricedTradeRoute{
				PricedTradeRoute{
					TradeRoute: TradeRoute{Base: "BCH", Quoted: "ETH", Side: SideBuy},
					Price:      2,
					Amount:     0.5,
				},
				PricedTradeRoute{
					TradeRoute: TradeRoute{Base: "BTC", Quoted: "BCH", Side: SideBuy},
					Price:      2,
					Amount:     0.25,
				},
			},
		},
		{
			name: "It should specify how much to trade per each trade route: Sell-Buy",
			args: args{
				trs: []PricedTradeRoute{
					PricedTradeRoute{
						TradeRoute: TradeRoute{Base: "ETH", Quoted: "BCH", Side: SideSell},
						Price:      0.5,
					},
					PricedTradeRoute{
						TradeRoute: TradeRoute{Base: "BTC", Quoted: "BCH", Side: SideBuy},
						Price:      2,
					},
				},
			},
			want: []PricedTradeRoute{
				PricedTradeRoute{
					TradeRoute: TradeRoute{Base: "ETH", Quoted: "BCH", Side: SideSell},
					Price:      0.5,
					Amount:     0.5,
				},
				PricedTradeRoute{
					TradeRoute: TradeRoute{Base: "BTC", Quoted: "BCH", Side: SideBuy},
					Price:      2,
					Amount:     0.25,
				},
			},
		},
		{
			name: "It should specify how much to trade per each trade route: Buy-Sell",
			args: args{
				trs: []PricedTradeRoute{
					PricedTradeRoute{
						TradeRoute: TradeRoute{Base: "BCH", Quoted: "ETH", Side: SideBuy},
						Price:      2,
					},
					PricedTradeRoute{
						TradeRoute: TradeRoute{Base: "BCH", Quoted: "BTC", Side: SideSell},
						Price:      0.5,
					},
				},
			},
			want: []PricedTradeRoute{
				PricedTradeRoute{
					TradeRoute: TradeRoute{Base: "BCH", Quoted: "ETH", Side: SideBuy},
					Price:      2,
					Amount:     0.5,
				},
				PricedTradeRoute{
					TradeRoute: TradeRoute{Base: "BCH", Quoted: "BTC", Side: SideSell},
					Price:      0.5,
					Amount:     0.25,
				},
			},
		},
		{
			name: "It should specify how much to trade per each trade route: Sell-Sell",
			args: args{
				trs: []PricedTradeRoute{
					PricedTradeRoute{
						TradeRoute: TradeRoute{Base: "ETH", Quoted: "BCH", Side: SideSell},
						Price:      0.5,
					},
					PricedTradeRoute{
						TradeRoute: TradeRoute{Base: "BCH", Quoted: "BTC", Side: SideSell},
						Price:      0.5,
					},
				},
			},
			want: []PricedTradeRoute{
				PricedTradeRoute{
					TradeRoute: TradeRoute{Base: "ETH", Quoted: "BCH", Side: SideSell},
					Price:      0.5,
					Amount:     0.5,
				},
				PricedTradeRoute{
					TradeRoute: TradeRoute{Base: "BCH", Quoted: "BTC", Side: SideSell},
					Price:      0.5,
					Amount:     0.25,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ResolveTradeRoute(tt.args.trs); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ResolveTradeRoute() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetPriceVia(t *testing.T) {
	type args struct {
		trs []PricedTradeRoute
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{
			name: "It should return the price via the given priced trade routes",
			args: args{
				trs: []PricedTradeRoute{
					PricedTradeRoute{
						TradeRoute: TradeRoute{Base: "ETH", Quoted: "BCH", Side: SideSell},
						Price:      0.5,
					},
					PricedTradeRoute{
						TradeRoute: TradeRoute{Base: "BCH", Quoted: "BTC", Side: SideSell},
						Price:      0.5,
					},
					PricedTradeRoute{
						TradeRoute: TradeRoute{Base: "BTC", Quoted: "XMR", Side: SideSell},
						Price:      10,
					},
					PricedTradeRoute{
						TradeRoute: TradeRoute{Base: "ZBP", Quoted: "XMR", Side: SideBuy},
						Price:      0.5,
					},
				},
			},
			want: 5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetPriceVia(tt.args.trs); got != tt.want {
				t.Errorf("GetPriceVia() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_GetTradeRoutePrice(t *testing.T) {
	type args struct {
		ptr PricedTradeRoute
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{
			name: "it should give the price of the symbol based on the side of the trade route",
			args: args{
				ptr: PricedTradeRoute{
					TradeRoute: TradeRoute{Base: "BTC", Quoted: "ETH", Side: SideBuy},
					Price:      2,
				},
			},
			want: 0.5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetTradeRoutePrice(tt.args.ptr); got != tt.want {
				t.Errorf("symPrice() = %v, want %v", got, tt.want)
			}
		})
	}
}
