package cryptomarketutil

import (
	"reflect"
	"testing"
	"time"

	"github.com/kylelemons/godebug/pretty"

	"bitbucket.org/pfzero/tradr/util/num"
)

var priceTime = time.Date(2018, 1, 1, 1, 1, 1, 1, time.Local)

func TestNewPriceList(t *testing.T) {
	type args struct {
		lim               int
		distilledBufLimit int
		bounds            *Variation
	}
	tests := []struct {
		name string
		args args
		want *PriceList
	}{
		{
			name: "It should create a new price list",
			args: args{
				lim:               100,
				distilledBufLimit: 10,
				bounds: &Variation{
					Up: 0.25,
				},
			},
			want: &PriceList{
				limit:             100,
				distilledBuf:      []Pricer{},
				distilledBufLimit: 10,
				prices:            []Pricer{},
				bounds:            &Variation{Up: 0.25, Down: -0.2},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewPriceList(tt.args.lim, tt.args.distilledBufLimit, tt.args.bounds); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewPriceList() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPriceList_GetAll(t *testing.T) {
	tests := []struct {
		name string
		p    *PriceList
		want []Pricer
	}{
		{
			name: "It should return all prices",
			p:    NewPriceList(100, 10, &Variation{}),
			want: []Pricer{},
		},
		{
			name: "It should return all prices",
			p: func() *PriceList {
				PriceList := NewPriceList(100, 10, &Variation{})
				PriceList.prices = []Pricer{&PriceConversion{
					FromPrice: 10,
					ToPrice:   1,
				}}
				return PriceList
			}(),
			want: []Pricer{&PriceConversion{
				FromPrice: 10,
				ToPrice:   1,
			}},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.p.GetAll(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("PriceList.GetAll() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPriceList_AsFloats(t *testing.T) {
	tests := []struct {
		name string
		p    *PriceList
		want num.FloatsList
	}{
		{
			name: "It should transform the list to a list of only prices",
			p: func() *PriceList {
				p := NewPriceList(100, 10, &Variation{})
				p.prices = []Pricer{
					&testPricer{p: 1, t: priceTime},
					&testPricer{p: 1.5, t: priceTime},
					&testPricer{p: 2.5, t: priceTime},
					&testPricer{p: 3.5, t: priceTime},
				}
				return p
			}(),
			want: num.FloatsList{1, 1.5, 2.5, 3.5},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.p.AsFloats(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("PriceList.AsFloats() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPriceList_AsPercVars(t *testing.T) {
	tests := []struct {
		name string
		p    *PriceList
		want num.FloatsList
	}{
		{
			name: "It should transform the list of prices to a list of price percentage variations",
			p: func() *PriceList {
				p := NewPriceList(100, 10, &Variation{})
				p.prices = []Pricer{
					&testPricer{p: 1, t: priceTime},
					&testPricer{p: 2, t: priceTime},
					&testPricer{p: 4, t: priceTime},
					&testPricer{p: 2, t: priceTime},
					&testPricer{p: 1, t: priceTime},
					&testPricer{p: 1.5, t: priceTime},
				}
				return p
			}(),
			want: num.FloatsList{1, 1, -0.5, -0.5, 0.5},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.p.AsPercVars(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("PriceList.AsPercVars() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPriceList_AsVars(t *testing.T) {
	tests := []struct {
		name string
		p    *PriceList
		want num.FloatsList
	}{
		{
			name: "It should transform the list of prices to list of price diffs",
			p: func() *PriceList {
				p := NewPriceList(100, 10, &Variation{})
				p.prices = []Pricer{
					&testPricer{p: 1, t: priceTime},
					&testPricer{p: 2, t: priceTime},
					&testPricer{p: 4, t: priceTime},
					&testPricer{p: 2, t: priceTime},
					&testPricer{p: 1, t: priceTime},
					&testPricer{p: 1.5, t: priceTime},
				}
				return p
			}(),
			want: num.FloatsList{1, 2, -2, -1, 0.5},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.p.AsVars(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("PriceList.AsVars() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPriceList_Filter(t *testing.T) {
	type args struct {
		fn PriceListIterator
	}
	tests := []struct {
		name string
		p    *PriceList
		args args
		want *PriceList
	}{
		{
			name: "It should filter based on the given filter function",
			p: func() *PriceList {
				p := NewPriceList(100, 10, &Variation{})
				p.prices = []Pricer{
					&testPricer{p: 1, t: priceTime},
					&testPricer{p: 2, t: priceTime},
					&testPricer{p: 4, t: priceTime},
					&testPricer{p: 2, t: priceTime},
					&testPricer{p: 1, t: priceTime},
					&testPricer{p: 1.5, t: priceTime},
				}
				return p
			}(),
			args: args{
				fn: func(i int, price Pricer) bool {
					return price.GetPrice() >= 2
				},
			},
			want: func() *PriceList {
				p := NewPriceList(100, 10, &Variation{})
				p.prices = []Pricer{
					&testPricer{p: 2, t: priceTime},
					&testPricer{p: 4, t: priceTime},
					&testPricer{p: 2, t: priceTime},
				}
				return p
			}(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.p.Filter(tt.args.fn); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("PriceList.Filter() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPriceList_Find(t *testing.T) {
	type args struct {
		fn PriceListIterator
	}
	tests := []struct {
		name string
		p    *PriceList
		args args
		want Pricer
	}{
		{
			name: "It should find the price that satisfies the given function",
			p: func() *PriceList {
				p := NewPriceList(100, 10, &Variation{})
				p.prices = []Pricer{
					&testPricer{p: 1, t: priceTime},
					&testPricer{p: 2, t: priceTime},
					&testPricer{p: 4, t: priceTime},
					&testPricer{p: 2, t: priceTime},
					&testPricer{p: 1, t: priceTime},
					&testPricer{p: 1.5, t: priceTime},
				}
				return p
			}(),
			args: args{
				fn: func(i int, price Pricer) bool {
					return price.GetPrice() == 4
				},
			},
			want: &testPricer{
				p: 4, t: priceTime,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.p.Find(tt.args.fn); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("PriceList.Find() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPriceList_Min(t *testing.T) {
	tests := []struct {
		name string
		p    *PriceList
		want Pricer
	}{
		{
			name: "It should find the min price in the list",
			p: func() *PriceList {
				p := NewPriceList(100, 10, &Variation{})
				p.prices = []Pricer{
					&testPricer{p: 1, t: priceTime},
					&testPricer{p: 2, t: priceTime},
					&testPricer{p: 4, t: priceTime},
					&testPricer{p: 2, t: priceTime},
					&testPricer{p: 1, t: priceTime},
					&testPricer{p: 1.5, t: priceTime},
				}
				return p
			}(),
			want: &testPricer{p: 1, t: priceTime},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.p.Min(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("PriceList.Min() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPriceList_Max(t *testing.T) {
	tests := []struct {
		name string
		p    *PriceList
		want Pricer
	}{
		{
			name: "It should find the max price in list",
			p: func() *PriceList {
				p := NewPriceList(100, 10, &Variation{})
				p.prices = []Pricer{
					&testPricer{p: 1, t: priceTime},
					&testPricer{p: 2, t: priceTime},
					&testPricer{p: 4, t: priceTime},
					&testPricer{p: 2, t: priceTime},
					&testPricer{p: 1, t: priceTime},
					&testPricer{p: 1.5, t: priceTime},
				}
				return p

			}(),
			want: &testPricer{p: 4, t: priceTime},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.p.Max(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("PriceList.Max() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPriceList_Avg(t *testing.T) {
	tests := []struct {
		name string
		p    *PriceList
		want float64
	}{
		{
			name: "It should return the average price of the price list",
			p: func() *PriceList {
				p := NewPriceList(100, 10, &Variation{})
				p.prices = []Pricer{
					&testPricer{p: 1, t: priceTime},
					&testPricer{p: 2, t: priceTime},
					&testPricer{p: 3, t: priceTime},
					&testPricer{p: 4, t: priceTime},
				}
				return p

			}(),
			want: 2.5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.p.Avg(); got != tt.want {
				t.Errorf("PriceList.Avg() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPriceList_Add(t *testing.T) {
	type args struct {
		price Pricer
	}
	tests := []struct {
		name string
		p    *PriceList
		args args
		want *PriceList
	}{
		{
			name: "It should add the price to the list of prices",
			p: func() *PriceList {
				p := NewPriceList(100, 10, &Variation{})
				p.prices = []Pricer{
					&testPricer{p: 1, t: priceTime},
				}
				return p
			}(),
			args: args{
				price: &testPricer{p: 1.9, t: priceTime},
			},
			want: func() *PriceList {
				p := NewPriceList(100, 10, &Variation{})
				p.prices = []Pricer{
					&testPricer{p: 1, t: priceTime},
					&testPricer{p: 1.9, t: priceTime},
				}
				return p
			}(),
		},
		{
			name: "It should add the price to the list of prices and also check the limit of prices",
			p: func() *PriceList {
				p := NewPriceList(3, 1, &Variation{})
				p.prices = []Pricer{
					&testPricer{p: 1, t: priceTime},
					&testPricer{p: 2, t: priceTime},
					&testPricer{p: 3, t: priceTime},
					&testPricer{p: 4, t: priceTime},
				}
				return p
			}(),
			args: args{
				price: &testPricer{p: 4.9, t: priceTime},
			},
			want: func() *PriceList {
				p := NewPriceList(3, 1, &Variation{})
				p.prices = []Pricer{
					&testPricer{p: 3, t: priceTime},
					&testPricer{p: 4, t: priceTime},
					&testPricer{p: 4.9, t: priceTime},
				}
				return p
			}(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.p.Add(tt.args.price); !reflect.DeepEqual(got, tt.want) {
				t.Error("Got diff between got and want")
				t.Error(pretty.Compare(got, tt.want))
			}
		})
	}
}

func TestPriceList_AddDistilled(t *testing.T) {
	type args struct {
		price Pricer
	}
	tests := []struct {
		name string
		p    *PriceList
		args args
		want *PriceList
	}{
		{
			name: "It should add prices only if they fit in the configured bounds",
			p: func() *PriceList {
				p := NewPriceList(100, 10, &Variation{Up: 0.25})
				p.prices = []Pricer{
					&testPricer{p: 1, t: priceTime},
					&testPricer{p: 1.25, t: priceTime},
					&testPricer{p: 1.5, t: priceTime},
					&testPricer{p: 1.75, t: priceTime},
					&testPricer{p: 2, t: priceTime},
				}
				return p
			}(),
			args: args{
				price: &testPricer{p: 2.00 + (0.249 * 2.00), t: priceTime},
			},
			want: func() *PriceList {
				p := NewPriceList(100, 10, &Variation{Up: 0.25})
				p.prices = []Pricer{
					&testPricer{p: 1, t: priceTime},
					&testPricer{p: 1.25, t: priceTime},
					&testPricer{p: 1.5, t: priceTime},
					&testPricer{p: 1.75, t: priceTime},
					&testPricer{p: 2, t: priceTime},
					&testPricer{p: 2.00 + (0.249 * 2.00), t: priceTime},
				}
				return p
			}(),
		},
		{
			name: "It should add the price to distilled buffer if it doesn't fit the current list",
			p: func() *PriceList {
				p := NewPriceList(100, 10, &Variation{Up: 0.25})
				p.prices = []Pricer{
					&testPricer{p: 1, t: priceTime},
					&testPricer{p: 1.25, t: priceTime},
					&testPricer{p: 1.5, t: priceTime},
					&testPricer{p: 1.75, t: priceTime},
					&testPricer{p: 2, t: priceTime},
				}
				return p
			}(),
			args: args{
				price: &testPricer{p: 2.00 + (0.251 * 2.00), t: priceTime},
			},
			want: func() *PriceList {
				p := NewPriceList(100, 10, &Variation{Up: 0.25})
				p.prices = []Pricer{
					&testPricer{p: 1, t: priceTime},
					&testPricer{p: 1.25, t: priceTime},
					&testPricer{p: 1.5, t: priceTime},
					&testPricer{p: 1.75, t: priceTime},
					&testPricer{p: 2, t: priceTime},
				}
				p.distilledBuf = []Pricer{
					&testPricer{p: 2.00 + (0.251 * 2.00), t: priceTime},
				}
				return p
			}(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.p.AddDistilled(tt.args.price); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("PriceList.AddDistilled() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPriceList_Seed(t *testing.T) {
	type args struct {
		seed []Pricer
	}
	tests := []struct {
		name string
		p    *PriceList
		args args
		want *PriceList
	}{
		{
			name: "It should seed the list of prices with the given list",
			p:    NewPriceList(100, 10, &Variation{}),
			args: args{
				seed: []Pricer{
					&testPricer{p: 1, t: priceTime},
				},
			},
			want: func() *PriceList {
				p := NewPriceList(100, 10, &Variation{})
				p.prices = []Pricer{
					&testPricer{p: 1, t: priceTime},
				}
				return p
			}(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.p.Seed(tt.args.seed); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("PriceList.Seed() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPriceList_Distill(t *testing.T) {
	tests := []struct {
		name string
		p    *PriceList
		want *PriceList
	}{
		{
			name: "It should distill the prices based on the given bounds; The distilled prices should be redistilled if it reaches the distilled buffer limit",
			p: func() *PriceList {
				p := NewPriceList(100, 5, &Variation{Up: 0.25})
				p.Seed([]Pricer{
					&testPricer{p: 10, t: priceTime},
					&testPricer{p: 12.45, t: priceTime},
					&testPricer{p: 15, t: priceTime},
					&testPricer{p: 17, t: priceTime},
					&testPricer{p: 15, t: priceTime},
					&testPricer{p: 12.45, t: priceTime},
					&testPricer{p: 22, t: priceTime}, // ->
					&testPricer{p: 28, t: priceTime},
					&testPricer{p: 27.8, t: priceTime},
					&testPricer{p: 28.3, t: priceTime},
					&testPricer{p: 27.6, t: priceTime},
					&testPricer{p: 27, t: priceTime},
					&testPricer{p: 28, t: priceTime},
					&testPricer{p: 29, t: priceTime},
				})
				return p
			}(),
			want: func() *PriceList {
				p := NewPriceList(100, 5, &Variation{Up: 0.25})
				p.Seed([]Pricer{
					&testPricer{p: 10, t: priceTime},
					&testPricer{p: 12.45, t: priceTime},
					&testPricer{p: 15, t: priceTime},
					&testPricer{p: 17, t: priceTime},
					&testPricer{p: 15, t: priceTime},
					&testPricer{p: 12.45, t: priceTime},
					&testPricer{p: 28, t: priceTime},
					&testPricer{p: 27.8, t: priceTime},
					&testPricer{p: 28.3, t: priceTime},
					&testPricer{p: 27.6, t: priceTime},
					&testPricer{p: 27, t: priceTime},
					&testPricer{p: 28, t: priceTime},
					&testPricer{p: 29, t: priceTime},
				})
				return p
			}(),
		},
		{
			name: "It should distill the prices based on the given bounds",
			p: func() *PriceList {
				p := NewPriceList(100, 5, &Variation{Up: 0.25})
				p.Seed([]Pricer{
					&testPricer{p: 10, t: priceTime},
					&testPricer{p: 12.45, t: priceTime},
					&testPricer{p: 15, t: priceTime},
					&testPricer{p: 17, t: priceTime},
					&testPricer{p: 15, t: priceTime},
					&testPricer{p: 12.45, t: priceTime},
					&testPricer{p: 22, t: priceTime},   // ->
					&testPricer{p: 28, t: priceTime},   // ->
					&testPricer{p: 27.8, t: priceTime}, // ->
					&testPricer{p: 28.3, t: priceTime}, // ->
					&testPricer{p: 13.5, t: priceTime},
					&testPricer{p: 14.8, t: priceTime},
					&testPricer{p: 14, t: priceTime},
					&testPricer{p: 15, t: priceTime},
				})
				return p
			}(),
			want: func() *PriceList {
				p := NewPriceList(100, 5, &Variation{Up: 0.25})
				p.Seed([]Pricer{
					&testPricer{p: 10, t: priceTime},
					&testPricer{p: 12.45, t: priceTime},
					&testPricer{p: 15, t: priceTime},
					&testPricer{p: 17, t: priceTime},
					&testPricer{p: 15, t: priceTime},
					&testPricer{p: 12.45, t: priceTime},
					&testPricer{p: 13.5, t: priceTime},
					&testPricer{p: 14.8, t: priceTime},
					&testPricer{p: 14, t: priceTime},
					&testPricer{p: 15, t: priceTime},
				})
				return p
			}(),
		},
		{
			name: "It should distill the prices based on the given bounds #2",
			p: func() *PriceList {
				p := NewPriceList(100, 5, &Variation{Up: 0.25})
				p.Seed([]Pricer{
					&testPricer{p: 10, t: priceTime},
					&testPricer{p: 12.45, t: priceTime},
					&testPricer{p: 15, t: priceTime},
					&testPricer{p: 17, t: priceTime},
					&testPricer{p: 15, t: priceTime},
					&testPricer{p: 12.45, t: priceTime},
					&testPricer{p: 22, t: priceTime},
					&testPricer{p: 23, t: priceTime},
					&testPricer{p: 24, t: priceTime},
					&testPricer{p: 30.1, t: priceTime}, // ->
					&testPricer{p: 23, t: priceTime},
					&testPricer{p: 14.8, t: priceTime}, // -> (in to-be-distilled buffer)
					&testPricer{p: 14, t: priceTime},   // -> (in to-be-distilled buffer)
					&testPricer{p: 15, t: priceTime},   // -> (in to-be-distilled buffer)
				})
				return p
			}(),
			want: func() *PriceList {
				p := NewPriceList(100, 5, &Variation{Up: 0.25})
				p.Seed([]Pricer{
					&testPricer{p: 10, t: priceTime},
					&testPricer{p: 12.45, t: priceTime},
					&testPricer{p: 15, t: priceTime},
					&testPricer{p: 17, t: priceTime},
					&testPricer{p: 15, t: priceTime},
					&testPricer{p: 12.45, t: priceTime},
					&testPricer{p: 22, t: priceTime},
					&testPricer{p: 23, t: priceTime},
					&testPricer{p: 24, t: priceTime},
					&testPricer{p: 23, t: priceTime},
				})
				p.distilledBuf = []Pricer{
					&testPricer{p: 14.8, t: priceTime},
					&testPricer{p: 14, t: priceTime},
					&testPricer{p: 15, t: priceTime},
				}
				return p
			}(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.p.Distill(); !reflect.DeepEqual(got, tt.want) {
				t.Error("Difference between got and want")
				gotList := got.AsFloats()
				wantList := tt.want.AsFloats()
				t.Errorf("Got = %v; Want = %v", gotList, wantList)
			}
		})
	}
}

func TestPriceList_canAdd(t *testing.T) {
	type args struct {
		price Pricer
	}
	tests := []struct {
		name string
		p    *PriceList
		args args
		want bool
	}{
		{
			name: "It should return true if the price to be added is lower than the maximum allowed price",
			p: func() *PriceList {
				p := NewPriceList(100, 5, &Variation{Up: 0.25})
				p.Seed([]Pricer{
					&testPricer{p: 10, t: priceTime},
				})
				return p
			}(),
			args: args{
				price: &testPricer{p: 12.5},
			},
			want: true,
		},
		{
			name: "It should return true if the price to be added is higher than the minimum allowed price",
			p: func() *PriceList {
				p := NewPriceList(100, 5, &Variation{Up: 0.25 /* , Down: -0.2 */})
				p.Seed([]Pricer{
					&testPricer{p: 10, t: priceTime},
				})
				return p
			}(),
			args: args{
				price: &testPricer{p: 8},
			},
			want: true,
		},
		{
			name: "It should return false if the price is lower than min accepted price",
			p: func() *PriceList {
				p := NewPriceList(100, 5, &Variation{Up: 0.25 /* , Down: -0.2 */})
				p.Seed([]Pricer{
					&testPricer{p: 10, t: priceTime},
				})
				return p
			}(),
			args: args{
				price: &testPricer{p: 7.99},
			},
			want: false,
		},
		{
			name: "It should return false if the price is higher than max accepted price",
			p: func() *PriceList {
				p := NewPriceList(100, 5, &Variation{Up: 0.25 /* , Down: -0.2 */})
				p.Seed([]Pricer{
					&testPricer{p: 10, t: priceTime},
				})
				return p
			}(),
			args: args{
				price: &testPricer{p: 12.51},
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.p.canAdd(tt.args.price); got != tt.want {
				t.Errorf("PriceList.canAdd() = %v, want %v", got, tt.want)
			}
		})
	}
}
