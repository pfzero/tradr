package cryptomarketutil

import (
	"math"
	"time"
)

// GetPriceVariation returns the price variation of the given
// 2 prices
func GetPriceVariation(priceFrom, priceTo float64) float64 {
	return (priceTo - priceFrom) / priceFrom
}

// GetAmortizedChance returns the variation needed in order to match the
// desired variation taken into consideration a noise level;
//
// eg. getAmortizedChance(-0.25, 0.05) -> will give the decrease needed
// in order to match the -0.25 target including noise; In this case
// it's going to be: -0.2857
//
// GetAmortizedChance(0.25, -0.1) ->  0.388
func GetAmortizedChance(targetChance float64, noise float64) float64 {
	// the formula was deducted on paper;
	return (1+targetChance)/(1+noise) - 1
}

// GetSymmetricDecrease returns the required decrease to match the same price
// eg. priceX increased with 0.25 to priceY; What is the percentage required
// to have priceY return to priceX? -> in this case 0.25*4/5 = 0.2
func GetSymmetricDecrease(increase float64) float64 {
	// the formula was deducted on paper;
	return increase / (increase + 1)
}

// GetSymmetricIncrease returns the invert of the above getSymmetricDecrease
func GetSymmetricIncrease(decrease float64) float64 {
	// the invert of the above decrease;
	return decrease / (1 - decrease)
}

// GetSymmetricVariation converts a partial variation to
// a symmetric - complete variation
// eg. getSymmetricVariation({up: 0.25}) -> {up: 0.25, down: -this.getSymmetricDecrease(0.25)}
func GetSymmetricVariation(
	variation Variation,
) *Variation {
	if variation.GetUpVariation() == 0 && variation.GetDownVariation() == 0 {
		return &variation
	}

	var up, down float64

	if variation.GetUpVariation() != 0 {
		up = math.Abs(variation.GetUpVariation())
	}

	if variation.GetDownVariation() != 0 {
		down = math.Abs(variation.GetDownVariation())
	}

	if up == 0 {
		up = GetSymmetricIncrease(down)
	}

	if down == 0 {
		down = GetSymmetricDecrease(up)
	}

	ret := &Variation{Up: up, Down: -down}
	return ret
}

// FillSymmetricVariation fills the missing (zeroed) variation bounds;
// if either Down or Up are zero, they are filled symmetrically using the
// GetSymmetricVariation method
func FillSymmetricVariation(variation Variation) *Variation {
	if variation.GetUpVariation() == 0 || variation.GetDownVariation() == 0 {
		return GetSymmetricVariation(variation)
	}

	// if the noise bounds are already set (non-zero),
	// then just return it
	return &variation
}

// GetSymmetricVariationFrom returns the symmetric variation
// from the given variation input
func GetSymmetricVariationFrom(variation float64) *Variation {
	rawVariation := &Variation{}
	if variation > 0 {
		rawVariation.Up = variation
	} else {
		rawVariation.Down = variation
	}
	return FillSymmetricVariation(*rawVariation)
}

// GetPercentile returns the Nth percentile from the given
// list of (sorted) numbers;
// eg. getPercentile([1:10], 0.9) -> 9
// getPercentile([1:100], 0.62) -> 62
func GetPercentile(sortedNums []float64, percentile float64) float64 {
	if percentile < 0 {
		return sortedNums[0]
	}
	if percentile > 1 {
		return sortedNums[len(sortedNums)-1]
	}

	lastIdx := float64(len(sortedNums) - 1)
	idx := int(math.Floor(lastIdx * percentile))
	return sortedNums[idx]
}

func getClosestEntryIndexChronologically(chronoSortedEntries []Timestamped, l, h int, t time.Time) int {

	if len(chronoSortedEntries) == 0 {
		return -1
	}

	if h == l {
		return l
	}

	var first, half, last int
	first = l
	last = h
	half = (first + last) / 2

	firstDiff := chronoSortedEntries[first].GetTimestamp().Sub(t)
	halfDiff := chronoSortedEntries[half].GetTimestamp().Sub(t)
	lastDiff := chronoSortedEntries[last].GetTimestamp().Sub(t)

	// if the given slice of prices doesn't contain
	// our timestamp (ie. timestamp can't be found between the prices)
	// then we simply return the closest index
	if firstDiff > 0 {
		return first
	}
	if lastDiff < 0 {
		return last
	}

	// check if any of the 3 points matches exactly
	// our timestamp or the price is out of our bounds
	if firstDiff == 0 {
		return first
	} else if halfDiff == 0 {
		return half
	} else if lastDiff == 0 {
		return last
	}

	// if there are 2 elements left only,
	// prevent infinite loops by comparing the 2
	// dates; always return the price that is before
	// our given time
	if h-1 == l {
		if firstDiff < lastDiff {
			return first
		}
		return last
	}

	// now search into the most relevant slice
	if halfDiff > 0 {
		return getClosestEntryIndexChronologically(chronoSortedEntries, first, half, t)
	}

	return getClosestEntryIndexChronologically(chronoSortedEntries, half, last, t)
}

// GetClosestEntryIndexChronologically returns the index of the price within the list that
// is the closest to the given timestamp
// The fn assumes that the list of prices given
// It implements binary search for the closest price by timestamp
func GetClosestEntryIndexChronologically(chronoSortedEntries []Timestamped, t time.Time) int {
	return getClosestEntryIndexChronologically(chronoSortedEntries, 0, len(chronoSortedEntries)-1, t)
}
