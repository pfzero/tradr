package cryptomarketutil

import (
	"math"
)

// PricedTradeRoute adds pricing information to each trade route
type PricedTradeRoute struct {
	TradeRoute TradeRoute
	Price      float64
	Amount     float64
}

// NewPricedTradeRoute creates a new priced trade route
func NewPricedTradeRoute(tr TradeRoute, p float64) *PricedTradeRoute {
	return &PricedTradeRoute{TradeRoute: tr, Price: p}
}

// ResolveTradeRoute resolves the given priced trade route; It assumes
// the amount to trade through the route is 1;
func ResolveTradeRoute(trs []PricedTradeRoute) []PricedTradeRoute {
	var lastAmount float64
	for i := 0; i < len(trs); i++ {
		if lastAmount == 0 {
			lastAmount = GetTradeRoutePrice(trs[i])
			trs[i].Amount = lastAmount
			continue
		}
		trs[i].Amount = lastAmount * GetTradeRoutePrice(trs[i])
	}
	return trs
}

// GetPriceVia returns the final price given a list of trade routes
func GetPriceVia(trs []PricedTradeRoute) float64 {
	var p float64 = 1

	if len(trs) == 0 {
		return math.NaN()
	}

	for i := 0; i < len(trs); i++ {
		step := trs[i]
		stepPrice := GetTradeRoutePrice(step)
		p = p * stepPrice
	}

	return p
}

// GetTradeRoutePrice returns the price for the given
// priced trade route
func GetTradeRoutePrice(ptr PricedTradeRoute) float64 {
	if ptr.TradeRoute.Side == SideBuy {
		return 1 / ptr.Price
	}
	return ptr.Price
}
