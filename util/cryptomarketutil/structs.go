package cryptomarketutil

import (
	"time"
)

// Timestamped describes an object that has
// a timestamp attached to it
type Timestamped interface {
	GetTimestamp() time.Time
}

// Valued describes an object that has a value
// attached to it (eg. price)
type Valued interface {
	GetValue() float64
}

// TimestampedValue describes an object that has both
// a value and a timestamp attached to it
type TimestampedValue interface {
	Timestamped
	Valued
}

// Pricer is an interface that describes all the things
// that have a price, including the date at which the
// price was recorded
// ToDO: Refactor everything to use Timestamped, Valuer
// or TimestampedValuer
type Pricer interface {
	GetPrice() float64
	GetTimestamp() time.Time
}

// CandlestickListResp is the datastructure used
// as response to adding a new price
type CandlestickListResp struct {
	IsNoisy          bool
	IsBullish        bool
	IsBearish        bool
	IsNewCandlestick bool
	Candlestick      *Candlestick
}

// SimplePrice is a data structure that
// holds the values for a price; it also implements
// the Pricer interface
type SimplePrice struct {
	Price     float64
	Timestamp time.Time
}

// GetPrice returns the price
func (sp *SimplePrice) GetPrice() float64 {
	return sp.Price
}

// GetTimestamp returns the timestamp
func (sp *SimplePrice) GetTimestamp() time.Time {
	return sp.Timestamp
}

// Variation represents variation levels
// presented in a stream of numbers (prices)
type Variation struct {
	// Up & Down represents the variation percentage between
	// two consecutive data points in time
	Up   float64
	Down float64
}

// GetUpVariation returns the up variation
func (v *Variation) GetUpVariation() float64 {
	return v.Up
}

// GetDownVariation returns the down variation
func (v *Variation) GetDownVariation() float64 {
	return v.Down
}

// IsZero checks wether this instance has zeroed fields
func (v *Variation) IsZero() bool {
	return v.Up == 0 && v.Down == 0
}
