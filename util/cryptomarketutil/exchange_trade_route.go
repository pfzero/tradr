package cryptomarketutil

import (
	"fmt"
	"sync"
)

const (
	// SideBuy represents the buy action
	// for a trade route
	SideBuy = "BUY"
	// SideSell represents the sell action
	// for a trade route
	SideSell = "SELL"
)

func impossibleTradeRoute(e, fsym, tsym string) error {
	return fmt.Errorf(`Impossible to trade from "%s" to "%s" on exchange "%s"`, fsym, tsym, e)
}

// TradeRoute represents a trade route
type TradeRoute struct {
	// non-aliased version
	Base   string
	Quoted string

	// aliased version
	ExchangeBase   string
	ExchangeQuoted string
	ExchangeSymbol string

	Side string
}

// ExchangeTradeRoutes is a helper data-structure that
// can compute trading routes between various pairs on an exchange
// that doesn't necessarily support direct pairing for the given symbols;
// eg. an exchange might have ETH_BTC & BTC_USDT, and thus this data-structure
// can give trade route from ETH-USDT (via BTC):
// 1. {PAIR: "ETHBTC", ACTION:"SELL"}
// 2. {PAIR: "BTCUSDT", ACTION:"SELL"}
// or if the exchange has pairs: ETH_BTC & USDT_BTC:
// 1. {PAIR: "ETHBTC", ACTION: "SELL"}
// 2. {PAIR: "USDTBTC", ACTION: "BUY"}
type ExchangeTradeRoutes struct {
	eName string

	exchangeAliases *ExchangeAssetAlias
	preferredAssets *ExchangePreferredAssets

	supportedPairsL sync.Mutex

	// base_asset:quoted_asset
	// eg. [eth][btc]="ETHBTC"
	supportedPairs map[string]map[string]string
}

// NewExchangeTradeRoutes creates a new instance of exchange trade routes
func NewExchangeTradeRoutes(exchangeName string, exchangeAliases *ExchangeAssetAlias, preferredAssets *ExchangePreferredAssets) *ExchangeTradeRoutes {
	return &ExchangeTradeRoutes{
		eName:           exchangeName,
		exchangeAliases: exchangeAliases,
		preferredAssets: preferredAssets,
		supportedPairs:  make(map[string]map[string]string),
	}
}

// SetPairs sets the list of supported trading pairs for the given exchange
func (etr *ExchangeTradeRoutes) SetPairs(pairs map[string]map[string]string) *ExchangeTradeRoutes {
	etr.supportedPairsL.Lock()
	defer etr.supportedPairsL.Unlock()

	etr.supportedPairs = pairs
	return etr
}

// AddPair adds a new supported trading pair
func (etr *ExchangeTradeRoutes) AddPair(fsym, tsym, symbol string) *ExchangeTradeRoutes {
	etr.supportedPairsL.Lock()
	defer etr.supportedPairsL.Unlock()
	etr.set(fsym, tsym, symbol)
	return etr
}

// GetExchangeTradeRoute returns the list of trades necessary to exchange from the given
// tsym to tsym on the given exchange
func (etr *ExchangeTradeRoutes) GetExchangeTradeRoute(fsym, tsym string) ([]TradeRoute, error) {
	etr.supportedPairsL.Lock()
	supported := etr.getCopyOfPairs()
	etr.supportedPairsL.Unlock()

	// check if there's direct trade route between the 2 symbols
	tradeRoute, ok := etr.getTradeRoute(supported, fsym, tsym)

	if ok {
		return []TradeRoute{*tradeRoute}, nil
	}

	// now check if there are intermediate routes via preferred assets
	toRet := []TradeRoute{}
	preferredAssets := etr.preferredAssets.GetPreferredAssetsForExchange(etr.eName)

	for _, preferredAsset := range preferredAssets {
		sym := preferredAsset.GetSymbol()
		first, firstOK := etr.getTradeRoute(supported, fsym, sym)
		second, secondOK := etr.getTradeRoute(supported, sym, tsym)
		if firstOK && secondOK {
			toRet = append(toRet, *first)
			toRet = append(toRet, *second)
			break
		}
	}

	// ToDO: Maybe also implement depth-first-search in order to get
	// the trade route independent from preferred assets
	if len(toRet) == 0 {
		return nil, impossibleTradeRoute(etr.eName, fsym, tsym)
	}

	return toRet, nil
}

// getCopyOfPairs makes a copy of the supported pairs on an exchange and returns
// the copy
func (etr *ExchangeTradeRoutes) getCopyOfPairs() map[string]map[string]string {
	supported := etr.supportedPairs
	cp := make(map[string]map[string]string)
	for k, v := range supported {
		_, ok := cp[k]
		if !ok {
			cp[k] = make(map[string]string)
		}

		cp[k] = v
	}

	return cp
}

func (etr *ExchangeTradeRoutes) set(fsym, tsym, symbol string) *ExchangeTradeRoutes {
	_, ok := etr.supportedPairs[fsym]
	if !ok {
		etr.supportedPairs[fsym] = make(map[string]string)
	}
	etr.supportedPairs[fsym][tsym] = symbol
	return etr
}

func (etr *ExchangeTradeRoutes) getTradeRoute(pairs map[string]map[string]string, fsym, tsym string) (*TradeRoute, bool) {

	fsymAlias := etr.exchangeAliases.GetAliasForAsset(etr.eName, fsym)
	tsymAlias := etr.exchangeAliases.GetAliasForAsset(etr.eName, tsym)

	fSymbol, fsymTradeOK := pairs[fsymAlias][tsymAlias]
	if fsymTradeOK {
		return &TradeRoute{
			Base:   fsym,
			Quoted: tsym,

			ExchangeBase:   fsymAlias,
			ExchangeQuoted: tsymAlias,
			ExchangeSymbol: fSymbol,

			Side: SideSell,
		}, true
	}
	tSymbol, tsymOk := pairs[tsymAlias][fsymAlias]
	if tsymOk {
		return &TradeRoute{
			Base:   tsym,
			Quoted: fsym,

			ExchangeBase:   tsymAlias,
			ExchangeQuoted: fsymAlias,
			ExchangeSymbol: tSymbol,

			Side: SideBuy,
		}, true
	}
	return nil, false
}
