package cryptomarketutil

import (
	"fmt"
	"time"
)

// MarketSnapshot holds a list of crypto market indexes
// like prices, timestamp, market-caps etc; Indexes are held
// relative to fiat-based currencies (eg. USD)
type MarketSnapshot struct {
	prices          map[string]map[string]Pricer
	globalMarketCap float64
}

// NewMarketSnapshot is the MarketSnapshot constructor
func NewMarketSnapshot() *MarketSnapshot {
	return &MarketSnapshot{
		prices: make(map[string]map[string]Pricer),
	}
}

// Set adds a new coin to the market snapshot
func (ms *MarketSnapshot) Set(coinSym, exchange string, price Pricer) {
	_, ok := ms.prices[exchange]
	if !ok {
		ms.prices[exchange] = make(map[string]Pricer)
	}
	ms.prices[exchange][coinSym] = price
}

// Get returns a new coin from the market snapshot
func (ms *MarketSnapshot) Get(coinSym, exchange string) Pricer {
	price, ok := ms.prices[exchange][coinSym]

	if !ok {
		return nil
	}

	return price
}

// HasAvailable checks wether the list of symbols are available in the current market snapshot
// for the given exchange
func (ms *MarketSnapshot) HasAvailable(exchange string, symbols ...string) bool {
	for _, sym := range symbols {
		_, ok := ms.prices[exchange][sym]
		if !ok {
			return false
		}
	}
	return true
}

// ListAvailable returns the list of available coins for each exchange
func (ms *MarketSnapshot) ListAvailable() map[string]map[string]struct{} {
	ret := make(map[string]map[string]struct{})
	for exchangeName, list := range ms.prices {
		ret[exchangeName] = make(map[string]struct{})
		for sym := range list {
			ret[exchangeName][sym] = struct{}{}
		}
	}
	return ret
}

// Convert returns the conversion price between 2 coins
func (ms *MarketSnapshot) Convert(fSym, tSym, exchange string) (Pricer, error) {
	fromPrice := ms.Get(fSym, exchange)
	toPrice := ms.Get(tSym, exchange)
	errNotFound := `Coin and exchange pair not found: "%s" - "%s"`

	if fromPrice == nil {
		return nil, fmt.Errorf(errNotFound, fSym, exchange)
	}

	if toPrice == nil {
		return nil, fmt.Errorf(errNotFound, fSym, exchange)
	}

	conversion := &PriceConversion{
		FSym:      fSym,
		TSym:      tSym,
		Exchange:  exchange,
		Timestamp: fromPrice.GetTimestamp(),
		FromPrice: fromPrice.GetPrice(),
		ToPrice:   toPrice.GetPrice(),
	}
	return conversion, nil
}

// GetTimestamp returns the timestamp of this market snapshot
func (ms *MarketSnapshot) GetTimestamp() time.Time {
	var timeStamp time.Time
	if len(ms.prices) == 0 {
		return timeStamp
	}

	for _, m := range ms.prices {
		if len(m) == 0 {
			continue
		}
		for _, p := range m {
			if p.GetTimestamp().After(timeStamp) {
				timeStamp = p.GetTimestamp()
			}
		}
	}

	return timeStamp
}

// IsZero checks wether the current market snapshot is
// zero
func (ms *MarketSnapshot) IsZero() bool {
	return ms.GetTimestamp().IsZero()
}

// GetGlobalMarketCap returns the global market cap of cryptocurrencies
func (ms *MarketSnapshot) GetGlobalMarketCap() float64 { return ms.globalMarketCap }
