package cryptomarketutil

import (
	"reflect"
	"testing"
)

func TestNewExchangeAssetAlias(t *testing.T) {
	tests := []struct {
		name string
		want *ExchangeAssetAlias
	}{
		{
			name: "It should return a new instance of ExchangeAssetAlias data structure",
			want: &ExchangeAssetAlias{
				aliases: make(map[string]map[string]string),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewExchangeAssetAlias(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewExchangeAssetAlias() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestExchangeAssetAlias_Save(t *testing.T) {
	type args struct {
		exchange string
		asset    string
		alias    string
	}
	tests := []struct {
		name string
		eaa  *ExchangeAssetAlias
		args args
		want *ExchangeAssetAlias
	}{
		{
			name: "It should save the insert the asset alias for the given exchange if doesn't exist",
			args: args{
				exchange: "Market",
				asset:    "MIOTA",
				alias:    "IOT",
			},
			eaa: NewExchangeAssetAlias(),
			want: &ExchangeAssetAlias{
				aliases: map[string]map[string]string{
					"Market": map[string]string{
						"MIOTA": "IOT",
					},
				},
			},
		},
		{
			name: "It should save update the asset alias for the given exchange if it exists",
			eaa: func() *ExchangeAssetAlias {
				eaa := NewExchangeAssetAlias()
				eaa.Save("Market", "MIOTA", "IOT")
				return eaa
			}(),
			args: args{
				exchange: "Market",
				asset:    "MIOTA",
				alias:    "IOTA",
			},
			want: &ExchangeAssetAlias{
				aliases: map[string]map[string]string{
					"Market": map[string]string{
						"MIOTA": "IOTA",
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.eaa.Save(tt.args.exchange, tt.args.asset, tt.args.alias); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ExchangeAssetAlias.Save() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestExchangeAssetAlias_Del(t *testing.T) {
	type args struct {
		exchange string
		asset    string
	}
	tests := []struct {
		name string
		eaa  *ExchangeAssetAlias
		args args
		want *ExchangeAssetAlias
	}{
		{
			name: "It should delete an existing alias",
			eaa: func() *ExchangeAssetAlias {
				eaa := NewExchangeAssetAlias()
				eaa.Save("Market", "MIOTA", "IOT")
				return eaa
			}(),
			args: args{
				exchange: "Market",
				asset:    "MIOTA",
			},
			want: &ExchangeAssetAlias{
				aliases: map[string]map[string]string{
					"Market": map[string]string{},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.eaa.Del(tt.args.exchange, tt.args.asset); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ExchangeAssetAlias.Del() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestExchangeAssetAlias_GetAliasForAsset(t *testing.T) {
	type args struct {
		exchange string
		asset    string
	}
	tests := []struct {
		name string
		eaa  *ExchangeAssetAlias
		args args
		want string
	}{
		{
			name: "It should return the asset alias for the specific exchange if found",
			eaa: func() *ExchangeAssetAlias {
				eaa := NewExchangeAssetAlias()
				eaa.Save("Market", "MIOTA", "IOT")
				return eaa
			}(),
			args: args{
				exchange: "Market",
				asset:    "MIOTA",
			},
			want: "IOT",
		},
		{
			name: "It should return the asset's name if no alias can be found",
			eaa: func() *ExchangeAssetAlias {
				eaa := NewExchangeAssetAlias()
				eaa.Save("Market", "MIOTA", "IOT")
				return eaa
			}(),
			args: args{
				exchange: "Market",
				asset:    "ADA",
			},
			want: "ADA",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.eaa.GetAliasForAsset(tt.args.exchange, tt.args.asset); got != tt.want {
				t.Errorf("ExchangeAssetAlias.GetAliasForAsset() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestExchangeAssetAlias_ensureExchangeNS(t *testing.T) {
	type args struct {
		exchange string
	}
	tests := []struct {
		name string
		eaa  *ExchangeAssetAlias
		args args
		want *ExchangeAssetAlias
	}{
		{
			name: "It should create a map for the given exchange",
			eaa:  NewExchangeAssetAlias(),
			args: args{exchange: "Market"},
			want: &ExchangeAssetAlias{
				aliases: map[string]map[string]string{
					"Market": map[string]string{},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.eaa.ensureExchangeNS(tt.args.exchange); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ExchangeAssetAlias.ensureExchangeNS() = %v, want %v", got, tt.want)
			}
		})
	}
}
