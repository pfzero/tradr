package num

import (
	"math"
)

// FIterator is used to iterate and filter a list of floats
type FIterator func(idx int, n float64) bool

// FCb is the callback function when iterating through the list
// without producing new results (side-effects iterations)
type FCb func(idx int, n float64)

// FloatsList represents a list of f64 numbers
type FloatsList []float64

// Len returns the length of the list
func (fl FloatsList) Len() int {
	return len(fl)
}

// Filter is used to filter the list of numbers
func (fl FloatsList) Filter(fn FIterator) FloatsList {
	accum := FloatsList{}
	fl.Each(func(i int, n float64) {
		if fn(i, n) {
			accum = append(accum, n)
		}
	})
	return accum
}

// Each takes a cb function which is called with each
// index and element within the list
func (fl FloatsList) Each(cb FCb) {
	for i := 0; i < len(fl); i++ {
		cb(i, fl[i])
	}
}

// Find returns the found number given the iterator; if not found,
// it returns 0 and -1
func (fl FloatsList) Find(fn FIterator) (float64, int) {
	for i := 0; i < len(fl); i++ {
		if fn(i, fl[i]) {
			return fl[i], i
		}
	}
	return 0, -1
}

// MinIdx returns the index of the smallest number in list;
// if the list is empty, it returns -1
func (fl FloatsList) MinIdx() int {
	if len(fl) == 0 {
		return -1
	}

	var m float64
	idx := -1
	for i := 0; i < len(fl); i++ {
		if idx == -1 {
			idx = i
			m = fl[i]
			continue
		}

		if fl[i] < m {
			m = fl[i]
			idx = i
		}
	}

	return idx
}

// Min returns the min value in list; if the list is empty,
// it returns math.NaN
func (fl FloatsList) Min() float64 {
	minIdx := fl.MinIdx()
	if minIdx == -1 {
		return math.NaN()
	}
	return fl[minIdx]
}

// MaxIdx returns the index of the biggest number in the list
func (fl FloatsList) MaxIdx() int {
	if len(fl) == 0 {
		return -1
	}

	var m float64
	idx := -1
	for i := 0; i < len(fl); i++ {
		if idx == -1 {
			idx = i
			m = fl[i]
			continue
		}

		if fl[i] > m {
			m = fl[i]
			idx = i
		}
	}
	return idx
}

// Max returns the max number in list
func (fl FloatsList) Max() float64 {
	maxIdx := fl.MaxIdx()
	if maxIdx == -1 {
		return math.NaN()
	}
	return fl[maxIdx]
}

// Avg returns the average of the list
func (fl FloatsList) Avg() float64 {
	var sum float64
	fl.Each(func(i int, n float64) {
		sum += n
	})
	return sum / float64(len(fl))
}

// Positive returns only the positive values
func (fl FloatsList) Positive() FloatsList {
	return fl.Filter(func(i int, n float64) bool {
		return n >= 0
	})
}

// Negative returns the list of negative values
func (fl FloatsList) Negative() FloatsList {
	return fl.Filter(func(i int, n float64) bool {
		return n < 0
	})
}
