package num

import (
	"math"
	"reflect"
	"testing"
)

func TestFloatsList_Len(t *testing.T) {
	tests := []struct {
		name string
		fl   FloatsList
		want int
	}{
		{
			name: "It should return the length of the list #0",
			fl:   FloatsList{},
			want: 0,
		},
		{
			name: "It should return the length of the list #5",
			fl:   FloatsList{1, 2, 3, 4, 5},
			want: 5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.fl.Len(); got != tt.want {
				t.Errorf("FloatsList.Len() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFloatsList_Filter(t *testing.T) {
	type args struct {
		fn FIterator
	}
	tests := []struct {
		name string
		fl   FloatsList
		args args
		want FloatsList
	}{
		{
			name: "It should filter the list based on the given function iterator #all",
			fl:   FloatsList{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
			args: args{
				fn: func(i int, n float64) bool {
					return true
				},
			},
			want: FloatsList{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
		},
		{
			name: "It should filter the list based on the given function iterator #some",
			fl:   FloatsList{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
			args: args{
				fn: func(i int, n float64) bool {
					return n >= 5
				},
			},
			want: FloatsList{5, 6, 7, 8, 9, 10},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.fl.Filter(tt.args.fn); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FloatsList.Filter() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFloatsList_Each(t *testing.T) {
	type args struct {
		cb FCb
	}

	type calledArg struct {
		idx    int
		number float64
	}

	fl := FloatsList{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	calledWith := []calledArg{}
	wantToBeCalledWith := []calledArg{
		calledArg{idx: 0, number: 1},
		calledArg{idx: 1, number: 2},
		calledArg{idx: 2, number: 3},
		calledArg{idx: 3, number: 4},
		calledArg{idx: 4, number: 5},
		calledArg{idx: 5, number: 6},
		calledArg{idx: 6, number: 7},
		calledArg{idx: 7, number: 8},
		calledArg{idx: 8, number: 9},
		calledArg{idx: 9, number: 10},
	}

	fl.Each(func(idx int, n float64) {
		calledWith = append(calledWith, calledArg{idx: idx, number: n})
	})

	if !reflect.DeepEqual(calledWith, wantToBeCalledWith) {
		t.Errorf("FloatsList.Each() called with: %v; Wanted to be called with %v", calledWith, wantToBeCalledWith)
	}
}

func TestFloatsList_Find(t *testing.T) {
	type args struct {
		fn FIterator
	}
	tests := []struct {
		name  string
		fl    FloatsList
		args  args
		want  float64
		want1 int
	}{
		{
			name: "It should return the number and index of the found item",
			fl:   FloatsList{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
			args: args{
				fn: func(idx int, n float64) bool {
					return n == 8
				},
			},
			want:  8,
			want1: 7,
		},
		{
			name: "It should return -1 as index when not found",
			fl:   FloatsList{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
			args: args{
				fn: func(idx int, n float64) bool {
					return n == 11
				},
			},
			want:  0,
			want1: -1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, got1 := tt.fl.Find(tt.args.fn)
			if got != tt.want {
				t.Errorf("FloatsList.Find() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("FloatsList.Find() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}

func TestFloatsList_MinIdx(t *testing.T) {
	tests := []struct {
		name string
		fl   FloatsList
		want int
	}{
		{
			name: "it should return the index of the min number in list",
			fl:   FloatsList{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
			want: 0,
		},
		{
			name: "it should return -1 if list is empty",
			fl:   FloatsList{},
			want: -1,
		},
		{
			name: "it should return the min index in list",
			fl:   FloatsList{1, 5, 7, 9, 3, 0},
			want: 5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.fl.MinIdx(); got != tt.want {
				t.Errorf("FloatsList.MinIdx() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFloatsList_Min(t *testing.T) {
	tests := []struct {
		name string
		fl   FloatsList
		want float64
	}{
		{
			name: "it should return the min number in list",
			fl:   FloatsList{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
			want: 1,
		},
		{
			name: "it should return math.NaN if list is empty",
			fl:   FloatsList{},
			want: math.NaN(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.fl.Min(); got != tt.want && !(math.IsNaN(got) && math.IsNaN(tt.want)) {
				t.Errorf("FloatsList.Min() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFloatsList_MaxIdx(t *testing.T) {
	tests := []struct {
		name string
		fl   FloatsList
		want int
	}{
		{
			name: "it should return the index of max number in the list",
			fl:   FloatsList{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
			want: 9,
		},
		{
			name: "it should return -1 if list is empty",
			fl:   FloatsList{},
			want: -1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.fl.MaxIdx(); got != tt.want {
				t.Errorf("FloatsList.MaxIdx() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFloatsList_Max(t *testing.T) {
	tests := []struct {
		name string
		fl   FloatsList
		want float64
	}{
		{
			name: "it should return the max number in list",
			fl:   FloatsList{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
			want: 10,
		},
		{
			name: "it should return math.NaN if list is empty",
			fl:   FloatsList{},
			want: math.NaN(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.fl.Max(); got != tt.want && !(math.IsNaN(got) && math.IsNaN(tt.want)) {
				t.Errorf("FloatsList.Max() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFloatsList_Avg(t *testing.T) {
	tests := []struct {
		name string
		fl   FloatsList
		want float64
	}{
		{
			name: "it should return the index of max number in the list",
			fl:   FloatsList{1, 2, 3, 4, 5, 6, 7, 8, 9, 10},
			want: 5.5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.fl.Avg(); got != tt.want {
				t.Errorf("FloatsList.Avg() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFloatsList_Positive(t *testing.T) {
	tests := []struct {
		name string
		fl   FloatsList
		want FloatsList
	}{
		{
			name: "it should return the list of positive numbers in list",
			fl:   FloatsList{-1, 0, 1, -2, 1, 2, -3, 1, 2, 3},
			want: FloatsList{0, 1, 1, 2, 1, 2, 3},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.fl.Positive(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FloatsList.Positive() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFloatsList_Negative(t *testing.T) {
	tests := []struct {
		name string
		fl   FloatsList
		want FloatsList
	}{
		{
			name: "it should return the list of positive numbers in list",
			fl:   FloatsList{-1, 0, 1, -2, 1, 2, -3, 1, 2, 3},
			want: FloatsList{-1, -2, -3},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.fl.Negative(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FloatsList.Negative() = %v, want %v", got, tt.want)
			}
		})
	}
}
