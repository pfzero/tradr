package num

import "testing"

func TestRoundInt(t *testing.T) {
	type args struct {
		f float64
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "It should round the given float 0.5 -> 1",
			args: args{
				f: 0.5,
			},
			want: 1,
		},
		{
			name: "It should round the given float 0.49 -> 0",
			args: args{
				f: 0.49,
			},
			want: 0,
		},
		{
			name: "It should round the given float -0.5 -> -1",
			args: args{
				f: -0.5,
			},
			want: -1,
		},
		{
			name: "It should round the given float -0.49 -> 0",
			args: args{
				f: -0.49,
			},
			want: -0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RoundInt(tt.args.f); got != tt.want {
				t.Errorf("RoundInt() = %v, want %v", got, tt.want)
			}
		})
	}
}
