package slice

import (
	"reflect"
)

// Reverse reverses (in-place) the elements
// of a list
func Reverse(input interface{}) {
	switch reflect.TypeOf(input).Kind() {
	case reflect.Slice:
		s := reflect.ValueOf(input)

		for i, j := 0, s.Len()-1; i < j; i, j = i+1, j-1 {
			elI := s.Index(i)
			elJ := s.Index(j)
			tmp := elI.Interface()
			elI.Set(elJ)
			elJ.Set(reflect.ValueOf(tmp))
		}
	}
}

// ReverseInts reverses a list of integers
func ReverseInts(s []int) {
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		s[i], s[j] = s[j], s[i]
	}
}

// ReverseFloat64s reverses a list of float64s
func ReverseFloat64s(s []float64) {
	for i, j := 0, len(s)-1; i < j; i, j = i+1, j-1 {
		s[i], s[j] = s[j], s[i]
	}
}
