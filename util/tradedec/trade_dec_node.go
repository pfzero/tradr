package tradedec

import "bitbucket.org/pfzero/tradr/util/tradeutil"

// DecisionNode represents a single logical unit that is
// capable to emit a signal based on two points in time;
// the points in our case are TradeContext(s)
type DecisionNode interface {
	GetName() string
	Exec(refCtx, ctx tradeutil.TradeContext) bool
	GetChildren() []DecisionNode
}

// DecisionNodeResolver is the signature of the fn that runs per each node
type DecisionNodeResolver func(refCtx, ctx tradeutil.TradeContext, node DecisionNode) bool
