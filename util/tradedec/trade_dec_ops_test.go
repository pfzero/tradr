package tradedec_test

import (
	"math/rand"
	"testing"
	"time"

	"bitbucket.org/pfzero/tradr/util/tradeutil"

	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"
	"bitbucket.org/pfzero/tradr/util/tradedec/testutil"

	"bitbucket.org/pfzero/tradr/util/tradedec"
)

const fsym = "TST"
const tsym = "USD"
const exchange = "TestGround"

const fillInterval = 30 * time.Second

const d = 24 * time.Hour
const w = 7 * d
const m = 30 * d
const y = 12 * m

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}

func TestPriceVariation(t *testing.T) {
	// Given
	node := tradedec.PriceVariation(fsym, tsym, exchange, 0.4)
	pts := `
		10->7->9->4->5.6->9->8->11->10.5->
		(14.01)->(14.5)->(18)->12
	`
	points := testutil.ParsePointSequence(pts, time.Now().Add(-d), time.Now())
	tt := prepareTableTest(0)
	tt.Prices = points

	refCtx := tt.MakeRefCtx(&testutil.Point{Price: 10, Time: time.Now().Add(-6 * d), Signal: false})
	fn := testutil.DecisionNodeFn(node, *refCtx)
	tt.Fn = fn

	// When
	results := tt.Run()

	// Then
	if len(results) > 0 {
		t.Errorf(`Expected no failure points but got %d`, len(results))
		t.Log("Logging errors")
		logFailurePoints(t, results)
	}
}

func TestPriceVariationFrom(t *testing.T) {
	node := tradedec.PriceVariationFrom(fsym, tsym, exchange, 0.4, 1)
	pts := `
		1.35->(10)->(7)->(9)->(4)->(5.6)->(9)->(8)
		(11)->(10.5)->(14.01)->(14.5)->(18)->
		(12)
	`
	points := testutil.ParsePointSequence(pts, time.Now().Add(-d), time.Now())

	tt := prepareTableTest(0)
	tt.Prices = points

	refCtx := tt.MakeRefCtx(&testutil.Point{Price: 10, Time: time.Now().Add(-6 * d), Signal: false})
	fn := testutil.DecisionNodeFn(node, *refCtx)
	tt.Fn = fn

	// When
	results := tt.Run()

	// Then
	if len(results) > 0 {
		t.Errorf(`Expected no failure points but got %d`, len(results))
		t.Log("Logging errors")
		logFailurePoints(t, results)
	}
}

func TestElapsed(t *testing.T) {
	node := tradedec.Elapsed(15 * d)
	points := []testutil.Point{
		testutil.Point{Price: 1, Time: time.Now().Add(-15 * d), Signal: false},
		testutil.Point{Price: 2, Time: time.Now().Add(-14 * d), Signal: false},
		testutil.Point{Price: 3, Time: time.Now().Add(-13 * d), Signal: false},
		testutil.Point{Price: 4, Time: time.Now().Add(-12 * d), Signal: false},
		testutil.Point{Price: 5, Time: time.Now().Add(-10 * d), Signal: false},
		testutil.Point{Price: 6, Time: time.Now().Add(-7 * d), Signal: false},
		testutil.Point{Price: 7, Time: time.Now().Add(-4 * d), Signal: false},
		testutil.Point{Price: 8, Time: time.Now().Add(-2 * d), Signal: false},
		testutil.Point{Price: 9, Time: time.Now().Add(-(d + time.Hour)), Signal: false},
		testutil.Point{Price: 10, Time: time.Now().Add(-2 * time.Hour), Signal: true},
		testutil.Point{Price: 11, Time: time.Now(), Signal: true},
	}

	tt := prepareTableTest(0)
	tt.Prices = points

	refCtx := tt.MakeRefCtx(&testutil.Point{Price: 0.5, Time: time.Now().Add(-16 * d), Signal: false})
	fn := testutil.DecisionNodeFn(node, *refCtx)
	tt.Fn = fn

	// When
	results := tt.Run()

	// Then
	if len(results) > 0 {
		t.Errorf(`Expected no failure points but got %d`, len(results))
		t.Log("Logging errors")
		logFailurePoints(t, results)
	}
}

func TestBeforeTime(t *testing.T) {
	node := tradedec.BeforeTime(time.Now().Add(-5 * d))
	points := []testutil.Point{
		testutil.Point{Price: 1, Time: time.Now().Add(-15 * d), Signal: true},
		testutil.Point{Price: 2, Time: time.Now().Add(-14 * d), Signal: true},
		testutil.Point{Price: 3, Time: time.Now().Add(-13 * d), Signal: true},
		testutil.Point{Price: 4, Time: time.Now().Add(-12 * d), Signal: true},
		testutil.Point{Price: 5, Time: time.Now().Add(-10 * d), Signal: true},
		testutil.Point{Price: 6, Time: time.Now().Add(-7 * d), Signal: true},
		testutil.Point{Price: 7, Time: time.Now().Add(-(5*d + time.Second)), Signal: true},
		testutil.Point{Price: 7, Time: time.Now().Add(-(4*d + 23*time.Hour + 59*time.Minute)), Signal: false},
		testutil.Point{Price: 8, Time: time.Now().Add(-2 * d), Signal: false},
		testutil.Point{Price: 9, Time: time.Now().Add(-(d + time.Hour)), Signal: false},
		testutil.Point{Price: 10, Time: time.Now().Add(-2 * time.Hour), Signal: false},
		testutil.Point{Price: 11, Time: time.Now(), Signal: false},
	}

	tt := prepareTableTest(0)
	tt.Prices = points

	refCtx := tt.MakeRefCtx(&testutil.Point{Price: 0.5, Time: time.Now(), Signal: false})
	fn := testutil.DecisionNodeFn(node, *refCtx)
	tt.Fn = fn

	// When
	results := tt.Run()

	// Then
	if len(results) > 0 {
		t.Errorf(`Expected no failure points but got %d`, len(results))
		t.Log("Logging errors")
		logFailurePoints(t, results)
	}
}

func TestAfterTime(t *testing.T) {
	node := tradedec.AfterTime(time.Now().Add(-5 * d))
	points := []testutil.Point{
		testutil.Point{Price: 1, Time: time.Now().Add(-15 * d), Signal: false},
		testutil.Point{Price: 2, Time: time.Now().Add(-14 * d), Signal: false},
		testutil.Point{Price: 3, Time: time.Now().Add(-13 * d), Signal: false},
		testutil.Point{Price: 4, Time: time.Now().Add(-12 * d), Signal: false},
		testutil.Point{Price: 5, Time: time.Now().Add(-10 * d), Signal: false},
		testutil.Point{Price: 6, Time: time.Now().Add(-7 * d), Signal: false},
		testutil.Point{Price: 7, Time: time.Now().Add(-(5*d + time.Second)), Signal: false},
		testutil.Point{Price: 7, Time: time.Now().Add(-(4*d + 23*time.Hour + 59*time.Minute)), Signal: true},
		testutil.Point{Price: 8, Time: time.Now().Add(-2 * d), Signal: true},
		testutil.Point{Price: 9, Time: time.Now().Add(-(d + time.Hour)), Signal: true},
		testutil.Point{Price: 10, Time: time.Now().Add(-2 * time.Hour), Signal: true},
		testutil.Point{Price: 11, Time: time.Now(), Signal: true},
	}

	tt := prepareTableTest(0)
	tt.Prices = points

	refCtx := tt.MakeRefCtx(&testutil.Point{Price: 0.5, Time: time.Now(), Signal: false})
	fn := testutil.DecisionNodeFn(node, *refCtx)
	tt.Fn = fn

	// When
	results := tt.Run()

	// Then
	if len(results) > 0 {
		t.Errorf(`Expected no failure points but got %d`, len(results))
		t.Log("Logging errors")
		logFailurePoints(t, results)
	}
}

func TestTrendFormedUp(t *testing.T) {
	node := tradedec.TrendFormed(fsym, tsym, exchange, 0.35)
	histoPts := `
	30 -> 28 -> 25 -> 21 ->
	23 -> 25 -> 29 ->
	26 -> 24 -> 22 -> 19 ->
	20 -> 22 -> 24 ->
	22 -> 18 -> 17 -> 14 -> 16 -> 11`
	pts := `
	10.5 -> 10 -> 9 -> 9.3 -> 9.4 -> 9 -> 8.8 -> 8.7 -> 8.3 -> 7.8 ->
	8 -> 9 -> 10 -> 10.5 -> 10.52 -> 
	(10.54) -> (10.55) -> (10.56) -> 10 -> 9.5 ->
	9 -> 7.5 -> 7 -> 6 -> 5 -> 6.5 ->
	(6.76) -> (7) -> (9) -> (10) -> 7`

	histoPoints := testutil.ParsePointSequence(histoPts, time.Now().Add(-14*d), time.Now().Add(-7*d))
	points := testutil.ParsePointSequence(pts, time.Now().Add(-6*d), time.Now())

	tt := prepareTableTest(0)
	tt.HistoPrices = histoPoints
	tt.Prices = points

	refCtx := tt.MakeRefCtx(&testutil.Point{Price: 0.5, Time: time.Now(), Signal: false})
	fn := testutil.DecisionNodeFn(node, *refCtx)
	tt.Fn = fn

	// When
	results := tt.Run()

	// Then
	if len(results) > 0 {
		t.Errorf(`Expected no failure points but got %d`, len(results))
		t.Log("Logging errors")
		logFailurePoints(t, results)
	}
}

func TestTrendFormedDown(t *testing.T) {
	node := tradedec.TrendFormed(fsym, tsym, exchange, -0.2)
	histoPts := `
	30 -> 28 -> 25 -> 21 ->
	23 -> 25 -> 29 ->
	26 -> 24 -> 22 -> 19 ->
	20 -> 22 -> 24 ->
	22 -> 18 -> 17 -> 14 -> 16 -> 11`
	pts := `
	14 -> 12 -> (11.2) -> (11.1) -> (8) -> 12 ->
	10.4 -> 9.601 -> (9.6) -> (9.59) -> (9)`

	histoPoints := testutil.ParsePointSequence(histoPts, time.Now().Add(-14*d), time.Now().Add(-7*d))
	points := testutil.ParsePointSequence(pts, time.Now().Add(-6*d), time.Now())

	tt := prepareTableTest(0)
	tt.HistoPrices = histoPoints
	tt.Prices = points

	refCtx := tt.MakeRefCtx(&testutil.Point{Price: 0.5, Time: time.Now(), Signal: false})
	fn := testutil.DecisionNodeFn(node, *refCtx)
	tt.Fn = fn

	// When
	results := tt.Run()

	// Then
	if len(results) > 0 {
		t.Errorf(`Expected no failure points but got %d`, len(results))
		t.Log("Logging errors")
		logFailurePoints(t, results)
	}
}

func TestTrendChange(t *testing.T) {
	candlestickListID := tradeutil.CandlestickListID{
		FSym:      fsym,
		TSym:      tsym,
		Exchange:  exchange,
		Variation: cryptomarketutil.Variation{Up: 0.05},
	}

	node := tradedec.TrendChange(candlestickListID)
	histoPts := `
	30 -> 28 -> 25 -> 21 ->
	23 -> 25 -> 29 ->
	26 -> 24 -> 22 -> 19 ->
	20 -> 22 -> 24 ->
	22 -> 18 -> 17 -> 14 -> 16 -> 11`
	pts := `
	10.5 -> 10 -> 9 -> 9.3 -> 9.4 -> 9 -> 8.8 -> 8.7 -> 8.3 -> 7.8 ->
	8 -> (8.2) -> 9 -> 10 -> 10.5 -> 10.52 -> 
	10.54 -> 10.55 -> (10) -> 9.5 ->
	9 -> 7.5 -> 7 -> 6 -> 5 -> (5.25) -> 5.8 -> 6 -> 6.4 ->
	6.76 -> 7 -> 9 -> 10 -> (9.523)`

	histoPoints := testutil.ParsePointSequence(histoPts, time.Now().Add(-14*d), time.Now().Add(-7*d))
	points := testutil.ParsePointSequence(pts, time.Now().Add(-6*d), time.Now())

	tt := prepareTableTest(0)
	tt.HistoPrices = histoPoints
	tt.Prices = points

	refCtx := tt.MakeRefCtx(&testutil.Point{Price: 0.5, Time: time.Now(), Signal: false})
	fn := testutil.DecisionNodeFn(node, *refCtx)
	tt.Fn = fn

	// When
	results := tt.Run()

	// Then
	if len(results) > 0 {
		t.Errorf(`Expected no failure points but got %d`, len(results))
		t.Log("Logging errors")
		logFailurePoints(t, results)
	}
}

func TestTrendTypeIncreasing(t *testing.T) {
	candlestickListID := tradeutil.CandlestickListID{
		FSym:      fsym,
		TSym:      tsym,
		Exchange:  exchange,
		Variation: cryptomarketutil.Variation{Up: 0.05},
	}

	node := tradedec.TrendType(candlestickListID, true)
	histoPts := `
	30 -> 28 -> 25 -> 21 ->
	23 -> 25 -> 29 ->
	26 -> 24 -> 22 -> 19 ->
	20 -> 22 -> 24 ->
	22 -> 18 -> 17 -> 14 -> 16 -> 11`
	pts := `
	10.5 -> 10 -> 9 -> 9.3 -> 9.4 -> 9 -> 8.8 -> 8.7 -> 8.3 -> 7.8 ->
	8 -> (8.2) -> (9) -> (10) -> (10.5) -> (10.52) -> 
	(10.54) -> (10.55) -> 10 -> 9.5 ->
	9 -> 7.5 -> 7 -> 6 -> 5 -> (5.25) -> (5.8) -> (6) -> (6.4) ->
	(6.76) -> (7) -> (9) -> (10) -> (9.525) -> 9.523`

	histoPoints := testutil.ParsePointSequence(histoPts, time.Now().Add(-14*d), time.Now().Add(-7*d))
	points := testutil.ParsePointSequence(pts, time.Now().Add(-6*d), time.Now())

	tt := prepareTableTest(0)
	tt.HistoPrices = histoPoints
	tt.Prices = points

	refCtx := tt.MakeRefCtx(&testutil.Point{Price: 0.5, Time: time.Now(), Signal: false})
	fn := testutil.DecisionNodeFn(node, *refCtx)
	tt.Fn = fn

	// When
	results := tt.Run()

	// Then
	if len(results) > 0 {
		t.Errorf(`Expected no failure points but got %d`, len(results))
		t.Log("Logging errors")
		logFailurePoints(t, results)
	}
}

func TestTrendTypeDecreasing(t *testing.T) {
	candlestickListID := tradeutil.CandlestickListID{
		FSym:      fsym,
		TSym:      tsym,
		Exchange:  exchange,
		Variation: cryptomarketutil.Variation{Up: 0.05},
	}

	node := tradedec.TrendType(candlestickListID, false)
	histoPts := `
	30 -> 28 -> 25 -> 21 ->
	23 -> 25 -> 29 ->
	26 -> 24 -> 22 -> 19 ->
	20 -> 22 -> 24 ->
	22 -> 18 -> 17 -> 14 -> 16 -> 11`
	pts := `
	(10.5) -> (10) -> (9) -> (9.3) -> (9.4) -> (9) -> (8.8) -> (8.7) -> (8.3) -> (7.8) ->
	(8) -> 8.2 -> 9 -> 10 -> 10.5 -> 10.52 -> 
	10.54 -> 10.55 -> (10) -> (9.5) ->
	(9) -> (7.5) -> (7) -> (6) -> (5) -> 5.25 -> 5.8 -> 6 -> 6.4 ->
	6.76 -> 7 -> 9 -> 10 -> 9.525 -> (9.523)`

	histoPoints := testutil.ParsePointSequence(histoPts, time.Now().Add(-14*d), time.Now().Add(-7*d))
	points := testutil.ParsePointSequence(pts, time.Now().Add(-6*d), time.Now())

	tt := prepareTableTest(0)
	tt.HistoPrices = histoPoints
	tt.Prices = points

	refCtx := tt.MakeRefCtx(&testutil.Point{Price: 0.5, Time: time.Now(), Signal: false})
	fn := testutil.DecisionNodeFn(node, *refCtx)
	tt.Fn = fn

	// When
	results := tt.Run()

	// Then
	if len(results) > 0 {
		t.Errorf(`Expected no failure points but got %d`, len(results))
		t.Log("Logging errors")
		logFailurePoints(t, results)
	}
}

func TestTrendPassedUp(t *testing.T) {
	// Given
	candlestickListID := tradeutil.CandlestickListID{
		FSym:      fsym,
		TSym:      tsym,
		Exchange:  exchange,
		Variation: cryptomarketutil.Variation{Up: 0.05},
	}
	threshold := 0.8
	node := tradedec.TrendPassed(candlestickListID, threshold)
	histoPts := `
	30 -> 28 -> 25 -> 21 ->
	23 -> 25 -> 29 ->
	26 -> 24 -> 22 -> 19 ->
	20 -> 22 -> 24 ->
	22 -> 18 -> 17 -> 14 -> 16 -> 11`
	pts := `
	10 -> 9 -> 9.3 -> 9.44 -> 8.9 -> 8.7 ->
	8 -> 9 -> 10 -> 11 -> 14 -> 14.39 ->
	13.8 -> 14.2 -> (14.5) -> (13.8) -> 10 -> 7 ->
	8.4 -> 12.59 -> (12.61)`

	histoPoints := testutil.ParsePointSequence(histoPts, time.Now().Add(-14*d), time.Now().Add(-7*d))
	points := testutil.ParsePointSequence(pts, time.Now().Add(-6*d), time.Now())

	tt := prepareTableTest(0)
	tt.HistoPrices = histoPoints
	tt.Prices = points

	refCtx := tt.MakeRefCtx(&testutil.Point{})
	fn := testutil.DecisionNodeFn(node, *refCtx)
	tt.Fn = fn

	// When
	results := tt.Run()

	// Then
	if len(results) > 0 {
		t.Errorf(`Expected no failure points but got %d`, len(results))
		t.Log("Logging errors")
		logFailurePoints(t, results)
	}
}

func TestTrendPassedDown(t *testing.T) {
	// Given
	candlestickListID := tradeutil.CandlestickListID{
		FSym:      fsym,
		TSym:      tsym,
		Exchange:  exchange,
		Variation: cryptomarketutil.Variation{Up: 0.05},
	}
	threshold := -0.5
	node := tradedec.TrendPassed(candlestickListID, threshold)
	histoPts := `
	30 -> 28 -> 25 -> 21 ->
	23 -> 25 -> 29 ->
	26 -> 24 -> 22 -> 19 ->
	20 -> 22 -> 24 ->
	22 -> 18 -> 17 -> 14 -> 16 -> 11`
	pts := `
	10 -> 9 -> 9.3 -> 9.44 -> 8.9 -> 8.7 ->
	(7.99) -> (7) -> (7.36) -> 9 -> 10 -> 11 -> 14 -> 10 -> (6.99)`

	histoPoints := testutil.ParsePointSequence(histoPts, time.Now().Add(-14*d), time.Now().Add(-7*d))
	points := testutil.ParsePointSequence(pts, time.Now().Add(-6*d), time.Now())

	tt := prepareTableTest(0)
	tt.HistoPrices = histoPoints
	tt.Prices = points

	refCtx := tt.MakeRefCtx(&testutil.Point{})
	fn := testutil.DecisionNodeFn(node, *refCtx)
	tt.Fn = fn

	// When
	results := tt.Run()

	// Then
	if len(results) > 0 {
		t.Errorf(`Expected no failure points but got %d`, len(results))
		t.Log("Logging errors")
		logFailurePoints(t, results)
	}
}

func prepareTableTest(fillVariation float64) *testutil.TableTest {
	tt := &testutil.TableTest{
		Coin: fsym, Exchange: exchange, Fill: true, FillNoise: cryptomarketutil.GetSymmetricVariationFrom(fillVariation),
		FillInterval: fillInterval,
	}
	if fillVariation == 0 {
		tt.Fill = false
	}
	return tt
}

func logFailurePoints(t *testing.T, results []testutil.FailurePoint) {
	for i := 0; i < len(results); i++ {
		result := results[i]
		if result.Expected {
			t.Errorf(`Expected to signal for this point:: Price - %.2f :: Timestamp: %s`, result.GetPrice(), result.GetTimestamp())
		} else {
			t.Errorf(`Expected not to signal for this point:: Price - %.2f :: Timestamp: %s`, result.GetPrice(), result.GetTimestamp())
		}
	}
}
