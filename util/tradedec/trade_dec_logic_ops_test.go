package tradedec_test

import (
	"testing"

	"bitbucket.org/pfzero/tradr/util/tradedec"
	"bitbucket.org/pfzero/tradr/util/tradedec/testutil"

	"bitbucket.org/pfzero/tradr/util/tradeutil"
)

func TestAnd(t *testing.T) {
	tests := []struct {
		name string
		args []tradedec.DecisionNode
		want bool
	}{
		{
			name: "Empty args",
			args: []tradedec.DecisionNode{},
			want: true,
		},
		{
			name: "All Truthy",
			args: []tradedec.DecisionNode{&testutil.TrueDecNode{}, &testutil.TrueDecNode{}},
			want: true,
		},
		{
			name: "One Falsy",
			args: []tradedec.DecisionNode{&testutil.TrueDecNode{}, &testutil.FalseDecNode{}},
			want: false,
		},
		{
			name: "All Falsy",
			args: []tradedec.DecisionNode{&testutil.FalseDecNode{}, &testutil.FalseDecNode{}},
			want: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			node := tradedec.And(tt.args...)
			refCtx := tradeutil.TradeContext{}
			ctx := tradeutil.TradeContext{}
			if got := node.Exec(refCtx, ctx); got != tt.want {
				t.Errorf("And() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOr(t *testing.T) {
	tests := []struct {
		name string
		args []tradedec.DecisionNode
		want bool
	}{
		{
			name: "Empty args",
			args: []tradedec.DecisionNode{},
			want: true,
		},
		{
			name: "All Truthy",
			args: []tradedec.DecisionNode{&testutil.TrueDecNode{}, &testutil.TrueDecNode{}},
			want: true,
		},
		{
			name: "One Truthy",
			args: []tradedec.DecisionNode{&testutil.TrueDecNode{}, &testutil.FalseDecNode{}},
			want: true,
		},
		{
			name: "All Falsy",
			args: []tradedec.DecisionNode{&testutil.FalseDecNode{}, &testutil.FalseDecNode{}},
			want: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			node := tradedec.Or(tt.args...)
			refCtx := tradeutil.TradeContext{}
			ctx := tradeutil.TradeContext{}
			if got := node.Exec(refCtx, ctx); got != tt.want {
				t.Errorf("And() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNot(t *testing.T) {
	tests := []struct {
		name string
		arg  tradedec.DecisionNode
		want bool
	}{
		{
			name: "Empty arg",
			want: true,
		},
		{
			name: "Truthy",
			arg:  &testutil.TrueDecNode{},
			want: false,
		},
		{
			name: "Falsy",
			arg:  &testutil.FalseDecNode{},
			want: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			node := tradedec.Not(tt.arg)
			refCtx := tradeutil.TradeContext{}
			ctx := tradeutil.TradeContext{}
			if got := node.Exec(refCtx, ctx); got != tt.want {
				t.Errorf("And() = %v, want %v", got, tt.want)
			}
		})
	}
}
