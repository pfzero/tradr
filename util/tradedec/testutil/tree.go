package testutil

import (
	"bitbucket.org/pfzero/tradr/util/tradedec"
	"bitbucket.org/pfzero/tradr/util/tradeutil"
)

// TreeExecFn creates an executor function from a decisional tree
// that can be provided to table testing utility;
func TreeExecFn(tree *tradedec.DecisionTree) ExecFn {
	return func(ctx tradeutil.TradeContext) (bool, error) {
		return tree.Exec(ctx)
	}
}
