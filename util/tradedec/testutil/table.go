package testutil

import (
	"math"
	"math/rand"
	"time"

	"bitbucket.org/pfzero/tradr/util/tradeutil"

	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"
)

// ExecFn represents the function executor that will be tested;
// ExecFn can encapsulate multiple data-structures that given a
// context (point in time) will return a bool signal
type ExecFn func(ctx tradeutil.TradeContext) (bool, error)

// Point represents a point that is feeded as initial data
type Point struct {
	Price  float64
	Time   time.Time
	Signal bool
}

// GetPrice returns the price of the point
func (pt *Point) GetPrice() float64 {
	return pt.Price
}

// GetTimestamp returns the timestamp
func (pt *Point) GetTimestamp() time.Time {
	return pt.Time
}

// FailurePoint is a datastructure that contains details
// for a failure point
type FailurePoint struct {
	*cryptomarketutil.SimplePrice
	Expected bool
	Received bool
}

// TableTest is a simple utility that allows for easier testing
// of various signal-based algorithms against of a set of price points in
// time
type TableTest struct {
	Coin         string
	Exchange     string
	HistoPrices  []Point
	Prices       []Point
	Fill         bool
	FillNoise    *cryptomarketutil.Variation
	FillInterval time.Duration

	tradeCtxMgr *tradeutil.TradeContextManager

	Fn ExecFn

	initialized    bool
	preparedPrices []Point
}

// MakeRefCtx creates a new reference context from the given point
func (tt *TableTest) MakeRefCtx(pt *Point) *tradeutil.TradeContext {
	ctxMgr := tradeutil.NewTradeContextManager(math.MaxInt32, "USD", "EUR")
	snap := cryptomarketutil.NewMarketSnapshot()
	snap.Set(tt.Coin, tt.Exchange, pt)
	ctx := ctxMgr.MakeContext(snap)
	return ctx
}

// Run runs the tests and returns all the failure points
// registered, if any
func (tt *TableTest) Run() []FailurePoint {
	tt.init()

	results := []FailurePoint{}
	for i := 0; i < len(tt.preparedPrices); i++ {
		point := tt.preparedPrices[i]
		snap := cryptomarketutil.NewMarketSnapshot()
		snap.Set(tt.Coin, tt.Exchange, &point)
		ctx := tt.tradeCtxMgr.MakeContextAndStore(snap)
		result, err := tt.Fn(*ctx)

		if err != nil {
			panic(err)
		}

		if result != point.Signal {
			results = append(results, FailurePoint{
				SimplePrice: &cryptomarketutil.SimplePrice{
					Price:     point.GetPrice(),
					Timestamp: point.GetTimestamp(),
				},
				Expected: point.Signal,
				Received: result,
			})
		}
	}
	return results
}

func (tt *TableTest) init() {
	if tt.initialized {
		return
	}
	tt.initialized = true
	tt.tradeCtxMgr = tradeutil.NewTradeContextManager(math.MaxInt32, "USD", "EUR")

	var histoprices []Point
	if tt.Fill {
		histoprices = tt.preparePrices(tt.HistoPrices)
	} else {
		histoprices = tt.HistoPrices
	}

	snaps := make([]cryptomarketutil.MarketSnapshot, len(histoprices))
	for i := 0; i < len(histoprices); i++ {
		snap := cryptomarketutil.NewMarketSnapshot()
		snap.Set(tt.Coin, tt.Exchange, &histoprices[i])
		snaps[i] = *snap
	}
	tt.tradeCtxMgr.FeedHistoMarketSnaps(snaps)

	if tt.Fill {
		tt.preparedPrices = tt.preparePrices(tt.Prices)
	} else {
		tt.preparedPrices = tt.Prices
	}
}

func (tt *TableTest) getPoints(from, to Point) []Point {
	pointsCnt := int(to.GetTimestamp().Sub(from.GetTimestamp()) / tt.FillInterval)
	points := make([]Point, pointsCnt)
	if pointsCnt == 0 {
		panic("Can't generate extra points between 2 points that have the same timestamp")
	}

	progression := (to.GetPrice() - from.GetPrice()) / float64(pointsCnt/3)

	symmetricNoise := cryptomarketutil.FillSymmetricVariation(*tt.FillNoise)
	rand.Seed(time.Now().UTC().UnixNano())
	for i := 0; i < pointsCnt; i++ {
		if i%3 == 0 {
			price := from.GetPrice() + float64(i)*progression
			ts := from.GetTimestamp().Add(time.Duration(i) * tt.FillInterval)
			points[i] = Point{
				price, ts, false,
			}
			continue
		}

		lastPrice := points[i-1].GetPrice()
		max := lastPrice + symmetricNoise.Up*lastPrice
		min := lastPrice + symmetricNoise.Down*lastPrice
		fillPrice := min + rand.Float64()*(max-min)
		ts := points[i-1].GetTimestamp().Add(tt.FillInterval)

		points[i] = Point{fillPrice, ts, false}
	}
	return points
}

func (tt *TableTest) preparePrices(prices []Point) []Point {
	if len(prices) == 0 {
		return prices
	}

	results := []Point{}
	for i := 1; i < len(prices); i++ {
		prev := prices[i-1]
		cur := prices[i]
		between := tt.getPoints(prev, cur)
		results = append(results, between...)
	}
	results = append(results, prices[len(prices)-1])
	return results
}
