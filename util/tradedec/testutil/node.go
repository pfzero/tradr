package testutil

import (
	"errors"

	"bitbucket.org/pfzero/tradr/util/tradedec"
	"bitbucket.org/pfzero/tradr/util/tradeutil"
)

// DecisionNodeFn creates a new function that can be provided to table test utility
// which can further can test the node against a set of points in time
func DecisionNodeFn(node tradedec.DecisionNode, refCtx tradeutil.TradeContext) ExecFn {
	return func(ctx tradeutil.TradeContext) (bool, error) {
		if refCtx.IsZero() {
			return false, errors.New(`refCtx wasn't set`)
		}

		return node.Exec(refCtx, ctx), nil
	}
}
