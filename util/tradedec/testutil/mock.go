package testutil

import (
	"bitbucket.org/pfzero/tradr/util/tradedec"
	"bitbucket.org/pfzero/tradr/util/tradeutil"
)

// TrueDecNode is a trade decision node that always
// resolves to a true value; handy when writing tests
type TrueDecNode struct{}

// GetName returns the name of the node
func (tdn *TrueDecNode) GetName() string {
	return "TrueDecNode"
}

// Exec executes the node
func (tdn *TrueDecNode) Exec(refCtx, ctx tradeutil.TradeContext) bool {
	return true
}

// GetChildren returns the childrens of this node - ie. empty slice
func (tdn *TrueDecNode) GetChildren() []tradedec.DecisionNode {
	return []tradedec.DecisionNode{}
}

// FalseDecNode is a trade decision node that always returns false
type FalseDecNode struct{}

// GetName returns the name of the node
func (fdn *FalseDecNode) GetName() string {
	return "FalseDecNode"
}

// GetChildren returns the childrens of this node - ie. empty slice
func (fdn *FalseDecNode) GetChildren() []tradedec.DecisionNode {
	return []tradedec.DecisionNode{}
}

// Exec returns false
func (fdn *FalseDecNode) Exec(refCtx, ctx tradeutil.TradeContext) bool {
	return false
}
