package testutil

import (
	"strconv"
	"strings"
	"time"
)

func getPoints(seq string) []Point {
	inlined := strings.Replace(seq, "\n", "", -1)
	inlined = strings.Replace(inlined, "\t", "", -1)
	trimmed := strings.Replace(inlined, " ", "", -1)
	split := strings.Split(trimmed, "->")
	points := make([]Point, len(split))
	for i := 0; i < len(split); i++ {
		pt := Point{}
		cur := split[i]
		if strings.Index(cur, "(") >= 0 || strings.Index(cur, ")") >= 0 {
			pt.Signal = true
			cur = strings.Replace(cur, "(", "", -1)
			cur = strings.Replace(cur, ")", "", -1)
		}

		parsed, err := strconv.ParseFloat(cur, 64)
		if err != nil {
			panic(err)
		}
		pt.Price = parsed
		points[i] = pt
	}
	return points
}

// ParsePointSequence allows for easily creating price points
// distributed at uniform time durations
// eg.
// str := 1 -> 2 -> (3) -> 4 -> 5 -> (6) -> 7 -> 8 -> 9 -> 10
// ParsePointSequence(str, time.Now().Add(-10 Days), time.Now()
// will yield 10 points distributed 1 per day (10 prices / 10 days) => 1 price / day
// Please note that (3) means that the point should signal true whereas
// 3 expects a falsy signal
func ParsePointSequence(seq string, from, to time.Time) []Point {
	points := getPoints(seq)
	dif := to.Sub(from)
	delta := dif / time.Duration(len(points))
	for i := 0; i < len(points); i++ {
		ts := time.Duration(i) * delta
		points[i].Time = from.Add(ts)
	}
	return points
}
