package tradedec

import (
	"time"

	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"
	"bitbucket.org/pfzero/tradr/util/tradeutil"
)

// PriceVariationOp represents a price variation operator
type PriceVariationOp struct {
	variation float64
	refPrice  float64
	exchange  string
	fsym      string
	tsym      string
}

// GetName returns the name of the operator
func (pv *PriceVariationOp) GetName() string {
	return "PriceVariation"
}

func (pv *PriceVariationOp) getRefPrice(refCtx tradeutil.TradeContext) float64 {
	var fPrice float64
	if pv.refPrice != 0 {
		fPrice = pv.refPrice
	} else {
		price, err := refCtx.GetPrice(pv.fsym, pv.tsym, pv.exchange)
		if err != nil {
			return -1
		}
		fPrice = price.GetPrice()
	}
	return fPrice
}

//
func (pv *PriceVariationOp) getCurrentPrice(ctx tradeutil.TradeContext) float64 {
	tPrice, e := ctx.GetPrice(pv.fsym, pv.tsym, pv.exchange)
	if e != nil {
		return -1
	}
	return tPrice.GetPrice()
}

// Exec executes the operator given 2 points in time
func (pv *PriceVariationOp) Exec(refCtx, ctx tradeutil.TradeContext) bool {

	fPrice := pv.getRefPrice(refCtx)
	tPrice := pv.getCurrentPrice(ctx)

	if fPrice == -1 || tPrice == -1 {
		return false
	}

	variation := cryptomarketutil.GetPriceVariation(fPrice, tPrice)

	if pv.variation < 0 {
		return variation < pv.variation
	}

	return variation > pv.variation
}

// GetChildren is used to implement the interface
func (pv *PriceVariationOp) GetChildren() []DecisionNode {
	return nil
}

// ElapsedOp represents the operator that checks
// wether a specific time duration has passed between
// 2 trade contexts
type ElapsedOp struct {
	duration time.Duration
}

// Exec executes the operator
func (e *ElapsedOp) Exec(refCtx, ctx tradeutil.TradeContext) bool {
	ctxTimestamp := ctx.GetTimestamp()
	refCtxTimestamp := refCtx.GetTimestamp()
	diff := ctxTimestamp.Sub(refCtxTimestamp)
	return diff > e.duration
}

// GetName returns the name of the decision node
func (e *ElapsedOp) GetName() string {
	return "Elapsed"
}

// GetChildren returns the childrens of this node
func (e *ElapsedOp) GetChildren() []DecisionNode {
	return nil
}

// TimeOp represents an operation that checks
// wether the given time has passed
type TimeOp struct {
	thresholdTime time.Time
	before        bool
}

// Exec executes the operator
func (to *TimeOp) Exec(refCtx, ctx tradeutil.TradeContext) bool {
	if to.before {
		return ctx.GetTimestamp().Before(to.thresholdTime)
	}
	return ctx.GetTimestamp().After(to.thresholdTime)
}

// GetName returns the name of the decision node
func (to *TimeOp) GetName() string {
	return "TimeOp"
}

// GetChildren returns the childrens of this node
func (to *TimeOp) GetChildren() []DecisionNode {
	return nil
}

// CandlestickOp encapsulates a logic for defining
// trend-based operations
type CandlestickOp struct {
	candlestickListID *tradeutil.CandlestickListID
	thresholds        *cryptomarketutil.Variation
	decisionMaker     func(refCtx, ctx tradeutil.TradeContext) bool
}

// Exec executes the operator
func (co *CandlestickOp) Exec(refCtx, ctx tradeutil.TradeContext) bool {
	return co.decisionMaker(refCtx, ctx)
}

// GetName returns the name of the decision node
func (co *CandlestickOp) GetName() string {
	return "CandlestickOp"
}

// GetChildren returns the childrens of this node
func (co *CandlestickOp) GetChildren() []DecisionNode {
	return nil
}

func (co *CandlestickOp) getPrice(ctx tradeutil.TradeContext) float64 {
	price, err := ctx.GetPrice(co.candlestickListID.FSym, co.candlestickListID.TSym, co.candlestickListID.Exchange)
	if err != nil {
		return -1
	}
	return price.GetPrice()
}

// PriceVariation returns the price variation operator configured with the given
// parameters
func PriceVariation(fsym, tsym, exchange string, variation float64) *PriceVariationOp {
	return &PriceVariationOp{
		fsym:      fsym,
		tsym:      tsym,
		exchange:  exchange,
		variation: variation,
	}
}

// PriceVariationFrom is like PriceVariation but also includes a reference price
func PriceVariationFrom(fsym, tsym, exchange string, variation, refPrice float64) *PriceVariationOp {
	return &PriceVariationOp{
		fsym:      fsym,
		tsym:      tsym,
		exchange:  exchange,
		variation: variation,
		refPrice:  refPrice,
	}
}

// Elapsed returns a new duration based logical operator that
// checks wether a specific duration has passed
func Elapsed(d time.Duration) *ElapsedOp {
	return &ElapsedOp{
		duration: d,
	}
}

// BeforeTime returns the operation needed to check
// wether the trade executes before the given time
func BeforeTime(t time.Time) *TimeOp {
	return &TimeOp{
		thresholdTime: t,
		before:        true,
	}
}

// AfterTime returns the operation needed to check
// wether the trade executes after the given time
func AfterTime(t time.Time) *TimeOp {
	return &TimeOp{
		thresholdTime: t,
		before:        false,
	}
}

// TrendChange checks wether trend for the given pair has changed
func TrendChange(id tradeutil.CandlestickListID) *CandlestickOp {
	return &CandlestickOp{
		candlestickListID: &id,
		decisionMaker: func(refCtx, ctx tradeutil.TradeContext) bool {
			candlestickResp := ctx.GetLastCandlestickResp(id)
			if candlestickResp == nil {
				return false
			}
			return candlestickResp.IsNewCandlestick
		},
	}
}

// TrendType checks wether trend is of certain type; eg. trend defined by
// +-7% noise is currently increasing or decreasing
func TrendType(id tradeutil.CandlestickListID, increasing bool) *CandlestickOp {
	return &CandlestickOp{
		candlestickListID: &id,
		decisionMaker: func(refCtx, ctx tradeutil.TradeContext) bool {
			lastCandlestick, err := ctx.GetLastCandlestick(id)
			if err != nil || lastCandlestick == nil {
				return false
			}

			flag := (increasing && lastCandlestick.IsIncreasing()) || (!increasing && lastCandlestick.IsDecreasing())
			return flag
		},
	}
}

// TrendFormed checks for a particular trend that formed; eg. checks wether a
// candlestick with the given variation was formed; The difference between TrendFormed
// and TrendPassed is that TrendFormed uses the same noise as the given variation
// whereas in TrendPassed the noise levels are different from the thresholds
func TrendFormed(fSym, tSym, exchange string, variation float64) DecisionNode {
	if variation == 0 {
		return And()
	}

	symmetricVar := cryptomarketutil.GetSymmetricVariationFrom(variation)

	candlestickListID := &tradeutil.CandlestickListID{
		FSym:      fSym,
		TSym:      tSym,
		Exchange:  exchange,
		Variation: *symmetricVar,
	}

	return &CandlestickOp{
		candlestickListID: candlestickListID,
		decisionMaker: func(refCtx, ctx tradeutil.TradeContext) bool {
			candlestickList, err := ctx.GetCandlestickList(*candlestickListID)
			if err != nil {
				return false
			}
			candlestick := candlestickList.GetLastCandlestick()
			if candlestick == nil {
				return false
			}

			currentPrice, err := ctx.GetPrice(fSym, tSym, exchange)
			if err != nil {
				return false
			}

			var v float64
			if candlestick.IsIncreasing() {
				v = symmetricVar.GetUpVariation()
			} else {
				v = symmetricVar.GetDownVariation()
			}
			candleFormedPrice := candlestick.PriceFrom.GetPrice() + v*candlestick.PriceFrom.GetPrice()

			if variation > 0 {
				return (candlestick.GetPercVariation() >= variation &&
					currentPrice.GetPrice() >= candleFormedPrice)
			}

			return (candlestick.GetPercVariation() <= variation &&
				currentPrice.GetPrice() <= candleFormedPrice)
		},
	}
}

// TrendPassed checks wether the trend has passed a certain threshold;
// if the given variation is positive then only the positive trends for that
// given pair will be taken into account; same applies for a negative variation
func TrendPassed(id tradeutil.CandlestickListID, variation float64) *CandlestickOp {
	v := &cryptomarketutil.Variation{}
	if variation >= 0 {
		v.Up = variation
	} else {
		v.Down = variation
	}

	return &CandlestickOp{
		candlestickListID: &id,
		thresholds:        v,
		decisionMaker: func(refCtx, ctx tradeutil.TradeContext) bool {
			candlestickList, err := ctx.GetCandlestickList(id)
			if err != nil {
				return false
			}

			var candlestick *cryptomarketutil.Candlestick
			candlestickListResp := ctx.GetLastCandlestickResp(id)

			if candlestickListResp.IsNewCandlestick {
				candlestick = candlestickList.GetLastConsolidatedCandlestick()
			} else {
				candlestick = candlestickList.GetLastCandlestick()
			}

			if candlestick == nil {
				return false
			}

			if variation >= 0 {
				return candlestick.GetPercVariation() > variation
			}

			return candlestick.GetPercVariation() < variation
		},
	}
}
