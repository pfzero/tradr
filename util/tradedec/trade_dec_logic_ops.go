package tradedec

import (
	"bitbucket.org/pfzero/tradr/util/tradeutil"
)

// LogicOperator represents the datastructure that encapsulates logic operation
// between multiple subnodes
type LogicOperator struct {
	Name     string
	Fn       DecisionNodeResolver
	children []DecisionNode
}

// GetName returns the name of the decision node
func (lo *LogicOperator) GetName() string {
	return lo.Name
}

// GetChildren returns the children of this logic operator
func (lo *LogicOperator) GetChildren() []DecisionNode {
	return lo.children
}

// AddChild adds a child to our decision node
func (lo *LogicOperator) AddChild(child DecisionNode) *LogicOperator {
	if child == nil {
		return lo
	}
	lo.children = append(lo.children, child)
	return lo
}

// AddChildren adds the given nodes as children to this node
func (lo *LogicOperator) AddChildren(children ...DecisionNode) *LogicOperator {
	for i := 0; i < len(children); i++ {
		node := children[i]
		lo.AddChild(node)
	}
	return lo
}

// Exec executes the node
func (lo *LogicOperator) Exec(refCtx, ctx tradeutil.TradeContext) bool {
	return lo.Fn(refCtx, ctx, lo)
}

// makeNode makes a new node
func makeLogicOp(name string) *LogicOperator {
	return &LogicOperator{
		Name:     name,
		children: []DecisionNode{},
	}
}

func andFn(refCtx, ctx tradeutil.TradeContext, node DecisionNode) bool {
	for _, el := range node.GetChildren() {
		signal := el.Exec(refCtx, ctx)
		if !signal {
			return false
		}
	}
	return true
}

func orFn(refCtx, ctx tradeutil.TradeContext, node DecisionNode) bool {
	// return true when no-op or function
	if len(node.GetChildren()) == 0 {
		return true
	}
	for _, el := range node.GetChildren() {
		if el.Exec(refCtx, ctx) {
			return true
		}
	}
	return false
}

func notFn(refCtx, ctx tradeutil.TradeContext, node DecisionNode) bool {
	children := node.GetChildren()
	if len(children) == 0 {
		return true
	}
	child := node.GetChildren()[0]
	return !child.Exec(refCtx, ctx)
}

// And creates a new decision node that is capable of
// AND-ing the child nodes
func And(nodes ...DecisionNode) DecisionNode {
	andNode := makeLogicOp("AND")
	andNode.Fn = andFn
	andNode.AddChildren(nodes...)
	return andNode
}

// Or creates a new decision node that is capable of Or-ing
// the child nodes
func Or(nodes ...DecisionNode) DecisionNode {
	orNode := makeLogicOp("OR")
	orNode.Fn = orFn
	orNode.AddChildren(nodes...)
	return orNode
}

// Not creates a new decision node that can apply a NOT
// to the given decision node
func Not(n DecisionNode) DecisionNode {
	notNode := makeLogicOp("NOT")
	notNode.Fn = notFn
	notNode.AddChild(n)
	return notNode
}
