package tradedec

import (
	"errors"
	"time"

	"bitbucket.org/pfzero/tradr/util/tradeutil"
)

// DecisionTree represents a tree of logical units
// that is used for emiting trade signals
type DecisionTree struct {
	root       DecisionNode
	refCtx     tradeutil.TradeContext
	currentCtx tradeutil.TradeContext
}

// CreateDecTree creates a new decision tree
func CreateDecTree() *DecisionTree {
	return &DecisionTree{
		root: And(),
	}
}

// CreateDecTreeWithCtx creates a decision tree with the given context
func CreateDecTreeWithCtx(ctx tradeutil.TradeContext) *DecisionTree {
	return &DecisionTree{
		root:   And(),
		refCtx: ctx,
	}
}

// GetRefCtx returns the reference context
func (dt *DecisionTree) GetRefCtx() tradeutil.TradeContext {
	return dt.refCtx
}

// SetRefCtx sets the reference context of this tree
func (dt *DecisionTree) SetRefCtx(ctx tradeutil.TradeContext) *DecisionTree {
	dt.refCtx = ctx
	return dt
}

// GetCurrentCtx returns the latest context used for evaluating the
// decision tree
func (dt *DecisionTree) GetCurrentCtx() tradeutil.TradeContext {
	return dt.currentCtx
}

// And creates an And node
func (dt *DecisionTree) And(nodes ...DecisionNode) *DecisionTree {
	children := dt.root.GetChildren()
	children = append(children, nodes...)
	dt.root = And(children...)
	return dt
}

// Or creates an OR node
func (dt *DecisionTree) Or(nodes ...DecisionNode) *DecisionTree {
	// newChild := Or(nodes...)
	newNodes := make([]DecisionNode, len(nodes))
	copy(newNodes, nodes)
	newNodes = append(newNodes, dt.root)
	dt.root = Or(newNodes...)
	return dt
}

// PriceVar creates a new price variation node
func (dt *DecisionTree) PriceVar(fsym, tsym, exchange string, variation float64) *DecisionTree {
	newChild := PriceVariation(fsym, tsym, exchange, variation)
	return dt.And(newChild)
}

// PriceVarFrom creates a new price variation node
func (dt *DecisionTree) PriceVarFrom(fsym, tsym, exchange string, variation, refPrice float64) *DecisionTree {
	newChild := PriceVariationFrom(fsym, tsym, exchange, variation, refPrice)
	return dt.And(newChild)
}

// Elapsed sets a new duration elapsed node
func (dt *DecisionTree) Elapsed(d time.Duration) *DecisionTree {
	newChild := Elapsed(d)
	return dt.And(newChild)
}

// NotElapsed creates a new node that signals until the specified
// duration elapses
func (dt *DecisionTree) NotElapsed(d time.Duration) *DecisionTree {
	newChild := Not(Elapsed(d))
	return dt.And(newChild)
}

// BeforeTime creates a node that signals until the given time
func (dt *DecisionTree) BeforeTime(t time.Time) *DecisionTree {
	newChild := BeforeTime(t)
	return dt.And(newChild)
}

// AfterTime creates a node that waits until the specified time
func (dt *DecisionTree) AfterTime(t time.Time) *DecisionTree {
	newChild := AfterTime(t)
	return dt.And(newChild)
}

// TrendChange adds a new node that listens for trend changes
func (dt *DecisionTree) TrendChange(id tradeutil.CandlestickListID) *DecisionTree {
	newChild := TrendChange(id)
	return dt.And(newChild)
}

// TrendPassed adds a new node to the tree that listens for trend
// variations
func (dt *DecisionTree) TrendPassed(id tradeutil.CandlestickListID, variation float64) *DecisionTree {
	newChild := TrendPassed(id, variation)
	return dt.And(newChild)
}

// Exec executes the decisional tree
func (dt *DecisionTree) Exec(ctx tradeutil.TradeContext) (bool, error) {
	if dt.refCtx.IsZero() {
		return false, errors.New(`DecisionTree:: RefContext wasn't set`)
	}
	dt.currentCtx = ctx
	return dt.root.Exec(dt.refCtx, ctx), nil
}
