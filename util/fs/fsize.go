package fs

import (
	"os"

	"github.com/dustin/go-humanize"
)

// GetFileSize returns the file's size in humanize string version
// 1024*1024 B = "1 MB"
func GetFileSize(src string) (string, error) {
	file, err := os.Open(src)
	if err != nil {
		return "", err
	}
	defer file.Close()
	stat, err := file.Stat()
	if err != nil {
		return "", err
	}
	size := uint64(stat.Size())
	return humanize.Bytes(size), nil
}
