package fs

import (
	"io"
	"os"
	"path/filepath"
)

// CopyFile from src to dest; Also creates the destination path
// recursively
func CopyFile(source, dest string) (err error) {
	dirPath, _ := filepath.Split(dest)
	pathErr := os.MkdirAll(dirPath, 0755)
	if pathErr != nil {
		if !os.IsExist(pathErr) {
			return pathErr
		}
	}
	sourcef, err := os.Open(source)
	if err != nil {
		return err
	}
	defer sourcef.Close()
	destinationf, err := os.Create(dest)
	if err != nil {
		return err
	}
	defer destinationf.Close()
	_, err = io.Copy(destinationf, sourcef)
	if err == nil {
		si, err := os.Stat(source)
		if err == nil {
			err = os.Chmod(dest, si.Mode())
		}
	}
	return
}
