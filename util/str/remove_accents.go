package str

import (
	"unicode"

	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"
)

func isMn(r rune) bool {
	return unicode.Is(unicode.Mn, r)
}

// RemoveAccents removes accents from strings;
// based on this example: https://gist.github.com/tkrajina/d423e9b9fc2e72d63072
func RemoveAccents(src string) string {
	dest := make([]byte, len(src))
	t := transform.Chain(norm.NFD, transform.RemoveFunc(isMn), norm.NFC)
	_, _, err := t.Transform(dest, []byte(src), true)
	if err != nil {
		return src
	}

	return string(dest)
}
