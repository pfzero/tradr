package str

import (
	"encoding/base64"

	"github.com/pborman/uuid"
)

// GenerateToken generates a unique random token
func GenerateToken() (ret string) {
	token := uuid.NewRandom()
	return base64.RawURLEncoding.EncodeToString([]byte(token))
}
