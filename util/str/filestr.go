package str

import (
	"strings"
)

// StringAsFilename replaces separators with - and removes characters listed in the regexp provided from string.
// Accents, spaces, and all characters not in A-Za-z0-9 are replaced.
func StringAsFilename(s string) string {

	// Remove any trailing space to avoid ending on -
	s = strings.Trim(s, " ")

	// Flatten accents first so that if we remove non-ascii we still get a legible name
	s = AccentsAsASCII(s)

	// Remove certain joining characters
	s = separators.ReplaceAllString(s, "")

	// Remove rest of illegal chars
	s = illegalChars.ReplaceAllString(s, "")

	return s
}
