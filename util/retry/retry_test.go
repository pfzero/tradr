package retry

import (
	"errors"
	"fmt"
	"reflect"
	"testing"
	"time"
)

func TestDo(t *testing.T) {
	type args struct {
		fn    func() error
		count int
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "It should repeat without throttling",
			args: args{
				fn: func() func() error {
					count := 0
					return func() error {
						count++
						if count == 1 {
							return errors.New("Some Error")
						}
						return nil
					}
				}(),
				count: 3,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := Do(tt.args.fn, tt.args.count); (err != nil) != tt.wantErr {
				t.Errorf("Do() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestDoWithThrottle(t *testing.T) {
	type args struct {
		fn       func() error
		count    int
		throttle time.Duration
	}

	tests := []struct {
		name        string
		args        args
		wantedError error
	}{
		{
			name: "It should run the given function once if it returns nil",
			args: args{
				fn:       func() error { return nil },
				count:    3,
				throttle: 1,
			},
			wantedError: nil,
		},
		{
			name: "It should run the given function count times and return the latest error if everytime the fn fails",
			args: args{
				fn: func() func() error {
					count := 0
					return func() error {
						count++
						return fmt.Errorf("Error no #%d", count)
					}
				}(),
				count:    3,
				throttle: 1,
			},
			wantedError: fmt.Errorf("Error no #%d", 3),
		},
		{
			name: "It should run the given function count times with throttling",
			args: args{
				fn: func(d time.Duration) func() error {
					var lastCallTime time.Time
					var lastErr = errors.New("Predictable Error")

					return func() error {
						if lastCallTime.IsZero() {
							lastCallTime = time.Now()
						}
						if time.Now().Sub(lastCallTime) > d {
							lastErr = errors.New("Non-Predictable Error")
						}
						lastCallTime = time.Now()

						return lastErr
					}
				}(15 * time.Millisecond), // allow for some side-effect tolerance
				count:    3,
				throttle: 10 * time.Millisecond,
			},
			wantedError: errors.New("Predictable Error"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := DoWithThrottle(tt.args.fn, tt.args.count, tt.args.throttle); !reflect.DeepEqual(err, tt.wantedError) {
				t.Errorf("DoWithThrottle() error = %v, wantErr = %v", err, tt.wantedError)
			}
		})
	}
}
