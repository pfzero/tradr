package retry

import (
	"time"
)

// Do is a shorthand for DoWithThrottle(fn, count, 0)
func Do(fn func() error, count int) error {
	return DoWithThrottle(fn, count, 0)
}

// DoWithThrottle runs the given fn count times until it gives up. After each
// try it sleeps for the given throttle duration
func DoWithThrottle(fn func() error, count int, throttle time.Duration) error {
	var lastErr error

	for i := 0; i < count; i++ {
		e := fn()
		if e == nil {
			return nil
		}
		lastErr = e
		if throttle > 0 {
			time.Sleep(throttle)
		}
	}

	return lastErr
}
