package tick

import (
	"sync"
	"time"

	"bitbucket.org/pfzero/tradr/util/str"
)

// S is a subscriber
type S struct {
	key string
	C   <-chan time.Time
}

// FanOutTicker takes a ticker and enables it to be
// emitted in a fan-out fashion;
type FanOutTicker struct {
	t          *time.Ticker
	listenersL sync.Mutex
	listeners  map[string]chan time.Time
}

// NewFanOutTicker creates a new FanOutTicker based
// on the given ticker
func NewFanOutTicker(d time.Duration) *FanOutTicker {
	fot := &FanOutTicker{
		t:         time.NewTicker(d),
		listeners: make(map[string]chan time.Time),
	}
	fot.start()
	return fot
}

// NewScheduledFanOutTicker creates a fan out ticker that first emits after the first parameter
// depletes and afterwords acts like a normal fan-out ticker
func NewScheduledFanOutTicker(after time.Duration, interval time.Duration) *FanOutTicker {
	fot := &FanOutTicker{listeners: make(map[string]chan time.Time)}
	go func() {
		time.Sleep(after)
		fot.stream(time.Now())
		fot.t = time.NewTicker(interval)
		fot.start()
	}()
	return fot
}

// Sub adds a new subscriber to the fan-out ticker
func (fot *FanOutTicker) Sub() *S {
	c := make(chan time.Time)
	s := &S{key: str.GenerateToken(), C: c}

	fot.listenersL.Lock()
	fot.listeners[s.key] = c
	fot.listenersL.Unlock()

	return s
}

// Unsub unsubscribes from the fan-out ticker
func (fot *FanOutTicker) Unsub(s *S) {
	if s == nil {
		return
	}
	fot.listenersL.Lock()
	c, ok := fot.listeners[s.key]
	if !ok {
		fot.listenersL.Unlock()
		return
	}
	close(c)
	delete(fot.listeners, s.key)
	fot.listenersL.Unlock()
}

func (fot *FanOutTicker) start() {
	go func() {
		for {
			t := <-fot.t.C
			fot.stream(t)
		}
	}()
}

func (fot *FanOutTicker) stream(t time.Time) {
	cnt := 0
	fot.listenersL.Lock()
	chans := make([]chan time.Time, len(fot.listeners))
	for _, c := range fot.listeners {
		chans[cnt] = c
		cnt++
	}
	fot.listenersL.Unlock()

	for _, c := range chans {
		select {
		case c <- t:
		default:
			// do nothing; if listener is not listening after the ticker, skip the current tick for him;
			// maybe he'll start listening from the next tick
		}
	}
}
