package tick

import (
	"testing"
	"time"
)

func TestNewFanOutTicker(t *testing.T) {
	type args struct {
		d time.Duration
	}
	tests := []struct {
		name string
		args args
	}{
		{
			name: "It should create a new Fan-Out ticker given a base ticker",
			args: args{
				d: 100 * time.Millisecond,
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewFanOutTicker(tt.args.d)
			if got.listeners == nil {
				t.Error("Expected listeners to be initialized and non-nil, but got nil")
			}

			if got.t == nil {
				t.Error("Expected ticker to be non-nil")
			}
		})
	}
}

func TestNewScheduledFanOutTicker(t *testing.T) {
	type args struct {
		after    time.Duration
		interval time.Duration
	}

	tests := []struct {
		name string
		args args
	}{
		{
			name: "It should first tick after the first given duration and then act like a fan-out ticker",
			args: args{after: 30 * time.Millisecond, interval: 80 * time.Millisecond},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewScheduledFanOutTicker(tt.args.after, tt.args.interval)

			s := got.Sub()

			select {
			case <-s.C:
			case <-time.NewTimer(tt.args.after + (20 * time.Millisecond)).C:
				t.Error("Expected to tick before the after timer but it didn't")
			}

			for i := 0; i < 2; i++ {
				select {
				case <-s.C:
				case <-time.NewTimer(tt.args.interval + (20 * time.Millisecond)).C:
					t.Error("Expected to tick before the interval timer but it didn't")
				}
			}
		})
	}
}

func TestFanOutTicker_Sub(t *testing.T) {
	tests := []struct {
		name string
		fot  *FanOutTicker
		want *S
	}{
		{
			name: "It should create a new subscription and return it",
			fot:  NewFanOutTicker(1 * time.Millisecond),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.fot.Sub()
			if got == nil {
				t.Error("Expected subscriber returned not to be nil, but got nil")
				return
			}
			if got.key == "" {
				t.Error("Expected subscriber key not to be empty but got empty")
			}
			if got.C == nil {
				t.Error("Expected channel to be non-nil")
			}
			if len(tt.fot.listeners) == 0 {
				t.Error("It should've stored the subscriber in listeners store but it didn't")
			}

			for i := 0; i < 10; i++ {
				select {
				case <-got.C:
				case <-time.NewTimer(2500 * time.Microsecond).C:
					t.Error("Expected subscriber to tick but it didn't")
				}
			}
		})
	}
}

func TestFanOutTicker_Unsub(t *testing.T) {
	type args struct {
		s *S
	}
	tests := []struct {
		name string
		fot  *FanOutTicker
		args args
	}{
		{
			name: "It should unsubscribe",
			fot: func() *FanOutTicker {
				const key = "deterministic_key"
				fot := NewFanOutTicker(1 * time.Millisecond)
				s := fot.Sub()
				fot.listeners[key] = fot.listeners[s.key]
				delete(fot.listeners, s.key)
				s.key = key
				return fot
			}(),
			args: args{
				s: &S{key: "deterministic_key", C: make(<-chan time.Time)},
			},
		},
		{
			name: "It should do nothing if the subscriber is nil",
			fot:  NewFanOutTicker(1 * time.Second),
			args: args{
				s: nil,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.fot.Unsub(tt.args.s)
			if len(tt.fot.listeners) > 0 {
				t.Errorf("FanOutTicker.Unsub(); Expected listeners store to have 0 length but got %d", len(tt.fot.listeners))
			}
		})
	}
}
