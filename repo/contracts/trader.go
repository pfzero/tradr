package contracts

import (
	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/repo/modelenum"
	"bitbucket.org/pfzero/tradr/repo/modelutil"
	"bitbucket.org/pfzero/tradr/util/tradeutil"
)

// StateChangeSubscriber represents the subscriber fn that will be
// called everytime the trader's state changes
type StateChangeSubscriber func(guid, oldState, newState string)

// TraderStats represents the trader's
// meta information
type TraderStats struct {
	State    string
	GUID     string
	Exchange string
}

// IsTraderActive checks wether the trader is active
func (ts TraderStats) IsTraderActive() bool {
	return ts.State == modelenum.TradeBotRunningState.String()
}

// IsTraderIdle returns true if the trader is idle
// (new or stopped state)
func (ts TraderStats) IsTraderIdle() bool {
	botState := ts.State
	newState := modelenum.TradeBotNewState.String()
	stoppedState := modelenum.TradeBotStoppedState.String()
	return botState == newState || botState == stoppedState
}

// IsTraderFinished returns true if the trader finished
func (ts TraderStats) IsTraderFinished() bool {
	botState := ts.State
	return botState == modelenum.TradeBotCompletedState.String()
}

// Trader represents the interface that needs to be implemented
// by all traders so that the trade manager can succesffully
// handle them; It contains list of needed methods so that
// a trader can be aware of the current market prices and his
// internal balance
type Trader interface {
	// GetStats returns the trader stats
	GetStats() TraderStats

	// MakeDecision is used to determine trader's reaction to
	// price changes; If the trader generates a trade decision
	// based on current market price, the trade decision will then
	// be processed further
	MakeDecision(tradeutil.TradeContext) *TraderResponse

	// GetLedger returns the ledger of the trader
	GetLedger() *modelutil.Ledger

	// SetBackTestMode sets the trader to run in backtesting mode
	SetBackTestMode(bool)

	// HandleProcessedTrade is a method for letting the trader
	// know that the trade was processed and added to its' ledger
	// (trade was processed => trade was either completed or canceled)
	HandleProcessedTrade(t *model.Trade) error

	// SubStateChange and UnsubStateChange allows for subscribing
	// to trader state change
	// The trader must return a subscriber hash id which can be used
	// to unsubscribe
	SubStateChange(fn StateChangeSubscriber) string
	UnsubStateChange(string)
}

// NotifyTraderWithProcessedDecision takes all processed trades within a trade decision and notifies
// the trader
func NotifyTraderWithProcessedDecision(trader Trader, dec *model.TradeDecision) error {
	trades := dec.GetTrades()
	if len(trades) == 0 {
		return nil
	}

	var err error
	for i := 0; i < len(trades); i++ {
		e := NotifyTraderWithProcessedTrade(trader, &trades[i])
		if e != nil {
			err = e
		}
	}

	return err
}

// NotifyTraderWithProcessedTrade notifies the trader that a trade has been processed
func NotifyTraderWithProcessedTrade(trader Trader, t *model.Trade) error {
	return trader.HandleProcessedTrade(t)
}
