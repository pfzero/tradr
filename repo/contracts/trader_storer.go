package contracts

import "time"

// TraderStorer represents the interface of a trader
// store
type TraderStorer interface {
	GetTraderByGUID(guid string) (Trader, error)
	GetTraderForBacktesting(refCtxTime time.Time, guid string) (Trader, error)
	GetActiveTraders() ([]Trader, error)
	GetAllTraders() ([]Trader, error)
	GetActiveTradersForExchange(eName string) ([]Trader, error)
	GetAllTradersForExchange(eName string) ([]Trader, error)
	Name() string
}
