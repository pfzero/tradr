package modelutil

import (
	"reflect"
	"testing"
	"time"

	"bitbucket.org/pfzero/tradr/repo/modelenum"

	"bitbucket.org/pfzero/tradr/repo/model"
)

func TestLedgerIOList_Add(t *testing.T) {
	type args struct {
		io model.LedgerIO
	}
	tests := []struct {
		name string
		liol *LedgerIOList
		args args
		want *LedgerIOList
	}{
		{
			name: "It should add the ledger i/o to the list",
			liol: &LedgerIOList{},
			args: args{
				io: model.LedgerIO{},
			},
			want: &LedgerIOList{
				ioList: []model.LedgerIO{model.LedgerIO{}},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.liol.Add(tt.args.io); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("LedgerIOList.Add() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLedgerIOList_Filter(t *testing.T) {
	type args struct {
		fn LedgerIOIterator
	}
	tests := []struct {
		name string
		liol *LedgerIOList
		args args
		want *LedgerIOList
	}{
		{
			name: "It should filter based on the given iterator",
			liol: &LedgerIOList{
				ioList: []model.LedgerIO{
					model.LedgerIO{AssetID: 1, Amount: 3, Type: modelenum.Deposit.String()},
					model.LedgerIO{AssetID: 1, Amount: 3, Type: modelenum.Withdrawal.String()},
					model.LedgerIO{AssetID: 2, Amount: 2.5, Type: modelenum.Deposit.String()},
				},
			},
			args: args{
				fn: func(lio *model.LedgerIO) bool {
					return lio.Type == modelenum.Deposit.String() && lio.AssetID == 1
				},
			},
			want: &LedgerIOList{
				ioList: []model.LedgerIO{
					model.LedgerIO{AssetID: 1, Amount: 3, Type: modelenum.Deposit.String()},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.liol.Filter(tt.args.fn); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("LedgerIOList.Filter() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLedgerIOList_Find(t *testing.T) {
	type args struct {
		fn LedgerIOIterator
	}
	tests := []struct {
		name string
		liol *LedgerIOList
		args args
		want *model.LedgerIO
	}{
		{
			name: "It should find based on the given iterator",
			liol: &LedgerIOList{
				ioList: []model.LedgerIO{
					model.LedgerIO{AssetID: 1, Amount: 3, Type: modelenum.Deposit.String()},
					model.LedgerIO{AssetID: 1, Amount: 3, Type: modelenum.Withdrawal.String()},
					model.LedgerIO{AssetID: 2, Amount: 2.5, Type: modelenum.Deposit.String()},
				},
			},
			args: args{
				fn: func(lio *model.LedgerIO) bool {
					return lio.Amount > 1 && lio.AssetID == 2 && lio.Type == modelenum.Deposit.String()
				},
			},
			want: &model.LedgerIO{AssetID: 2, Amount: 2.5, Type: modelenum.Deposit.String()},
		},
		{
			name: "It should return nil if no ledger i/o was found",
			liol: &LedgerIOList{
				ioList: []model.LedgerIO{
					model.LedgerIO{AssetID: 1, Amount: 3, Type: modelenum.Deposit.String()},
					model.LedgerIO{AssetID: 1, Amount: 3, Type: modelenum.Withdrawal.String()},
					model.LedgerIO{AssetID: 2, Amount: 2.5, Type: modelenum.Deposit.String()},
				},
			},
			args: args{
				fn: func(lio *model.LedgerIO) bool {
					return lio.AssetID == 5
				},
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.liol.Find(tt.args.fn); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("LedgerIOList.Find() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLedgerIOList_GetAll(t *testing.T) {
	tests := []struct {
		name string
		liol *LedgerIOList
		want []model.LedgerIO
	}{
		{
			name: "It should return the list of ledger i/o",
			liol: &LedgerIOList{
				ioList: []model.LedgerIO{
					model.LedgerIO{AssetID: 1, Amount: 3, Type: modelenum.Deposit.String()},
					model.LedgerIO{AssetID: 1, Amount: 3, Type: modelenum.Withdrawal.String()},
					model.LedgerIO{AssetID: 2, Amount: 2.5, Type: modelenum.Deposit.String()},
				},
			},
			want: []model.LedgerIO{
				model.LedgerIO{AssetID: 1, Amount: 3, Type: modelenum.Deposit.String()},
				model.LedgerIO{AssetID: 1, Amount: 3, Type: modelenum.Withdrawal.String()},
				model.LedgerIO{AssetID: 2, Amount: 2.5, Type: modelenum.Deposit.String()},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.liol.GetAll(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("LedgerIOList.GetAll() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLedgerIOList_Len(t *testing.T) {
	tests := []struct {
		name string
		liol *LedgerIOList
		want int
	}{
		{
			name: "It should return the number of ledger i/o transactions",
			liol: &LedgerIOList{
				ioList: []model.LedgerIO{
					model.LedgerIO{AssetID: 1, Amount: 3, Type: modelenum.Deposit.String()},
					model.LedgerIO{AssetID: 1, Amount: 3, Type: modelenum.Withdrawal.String()},
					model.LedgerIO{AssetID: 2, Amount: 2.5, Type: modelenum.Deposit.String()},
				},
			},
			want: 3,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.liol.Len(); got != tt.want {
				t.Errorf("LedgerIOList.Len() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLedgerIOList_GetAfter(t *testing.T) {
	type args struct {
		t time.Time
	}

	tests := []struct {
		name string
		liol *LedgerIOList
		args args
		want *LedgerIOList
	}{
		{
			name: "It should return the list of ledger i/o after the given time",
			liol: &LedgerIOList{
				ioList: []model.LedgerIO{
					model.LedgerIO{AssetID: 1, Amount: 3, Type: modelenum.Deposit.String(),
						Identity: model.Identity{CreatedAt: time.Date(2016, 1, 1, 0, 0, 0, 0, time.Local)},
					},
					model.LedgerIO{AssetID: 1, Amount: 3, Type: modelenum.Withdrawal.String(),
						Identity: model.Identity{CreatedAt: time.Date(2017, 1, 1, 0, 0, 0, 0, time.Local)},
					},
					model.LedgerIO{AssetID: 2, Amount: 2.5, Type: modelenum.Deposit.String(),
						Identity: model.Identity{CreatedAt: time.Date(2018, 1, 1, 0, 0, 0, 0, time.Local)},
					},
				},
			},
			args: args{
				t: time.Date(2017, 12, 1, 0, 0, 0, 0, time.Local),
			},
			want: &LedgerIOList{
				ioList: []model.LedgerIO{
					model.LedgerIO{AssetID: 2, Amount: 2.5, Type: modelenum.Deposit.String(),
						Identity: model.Identity{CreatedAt: time.Date(2018, 1, 1, 0, 0, 0, 0, time.Local)},
					},
				},
			},
		},
		{
			name: "It should return the list of ledger i/o after the given time",
			liol: &LedgerIOList{
				ioList: []model.LedgerIO{
					model.LedgerIO{AssetID: 1, Amount: 3, Type: modelenum.Deposit.String(),
						Identity: model.Identity{CreatedAt: time.Date(2016, 1, 1, 0, 0, 0, 0, time.Local)},
					},
					model.LedgerIO{AssetID: 1, Amount: 3, Type: modelenum.Withdrawal.String(),
						Identity: model.Identity{CreatedAt: time.Date(2017, 1, 1, 0, 0, 0, 0, time.Local)},
					},
					model.LedgerIO{AssetID: 2, Amount: 2.5, Type: modelenum.Deposit.String(),
						Identity: model.Identity{CreatedAt: time.Date(2018, 1, 1, 0, 0, 0, 0, time.Local)},
					},
				},
			},
			args: args{
				t: time.Date(2017, 1, 1, 0, 0, 0, 0, time.Local),
			},
			want: &LedgerIOList{
				ioList: []model.LedgerIO{
					model.LedgerIO{AssetID: 1, Amount: 3, Type: modelenum.Withdrawal.String(),
						Identity: model.Identity{CreatedAt: time.Date(2017, 1, 1, 0, 0, 0, 0, time.Local)},
					},
					model.LedgerIO{AssetID: 2, Amount: 2.5, Type: modelenum.Deposit.String(),
						Identity: model.Identity{CreatedAt: time.Date(2018, 1, 1, 0, 0, 0, 0, time.Local)},
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.liol.GetAfter(tt.args.t); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("LedgerIOList.GetAfter() = %v, want %v", got, tt.want)
			}
		})
	}
}
