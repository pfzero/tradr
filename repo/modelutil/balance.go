package modelutil

import (
	"fmt"

	"bitbucket.org/pfzero/tradr/logs"
)

// Balance is a utility datastructure
// that helps for keeping currency balance
// it supports 3 types of transactions:
// deposit, withdraw and trade
type Balance struct {
	balance map[string]float64
}

// NewBalance returns a new instance of Balance
// data structure
func NewBalance() *Balance {
	return &Balance{
		balance: make(map[string]float64),
	}
}

// GetRaw returns the raw balance
func (tb *Balance) GetRaw() map[string]float64 {
	copy := make(map[string]float64)
	for k, v := range tb.balance {
		copy[k] = v
	}
	return copy
}

// Deposit allows a deposit to the current balance
func (tb *Balance) Deposit(part TransactionPart) {
	_, ok := tb.balance[part.GetSymbol()]

	if !ok {
		tb.balance[part.GetSymbol()] = 0
	}

	tb.balance[part.GetSymbol()] += part.GetAmount()
}

// GetBalanceFor returns the balance for the given currency
// symbol
func (tb *Balance) GetBalanceFor(sym string) float64 {
	existingBalance, ok := tb.balance[sym]
	if !ok {
		return 0
	}
	return existingBalance
}

// CanWithraw checks wether the intended withdrawal transaction
// is possible
func (tb *Balance) CanWithraw(part TransactionPart) bool {
	existingBalance, ok := tb.balance[part.GetSymbol()]
	if !ok {
		return false
	}
	return existingBalance >= part.GetAmount()
}

// Withdraw performs a withdrawal on the current balance
func (tb *Balance) Withdraw(part TransactionPart) error {
	if part.GetAmount() == 0 {
		return nil
	}

	if !tb.CanWithraw(part) {
		logs.GetLogger().Errorf("Tried to withdraw %s - %f; Have %f", part.GetSymbol(), part.GetAmount(), tb.balance[part.GetSymbol()])
		return fmt.Errorf(`Cannot withraw that amount of symbol: "%s". Insufficient funds`, part.GetSymbol())
	}
	tb.balance[part.GetSymbol()] -= part.GetAmount()
	return nil
}

// Trade performs a trade between 2 currencies
func (tb *Balance) Trade(from TransactionPart, to TransactionPart) error {
	if !tb.CanWithraw(from) {
		return fmt.Errorf(`Cannot trade that amount of currency: "%s". Available amount: %f; Intended Amount: %f; Diff: %f`,
			from.GetSymbol(),
			tb.balance[from.GetSymbol()],
			from.GetAmount(),
			tb.balance[from.GetSymbol()]-from.GetAmount(),
		)
	}
	tb.Withdraw(from)
	tb.Deposit(to)
	return nil
}
