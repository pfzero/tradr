package modelutil

import "testing"

func TestLedgerFragment_GetSymbol(t *testing.T) {
	tests := []struct {
		name string
		la   *LedgerFragment
		want string
	}{
		{
			name: "It should return the symbol of the ledger fragment",
			la: &LedgerFragment{
				Symbol: "ADA",
				Amount: 1,
			},
			want: "ADA",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.la.GetSymbol(); got != tt.want {
				t.Errorf("LedgerFragment.GetSymbol() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLedgerFragment_GetAmount(t *testing.T) {
	tests := []struct {
		name string
		la   *LedgerFragment
		want float64
	}{
		{
			name: "It should return the amount of the ledger fragment",
			la: &LedgerFragment{
				Symbol: "ADA",
				Amount: 1,
			},
			want: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.la.GetAmount(); got != tt.want {
				t.Errorf("LedgerFragment.GetAmount() = %v, want %v", got, tt.want)
			}
		})
	}
}
