package modelutil

import (
	"reflect"
	"testing"
)

func TestNewBalance(t *testing.T) {
	tests := []struct {
		name string
		want *Balance
	}{
		{
			name: "It should return a new instance of balance",
			want: &Balance{balance: make(map[string]float64)},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewBalance(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewBalance() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBalance_GetRaw(t *testing.T) {
	tests := []struct {
		name string
		tb   *Balance
		want map[string]float64
	}{
		{
			name: "It should get empty balance if it wasn't modified",
			tb:   NewBalance(),
			want: map[string]float64{},
		},
		{
			name: "It should get empty balance",
			tb: &Balance{
				balance: map[string]float64{
					"ADA": 5,
				},
			},
			want: map[string]float64{
				"ADA": 5,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tb.GetRaw(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Balance.GetRaw() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBalance_Deposit(t *testing.T) {
	type args struct {
		part TransactionPart
	}
	tests := []struct {
		name string
		tb   *Balance
		args args
		want *Balance
	}{
		{
			name: "It should deposit the given amount for the given symbol",
			tb:   NewBalance(),
			args: args{
				part: &LedgerFragment{Symbol: "ADA", Amount: 5},
			},
			want: &Balance{
				balance: map[string]float64{
					"ADA": 5,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.tb.Deposit(tt.args.part)
			if !reflect.DeepEqual(tt.tb, tt.want) {
				t.Errorf("Balance.Deposit(); got = %v; want = %v;", tt.tb, tt.want)
			}
		})
	}
}

func TestBalance_GetBalanceFor(t *testing.T) {
	type args struct {
		sym string
	}
	tests := []struct {
		name string
		tb   *Balance
		args args
		want float64
	}{
		{
			name: "It should return the balance for the given symbol",
			tb: &Balance{
				balance: map[string]float64{
					"ADA": 10,
				},
			},
			args: args{sym: "ADA"},
			want: 10,
		},
		{
			name: "It should return 0 if symbol not found",
			tb: &Balance{
				balance: map[string]float64{
					"ADA": 10,
				},
			},
			args: args{sym: "NOT_FOUND_SYMBOL"},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tb.GetBalanceFor(tt.args.sym); got != tt.want {
				t.Errorf("Balance.GetBalanceFor() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBalance_CanWithraw(t *testing.T) {
	type args struct {
		part TransactionPart
	}
	tests := []struct {
		name string
		tb   *Balance
		args args
		want bool
	}{
		{
			name: "it should return true if the amount is withdrawable",
			tb:   &Balance{balance: map[string]float64{"ADA": 8}},
			args: args{part: &LedgerFragment{Symbol: "ADA", Amount: 8}},
			want: true,
		},
		{
			name: "it should return false if the amount is not withdrawable",
			tb:   &Balance{balance: map[string]float64{"ADA": 8}},
			args: args{part: &LedgerFragment{Symbol: "ADA", Amount: 10}},
			want: false,
		},
		{
			name: "it should return false if the symbol doesn't exist",
			tb:   &Balance{balance: map[string]float64{"ADA": 8}},
			args: args{part: &LedgerFragment{Symbol: "ETH", Amount: 10}},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tb.CanWithraw(tt.args.part); got != tt.want {
				t.Errorf("Balance.CanWithraw() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBalance_Withdraw(t *testing.T) {
	type args struct {
		part TransactionPart
	}

	tests := []struct {
		name    string
		tb      *Balance
		args    args
		want    *Balance
		wantErr bool
	}{
		{
			name:    "It should withdraw the requested symbol and amount",
			tb:      &Balance{balance: map[string]float64{"ADA": 10}},
			args:    args{part: &LedgerFragment{Symbol: "ADA", Amount: 8}},
			want:    &Balance{balance: map[string]float64{"ADA": 2}},
			wantErr: false,
		},
		{
			name:    "It should return error if withdrawal is not possible",
			tb:      &Balance{balance: map[string]float64{"ADA": 10}},
			args:    args{part: &LedgerFragment{Symbol: "ADA", Amount: 11}},
			want:    &Balance{balance: map[string]float64{"ADA": 10}},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.tb.Withdraw(tt.args.part); (err != nil) != tt.wantErr {
				t.Errorf("Balance.Withdraw() error = %v, wantErr %v", err, tt.wantErr)
			}

			if !reflect.DeepEqual(tt.tb, tt.want) {
				t.Errorf("Balance.Withdraw() incorrect balance after withdrawing; Got = %v; Want = %v", tt.tb, tt.want)
			}
		})
	}
}

func TestBalance_Trade(t *testing.T) {
	type args struct {
		from TransactionPart
		to   TransactionPart
	}
	tests := []struct {
		name    string
		tb      *Balance
		args    args
		want    *Balance
		wantErr bool
	}{
		{
			name: "It should trade between the 2 symbols",
			tb:   &Balance{balance: map[string]float64{"ADA": 10, "ETH": 5}},
			args: args{
				from: &LedgerFragment{Symbol: "ADA", Amount: 5},
				to:   &LedgerFragment{Symbol: "ETH", Amount: 2},
			},
			want:    &Balance{balance: map[string]float64{"ADA": 5, "ETH": 7}},
			wantErr: false,
		},
		{
			name: "It should not trade if the requested amount is too big",
			tb:   &Balance{balance: map[string]float64{"ADA": 10, "ETH": 5}},
			args: args{
				from: &LedgerFragment{Symbol: "ADA", Amount: 20},
				to:   &LedgerFragment{Symbol: "ETH", Amount: 10},
			},
			want:    &Balance{balance: map[string]float64{"ADA": 10, "ETH": 5}},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.tb.Trade(tt.args.from, tt.args.to); (err != nil) != tt.wantErr {
				t.Errorf("Balance.Trade() error = %v, wantErr %v", err, tt.wantErr)
			}
			if !reflect.DeepEqual(tt.tb, tt.want) {
				t.Errorf("Balance.Trade() incorrect balance; Got = %v; Want = %v", tt.tb, tt.want)
			}
		})
	}
}
