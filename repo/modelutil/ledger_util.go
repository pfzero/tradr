package modelutil

import (
	"bitbucket.org/pfzero/tradr/logs"
	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/repo/modelenum"
	"github.com/palantir/stacktrace"
	"github.com/sirupsen/logrus"
)

// TransactionPart represents a single part
// of a transaction;
// eg. Trade TransactionPart(1 ETH) -> TransactionPart(0.5 BTC)
type TransactionPart interface {
	GetSymbol() string
	GetAmount() float64
}

// LedgerTrade represents a trade between
// 2 assets
type LedgerTrade struct {
	From TransactionPart
	To   TransactionPart
}

// LedgerFragment is a datastructure that
// represents a fragment from a trade;
// it implements the TransactionPart interface
// eg. Trade 3 ETH -> 1 BCH =>
// 1st fragment: 3 ETH
// 2nd fragment: 1 BCH
type LedgerFragment struct {
	Symbol string
	Amount float64
}

// GetSymbol returns the symbol of the fragment
func (la *LedgerFragment) GetSymbol() string {
	return la.Symbol
}

// GetAmount returns the amount encoded within this fragment
func (la *LedgerFragment) GetAmount() float64 {
	return la.Amount
}

// AddTradeDecisionToLedger adds all the trades of a trade decision to the
// ledger
func AddTradeDecisionToLedger(dec *model.TradeDecision, l *Ledger) error {
	trades := dec.GetTrades()
	defer l.Acquire().Release()

	for i := 0; i < len(trades); i++ {
		trade := &trades[i]
		AddTradeToLedger(trade, l)
	}

	return nil
}

// AddTradeToLedger adds a trade to the given ledger
func AddTradeToLedger(trade *model.Trade, l *Ledger) {
	switch trade.Status {
	case modelenum.NewTrade.String():
		l.StageTrade(trade)
	case modelenum.InProgressTrade.String():
		l.StageTrade(trade)
	case modelenum.CanceledTrade.String():
		l.CancelTrade(trade)
	case modelenum.CompletedTrade.String():
		l.CommitTrade(trade)
	case modelenum.ErroredTrade.String():
		l.CancelTrade(trade)
	default:
		logs.GetLogger().WithFields(logrus.Fields{
			"module": "modelutil.ledger_util.AddTradeDecisionToLedger",
			"stack":  stacktrace.NewError("Unknown trade state %s", trade.Status),
		}).
			Errorf("Unknown trade state: %s", trade.Status)
	}
}
