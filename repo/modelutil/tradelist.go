package modelutil

import (
	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/repo/modelenum"
)

// TradeList is a data structure that encapsulates a list of
// trades
type TradeList struct {
	trades []model.Trade
}

// Add inserts a new trade to the list; It keeps the list of
// trades in asc order by their tradefinishedtime, or created time
// if tradefinishedtime is missing
func (tl *TradeList) Add(trade model.Trade) *TradeList {

	if len(tl.trades) == 0 {
		tl.trades = append(tl.trades, trade)
		return tl
	}

	firstExistingTimestamp := tl.trades[0].GetTradeRelevantTime()
	lastExistingTimestamp := tl.trades[len(tl.trades)-1].GetTradeRelevantTime()
	tradeTimestamp := trade.GetTradeRelevantTime()

	if !lastExistingTimestamp.After(tradeTimestamp) {
		tl.trades = append(tl.trades, trade)
		return tl
	}

	if !firstExistingTimestamp.Before(tradeTimestamp) {
		tl.trades = append([]model.Trade{trade}, tl.trades...)
		return tl
	}

	for i := len(tl.trades) - 1; i >= 0; i-- {
		existingTrade := &tl.trades[i]
		existingTimestamp := existingTrade.GetTradeRelevantTime()
		if existingTimestamp.Before(tradeTimestamp) {
			rest := make([]model.Trade, len(tl.trades[i+1:]))
			copy(rest, tl.trades[i+1:])
			leftSlice := append(tl.trades[:i+1], trade)
			tl.trades = append(leftSlice, rest...)
			break
		}
	}
	return tl
}

// Clone returns a new TradeList with all the trades of the current
// TradeList copied in the new one
func (tl *TradeList) Clone() *TradeList {
	cp := make([]model.Trade, len(tl.trades))
	copy(cp, tl.trades)
	return &TradeList{trades: cp}
}

// Filter filters the list based on the given filter function
func (tl *TradeList) Filter(f func(t *model.Trade) bool) *TradeList {
	res := make([]model.Trade, 0, cap(tl.trades))
	for i := 0; i < len(tl.trades); i++ {
		trade := &tl.trades[i]
		if f(trade) {
			res = append(res, *trade)
		}
	}
	return &TradeList{trades: res}
}

// FilterByStatus returns the trades filtered by status
func (tl *TradeList) FilterByStatus(status modelenum.TradeState) *TradeList {
	return tl.Filter(func(t *model.Trade) bool { return t.Status == status.String() })
}

// Find find the trade based on the given find function
func (tl *TradeList) Find(f func(t *model.Trade) bool) *model.Trade {
	for i := 0; i < len(tl.trades); i++ {
		trade := &tl.trades[i]
		if f(trade) {
			return trade
		}
	}
	return nil
}

// GetAll returns the list of all trades
func (tl *TradeList) GetAll() []model.Trade {
	return tl.trades
}

// GetByID returns the trade that has the given id
func (tl *TradeList) GetByID(tradeID uint) *model.Trade {
	for i := 0; i < len(tl.trades); i++ {
		trade := &tl.trades[i]
		if trade.GetID() == tradeID {
			return trade
		}
	}
	return nil
}

// GetByIndex returns the trade that can be found at the given
// index
func (tl *TradeList) GetByIndex(idx int) *model.Trade {
	if idx >= 0 && idx < len(tl.trades) {
		return &tl.trades[idx]
	}
	return nil
}

// GetBetween returns the list of trades that were between the two
// given symbols
func (tl *TradeList) GetBetween(fsym, tsym string) *TradeList {
	return tl.Filter(func(t *model.Trade) bool {
		return t.From.Symbol == fsym && t.To.Symbol == tsym
	})
}

// Len returns the number of trades within the list
func (tl *TradeList) Len() int {
	return len(tl.trades)
}
