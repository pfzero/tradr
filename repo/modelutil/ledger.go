package modelutil

import (
	"sync"

	"bitbucket.org/pfzero/tradr/logs"

	"bitbucket.org/pfzero/tradr/repo/modelenum"
	"bitbucket.org/pfzero/tradr/util/str"

	"bitbucket.org/pfzero/tradr/repo/model"
)

// Ledger is a thread-safe data structure that encapsulates a list of
// trades
type Ledger struct {
	tradeList *TradeList
	ioList    *LedgerIOList

	resourceLock sync.Mutex

	stagedTrades map[string]model.Trade

	// lazily constructed per-asset balance;
	// (investments made in other assets from the given assets
	// + profits returned from these)
	assetBalances map[string]*Balance

	totalBalance *Balance
}

// NewLedger is the Ledger datastructure
// constructor
func NewLedger() *Ledger {
	return &Ledger{
		tradeList:     &TradeList{},
		ioList:        &LedgerIOList{},
		stagedTrades:  map[string]model.Trade{},
		assetBalances: make(map[string]*Balance),
		totalBalance:  NewBalance(),
	}
}

// Acquire locks the ledger so it can be used by only 1
// external client
func (ledger *Ledger) Acquire() *Ledger {
	ledger.resourceLock.Lock()
	return ledger
}

// Release releases the ledger so it can be used by
// other clients waiting for it
func (ledger *Ledger) Release() *Ledger {
	ledger.resourceLock.Unlock()
	return ledger
}

// AddLedgerIO adds a new ledger i/o to the list
func (ledger *Ledger) AddLedgerIO(lio *model.LedgerIO) *Ledger {
	ledger.ioList.Add(*lio)

	if lio.Type == modelenum.Deposit.String() {
		ledger.totalBalance.Deposit(ConvertLedgerIOToLedgerFragment(lio))
	} else {
		ledger.totalBalance.Withdraw(ConvertLedgerIOToLedgerFragment(lio))
	}

	if ledger.isAssetBalanceActive() {
		ledger.addAssetLedgerIO(lio)
	}

	return ledger
}

// StageTrade stages the given trade so that the balance correctly
// reflects this intent
func (ledger *Ledger) StageTrade(t *model.Trade) *Ledger {
	if ledger.isTradeStaged(t) {
		return ledger
	}

	if t.LedgerTxID == "" {
		t.LedgerTxID = str.GenerateToken()
	}

	ledger.stagedTrades[t.LedgerTxID] = *t

	err := ledger.totalBalance.Withdraw(&LedgerFragment{
		Amount: t.FromAmountIntent,
		Symbol: t.From.Symbol,
	})

	if err != nil {
		logs.GetLogger().Errorf("Tried to withdraw from total balance %f %s but got error %s", t.FromAmountIntent, t.From.Symbol, err.Error())
	}

	if ledger.isAssetBalanceActive() {
		ledger.stageAssetTrade(t)
	}

	return ledger
}

// CommitTrade commits the previously staged trade
func (ledger *Ledger) CommitTrade(t *model.Trade) *Ledger {

	if !ledger.isTradeStaged(t) {
		if ledger.isAssetBalanceActive() {
			ledger.addAssetTrade(t)
		}

		return ledger.addTrade(t)
	}

	stagedTrade := ledger.stagedTrades[t.LedgerTxID]

	// revert the previous withdrawal so that now
	// we can work with the actual trade amount instead
	// of the intended amount
	ledger.totalBalance.Deposit(&LedgerFragment{
		Symbol: stagedTrade.From.Symbol,
		Amount: stagedTrade.FromAmountIntent,
	})

	if ledger.isAssetBalanceActive() {
		ledger.commitAssetTrade(t)
	}

	delete(ledger.stagedTrades, t.LedgerTxID)

	return ledger.addTrade(t)
}

// CancelTrade cancels a previously staged trade
func (ledger *Ledger) CancelTrade(t *model.Trade) *Ledger {

	if !ledger.isTradeStaged(t) {
		return ledger
	}

	stagedTrade := ledger.stagedTrades[t.LedgerTxID]

	// revert the previous withdrawal so that now
	// we can work with the actual trade amount instead
	// of the intended amount
	ledger.totalBalance.Deposit(&LedgerFragment{
		Symbol: stagedTrade.From.Symbol,
		Amount: stagedTrade.FromAmountIntent,
	})

	if ledger.isAssetBalanceActive() {
		ledger.cancelAssetTrade(t)
	}

	delete(ledger.stagedTrades, t.LedgerTxID)

	return ledger
}

// Feed feeds the ledger with the given trades and ledgerIOs
func (ledger *Ledger) Feed(trades []model.Trade, ioList []model.LedgerIO) *Ledger {
	currentTradeIDX := 0
	currentLedgerIOIDX := 0

	for currentLedgerIOIDX < len(ioList) || currentTradeIDX < len(trades) {
		var currentTrade *model.Trade
		var currentLedgerIO *model.LedgerIO

		if currentTradeIDX == len(trades) {
			currentLedgerIO = &ioList[currentLedgerIOIDX]
			ledger.AddLedgerIO(currentLedgerIO)
			currentLedgerIOIDX++
			continue
		}

		if currentLedgerIOIDX == len(ioList) {
			currentTrade = &trades[currentTradeIDX]
			AddTradeToLedger(currentTrade, ledger)
			currentTradeIDX++
			continue
		}

		currentTrade = &trades[currentTradeIDX]
		currentLedgerIO = &ioList[currentLedgerIOIDX]

		if currentTrade.GetTradeRelevantTime().Before(currentLedgerIO.CreatedAt) {
			currentTrade = &trades[currentTradeIDX]
			AddTradeToLedger(currentTrade, ledger)
			currentTradeIDX++
			continue
		}

		ledger.AddLedgerIO(currentLedgerIO)
		currentLedgerIOIDX++
	}

	return ledger
}

// GetTradeList returns the list of trades from this ledger
func (ledger *Ledger) GetTradeList() *TradeList {
	return ledger.tradeList
}

// GetLedgerIOList returns the list of ledger i/o transactions
func (ledger *Ledger) GetLedgerIOList() *LedgerIOList {
	return ledger.ioList
}

// GetTotalBalance returns the total balance of the ledger
func (ledger *Ledger) GetTotalBalance() *Balance {
	return ledger.totalBalance
}

// GetAssetInvestmentBalance computes the individual asset balance; Deposits
// plus withdrawals for that asset and investments + profits yields the asset's
// balance;
func (ledger *Ledger) GetAssetInvestmentBalance(assetSymbol string) *Balance {
	if !ledger.isAssetBalanceActive() {
		ledger.ensureAssetBalanceInitialized(assetSymbol)
		ledger.Rebuild()
	}

	return ledger.assetBalances[assetSymbol]
}

// GetBetween returns the list of trades between the given symbols; it also includes
// staged trades
func (ledger *Ledger) GetBetween(fsym, tsym string) *TradeList {
	list := ledger.GetTradeList().GetBetween(fsym, tsym)

	for k := range ledger.stagedTrades {
		trade := ledger.stagedTrades[k]
		if trade.From.Symbol == fsym && trade.To.Symbol == tsym {
			list.Add(trade)
		}
	}

	return list
}

// GetInvestments return the list of investments from fsym to tsym assets
func (ledger *Ledger) GetInvestments(fsym, tsym string) *TradeList {
	return ledger.GetBetween(fsym, tsym).Filter(func(t *model.Trade) bool {
		return t.LinkedTradeID == 0
	})
}

// GetProfits returns the list of profits made from fsym to tsym
func (ledger *Ledger) GetProfits(fsym, tsym string) *TradeList {
	return ledger.GetBetween(tsym, fsym).Filter(func(t *model.Trade) bool {
		return t.LinkedTradeID > 0
	})
}

// GetUnProfitedInvestments returns the list of unprofited investments
func (ledger *Ledger) GetUnProfitedInvestments(fsym, tsym string) *TradeList {
	investments := ledger.GetInvestments(fsym, tsym)
	profits := ledger.GetProfits(fsym, tsym)

	unProfitedInvestments := investments.Filter(func(t *model.Trade) bool {
		found := profits.Find(func(profit *model.Trade) bool { return profit.LinkedTradeID == t.GetID() })
		return found == nil
	})

	return unProfitedInvestments
}

// GetLastInvestment returns the latest investment made between the given
// 2 symbols; It first searches through staged trades; if not found, it searches
// through ledger's trade list; otherwise it returns the latest trade from the
// list of trades found in staged trades;
func (ledger *Ledger) GetLastInvestment(fsym, tsym string) *model.Trade {
	investments := ledger.GetInvestments(fsym, tsym)
	if investments.Len() == 0 {
		return nil
	}
	return investments.GetByIndex(investments.Len() - 1)
}

// IsLastInvestmentPending checks if the latest investment made is unprofited; it first
// looks for any staged trades and then searches through the trade list
func (ledger *Ledger) IsLastInvestmentPending(fsym, tsym string) bool {
	lastInvestment := ledger.GetLastInvestment(fsym, tsym)
	if lastInvestment == nil {
		return false
	}

	profits := ledger.GetProfits(fsym, tsym)
	found := profits.Find(func(t *model.Trade) bool { return t.LinkedTradeID == lastInvestment.GetID() })
	return found == nil
}

// GetInvestmentStreak returns the list of latest consecutive investments between fsym and tsym;
// the list starts from the last profit made for this pair
func (ledger *Ledger) GetInvestmentStreak(fsym, tsym string) *TradeList {
	investments := ledger.GetInvestments(fsym, tsym)
	profits := ledger.GetProfits(fsym, tsym)

	trades := investments.GetAll()
	streak := []model.Trade{}

	for i := len(trades) - 1; i >= 0; i-- {
		investment := trades[i]
		if profits.Find(func(t *model.Trade) bool { return t.LinkedTradeID == investment.GetID() }) == nil {
			streak = append([]model.Trade{investment}, streak...)
		} else {
			break
		}
	}

	return &TradeList{trades: streak}
}

// GetTradeBackAmount gets the amount required to trade back; If after the given investments
// there have been withdrawals, then this amount needs to be a percentage of the amount
// left; We don't provide the percentage with high fidelity; if multiple investments and
// withdrawals were made after the given investment, then all the withdrawals are computed
// as a percentage and the
func (ledger *Ledger) GetTradeBackAmount(investment *model.Trade) float64 {
	from := investment.From.Symbol
	to := investment.To.Symbol
	bought := investment.ToAmountTrade
	balance := ledger.GetAssetInvestmentBalance(from)
	existing := balance.GetBalanceFor(to)

	// if verything was withdrawn...
	if existing == 0 {
		return 0
	}

	withdrawals := ledger.GetLedgerIOList().
		GetWithdrawalsFor(to)

	// nothing withdrawn
	if withdrawals.Len() == 0 {
		return bought
	}

	unProfitedInvestments := ledger.GetUnProfitedInvestments(from, to).
		FilterByStatus(modelenum.CompletedTrade)

	// in this case there aren't any investments unprofited; meaning that this investment
	// too has a profit trade associated as well
	// if unProfitedInvestments.Len() == 0 {
	// 	return 0
	// }

	// Get withdrawals after the first unprofited investment to see how all the withdrawals
	// impacted the investments
	withdrawals = withdrawals.GetAfter(unProfitedInvestments.GetByIndex(0).GetTradeRelevantTime())

	var balanceWithoutWithdrawals float64

	for i := 0; i < unProfitedInvestments.Len(); i++ {
		balanceWithoutWithdrawals += unProfitedInvestments.GetByIndex(i).ToAmountTrade
	}

	percentage := (balanceWithoutWithdrawals - existing) / balanceWithoutWithdrawals

	return bought - percentage*bought
}

// Rebuild rebuilds the ledger; it can be useful when
// asset balances are activated
func (ledger *Ledger) Rebuild() *Ledger {
	tradeList := ledger.GetTradeList().Clone()

	for k := range ledger.stagedTrades {
		trade := ledger.stagedTrades[k]
		tradeList.Add(trade)
	}

	trades := tradeList.GetAll()
	ioList := ledger.ioList.GetAll()

	useAssetBalance := false
	assetBalance := "" // check if asset balances are needed
	for k := range ledger.assetBalances {
		assetBalance = k
		break
	}

	if assetBalance != "" {
		useAssetBalance = true
	}

	if useAssetBalance {
		ledger.assetBalances = map[string]*Balance{assetBalance: NewBalance()}
	} else {
		ledger.assetBalances = map[string]*Balance{}
	}

	ledger.tradeList = &TradeList{}
	ledger.ioList = &LedgerIOList{}
	ledger.stagedTrades = map[string]model.Trade{}
	ledger.totalBalance = NewBalance()

	return ledger.Feed(trades, ioList)
}

func (ledger *Ledger) isTradeStaged(t *model.Trade) bool {
	_, ok := ledger.stagedTrades[t.LedgerTxID]
	return ok
}

func (ledger *Ledger) addTrade(t *model.Trade) *Ledger {
	ledgerTrade := ConvertTradeToUtilTrade(t)
	ledger.totalBalance.Trade(ledgerTrade.From, ledgerTrade.To)
	ledger.tradeList.Add(*t)
	return ledger
}

func (ledger *Ledger) isAssetBalanceActive() bool {
	return len(ledger.assetBalances) > 0
}

func (ledger *Ledger) addAssetLedgerIO(lio *model.LedgerIO) *Ledger {
	sym := lio.Asset.Symbol
	ledger.ensureAssetBalanceInitialized(sym)
	balance := ledger.assetBalances[sym]

	if lio.Type == modelenum.Deposit.String() {
		balance.Deposit(ConvertLedgerIOToLedgerFragment(lio))
	} else {

		var remainingTotalBalance float64
		summary := ConvertLedgerIOToLedgerFragment(lio)
		for _, balance := range ledger.assetBalances {
			assetBalance := balance.GetBalanceFor(summary.GetSymbol())
			remainingTotalBalance += assetBalance
		}
		percentage := summary.GetAmount() / remainingTotalBalance
		for _, balance := range ledger.assetBalances {
			assetBalance := balance.GetBalanceFor(summary.GetSymbol())
			toWithdraw := percentage * assetBalance
			balance.Withdraw(&LedgerFragment{
				Amount: toWithdraw,
				Symbol: summary.GetSymbol(),
			})
		}
	}

	return ledger
}

func (ledger *Ledger) addAssetTrade(t *model.Trade) *Ledger {
	isInvestment := t.LinkedTradeID == 0
	var affectedBalance string

	if isInvestment {
		affectedBalance = t.From.Symbol
	} else {
		affectedBalance = t.To.Symbol
	}

	ledger.ensureAssetBalanceInitialized(affectedBalance)
	balance := ledger.assetBalances[affectedBalance]
	parts := ConvertTradeToUtilTrade(t)
	balance.Trade(parts.From, parts.To)
	return ledger
}

func (ledger *Ledger) stageAssetTrade(t *model.Trade) *Ledger {
	var baseSym string

	if t.LinkedTradeID == 0 {
		baseSym = t.From.Symbol
	} else {
		baseSym = t.To.Symbol
	}

	ledger.ensureAssetBalanceInitialized(baseSym)
	balance := ledger.assetBalances[baseSym]

	err := balance.Withdraw(&LedgerFragment{
		Symbol: t.From.Symbol,
		Amount: t.FromAmountIntent,
	})

	if err != nil {
		logs.GetLogger().Errorf("Tried to withdraw %f %s from asset balance; Got error: %s", t.FromAmountIntent, t.From.Symbol, err.Error())
	}

	return ledger
}

func (ledger *Ledger) commitAssetTrade(finalTrade *model.Trade) *Ledger {
	var baseSym string

	if finalTrade.LinkedTradeID == 0 {
		baseSym = finalTrade.From.Symbol
	} else {
		baseSym = finalTrade.To.Symbol
	}

	if _, ok := ledger.stagedTrades[finalTrade.LedgerTxID]; !ok {
		return ledger.addAssetTrade(finalTrade)
	}

	ledger.ensureAssetBalanceInitialized(baseSym)
	balance := ledger.assetBalances[baseSym]
	stagedTrade := ledger.stagedTrades[finalTrade.LedgerTxID]

	balance.Deposit(&LedgerFragment{
		Symbol: stagedTrade.From.Symbol,
		Amount: stagedTrade.FromAmountIntent,
	})

	return ledger.addAssetTrade(finalTrade)
}

func (ledger *Ledger) cancelAssetTrade(finalTrade *model.Trade) *Ledger {
	var baseSym string

	if finalTrade.LinkedTradeID == 0 {
		baseSym = finalTrade.From.Symbol
	} else {
		baseSym = finalTrade.To.Symbol
	}

	if _, ok := ledger.stagedTrades[finalTrade.LedgerTxID]; !ok {
		return ledger
	}

	ledger.ensureAssetBalanceInitialized(baseSym)

	balance := ledger.assetBalances[baseSym]
	stagedTrade := ledger.stagedTrades[finalTrade.LedgerTxID]

	balance.Deposit(&LedgerFragment{
		Symbol: stagedTrade.From.Symbol,
		Amount: stagedTrade.FromAmountIntent,
	})

	return ledger
}

func (ledger *Ledger) ensureAssetBalanceInitialized(sym string) {
	if _, ok := ledger.assetBalances[sym]; !ok {
		ledger.assetBalances[sym] = NewBalance()
	}
}
