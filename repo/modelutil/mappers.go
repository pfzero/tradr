package modelutil

import (
	"bitbucket.org/pfzero/tradr/repo/model"
)

// ConvertTradeToUtilTrade converts a trade model to a ledger trade
func ConvertTradeToUtilTrade(t *model.Trade) *LedgerTrade {

	if !t.IsCompleteTrade() {
		return nil
	}

	return &LedgerTrade{
		From: &LedgerFragment{
			Symbol: t.From.Symbol,
			Amount: t.FromAmountTrade,
		},
		To: &LedgerFragment{
			Symbol: t.To.Symbol,
			Amount: t.ToAmountTrade,
		},
	}
}

// ConvertLedgerIOToLedgerFragment converts a user transaction to a ledger fragment
func ConvertLedgerIOToLedgerFragment(lio *model.LedgerIO) *LedgerFragment {
	return &LedgerFragment{
		Symbol: lio.Asset.Symbol,
		Amount: lio.Amount,
	}
}

// ConvertTradeDecisionToLedgerTrades converts a trade decision's trades to a list
// of ledger trades
func ConvertTradeDecisionToLedgerTrades(td *model.TradeDecision) []LedgerTrade {
	utilTrades := []LedgerTrade{}
	for _, trade := range td.Trades {
		if !trade.IsCompleteTrade() {
			continue
		}

		utilTrade := ConvertTradeToUtilTrade(&trade)
		if utilTrade != nil {
			utilTrades = append(utilTrades, *utilTrade)
		}
	}
	return utilTrades
}
