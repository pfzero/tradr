package modelutil

import (
	"reflect"
	"testing"
	"time"

	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/repo/modelenum"
	"github.com/kylelemons/godebug/pretty"
)

func TestNewLedger(t *testing.T) {
	tests := []struct {
		name string
		want *Ledger
	}{
		{
			name: "It should create a new Ledger",
			want: &Ledger{
				tradeList:     &TradeList{},
				ioList:        &LedgerIOList{},
				stagedTrades:  make(map[string]model.Trade),
				assetBalances: make(map[string]*Balance),
				totalBalance:  NewBalance(),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewLedger(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewLedger() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLedger_Acquire(t *testing.T) {
	tests := []struct {
		name   string
		ledger *Ledger
		want   *Ledger
	}{
		{
			name:   "It should acquire lock",
			ledger: &Ledger{},
			want:   &Ledger{},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.want.resourceLock.Lock()

			if got := tt.ledger.Acquire(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Ledger.Acquire() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLedger_Release(t *testing.T) {
	tests := []struct {
		name   string
		ledger *Ledger
		want   *Ledger
	}{
		{
			name: "It should release lock",
			ledger: func() *Ledger {
				l := &Ledger{}
				l.resourceLock.Lock()
				return l
			}(),
			want: &Ledger{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.ledger.Release(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Ledger.Release() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLedger_AddLedgerIO(t *testing.T) {
	type args struct {
		lio *model.LedgerIO
	}
	tests := []struct {
		name   string
		ledger *Ledger
		args   args
		want   *Ledger
	}{
		{
			name:   "It should add a new entry to the list of i/o of the ledger and deposit to total balance",
			ledger: NewLedger(),
			args: args{
				lio: &model.LedgerIO{
					Type: modelenum.Deposit.String(), Amount: 10,
					Asset: model.TradeableAsset{Symbol: "ADA"},
				},
			},
			want: func() *Ledger {
				l := NewLedger()
				l.ioList.ioList = []model.LedgerIO{
					model.LedgerIO{
						Type: modelenum.Deposit.String(), Amount: 10,
						Asset: model.TradeableAsset{Symbol: "ADA"},
					},
				}
				l.totalBalance.balance["ADA"] = 10
				return l
			}(),
		},
		{
			name: "It should add a new entry to the list of i/o of the ledger and withdraw from total balance",
			ledger: func() *Ledger {
				l := NewLedger()
				l.ioList.ioList = []model.LedgerIO{
					model.LedgerIO{
						Type: modelenum.Deposit.String(), Amount: 20,
						Asset: model.TradeableAsset{Symbol: "ADA"},
					},
				}
				l.totalBalance.balance["ADA"] = 20
				return l
			}(),
			args: args{
				lio: &model.LedgerIO{
					Type: modelenum.Withdrawal.String(), Amount: 10,
					Asset: model.TradeableAsset{Symbol: "ADA"},
				},
			},
			want: func() *Ledger {
				l := NewLedger()
				l.ioList.ioList = []model.LedgerIO{
					model.LedgerIO{
						Type: modelenum.Deposit.String(), Amount: 20,
						Asset: model.TradeableAsset{Symbol: "ADA"},
					},
					model.LedgerIO{
						Type: modelenum.Withdrawal.String(), Amount: 10,
						Asset: model.TradeableAsset{Symbol: "ADA"},
					},
				}
				l.totalBalance.balance["ADA"] = 10
				return l
			}(),
		},
		{
			name: "It should add a new deposit entry to the list of i/o of the ledger and asset specific balance",
			ledger: func() *Ledger {
				l := NewLedger()
				l.ioList.ioList = []model.LedgerIO{
					model.LedgerIO{
						Type: modelenum.Deposit.String(), Amount: 10,
						Asset: model.TradeableAsset{Symbol: "ADA"},
					},
				}
				l.totalBalance.balance["ADA"] = 10
				l.assetBalances["ADA"] = NewBalance()
				l.assetBalances["ADA"].balance["ADA"] = 10
				return l
			}(),
			args: args{
				lio: &model.LedgerIO{
					Type: modelenum.Deposit.String(), Amount: 10,
					Asset: model.TradeableAsset{Symbol: "ADA"},
				},
			},
			want: func() *Ledger {
				l := NewLedger()
				l.ioList.ioList = []model.LedgerIO{
					model.LedgerIO{
						Type: modelenum.Deposit.String(), Amount: 10,
						Asset: model.TradeableAsset{Symbol: "ADA"},
					},
					model.LedgerIO{
						Type: modelenum.Deposit.String(), Amount: 10,
						Asset: model.TradeableAsset{Symbol: "ADA"},
					},
				}
				l.totalBalance.balance["ADA"] = 20
				l.assetBalances["ADA"] = NewBalance()
				l.assetBalances["ADA"].balance["ADA"] = 20
				return l
			}(),
		},
		{
			name: "It should add a new withdrawal entry to the list of i/o of the ledger and withdraw proportionally from asset specific balance",
			ledger: func() *Ledger {
				l := NewLedger()
				l.ioList.ioList = []model.LedgerIO{
					model.LedgerIO{
						Type: modelenum.Deposit.String(), Amount: 100,
						Asset: model.TradeableAsset{Symbol: "ADA"},
					},
				}
				l.totalBalance.balance["ADA"] = 100
				l.assetBalances["ADA"] = NewBalance()
				l.assetBalances["ADA"].balance["ADA"] = 50
				l.assetBalances["ETH"] = NewBalance()
				l.assetBalances["ETH"].balance["ADA"] = 40
				l.assetBalances["BTC"] = NewBalance()
				l.assetBalances["BTC"].balance["ADA"] = 10
				l.assetBalances["SOME_OTHER_COIN"] = NewBalance()
				l.assetBalances["SOME_OTHER_COIN"].balance["ADA"] = 0
				return l
			}(),
			args: args{
				lio: &model.LedgerIO{
					Type: modelenum.Withdrawal.String(), Amount: 10,
					Asset: model.TradeableAsset{Symbol: "ADA"},
				},
			},
			want: func() *Ledger {
				l := NewLedger()
				l.ioList.ioList = []model.LedgerIO{
					model.LedgerIO{
						Type: modelenum.Deposit.String(), Amount: 100,
						Asset: model.TradeableAsset{Symbol: "ADA"},
					},
					model.LedgerIO{
						Type: modelenum.Withdrawal.String(), Amount: 10,
						Asset: model.TradeableAsset{Symbol: "ADA"},
					},
				}
				l.totalBalance.balance["ADA"] = 90

				l.assetBalances["ADA"] = NewBalance()
				l.assetBalances["ADA"].balance["ADA"] = 45
				l.assetBalances["ETH"] = NewBalance()
				l.assetBalances["ETH"].balance["ADA"] = 36
				l.assetBalances["BTC"] = NewBalance()
				l.assetBalances["BTC"].balance["ADA"] = 9
				l.assetBalances["SOME_OTHER_COIN"] = NewBalance()
				l.assetBalances["SOME_OTHER_COIN"].balance["ADA"] = 0
				return l
			}(),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.ledger.AddLedgerIO(tt.args.lio); !reflect.DeepEqual(got, tt.want) {
				t.Error("Ledger.AddLedgerIO() got diff between want and got")
				t.Error(pretty.Compare(tt.want, got))
			}
		})
	}
}

func TestLedger_StageTrade(t *testing.T) {
	type args struct {
		t *model.Trade
	}
	type want struct {
		uuid            string
		stagedTradesCnt int
		totalBalance    map[string]float64
		assetBalances   map[string]map[string]float64
	}

	tests := []struct {
		name   string
		ledger *Ledger
		args   args
		want   *want
	}{
		{
			name: "It should stage the trade and update the balance accordingly",
			ledger: func() *Ledger {
				l := NewLedger()
				l.AddLedgerIO(&model.LedgerIO{
					Type: modelenum.Deposit.String(), Amount: 20,
					Asset: model.TradeableAsset{Symbol: "ADA"},
				})
				return l
			}(),
			args: args{
				t: &model.Trade{
					Identity: model.Identity{ID: 1},
					From: model.TradeableAsset{
						Symbol: "ADA",
					},
					To: model.TradeableAsset{
						Symbol: "XRP",
					},
					FromAmountIntent: 5,
					ToAmountIntent:   1,
				},
			},
			want: func() *want {
				return &want{
					stagedTradesCnt: 1,
					totalBalance: map[string]float64{
						"ADA": 15,
					},
				}
			}(),
		},
		{
			name: "It should not stage the same trade twice",
			ledger: func() *Ledger {
				l := NewLedger()
				l.AddLedgerIO(&model.LedgerIO{
					Type: modelenum.Deposit.String(), Amount: 20,
					Asset: model.TradeableAsset{Symbol: "ADA"},
				})
				l.StageTrade(&model.Trade{
					Identity: model.Identity{ID: 1},
					From: model.TradeableAsset{
						Symbol: "ADA",
					},
					To: model.TradeableAsset{
						Symbol: "XRP",
					},
					FromAmountIntent: 5,
					ToAmountIntent:   1,
					LedgerTxID:       "Already_Staged_ID",
				})
				return l
			}(),
			args: args{
				t: &model.Trade{
					Identity: model.Identity{ID: 1},
					From: model.TradeableAsset{
						Symbol: "ADA",
					},
					To: model.TradeableAsset{
						Symbol: "XRP",
					},
					FromAmountIntent: 5,
					ToAmountIntent:   1,
					LedgerTxID:       "Already_Staged_ID",
				},
			},
			want: func() *want {
				return &want{
					stagedTradesCnt: 1,
					totalBalance: map[string]float64{
						"ADA": 15,
					},
				}
			}(),
		},
		{
			name: "It should stage the trade both in totalbalance and asset specific balance",
			ledger: func() *Ledger {
				l := NewLedger()
				l.assetBalances["ADA"] = NewBalance()
				l.AddLedgerIO(&model.LedgerIO{
					Type: modelenum.Deposit.String(), Amount: 20,
					Asset: model.TradeableAsset{Symbol: "ADA"},
				})
				return l
			}(),
			args: args{
				t: &model.Trade{
					Identity: model.Identity{ID: 1},
					From: model.TradeableAsset{
						Symbol: "ADA",
					},
					To: model.TradeableAsset{
						Symbol: "XRP",
					},
					FromAmountIntent: 5,
					ToAmountIntent:   1,
				},
			},
			want: func() *want {
				return &want{
					stagedTradesCnt: 1,
					totalBalance: map[string]float64{
						"ADA": 15,
					},
					assetBalances: map[string]map[string]float64{
						"ADA": map[string]float64{"ADA": 15},
					},
				}
			}(),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.ledger.StageTrade(tt.args.t)
			if !reflect.DeepEqual(got.totalBalance.balance, tt.want.totalBalance) {
				t.Errorf("Ledger.StageTrade(); diff between totalBalance got and wanted totalBalance")
				t.Error(pretty.Compare(got.totalBalance.balance, tt.want.totalBalance))
			}

			if len(got.stagedTrades) != tt.want.stagedTradesCnt {
				t.Errorf("Ledger.StageTrade() has %d staged trades; wanted %d", len(got.stagedTrades), tt.want.stagedTradesCnt)
			}

			if tt.args.t.LedgerTxID == "" {
				t.Errorf("Ledger.StageTrade() should've added the LedgerTxID field but got empty")
			}

			if tt.want.uuid != "" && tt.args.t.LedgerTxID != tt.want.uuid {
				t.Errorf("Ledger.StageTrade() populated LedgerTxID with: %s, wanted %s", tt.args.t.LedgerTxID, tt.want.uuid)
			}
			if tt.want.assetBalances != nil {
				assetBalances := make(map[string]map[string]float64)
				for sym, balance := range got.assetBalances {
					assetBalances[sym] = balance.GetRaw()
				}
				if !reflect.DeepEqual(assetBalances, tt.want.assetBalances) {
					t.Error("Ledger.StageTrade() diff between want and got")
					t.Error(pretty.Compare(tt.want.assetBalances, assetBalances))
				}
			}
		})
	}
}

func TestLedger_CommitTrade(t *testing.T) {
	type args struct {
		t *model.Trade
	}

	tests := []struct {
		name   string
		ledger *Ledger
		args   args
		want   *Ledger
	}{
		{
			name: "It should commit the previously staged trade",
			ledger: func() *Ledger {
				l := NewLedger()
				l.AddLedgerIO(&model.LedgerIO{
					Type: modelenum.Deposit.String(), Amount: 20,
					Asset: model.TradeableAsset{Symbol: "ADA"},
				})
				l.StageTrade(&model.Trade{
					Identity: model.Identity{ID: 1},
					From: model.TradeableAsset{
						Symbol: "ADA",
					},
					To: model.TradeableAsset{
						Symbol: "XRP",
					},
					FromAmountIntent: 5,
					ToAmountIntent:   1,
					LedgerTxID:       "Already_Staged_ID",
				})
				return l
			}(),
			args: args{
				t: &model.Trade{
					Identity: model.Identity{ID: 1},
					From: model.TradeableAsset{
						Symbol: "ADA",
					},
					To: model.TradeableAsset{
						Symbol: "XRP",
					},

					// As you can see, there can be a difference
					// between the intended amounts and the actual
					// traded amounts; Ofc, the difference will never
					// look that big but you got the idea;
					// Those differences can appear due to various exchange
					// api rules like for example not accepting floats as
					// traded amounts (this is the case for some coins in binance)
					FromAmountTrade: 3,
					ToAmountTrade:   2,
					LedgerTxID:      "Already_Staged_ID",
					Status:          modelenum.CompletedTrade.String(),
				},
			},
			want: func() *Ledger {
				l := NewLedger()
				l.ioList.ioList = []model.LedgerIO{
					model.LedgerIO{
						Type: modelenum.Deposit.String(), Amount: 20,
						Asset: model.TradeableAsset{Symbol: "ADA"},
					},
				}
				l.tradeList.trades = []model.Trade{
					model.Trade{
						Identity: model.Identity{ID: 1},
						From: model.TradeableAsset{
							Symbol: "ADA",
						},
						To: model.TradeableAsset{
							Symbol: "XRP",
						},
						FromAmountTrade: 3,
						ToAmountTrade:   2,
						LedgerTxID:      "Already_Staged_ID",
						Status:          modelenum.CompletedTrade.String(),
					},
				}
				// this should be cleared as well
				l.stagedTrades = make(map[string]model.Trade)
				l.totalBalance.balance["ADA"] = 17
				l.totalBalance.balance["XRP"] = 2
				return l
			}(),
		},
		{
			name: "It should just add the trade if it wasn't staged",
			ledger: func() *Ledger {
				l := NewLedger()
				l.AddLedgerIO(&model.LedgerIO{
					Type: modelenum.Deposit.String(), Amount: 20,
					Asset: model.TradeableAsset{Symbol: "ADA"},
				})
				return l
			}(),
			args: args{
				t: &model.Trade{
					Identity: model.Identity{ID: 1},
					From: model.TradeableAsset{
						Symbol: "ADA",
					},
					To: model.TradeableAsset{
						Symbol: "XRP",
					},

					// As you can see, there can be a difference
					// between the intended amounts and the actual
					// traded amounts; Ofc, the difference will never
					// look that big but you got the idea;
					// Those differences can appear due to various exchange
					// api rules like for example not accepting floats as
					// traded amounts (this is the case for some coins in binance)
					FromAmountTrade: 3,
					ToAmountTrade:   2,
					LedgerTxID:      "some_historically_staged_tx_id (this happens when the process restarts for eg.)",
					Status:          modelenum.CompletedTrade.String(),
				},
			},
			want: func() *Ledger {
				l := NewLedger()
				l.ioList.ioList = []model.LedgerIO{
					model.LedgerIO{
						Type: modelenum.Deposit.String(), Amount: 20,
						Asset: model.TradeableAsset{Symbol: "ADA"},
					},
				}
				l.tradeList.trades = []model.Trade{
					model.Trade{
						Identity: model.Identity{ID: 1},
						From: model.TradeableAsset{
							Symbol: "ADA",
						},
						To: model.TradeableAsset{
							Symbol: "XRP",
						},
						FromAmountTrade: 3,
						ToAmountTrade:   2,
						LedgerTxID:      "some_historically_staged_tx_id (this happens when the process restarts for eg.)",
						Status:          modelenum.CompletedTrade.String(),
					},
				}
				// this should be cleared as well
				l.stagedTrades = make(map[string]model.Trade)
				l.totalBalance.balance["ADA"] = 17
				l.totalBalance.balance["XRP"] = 2
				return l
			}(),
		},
		{
			name: "It should commit the trade to asset specific balance",
			ledger: func() *Ledger {
				l := NewLedger()

				// l.assetBalances["SOME_COIN"] = NewBalance()
				l.AddLedgerIO(&model.LedgerIO{
					Type: modelenum.Deposit.String(), Amount: 20,
					Asset: model.TradeableAsset{Symbol: "ADA"},
				})
				l.GetAssetInvestmentBalance("ADA") // enable asset-specific-balance tracking
				l.StageTrade(&model.Trade{
					Identity: model.Identity{ID: 1},
					From: model.TradeableAsset{
						Symbol: "ADA",
					},
					To: model.TradeableAsset{
						Symbol: "XRP",
					},
					FromAmountIntent: 5,
					ToAmountIntent:   1,
					LedgerTxID:       "Already_Staged_ID",
				})

				return l
			}(),
			args: args{
				t: &model.Trade{
					Identity: model.Identity{ID: 1},
					From: model.TradeableAsset{
						Symbol: "ADA",
					},
					To: model.TradeableAsset{
						Symbol: "XRP",
					},

					// As you can see, there can be a difference
					// between the intended amounts and the actual
					// traded amounts; Ofc, the difference will never
					// look that big but you got the idea;
					// Those differences can appear due to various exchange
					// api rules like for example not accepting floats as
					// traded amounts (this is the case for some coins in binance)
					FromAmountTrade: 3,
					ToAmountTrade:   2,
					LedgerTxID:      "Already_Staged_ID",
					Status:          modelenum.CompletedTrade.String(),
				},
			},
			want: func() *Ledger {
				l := NewLedger()
				l.ioList.ioList = []model.LedgerIO{
					model.LedgerIO{
						Type: modelenum.Deposit.String(), Amount: 20,
						Asset: model.TradeableAsset{Symbol: "ADA"},
					},
				}
				l.tradeList.trades = []model.Trade{
					model.Trade{
						Identity: model.Identity{ID: 1},
						From: model.TradeableAsset{
							Symbol: "ADA",
						},
						To: model.TradeableAsset{
							Symbol: "XRP",
						},
						FromAmountTrade: 3,
						ToAmountTrade:   2,
						LedgerTxID:      "Already_Staged_ID",
						Status:          modelenum.CompletedTrade.String(),
					},
				}
				// this should be cleared as well
				l.stagedTrades = make(map[string]model.Trade)

				l.totalBalance.balance["ADA"] = 17
				l.totalBalance.balance["XRP"] = 2

				l.assetBalances["ADA"] = NewBalance()
				l.assetBalances["ADA"].balance["ADA"] = 17
				l.assetBalances["ADA"].balance["XRP"] = 2
				return l
			}(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.ledger.CommitTrade(tt.args.t); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Ledger.CommitTrade() diff between wanted and got")
				t.Error(pretty.Compare(got, tt.want))
			}
		})
	}
}

func TestLedger_CancelTrade(t *testing.T) {
	type args struct {
		t *model.Trade
	}
	tests := []struct {
		name   string
		ledger *Ledger
		args   args
		want   *Ledger
	}{
		{
			name: "It should cancel the previously staged trade",
			ledger: func() *Ledger {
				l := NewLedger()
				l.AddLedgerIO(&model.LedgerIO{
					Type: modelenum.Deposit.String(), Amount: 20,
					Asset: model.TradeableAsset{Symbol: "ADA"},
				})
				l.StageTrade(&model.Trade{
					Identity: model.Identity{ID: 1},
					From: model.TradeableAsset{
						Symbol: "ADA",
					},
					To: model.TradeableAsset{
						Symbol: "XRP",
					},
					FromAmountIntent: 5,
					ToAmountIntent:   1,
					LedgerTxID:       "Already_Staged_ID",
				})
				return l
			}(),
			args: args{
				t: &model.Trade{
					Identity: model.Identity{ID: 1},
					From: model.TradeableAsset{
						Symbol: "ADA",
					},
					To: model.TradeableAsset{
						Symbol: "XRP",
					},
					FromAmountIntent: 5,
					ToAmountIntent:   1,
					LedgerTxID:       "Already_Staged_ID",
					Status:           modelenum.CanceledTrade.String(),
				},
			},
			want: func() *Ledger {
				l := NewLedger()
				l.ioList.ioList = []model.LedgerIO{
					model.LedgerIO{
						Type: modelenum.Deposit.String(), Amount: 20,
						Asset: model.TradeableAsset{Symbol: "ADA"},
					},
				}

				// this should be reverted as well
				l.stagedTrades = make(map[string]model.Trade)
				l.totalBalance.balance["ADA"] = 20
				return l
			}(),
		},
		{
			name: "It should cancel the trade from asset specific balance",
			ledger: func() *Ledger {
				l := NewLedger()

				// l.assetBalances["SOME_COIN"] = NewBalance()
				l.AddLedgerIO(&model.LedgerIO{
					Type: modelenum.Deposit.String(), Amount: 20,
					Asset: model.TradeableAsset{Symbol: "ADA"},
				})

				l.GetAssetInvestmentBalance("ADA") // enable asset-specific-balance tracking

				l.StageTrade(&model.Trade{
					Identity: model.Identity{ID: 1},
					From: model.TradeableAsset{
						Symbol: "ADA",
					},
					To: model.TradeableAsset{
						Symbol: "XRP",
					},
					FromAmountIntent: 5,
					ToAmountIntent:   1,
					LedgerTxID:       "Already_Staged_ID",
					Status:           modelenum.InProgressTrade.String(),
				})
				return l
			}(),
			args: args{
				t: &model.Trade{
					Identity: model.Identity{ID: 1},
					From: model.TradeableAsset{
						Symbol: "ADA",
					},
					To: model.TradeableAsset{
						Symbol: "XRP",
					},
					FromAmountIntent: 5,
					ToAmountIntent:   1,
					LedgerTxID:       "Already_Staged_ID",
					Status:           modelenum.CanceledTrade.String(),
				},
			},
			want: func() *Ledger {
				l := NewLedger()
				l.ioList.ioList = []model.LedgerIO{
					model.LedgerIO{
						Type: modelenum.Deposit.String(), Amount: 20,
						Asset: model.TradeableAsset{Symbol: "ADA"},
					},
				}

				// this should be cleared as well
				l.stagedTrades = make(map[string]model.Trade)

				l.totalBalance.balance["ADA"] = 20

				l.assetBalances["ADA"] = NewBalance()
				l.assetBalances["ADA"].balance["ADA"] = 20
				return l
			}(),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.ledger.CancelTrade(tt.args.t); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Ledger.CancelTrade() diff between want and got")
				t.Error(pretty.Compare(got, tt.want))
			}
		})
	}
}

func TestLedger_GetTradeList(t *testing.T) {
	tests := []struct {
		name   string
		ledger *Ledger
		want   *TradeList
	}{
		{
			name:   "it should return the list of trades",
			ledger: NewLedger(),
			want:   &TradeList{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.ledger.GetTradeList(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Ledger.GetTradeList() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLedger_isTradeStaged(t *testing.T) {
	type args struct {
		t *model.Trade
	}
	tests := []struct {
		name   string
		ledger *Ledger
		args   args
		want   bool
	}{
		{
			name:   "it should return false if trade is not staged",
			ledger: NewLedger(),
			args: args{
				t: &model.Trade{LedgerTxID: "Some_Tx_ID"},
			},
			want: false,
		},
		{
			name: "it should return true if trade is staged",
			ledger: func() *Ledger {
				l := NewLedger()
				l.stagedTrades["Some_Tx_ID"] = model.Trade{LedgerTxID: "Some_Tx_ID"}
				return l
			}(),
			args: args{
				t: &model.Trade{LedgerTxID: "Some_Tx_ID"},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.ledger.isTradeStaged(tt.args.t); got != tt.want {
				t.Errorf("Ledger.isTradeStaged() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLedger_GetLedgerIOList(t *testing.T) {
	tests := []struct {
		name   string
		ledger *Ledger
		want   *LedgerIOList
	}{
		{
			name:   "It should return the list of ledger i/o transactions",
			ledger: NewLedger(),
			want:   &LedgerIOList{},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.ledger.GetLedgerIOList(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Ledger.GetLedgerIOList() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLedger_GetLastInvestment(t *testing.T) {
	type args struct {
		fsym string
		tsym string
	}
	tests := []struct {
		name   string
		ledger *Ledger
		args   args
		want   *model.Trade
	}{
		{
			name: "It should return the latest investment staged made between the 2 coins",
			ledger: func() *Ledger {
				l := NewLedger()
				l.stagedTrades["some_id"] = model.Trade{
					Identity: model.Identity{ID: 1, UpdatedAt: time.Date(2018, 1, 1, 1, 1, 1, 1, time.Local)},
					From: model.TradeableAsset{
						Symbol: "ADA",
					},
					To: model.TradeableAsset{
						Symbol: "XRP",
					},
					FromAmountIntent: 5,
					ToAmountIntent:   2,
					LedgerTxID:       "some_id",
				}
				l.stagedTrades["some_other_id"] = model.Trade{
					Identity: model.Identity{ID: 1, UpdatedAt: time.Date(2018, 2, 1, 1, 1, 1, 1, time.Local)},
					From: model.TradeableAsset{
						Symbol: "ADA",
					},
					To: model.TradeableAsset{
						Symbol: "XRP",
					},
					FromAmountIntent: 8,
					ToAmountIntent:   4,
					LedgerTxID:       "some_other_id",
				}
				return l
			}(),
			args: args{fsym: "ADA", tsym: "XRP"},
			want: &model.Trade{
				Identity: model.Identity{ID: 1, UpdatedAt: time.Date(2018, 2, 1, 1, 1, 1, 1, time.Local)},
				From: model.TradeableAsset{
					Symbol: "ADA",
				},
				To: model.TradeableAsset{
					Symbol: "XRP",
				},
				FromAmountIntent: 8,
				ToAmountIntent:   4,
				LedgerTxID:       "some_other_id",
			},
		},
		{
			name: "It should return the latest investment completed between the 2 coins",
			ledger: func() *Ledger {
				l := NewLedger()
				td := time.Date(2018, 1, 1, 1, 1, 1, 1, time.Local)

				l.tradeList.trades = []model.Trade{
					model.Trade{
						Identity: model.Identity{ID: 1},
						From: model.TradeableAsset{
							Symbol: "ADA",
						},
						To: model.TradeableAsset{
							Symbol: "XRP",
						},
						FromAmountTrade: 5,
						ToAmountTrade:   2,
						LedgerTxID:      "some_id",
						TradeFinishTime: &td,
						Status:          modelenum.CompletedTrade.String(),
					},
				}
				return l
			}(),
			args: args{fsym: "ADA", tsym: "XRP"},
			want: &model.Trade{
				Identity: model.Identity{ID: 1},
				From: model.TradeableAsset{
					Symbol: "ADA",
				},
				To: model.TradeableAsset{
					Symbol: "XRP",
				},
				FromAmountTrade: 5,
				ToAmountTrade:   2,
				LedgerTxID:      "some_id",
				TradeFinishTime: func() *time.Time {
					t := time.Date(2018, 1, 1, 1, 1, 1, 1, time.Local)
					return &t
				}(),
				Status: modelenum.CompletedTrade.String(),
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.ledger.GetLastInvestment(tt.args.fsym, tt.args.tsym); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Ledger.GetLastInvestment() got diff between want and got")
				t.Errorf(pretty.Compare(tt.want, got))
			}
		})
	}
}

func TestLedger_GetAssetInvestmentBalance(t *testing.T) {
	type args struct {
		assetSymbol string
	}
	tests := []struct {
		name   string
		ledger *Ledger
		args   args
		want   *Balance
	}{
		{
			name: "It should compute the assets balance and return it",
			ledger: func() *Ledger {
				l := NewLedger()
				l.AddLedgerIO(&model.LedgerIO{
					Identity: model.Identity{
						CreatedAt: time.Date(2016, 1, 1, 0, 0, 0, 0, time.Local),
					},
					Type: modelenum.Deposit.String(), Amount: 20,
					Asset: model.TradeableAsset{Symbol: "ADA"},
				})
				l.CommitTrade(&model.Trade{
					Identity: model.Identity{ID: 1},
					From: model.TradeableAsset{
						Symbol: "ADA",
					},
					To: model.TradeableAsset{
						Symbol: "XRP",
					},
					FromAmountTrade: 3,
					ToAmountTrade:   2,
					LedgerTxID:      "some_tx_id",
					TradeFinishTime: func() *time.Time { t := time.Date(2017, 1, 1, 0, 0, 0, 0, time.Local); return &t }(),
					Status:          modelenum.CompletedTrade.String(),
				})
				l.CommitTrade(&model.Trade{
					Identity: model.Identity{ID: 2},
					From: model.TradeableAsset{
						Symbol: "XRP",
					},
					To: model.TradeableAsset{
						Symbol: "ADA",
					},
					LinkedTradeID:   1,
					FromAmountTrade: 1,
					ToAmountTrade:   2,
					LedgerTxID:      "some_tx_id",
					TradeFinishTime: func() *time.Time { t := time.Date(2018, 1, 1, 0, 0, 0, 0, time.Local); return &t }(),
					Status:          modelenum.CompletedTrade.String(),
				})
				return l
			}(),
			args: args{
				assetSymbol: "ADA",
			},
			want: func() *Balance {
				b := NewBalance()
				b.balance["ADA"] = 19
				b.balance["XRP"] = 1
				return b
			}(),
		},
		{
			name: "It should just return the asset balance if already computed",
			ledger: func() *Ledger {
				l := NewLedger()
				l.AddLedgerIO(&model.LedgerIO{
					Type: modelenum.Deposit.String(), Amount: 20,
					Asset: model.TradeableAsset{Symbol: "ADA"},
				})
				l.CommitTrade(&model.Trade{
					Identity: model.Identity{ID: 1},
					From: model.TradeableAsset{
						Symbol: "ADA",
					},
					To: model.TradeableAsset{
						Symbol: "XRP",
					},
					FromAmountTrade: 3,
					ToAmountTrade:   2,
					LedgerTxID:      "some_tx_id",
					Status:          modelenum.CompletedTrade.String(),
				})
				l.CommitTrade(&model.Trade{
					Identity: model.Identity{ID: 2},
					From: model.TradeableAsset{
						Symbol: "XRP",
					},
					To: model.TradeableAsset{
						Symbol: "ADA",
					},
					LinkedTradeID:   1,
					FromAmountTrade: 1,
					ToAmountTrade:   2,
					LedgerTxID:      "some_tx_id",
					Status:          modelenum.CompletedTrade.String(),
				})
				l.GetAssetInvestmentBalance("WHATEVER_SYMBOL") // enable and compute asset balance
				return l
			}(),
			args: args{
				assetSymbol: "ADA",
			},
			want: func() *Balance {
				b := NewBalance()
				b.balance["ADA"] = 19
				b.balance["XRP"] = 1
				return b
			}(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.ledger.GetAssetInvestmentBalance(tt.args.assetSymbol); !reflect.DeepEqual(got, tt.want) {
				t.Error("Ledger.GetAssetInvestmentBalance() diff between got and want")
				t.Error(pretty.Compare(got, tt.want))
			}
		})
	}
}

func TestLedger_GetTotalBalance(t *testing.T) {
	tests := []struct {
		name   string
		ledger *Ledger
		want   *Balance
	}{
		{
			name: "It should return the total balance",
			ledger: func() *Ledger {
				l := NewLedger()
				l.AddLedgerIO(&model.LedgerIO{
					Type:   modelenum.Deposit.String(),
					Asset:  model.TradeableAsset{Symbol: "ADA"},
					Amount: 5,
				})
				return l
			}(),
			want: &Balance{
				balance: map[string]float64{"ADA": 5},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.ledger.GetTotalBalance(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Ledger.GetTotalBalance() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLedger_IsLastInvestmentPending(t *testing.T) {
	type args struct {
		fsym string
		tsym string
	}
	tests := []struct {
		name   string
		ledger *Ledger
		args   args
		want   bool
	}{
		{
			name: "It should return true if there is a staged trade",
			ledger: func() *Ledger {
				l := NewLedger()
				l.AddLedgerIO(&model.LedgerIO{
					Type:   modelenum.Deposit.String(),
					Asset:  model.TradeableAsset{Symbol: "ADA"},
					Amount: 5,
				})
				l.StageTrade(&model.Trade{
					Identity: model.Identity{ID: 1},
					From: model.TradeableAsset{
						Symbol: "ADA",
					},
					To: model.TradeableAsset{
						Symbol: "XRP",
					},
					FromAmountIntent: 5,
					ToAmountIntent:   1,
					LedgerTxID:       "Already_Staged_ID",
				})

				return l
			}(),
			args: args{fsym: "ADA", tsym: "XRP"},
			want: true,
		},
		{
			name: "It should check if the latest investment is pending in the list of commited trades (tradeList)",
			ledger: func() *Ledger {
				l := NewLedger()
				l.AddLedgerIO(&model.LedgerIO{
					Type:   modelenum.Deposit.String(),
					Asset:  model.TradeableAsset{Symbol: "ADA"},
					Amount: 5,
				})
				return l
			}(),
			args: args{fsym: "ADA", tsym: "XRP"},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.ledger.IsLastInvestmentPending(tt.args.fsym, tt.args.tsym); got != tt.want {
				t.Errorf("Ledger.IsLastInvestmentPending() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLedger_addTrade(t *testing.T) {
	type args struct {
		t *model.Trade
	}
	tests := []struct {
		name   string
		ledger *Ledger
		args   args
		want   *Ledger
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.ledger.addTrade(tt.args.t); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Ledger.addTrade() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLedger_isAssetBalanceActive(t *testing.T) {
	tests := []struct {
		name   string
		ledger *Ledger
		want   bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.ledger.isAssetBalanceActive(); got != tt.want {
				t.Errorf("Ledger.isAssetBalanceActive() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLedger_addAssetLedgerIO(t *testing.T) {
	type args struct {
		lio *model.LedgerIO
	}
	tests := []struct {
		name   string
		ledger *Ledger
		args   args
		want   *Ledger
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.ledger.addAssetLedgerIO(tt.args.lio); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Ledger.addAssetLedgerIO() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLedger_addAssetTrade(t *testing.T) {
	type args struct {
		t *model.Trade
	}
	tests := []struct {
		name   string
		ledger *Ledger
		args   args
		want   *Ledger
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.ledger.addAssetTrade(tt.args.t); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Ledger.addAssetTrade() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLedger_stageAssetTrade(t *testing.T) {
	type args struct {
		t *model.Trade
	}
	tests := []struct {
		name   string
		ledger *Ledger
		args   args
		want   *Ledger
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.ledger.stageAssetTrade(tt.args.t); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Ledger.stageAssetTrade() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLedger_commitAssetTrade(t *testing.T) {
	type args struct {
		finalTrade *model.Trade
	}
	tests := []struct {
		name   string
		ledger *Ledger
		args   args
		want   *Ledger
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.ledger.commitAssetTrade(tt.args.finalTrade); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Ledger.commitAssetTrade() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestLedger_GetTradeBackAmount(t *testing.T) {
	type args struct {
		investment *model.Trade
	}
	tests := []struct {
		name   string
		ledger *Ledger
		args   args
		want   float64
	}{
		{
			name: "It should return the percentage of the amount available to make profit of",
			ledger: func() *Ledger {
				l := NewLedger()
				l.AddLedgerIO(&model.LedgerIO{ // 20 ADA initial
					Identity: model.Identity{
						CreatedAt: time.Date(2016, 1, 1, 0, 0, 0, 0, time.Local),
					},
					Type: modelenum.Deposit.String(), Amount: 20,
					Asset: model.TradeableAsset{Symbol: "ADA"},
				})

				l.CommitTrade(&model.Trade{ // 5 ADA, 10 XRP
					Identity: model.Identity{ID: 1},
					From: model.TradeableAsset{
						Symbol: "ADA",
					},
					To: model.TradeableAsset{
						Symbol: "XRP",
					},
					FromAmountTrade: 15,
					ToAmountTrade:   10,
					LedgerTxID:      "some_tx_id",
					TradeFinishTime: func() *time.Time { t := time.Date(2017, time.January, 1, 0, 0, 0, 0, time.Local); return &t }(),
					Status:          modelenum.CompletedTrade.String(),
				})
				l.AddLedgerIO(&model.LedgerIO{ // 5 ADA, 5 XRP
					Identity: model.Identity{
						CreatedAt: time.Date(2018, time.January, 15, 0, 0, 0, 0, time.Local),
					},
					Type:   modelenum.Withdrawal.String(),
					Asset:  model.TradeableAsset{Symbol: "XRP"},
					Amount: 5,
				})
				l.CommitTrade(&model.Trade{ // 2 ADA, 7 XRP
					Identity: model.Identity{ID: 4},
					From: model.TradeableAsset{
						Symbol: "ADA",
					},
					To: model.TradeableAsset{
						Symbol: "XRP",
					},
					FromAmountTrade: 3,
					ToAmountTrade:   2,
					LedgerTxID:      "some_tx_id",
					TradeFinishTime: func() *time.Time { t := time.Date(2018, time.February, 16, 0, 0, 0, 0, time.Local); return &t }(),
					Status:          modelenum.CompletedTrade.String(),
				})
				l.CommitTrade(&model.Trade{ // 6 ADA, 5 XRP
					Identity: model.Identity{ID: 5},
					From: model.TradeableAsset{
						Symbol: "XRP",
					},
					To: model.TradeableAsset{
						Symbol: "ADA",
					},
					FromAmountTrade: 2,
					ToAmountTrade:   4,
					LedgerTxID:      "some_tx_id",
					TradeFinishTime: func() *time.Time { t := time.Date(2018, time.February, 17, 0, 0, 0, 0, time.Local); return &t }(),
					Status:          modelenum.CompletedTrade.String(),
					LinkedTradeID:   4,
				})

				l.CommitTrade(&model.Trade{ // 1 ADA, 9 XRP
					Identity: model.Identity{ID: 6},
					From: model.TradeableAsset{
						Symbol: "ADA",
					},
					To: model.TradeableAsset{
						Symbol: "XRP",
					},
					FromAmountTrade: 5,
					ToAmountTrade:   4,
					LedgerTxID:      "some_tx_id",
					TradeFinishTime: func() *time.Time { t := time.Date(2018, time.February, 18, 0, 0, 0, 0, time.Local); return &t }(),
					Status:          modelenum.CompletedTrade.String(),
				})

				return l
			}(),
			args: args{
				investment: &model.Trade{
					Identity: model.Identity{ID: 1},
					From: model.TradeableAsset{
						Symbol: "ADA",
					},
					To: model.TradeableAsset{
						Symbol: "XRP",
					},
					FromAmountTrade: 15,
					ToAmountTrade:   10,
					LedgerTxID:      "some_tx_id",
					TradeFinishTime: func() *time.Time { t := time.Date(2017, time.January, 1, 0, 0, 0, 0, time.Local); return &t }(),
					Status:          modelenum.CompletedTrade.String(),
				},
			},
			want: 10.00 - (10.00 * ((14.00 - 9.00) / 14.00)), // balanceWithoutWithdrawals - existingBal / balanceWithoutWithdrawals = (14-9)/14
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.ledger.GetTradeBackAmount(tt.args.investment); got != tt.want {
				t.Errorf("Ledger.GetTradeBackAmount() = %v, want %v", got, tt.want)
			}
		})
	}
}

// func TestTradeList_GetInvestments(t *testing.T) {
// 	type args struct {
// 		fsym string
// 		tsym string
// 	}
// 	tests := []struct {
// 		name string
// 		tl   *TradeList
// 		args args
// 		want *TradeList
// 	}{
// 		{
// 			name: "It should return all the investments between 2 symbols",
// 			tl: &TradeList{
// 				trades: []model.Trade{
// 					model.Trade{
// 						Identity: model.Identity{ID: 1},
// 						From: model.TradeableAsset{
// 							Symbol: "ETH",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "XRP",
// 						},
// 						TradeFinishTime: func() *time.Time { t := time.Date(2016, 5, 5, 5, 5, 5, 5, time.Local); return &t }(),
// 					},
// 					model.Trade{
// 						Identity: model.Identity{ID: 2},
// 						From: model.TradeableAsset{
// 							Symbol: "XRP",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "ETH",
// 						},
// 						TradeFinishTime: func() *time.Time { t := time.Date(2017, 8, 5, 5, 5, 5, 5, time.Local); return &t }(),
// 					},
// 					model.Trade{
// 						Identity: model.Identity{ID: 3},
// 						From: model.TradeableAsset{
// 							Symbol: "BTC",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "BCH",
// 						},
// 						TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }(),
// 					},
// 					model.Trade{
// 						Identity: model.Identity{ID: 4},
// 						From: model.TradeableAsset{
// 							Symbol: "ETH",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "XRP",
// 						},
// 						LinkedTradeID:   2,
// 						TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }(),
// 					},
// 					model.Trade{
// 						Identity: model.Identity{ID: 5},
// 						From: model.TradeableAsset{
// 							Symbol: "USDT",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "ZCASH",
// 						},
// 						TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }(),
// 					},
// 				},
// 			},
// 			args: args{fsym: "ETH", tsym: "XRP"},
// 			want: &TradeList{
// 				trades: []model.Trade{
// 					model.Trade{
// 						Identity: model.Identity{ID: 1},
// 						From: model.TradeableAsset{
// 							Symbol: "ETH",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "XRP",
// 						},
// 						TradeFinishTime: func() *time.Time { t := time.Date(2016, 5, 5, 5, 5, 5, 5, time.Local); return &t }(),
// 					},
// 				},
// 			},
// 		},
// 	}
// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			if got := tt.tl.GetInvestments(tt.args.fsym, tt.args.tsym); !reflect.DeepEqual(got, tt.want) {
// 				t.Error("Tradelist.GetInvestments() diff between got and want")
// 				t.Error(pretty.Compare(got, tt.want))
// 			}
// 		})
// 	}
// }

// func TestTradeList_GetProfits(t *testing.T) {
// 	type args struct {
// 		fsym string
// 		tsym string
// 	}
// 	tests := []struct {
// 		name string
// 		tl   *TradeList
// 		args args
// 		want *TradeList
// 	}{
// 		{
// 			name: "It should return the profits made between 2 symbols",
// 			tl: &TradeList{
// 				trades: []model.Trade{
// 					model.Trade{
// 						Identity: model.Identity{ID: 1},
// 						From: model.TradeableAsset{
// 							Symbol: "ETH",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "XRP",
// 						},
// 						TradeFinishTime: func() *time.Time { t := time.Date(2016, 5, 5, 5, 5, 5, 5, time.Local); return &t }(),
// 					},
// 					model.Trade{
// 						Identity: model.Identity{ID: 2},
// 						From: model.TradeableAsset{
// 							Symbol: "XRP",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "ETH",
// 						},
// 						TradeFinishTime: func() *time.Time { t := time.Date(2017, 8, 5, 5, 5, 5, 5, time.Local); return &t }(),
// 					},
// 					model.Trade{
// 						Identity: model.Identity{ID: 3},
// 						From: model.TradeableAsset{
// 							Symbol: "BTC",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "BCH",
// 						},
// 						TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }(),
// 					},
// 					model.Trade{
// 						Identity: model.Identity{ID: 4},
// 						From: model.TradeableAsset{
// 							Symbol: "ETH",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "XRP",
// 						},
// 						LinkedTradeID:   2,
// 						TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }(),
// 					},
// 					model.Trade{
// 						Identity: model.Identity{ID: 5},
// 						From: model.TradeableAsset{
// 							Symbol: "USDT",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "ZCASH",
// 						},
// 						TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }(),
// 					},
// 				},
// 			},
// 			args: args{fsym: "XRP", tsym: "ETH"},
// 			want: &TradeList{
// 				trades: []model.Trade{
// 					model.Trade{
// 						Identity: model.Identity{ID: 4},
// 						From: model.TradeableAsset{
// 							Symbol: "ETH",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "XRP",
// 						},
// 						LinkedTradeID:   2,
// 						TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }(),
// 					},
// 				},
// 			},
// 		},
// 	}

// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			if got := tt.tl.GetProfits(tt.args.fsym, tt.args.tsym); !reflect.DeepEqual(got, tt.want) {
// 				t.Error("TradeList.GetProfits() diff between got and want")
// 				t.Error(pretty.Compare(got, tt.want))
// 			}
// 		})
// 	}
// }

// func TestTradeList_GetUnProfitedInvestments(t *testing.T) {
// 	type args struct {
// 		fsym string
// 		tsym string
// 	}
// 	tests := []struct {
// 		name string
// 		tl   *TradeList
// 		args args
// 		want *TradeList
// 	}{
// 		{
// 			name: "It should return the unprofited investments",
// 			tl: &TradeList{
// 				trades: []model.Trade{
// 					model.Trade{
// 						Identity: model.Identity{ID: 1},
// 						From: model.TradeableAsset{
// 							Symbol: "ETH",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "XRP",
// 						},
// 						TradeFinishTime: func() *time.Time { t := time.Date(2016, 5, 5, 5, 5, 5, 5, time.Local); return &t }(),
// 					},
// 					model.Trade{
// 						Identity: model.Identity{ID: 2},
// 						From: model.TradeableAsset{
// 							Symbol: "XRP",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "ETH",
// 						},
// 						LinkedTradeID:   1,
// 						TradeFinishTime: func() *time.Time { t := time.Date(2017, 8, 5, 5, 5, 5, 5, time.Local); return &t }(),
// 					},
// 					model.Trade{
// 						Identity: model.Identity{ID: 3},
// 						From: model.TradeableAsset{
// 							Symbol: "BTC",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "BCH",
// 						},
// 						TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }(),
// 					},
// 					model.Trade{
// 						Identity: model.Identity{ID: 4},
// 						From: model.TradeableAsset{
// 							Symbol: "ETH",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "XRP",
// 						},
// 						TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }(),
// 					},
// 					model.Trade{
// 						Identity: model.Identity{ID: 5},
// 						From: model.TradeableAsset{
// 							Symbol: "USDT",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "ZCASH",
// 						},
// 						TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }(),
// 					},
// 				},
// 			},
// 			args: args{fsym: "ETH", tsym: "XRP"},
// 			want: &TradeList{
// 				trades: []model.Trade{
// 					model.Trade{
// 						Identity: model.Identity{ID: 4},
// 						From: model.TradeableAsset{
// 							Symbol: "ETH",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "XRP",
// 						},
// 						TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }(),
// 					},
// 				},
// 			},
// 		},
// 		{
// 			name: "It should return no investments if none were made",
// 			tl: &TradeList{
// 				trades: []model.Trade{
// 					model.Trade{
// 						Identity: model.Identity{ID: 1},
// 						From: model.TradeableAsset{
// 							Symbol: "ETH",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "XRP",
// 						},
// 						TradeFinishTime: func() *time.Time { t := time.Date(2016, 5, 5, 5, 5, 5, 5, time.Local); return &t }(),
// 					},
// 					model.Trade{
// 						Identity: model.Identity{ID: 2},
// 						From: model.TradeableAsset{
// 							Symbol: "XRP",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "ETH",
// 						},
// 						TradeFinishTime: func() *time.Time { t := time.Date(2017, 8, 5, 5, 5, 5, 5, time.Local); return &t }(),
// 					},
// 					model.Trade{
// 						Identity: model.Identity{ID: 3},
// 						From: model.TradeableAsset{
// 							Symbol: "BTC",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "BCH",
// 						},
// 						TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }(),
// 					},
// 					model.Trade{
// 						Identity: model.Identity{ID: 4},
// 						From: model.TradeableAsset{
// 							Symbol: "ETH",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "XRP",
// 						},
// 						LinkedTradeID:   2,
// 						TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }(),
// 					},
// 					model.Trade{
// 						Identity: model.Identity{ID: 5},
// 						From: model.TradeableAsset{
// 							Symbol: "USDT",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "ZCASH",
// 						},
// 						TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }(),
// 					},
// 				},
// 			},
// 			args: args{fsym: "ADA", tsym: "XRP"},
// 			want: &TradeList{
// 				trades: []model.Trade{},
// 			},
// 		},
// 	}

// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			if got := tt.tl.GetUnProfitedInvestments(tt.args.fsym, tt.args.tsym); !reflect.DeepEqual(got, tt.want) {
// 				t.Error("TradeList.GetUnProfitedInvestments(); diff beetween got and want")
// 				t.Error(pretty.Compare(got, tt.want))
// 			}
// 		})
// 	}
// }

// func TestTradeList_GetLastInvestment(t *testing.T) {
// 	type args struct {
// 		fsym string
// 		tsym string
// 	}
// 	tests := []struct {
// 		name string
// 		tl   *TradeList
// 		args args
// 		want *model.Trade
// 	}{
// 		{
// 			name: "It should return the latest investment made",
// 			tl: &TradeList{
// 				trades: []model.Trade{
// 					model.Trade{
// 						Identity: model.Identity{ID: 1},
// 						From: model.TradeableAsset{
// 							Symbol: "ETH",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "XRP",
// 						},
// 						TradeFinishTime: func() *time.Time { t := time.Date(2016, 5, 5, 5, 5, 5, 5, time.Local); return &t }(),
// 					},
// 					model.Trade{
// 						Identity: model.Identity{ID: 2},
// 						From: model.TradeableAsset{
// 							Symbol: "XRP",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "ETH",
// 						},
// 						TradeFinishTime: func() *time.Time { t := time.Date(2017, 8, 5, 5, 5, 5, 5, time.Local); return &t }(),
// 					},
// 					model.Trade{
// 						Identity: model.Identity{ID: 3},
// 						From: model.TradeableAsset{
// 							Symbol: "BTC",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "BCH",
// 						},
// 						TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }(),
// 					},
// 					model.Trade{
// 						Identity: model.Identity{ID: 4},
// 						From: model.TradeableAsset{
// 							Symbol: "ETH",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "XRP",
// 						},
// 						TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }(),
// 					},
// 				},
// 			},
// 			args: args{fsym: "ETH", tsym: "XRP"},
// 			want: &model.Trade{
// 				Identity: model.Identity{ID: 4},
// 				From: model.TradeableAsset{
// 					Symbol: "ETH",
// 				},
// 				To: model.TradeableAsset{
// 					Symbol: "XRP",
// 				},
// 				TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }(),
// 			},
// 		},
// 		{
// 			name: "It should return nil if investments weren't made",
// 			tl: &TradeList{
// 				trades: []model.Trade{
// 					model.Trade{
// 						Identity: model.Identity{ID: 4},
// 						From: model.TradeableAsset{
// 							Symbol: "ETH",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "XRP",
// 						},
// 						TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }(),
// 					},
// 				},
// 			},
// 			args: args{fsym: "BTC", tsym: "ETH"},
// 			want: nil,
// 		},
// 	}

// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			if got := tt.tl.GetLastInvestment(tt.args.fsym, tt.args.tsym); !reflect.DeepEqual(got, tt.want) {
// 				t.Error("TradeList.GetLastInvestment(); diff between got and want")
// 				t.Error(pretty.Compare(got, tt.want))
// 			}
// 		})
// 	}
// }

// func TestTradeList_IsLastInvestmentPending(t *testing.T) {
// 	type args struct {
// 		fsym string
// 		tsym string
// 	}
// 	tests := []struct {
// 		name string
// 		tl   *TradeList
// 		args args
// 		want bool
// 	}{
// 		{
// 			name: "It should return true if last investment is pending",
// 			tl: &TradeList{
// 				trades: []model.Trade{
// 					model.Trade{
// 						Identity: model.Identity{ID: 1},
// 						From: model.TradeableAsset{
// 							Symbol: "ETH",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "XRP",
// 						},
// 						TradeFinishTime: func() *time.Time { t := time.Date(2016, 5, 5, 5, 5, 5, 5, time.Local); return &t }(),
// 					},
// 				},
// 			},
// 			args: args{fsym: "ETH", tsym: "XRP"},
// 			want: true,
// 		},
// 		{
// 			name: "It should return false if last investment is not pending",
// 			tl: &TradeList{
// 				trades: []model.Trade{
// 					model.Trade{
// 						Identity: model.Identity{ID: 1},
// 						From: model.TradeableAsset{
// 							Symbol: "ETH",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "XRP",
// 						},
// 						TradeFinishTime: func() *time.Time { t := time.Date(2016, 5, 5, 5, 5, 5, 5, time.Local); return &t }(),
// 					},
// 					model.Trade{
// 						Identity: model.Identity{ID: 2},
// 						From: model.TradeableAsset{
// 							Symbol: "XRP",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "ETH",
// 						},
// 						LinkedTradeID:   1,
// 						TradeFinishTime: func() *time.Time { t := time.Date(2017, 5, 5, 5, 5, 5, 5, time.Local); return &t }(),
// 					},
// 				},
// 			},
// 			args: args{fsym: "ETH", tsym: "XRP"},
// 			want: false,
// 		},
// 		{
// 			name: "It should return false if no investments made",
// 			tl: &TradeList{
// 				trades: []model.Trade{
// 					model.Trade{
// 						Identity: model.Identity{ID: 1},
// 						From: model.TradeableAsset{
// 							Symbol: "ETH",
// 						},
// 						To: model.TradeableAsset{
// 							Symbol: "XRP",
// 						},
// 						TradeFinishTime: func() *time.Time { t := time.Date(2016, 5, 5, 5, 5, 5, 5, time.Local); return &t }(),
// 					},
// 				},
// 			},
// 			args: args{fsym: "BTC", tsym: "ETH"},
// 			want: false,
// 		},
// 	}

// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			if got := tt.tl.IsLastInvestmentPending(tt.args.fsym, tt.args.tsym); got != tt.want {
// 				t.Errorf("TradeList.IsLastInvestmentPending() = %v, want %v", got, tt.want)
// 			}
// 		})
// 	}
// }

// func TestTradeList_GetInvestmentStreak(t *testing.T) {
// 	type args struct {
// 		fsym string
// 		tsym string
// 	}
// 	tests := []struct {
// 		name string
// 		tl   *TradeList
// 		args args
// 		want *TradeList
// 	}{
// 		{
// 			name: "It should return the number of latest investments that weren't profited #1",
// 			tl: &TradeList{
// 				trades: []model.Trade{
// 					model.Trade{
// 						Identity: model.Identity{ID: 1},
// 						From:     model.TradeableAsset{Symbol: "ETH"},
// 						To:       model.TradeableAsset{Symbol: "XRP"},
// 					},
// 					model.Trade{
// 						Identity: model.Identity{ID: 2},
// 						From:     model.TradeableAsset{Symbol: "ETH"},
// 						To:       model.TradeableAsset{Symbol: "XRP"},
// 					},
// 					model.Trade{
// 						// this trade is counted as an investment rather
// 						// than a profit since it doesn't have set the LinkedTradeID field
// 						Identity: model.Identity{ID: 3},
// 						From:     model.TradeableAsset{Symbol: "XRP"},
// 						To:       model.TradeableAsset{Symbol: "ETH"},
// 					},
// 				},
// 			},
// 			args: args{fsym: "ETH", tsym: "XRP"},
// 			want: &TradeList{
// 				trades: []model.Trade{
// 					model.Trade{
// 						Identity: model.Identity{ID: 1},
// 						From:     model.TradeableAsset{Symbol: "ETH"},
// 						To:       model.TradeableAsset{Symbol: "XRP"},
// 					},
// 					model.Trade{
// 						Identity: model.Identity{ID: 2},
// 						From:     model.TradeableAsset{Symbol: "ETH"},
// 						To:       model.TradeableAsset{Symbol: "XRP"},
// 					},
// 				},
// 			},
// 		},
// 		{
// 			name: "It should return the number of latest investments that weren't profited #2",
// 			tl: &TradeList{
// 				trades: []model.Trade{
// 					model.Trade{
// 						Identity: model.Identity{ID: 1},
// 						From:     model.TradeableAsset{Symbol: "ETH"},
// 						To:       model.TradeableAsset{Symbol: "XRP"},
// 					},
// 					model.Trade{
// 						Identity: model.Identity{ID: 2},
// 						From:     model.TradeableAsset{Symbol: "ETH"},
// 						To:       model.TradeableAsset{Symbol: "XRP"},
// 					},
// 					model.Trade{
// 						Identity:      model.Identity{ID: 3},
// 						From:          model.TradeableAsset{Symbol: "XRP"},
// 						To:            model.TradeableAsset{Symbol: "ETH"},
// 						LinkedTradeID: 2,
// 					},
// 				},
// 			},
// 			args: args{fsym: "ETH", tsym: "XRP"},
// 			want: &TradeList{
// 				trades: []model.Trade{},
// 			},
// 		},
// 		{
// 			name: "It should return the number of latest investments that weren't profited #3",
// 			tl: &TradeList{
// 				trades: []model.Trade{
// 					model.Trade{
// 						Identity: model.Identity{ID: 1},
// 						From:     model.TradeableAsset{Symbol: "ETH"},
// 						To:       model.TradeableAsset{Symbol: "XRP"},
// 					},
// 					model.Trade{
// 						Identity: model.Identity{ID: 2},
// 						From:     model.TradeableAsset{Symbol: "ETH"},
// 						To:       model.TradeableAsset{Symbol: "XRP"},
// 					},
// 					model.Trade{
// 						Identity:      model.Identity{ID: 3},
// 						From:          model.TradeableAsset{Symbol: "XRP"},
// 						To:            model.TradeableAsset{Symbol: "ETH"},
// 						LinkedTradeID: 2,
// 					},
// 					model.Trade{
// 						Identity: model.Identity{ID: 4},
// 						From:     model.TradeableAsset{Symbol: "ETH"},
// 						To:       model.TradeableAsset{Symbol: "XRP"},
// 					},
// 				},
// 			},
// 			args: args{fsym: "ETH", tsym: "XRP"},
// 			want: &TradeList{
// 				trades: []model.Trade{
// 					model.Trade{
// 						Identity: model.Identity{ID: 4},
// 						From:     model.TradeableAsset{Symbol: "ETH"},
// 						To:       model.TradeableAsset{Symbol: "XRP"},
// 					},
// 				},
// 			},
// 		},
// 	}

// 	for _, tt := range tests {
// 		t.Run(tt.name, func(t *testing.T) {
// 			if got := tt.tl.GetInvestmentStreak(tt.args.fsym, tt.args.tsym); !reflect.DeepEqual(got, tt.want) {
// 				t.Error("TradeList.GetInvestmentStreak(); diff between got and want")
// 				t.Error(pretty.Compare(got.trades, tt.want.trades))
// 			}
// 		})
// 	}
// }
