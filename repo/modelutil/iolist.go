package modelutil

import (
	"time"

	"bitbucket.org/pfzero/tradr/repo/modelenum"

	"bitbucket.org/pfzero/tradr/repo/model"
)

// LedgerIOIterator is an iterator function
type LedgerIOIterator func(io *model.LedgerIO) bool

// LedgerIOList is a helper data structure that
// stores a listo f ledger i/o
type LedgerIOList struct {
	ioList []model.LedgerIO
}

// Add adds a new i/o to the list of ledger i/o
func (liol *LedgerIOList) Add(io model.LedgerIO) *LedgerIOList {
	liol.ioList = append(liol.ioList, io)
	return liol
}

// Filter filters the io list based on the given iterator fn
func (liol *LedgerIOList) Filter(fn LedgerIOIterator) *LedgerIOList {
	filtered := []model.LedgerIO{}
	for i := 0; i < len(liol.ioList); i++ {
		item := &liol.ioList[i]
		if fn(item) {
			filtered = append(filtered, *item)
		}
	}

	return &LedgerIOList{ioList: filtered}
}

// Find returns the first item found based on the given iterator fn
func (liol *LedgerIOList) Find(fn LedgerIOIterator) *model.LedgerIO {
	var found *model.LedgerIO
	for i := 0; i < len(liol.ioList); i++ {
		item := &liol.ioList[i]
		if fn(item) {
			found = item
			break
		}
	}
	return found
}

// GetAll returns the list of all ledger i/o
func (liol *LedgerIOList) GetAll() []model.LedgerIO {
	return liol.ioList
}

// Len returns the length of the list of i/o
func (liol *LedgerIOList) Len() int {
	return len(liol.ioList)
}

// GetAfter returns the list of i/o transactions
// after the given time
func (liol *LedgerIOList) GetAfter(t time.Time) *LedgerIOList {
	return liol.Filter(func(lio *model.LedgerIO) bool {
		return lio.CreatedAt.After(t) || lio.CreatedAt.Equal(t)
	})
}

// GetLedgerIOForSym returns the list of ledger i/o for the given symbol
func (liol *LedgerIOList) GetLedgerIOForSym(sym string) *LedgerIOList {
	return liol.Filter(func(lio *model.LedgerIO) bool {
		return lio.Asset.Symbol == sym
	})
}

// GetWithdrawalsFor returns the withdrawals for the given symbol
func (liol *LedgerIOList) GetWithdrawalsFor(sym string) *LedgerIOList {
	return liol.GetLedgerIOForSym(sym).
		Filter(func(lio *model.LedgerIO) bool {
			return lio.Type == modelenum.Withdrawal.String()
		})
}

// GetDepositsFor returns the deposits for the given symbol
func (liol *LedgerIOList) GetDepositsFor(sym string) *LedgerIOList {
	return liol.GetLedgerIOForSym(sym).
		Filter(func(lio *model.LedgerIO) bool {
			return lio.Type == modelenum.Deposit.String()
		})
}
