package modelutil

import (
	"reflect"
	"testing"
	"time"

	"github.com/kylelemons/godebug/pretty"

	"bitbucket.org/pfzero/tradr/repo/model"
)

func TestTradeList_Add(t *testing.T) {
	type args struct {
		trades []model.Trade
	}
	tests := []struct {
		name string
		tl   *TradeList
		args args
		want *TradeList
	}{
		{
			name: "it should add the models sorted chronologically",
			tl:   &TradeList{},
			args: args{
				trades: []model.Trade{
					model.Trade{Identity: model.Identity{UpdatedAt: time.Date(2018, 1, 1, 1, 1, 1, 1, time.Local)}},
					model.Trade{TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
					model.Trade{TradeFinishTime: func() *time.Time { t := time.Date(2016, 5, 5, 5, 5, 5, 5, time.Local); return &t }()},
					model.Trade{TradeFinishTime: func() *time.Time { t := time.Date(2017, 8, 5, 5, 5, 5, 5, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{UpdatedAt: time.Date(2018, 3, 1, 1, 1, 1, 1, time.Local)}},
				},
			},
			want: &TradeList{
				trades: []model.Trade{
					model.Trade{TradeFinishTime: func() *time.Time { t := time.Date(2016, 5, 5, 5, 5, 5, 5, time.Local); return &t }()},
					model.Trade{TradeFinishTime: func() *time.Time { t := time.Date(2017, 8, 5, 5, 5, 5, 5, time.Local); return &t }()},
					model.Trade{TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{UpdatedAt: time.Date(2018, 1, 1, 1, 1, 1, 1, time.Local)}},
					model.Trade{Identity: model.Identity{UpdatedAt: time.Date(2018, 3, 1, 1, 1, 1, 1, time.Local)}},
				},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, trade := range tt.args.trades {
				tt.tl.Add(trade)
			}

			if !reflect.DeepEqual(tt.tl, tt.want) {
				t.Error("TradeList.Add(); Difference between got and want", tt.tl, tt.want)
				t.Error(pretty.Compare(tt.tl, tt.want))
			}
		})
	}
}

func TestTradeList_Filter(t *testing.T) {
	type args struct {
		f func(t *model.Trade) bool
	}
	tests := []struct {
		name string
		tl   *TradeList
		args args
		want *TradeList
	}{
		{
			name: "It should filter based on the provided func #1",
			tl: &TradeList{
				trades: []model.Trade{
					model.Trade{Identity: model.Identity{ID: 1}, TradeFinishTime: func() *time.Time { t := time.Date(2016, 5, 5, 5, 5, 5, 5, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 2}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 8, 5, 5, 5, 5, 5, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 3}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 4, UpdatedAt: time.Date(2018, 1, 1, 1, 1, 1, 1, time.Local)}},
					model.Trade{Identity: model.Identity{ID: 5, UpdatedAt: time.Date(2018, 3, 1, 1, 1, 1, 1, time.Local)}},
				},
			},
			args: args{
				f: func(t *model.Trade) bool {
					if t.GetID() == 3 {
						return true
					}
					return false
				},
			},
			want: &TradeList{
				trades: []model.Trade{
					model.Trade{Identity: model.Identity{ID: 3}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
				},
			},
		},
		{
			name: "It should filter based on the provided func #2",
			tl: &TradeList{
				trades: []model.Trade{
					model.Trade{Identity: model.Identity{ID: 1}, TradeFinishTime: func() *time.Time { t := time.Date(2016, 5, 5, 5, 5, 5, 5, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 2}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 8, 5, 5, 5, 5, 5, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 3}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 4}, TradeFinishTime: func() *time.Time { t := time.Date(2018, 1, 1, 1, 1, 1, 1, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 5}, TradeFinishTime: func() *time.Time { t := time.Date(2018, 3, 1, 1, 1, 1, 1, time.Local); return &t }()},
				},
			},
			args: args{
				f: func(t *model.Trade) bool {
					d := t.TradeFinishTime
					filterTime := time.Date(2017, 12, 1, 0, 0, 0, 0, time.Local)
					return d.After(filterTime)
				},
			},
			want: &TradeList{
				trades: []model.Trade{
					model.Trade{Identity: model.Identity{ID: 4}, TradeFinishTime: func() *time.Time { t := time.Date(2018, 1, 1, 1, 1, 1, 1, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 5}, TradeFinishTime: func() *time.Time { t := time.Date(2018, 3, 1, 1, 1, 1, 1, time.Local); return &t }()},
				},
			},
		},
		{
			name: "It should return all if profided func returns true for all",
			tl: &TradeList{
				trades: []model.Trade{
					model.Trade{Identity: model.Identity{ID: 1}, TradeFinishTime: func() *time.Time { t := time.Date(2016, 5, 5, 5, 5, 5, 5, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 2}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 8, 5, 5, 5, 5, 5, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 3}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 4}, TradeFinishTime: func() *time.Time { t := time.Date(2018, 1, 1, 1, 1, 1, 1, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 5}, TradeFinishTime: func() *time.Time { t := time.Date(2018, 3, 1, 1, 1, 1, 1, time.Local); return &t }()},
				},
			},
			args: args{
				f: func(t *model.Trade) bool {
					return t.GetID() > 0
				},
			},
			want: &TradeList{
				trades: []model.Trade{
					model.Trade{Identity: model.Identity{ID: 1}, TradeFinishTime: func() *time.Time { t := time.Date(2016, 5, 5, 5, 5, 5, 5, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 2}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 8, 5, 5, 5, 5, 5, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 3}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 4}, TradeFinishTime: func() *time.Time { t := time.Date(2018, 1, 1, 1, 1, 1, 1, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 5}, TradeFinishTime: func() *time.Time { t := time.Date(2018, 3, 1, 1, 1, 1, 1, time.Local); return &t }()},
				},
			},
		},
		{
			name: "It should return empty if profided func returns false for all",
			tl: &TradeList{
				trades: []model.Trade{
					model.Trade{Identity: model.Identity{ID: 1}, TradeFinishTime: func() *time.Time { t := time.Date(2016, 5, 5, 5, 5, 5, 5, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 2}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 8, 5, 5, 5, 5, 5, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 3}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 4}, TradeFinishTime: func() *time.Time { t := time.Date(2018, 1, 1, 1, 1, 1, 1, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 5}, TradeFinishTime: func() *time.Time { t := time.Date(2018, 3, 1, 1, 1, 1, 1, time.Local); return &t }()},
				},
			},
			args: args{
				f: func(t *model.Trade) bool {
					filterTime := time.Date(2019, 1, 1, 1, 1, 1, 1, time.Local)
					return t.TradeFinishTime.After(filterTime)
				},
			},
			want: &TradeList{
				trades: []model.Trade{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tl.Filter(tt.args.f); !reflect.DeepEqual(got, tt.want) {
				t.Error("TradeList.Filter(); Difference between got and want")
				t.Error(pretty.Compare(got.trades, tt.want.trades))
			}
		})
	}
}

func TestTradeList_Find(t *testing.T) {
	type args struct {
		f func(t *model.Trade) bool
	}
	tests := []struct {
		name string
		tl   *TradeList
		args args
		want *model.Trade
	}{
		{
			name: "It should return the found item",
			tl: &TradeList{
				trades: []model.Trade{
					model.Trade{Identity: model.Identity{ID: 1}, TradeFinishTime: func() *time.Time { t := time.Date(2016, 5, 5, 5, 5, 5, 5, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 2}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 8, 5, 5, 5, 5, 5, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 3}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 4}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 5}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
				},
			},
			args: args{
				f: func(t *model.Trade) bool {
					if t.GetID() == 3 {
						return true
					}
					return false
				},
			},
			want: &model.Trade{Identity: model.Identity{ID: 3}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
		},
		{
			name: "It should return nil if item not found",
			tl: &TradeList{
				trades: []model.Trade{
					model.Trade{Identity: model.Identity{ID: 1}, TradeFinishTime: func() *time.Time { t := time.Date(2016, 5, 5, 5, 5, 5, 5, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 2}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 8, 5, 5, 5, 5, 5, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 3}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 4}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 5}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
				},
			},
			args: args{
				f: func(t *model.Trade) bool {
					return false
				},
			},
			want: nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tl.Find(tt.args.f); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TradeList.Find() = %v, want %v", got, tt.want)
				t.Error("TradeList.Find(); Difference between got and want")
				t.Error(pretty.Compare(got, tt.want))
			}
		})
	}
}

func TestTradeList_GetAll(t *testing.T) {
	tests := []struct {
		name string
		tl   *TradeList
		want []model.Trade
	}{
		{
			name: "It should return the list of existing trades",
			tl:   &TradeList{trades: []model.Trade{}},
			want: []model.Trade{},
		},
		{
			name: "It should return the list of existing trades",
			tl: &TradeList{trades: []model.Trade{
				model.Trade{Identity: model.Identity{ID: 1}, TradeFinishTime: func() *time.Time { t := time.Date(2016, 5, 5, 5, 5, 5, 5, time.Local); return &t }()},
				model.Trade{Identity: model.Identity{ID: 2}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 8, 5, 5, 5, 5, 5, time.Local); return &t }()},
			}},
			want: []model.Trade{
				model.Trade{Identity: model.Identity{ID: 1}, TradeFinishTime: func() *time.Time { t := time.Date(2016, 5, 5, 5, 5, 5, 5, time.Local); return &t }()},
				model.Trade{Identity: model.Identity{ID: 2}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 8, 5, 5, 5, 5, 5, time.Local); return &t }()},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tl.GetAll(); !reflect.DeepEqual(got, tt.want) {
				t.Error("TradeList.GetAll(); Diff between got and want")
				t.Error(pretty.Compare(got, tt.want))
			}
		})
	}
}

func TestTradeList_GetByID(t *testing.T) {
	type args struct {
		tradeID uint
	}
	tests := []struct {
		name string
		tl   *TradeList
		args args
		want *model.Trade
	}{
		{
			name: "It should return trade by id if found",
			tl: &TradeList{
				trades: []model.Trade{

					model.Trade{Identity: model.Identity{ID: 1}, TradeFinishTime: func() *time.Time { t := time.Date(2016, 5, 5, 5, 5, 5, 5, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 2}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 8, 5, 5, 5, 5, 5, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 3}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 4}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 5}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
				},
			},
			args: args{tradeID: 5},
			want: &model.Trade{Identity: model.Identity{ID: 5}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
		},
		{
			name: "It should return nil if not found",
			tl: &TradeList{
				trades: []model.Trade{
					model.Trade{Identity: model.Identity{ID: 1}, TradeFinishTime: func() *time.Time { t := time.Date(2016, 5, 5, 5, 5, 5, 5, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 2}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 8, 5, 5, 5, 5, 5, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 3}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 4}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 5}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
				},
			},
			args: args{tradeID: 10},
			want: nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tl.GetByID(tt.args.tradeID); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TradeList.GetByID() = %v, want %v", got, tt.want)
				t.Error("TradeList.GetByID(); Diff between got and want")
				t.Error(pretty.Compare(got, tt.want))
			}
		})
	}
}

func TestTradeList_GetByIndex(t *testing.T) {
	type args struct {
		idx int
	}
	tests := []struct {
		name string
		tl   *TradeList
		args args
		want *model.Trade
	}{
		{
			name: "It should return trade by index if in range",
			tl: &TradeList{
				trades: []model.Trade{
					model.Trade{Identity: model.Identity{ID: 1}, TradeFinishTime: func() *time.Time { t := time.Date(2016, 5, 5, 5, 5, 5, 5, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 2}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 8, 5, 5, 5, 5, 5, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 3}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 4}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 5}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
				},
			},
			args: args{idx: 1},
			want: &model.Trade{Identity: model.Identity{ID: 2}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 8, 5, 5, 5, 5, 5, time.Local); return &t }()},
		},
		{
			name: "It should return nil if idx not in range",
			tl: &TradeList{
				trades: []model.Trade{
					model.Trade{Identity: model.Identity{ID: 1}, TradeFinishTime: func() *time.Time { t := time.Date(2016, 5, 5, 5, 5, 5, 5, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 2}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 8, 5, 5, 5, 5, 5, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 3}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 4}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
					model.Trade{Identity: model.Identity{ID: 5}, TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }()},
				},
			},
			args: args{idx: 5},
			want: nil,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tl.GetByIndex(tt.args.idx); !reflect.DeepEqual(got, tt.want) {
				t.Error("TradeList.GetByIndex(); Diff between got and want")
				t.Error(pretty.Compare(got, tt.want))
			}
		})
	}
}

func TestTradeList_GetBetween(t *testing.T) {
	type args struct {
		fsym string
		tsym string
	}
	tests := []struct {
		name string
		tl   *TradeList
		args args
		want *TradeList
	}{
		{
			name: "It should get all the trades between 2 symbols",
			tl: &TradeList{
				trades: []model.Trade{
					model.Trade{
						Identity: model.Identity{ID: 1},
						From: model.TradeableAsset{
							Symbol: "ETH",
						},
						To: model.TradeableAsset{
							Symbol: "XRP",
						},
						TradeFinishTime: func() *time.Time { t := time.Date(2016, 5, 5, 5, 5, 5, 5, time.Local); return &t }(),
					},
					model.Trade{
						Identity: model.Identity{ID: 2},
						From: model.TradeableAsset{
							Symbol: "XRP",
						},
						To: model.TradeableAsset{
							Symbol: "XLM",
						},
						TradeFinishTime: func() *time.Time { t := time.Date(2017, 8, 5, 5, 5, 5, 5, time.Local); return &t }(),
					},
					model.Trade{
						Identity: model.Identity{ID: 3},
						From: model.TradeableAsset{
							Symbol: "BTC",
						},
						To: model.TradeableAsset{
							Symbol: "BCH",
						},
						TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }(),
					},
					model.Trade{
						Identity: model.Identity{ID: 4},
						From: model.TradeableAsset{
							Symbol: "ETH",
						},
						To: model.TradeableAsset{
							Symbol: "XRP",
						},
						TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }(),
					},
					model.Trade{
						Identity: model.Identity{ID: 5},
						From: model.TradeableAsset{
							Symbol: "USDT",
						},
						To: model.TradeableAsset{
							Symbol: "ZCASH",
						},
						TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }(),
					},
				},
			},
			args: args{fsym: "ETH", tsym: "XRP"},
			want: &TradeList{
				trades: []model.Trade{
					model.Trade{
						Identity: model.Identity{ID: 1},
						From: model.TradeableAsset{
							Symbol: "ETH",
						},
						To: model.TradeableAsset{
							Symbol: "XRP",
						},
						TradeFinishTime: func() *time.Time { t := time.Date(2016, 5, 5, 5, 5, 5, 5, time.Local); return &t }(),
					},
					model.Trade{
						Identity: model.Identity{ID: 4},
						From: model.TradeableAsset{
							Symbol: "ETH",
						},
						To: model.TradeableAsset{
							Symbol: "XRP",
						},
						TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }(),
					},
				},
			},
		},
		{
			name: "It should have no trades if none are found",
			tl: &TradeList{
				trades: []model.Trade{
					model.Trade{
						Identity: model.Identity{ID: 1},
						From: model.TradeableAsset{
							Symbol: "ETH",
						},
						To: model.TradeableAsset{
							Symbol: "XRP",
						},
						TradeFinishTime: func() *time.Time { t := time.Date(2016, 5, 5, 5, 5, 5, 5, time.Local); return &t }(),
					},
					model.Trade{
						Identity: model.Identity{ID: 2},
						From: model.TradeableAsset{
							Symbol: "XRP",
						},
						To: model.TradeableAsset{
							Symbol: "XLM",
						},
						TradeFinishTime: func() *time.Time { t := time.Date(2017, 8, 5, 5, 5, 5, 5, time.Local); return &t }(),
					},
					model.Trade{
						Identity: model.Identity{ID: 3},
						From: model.TradeableAsset{
							Symbol: "BTC",
						},
						To: model.TradeableAsset{
							Symbol: "BCH",
						},
						TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }(),
					},
					model.Trade{
						Identity: model.Identity{ID: 4},
						From: model.TradeableAsset{
							Symbol: "ETH",
						},
						To: model.TradeableAsset{
							Symbol: "XRP",
						},
						TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }(),
					},
					model.Trade{
						Identity: model.Identity{ID: 5},
						From: model.TradeableAsset{
							Symbol: "USDT",
						},
						To: model.TradeableAsset{
							Symbol: "ZCASH",
						},
						TradeFinishTime: func() *time.Time { t := time.Date(2017, 11, 1, 1, 1, 1, 1, time.Local); return &t }(),
					},
				},
			},
			args: args{fsym: "BTC", tsym: "USD"},
			want: &TradeList{
				trades: []model.Trade{},
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tl.GetBetween(tt.args.fsym, tt.args.tsym); !reflect.DeepEqual(got, tt.want) {
				t.Error("TradeList.GetBetween(); Diff between got and want")
				t.Error(pretty.Compare(got, tt.want))
			}
		})
	}
}

func TestTradeList_Len(t *testing.T) {
	tests := []struct {
		name string
		tl   *TradeList
		want int
	}{
		{
			name: "It should return the number of trades stored in trade list #1",
			tl:   &TradeList{trades: []model.Trade{}},
			want: 0,
		},
		{
			name: "It should return the number of trades stored in trade list #2",
			tl:   &TradeList{trades: []model.Trade{model.Trade{}, model.Trade{}}},
			want: 2,
		},
		{
			name: "It should return the number of trades stored in trade list #3",
			tl:   &TradeList{},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tl.Len(); got != tt.want {
				t.Errorf("TradeList.Len() = %v, want %v", got, tt.want)
			}
		})
	}
}
