package modelenum

//go:generate stringer -type=TradeState

// TradeState represents the state
// that a transaction happens to be
type TradeState int

const (
	// NewTrade represents a newly created trade
	NewTrade TradeState = iota

	// InProgressTrade state means the trade
	// is being processed
	InProgressTrade

	// CanceledTrade state means the trade was
	// cancelled
	CanceledTrade

	// CompletedTrade state means the trade
	// successfully finished
	CompletedTrade

	// ErroredTrade means that the trade
	// was rejected
	ErroredTrade
)

// GetTradeStates returns the available sates of a
// trade
func GetTradeStates() []string {
	states := []string{
		NewTrade.String(),
		InProgressTrade.String(),
		CanceledTrade.String(),
		CompletedTrade.String(),
		ErroredTrade.String(),
	}
	return states
}
