package modelenum

//go:generate stringer -type=NotificationState

// NotificationState represents the state
// of a notification
type NotificationState int

const (
	// PendingNotif means the notification
	// is yet to be send
	PendingNotif NotificationState = iota
	// CompletedNotif means the notification
	// has been successfully sent
	CompletedNotif
	// ErroredNotif means the notif
	// failed to be sent
	ErroredNotif
)

// GetNotificationStates returns the notification available
// states
func GetNotificationStates() []string {
	states := []string{PendingNotif.String(), CompletedNotif.String(), ErroredNotif.String()}
	return states
}
