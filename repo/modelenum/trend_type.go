package modelenum

//go:generate stringer -type=TrendType

// TrendType represents the type of the
// trend
type TrendType int

const (
	// TrendTypeAny represents either increasing
	// or decreasing trend type
	TrendTypeAny TrendType = iota
	// TrendTypeIncreasing represents a trend
	// that is increasing
	TrendTypeIncreasing
	// TrendTypeDecreasing represents a trend
	// that is decreasing
	TrendTypeDecreasing
)

// GetTrendTypes returns the available trend types
func GetTrendTypes() []string {
	types := []string{TrendTypeAny.String(), TrendTypeIncreasing.String(), TrendTypeDecreasing.String()}
	return types
}
