package modelenum

//go:generate stringer -type=TradeBotState

// TradeBotState represents the state of a
// bot
type TradeBotState int

const (
	// TradeBotNewState represents a newly created bot
	TradeBotNewState TradeBotState = iota
	// TradeBotRunningState indicates that the bot
	// is healthy and running
	TradeBotRunningState
	// TradeBotStoppedState represents the stopped / paused
	// state of a trader bot
	TradeBotStoppedState
	// TradeBotCompletedState represents the state of the bot
	// that has completed running
	TradeBotCompletedState
	// TradeBotErroredState indicates wether the bot
	// stopped due to an error
	TradeBotErroredState
)

// GetTradeBotStates returns the available states for a trading bot
func GetTradeBotStates() []string {
	states := []string{
		TradeBotNewState.String(),
		TradeBotRunningState.String(),
		TradeBotStoppedState.String(),
		TradeBotCompletedState.String(),
		TradeBotErroredState.String(),
	}
	return states
}
