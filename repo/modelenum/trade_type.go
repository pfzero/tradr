package modelenum

//go:generate stringer -type=TradeType

// TradeType represents the type of the
// trade
type TradeType int

const (
	// TradeTypeMarket represents a trade
	// that will happen at market price
	TradeTypeMarket TradeType = iota
	// TradeTypeLimit represents a trade
	// that is limited to a certain (threshold)
	// accepted price
	TradeTypeLimit
)

// GetTradeTypes returns the available trade types
func GetTradeTypes() []string {
	types := []string{TradeTypeMarket.String(), TradeTypeLimit.String()}
	return types
}
