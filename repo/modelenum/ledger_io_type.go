package modelenum

//go:generate stringer -type=LedgerIOType

// LedgerIOType represents the possible types
// of actions that a user can take besides the trade
// family of actions
type LedgerIOType int

const (
	// Withdrawal represents a Withdrawal transaction
	Withdrawal LedgerIOType = iota
	// Deposit represents a deposit transaction
	Deposit
)

// GetLedgerIOTypes returns the available user transaction
// types
func GetLedgerIOTypes() []string {
	types := []string{Deposit.String(), Withdrawal.String()}
	return types
}
