package model

// ExchangeSymbolAlias represents a data structure which can map
// system asset symbols to exchange specific symbol
type ExchangeSymbolAlias struct {
	Identity
	ExchangeID  uint
	Exchange    CryptoExchange
	AssetSymbol string
	AssetAlias  string
}
