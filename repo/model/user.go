package model

import "time"

// User is the model of users
type User struct {
	Identity
	Email string `gorm:"type:varchar(100);unique_index" sql:"index"`
	// ToDO: Check ho to expose users publicly without their passwords :(
	Password string
	Name     string
	Gender   string
	Role     Role
	RoleID   uint `json:"-"`

	// Confirm
	ConfirmToken string
	Confirmed    bool

	// Recover
	RecoverToken       string
	RecoverTokenExpiry *time.Time

	// Exchanges
	Exchanges []UserCryptoExchange
}

// DisplayName returns the display name of the user
func (user User) DisplayName() string {
	if user.Name != "" {
		return user.Name
	}
	return user.Email
}
