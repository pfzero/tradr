package model

// TradeError represents the error data structure
// that can occur on a trade
type TradeError struct {
	Identity
	Code        int
	Message     string
	Description string
}
