package model

// MultiCoinTradeCfg represents the configuration object
// that starts a multi-coin-pair-trader (aka. multi-coin-trader) strategy
type MultiCoinTradeCfg struct {
	Identity

	BeneficiaryID uint
	Beneficiary   User

	AutoTrade bool
	Notify    bool

	ExchangeID uint
	Exchange   CryptoExchange

	MinimumFiatAmount float64

	TradeTrendStepColID uint
	TradeTrendStepCol   TradeTrendStepCol

	InvCooldownConditionID uint
	InvCooldownCondition   TradeCondition

	InvConditionID uint
	InvCondition   TradeCondition

	ProfitConditionID uint
	ProfitCondition   TradeCondition

	BasePortfolioID uint
	BasePortfolio   Portfolio

	TargetPortfolioID uint
	TargetPortfolio   Portfolio

	// One of Trader state(s)
	State string `sql:"index"`
}
