package model

// UserCryptoExchange represents the list of
// exchanges that a user can have
type UserCryptoExchange struct {
	Identity
	Name       string
	UserID     uint
	User       User
	ExchangeID uint
	Exchange   CryptoExchange
	APIKey     string
}
