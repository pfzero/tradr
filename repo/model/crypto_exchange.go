package model

// CryptoExchange is the model for storing
// metadata about cryptocurrency exchanges
type CryptoExchange struct {
	Identity
	Name string `gorm:"type:varchar(100);unique_index" sql:"index"`
}
