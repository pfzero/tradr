package model

// Role represents the model for storing
// roles in the system
type Role struct {
	Identity
	Name string
}
