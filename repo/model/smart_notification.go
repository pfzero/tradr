package model

import "time"

// SmartNotification is the data structure
// that encapsulates the rules needed for triggering
// a smart notification
type SmartNotification struct {
	Identity

	// the user on behalf of which the notification
	// will run
	BeneficiaryID uint
	Beneficiary   User

	// Specifies if also an automatic trade
	// should happen
	AutoTrade bool
	// specifies fixed or percentage amount to trade
	// optional. Needed only when AutoTrade is set
	TradeAmount float64
	// optional reference to the exchange where
	// the auto-trade (if set) should take place
	ExchangeID uint
	Exchange   CryptoExchange

	// Smart notification works with a pair
	// of tradeable assets
	PairFromID uint
	PairFrom   TradeableAsset
	PairToID   uint
	PairTo     TradeableAsset

	// RefContextTime specifies what custom time to apply as
	// reference contexst to the trade condition
	RefContextTime *time.Time `json:"refContext"`

	// RefPrice is used to run the trade condition
	// against the given refPrice (in cases when the
	// ref context is not enough)
	RefPrice float64 `json:"refPrice"`

	TradeConditionID uint
	TradeCondition   TradeCondition

	// One of Trader state(s)
	State string `sql:"index"`
}
