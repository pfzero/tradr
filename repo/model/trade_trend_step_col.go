package model

// TradeTrendStepCol represents a col of trend steps
type TradeTrendStepCol struct {
	Identity
	Name string

	// List of steps
	TradeTrendSteps []TradeTrendStep

	// CycleSteps enables to cycle the steps
	CycleSteps bool
}

// GetTrendLevelAmount returns the amount percentage configured for the given
// trendCount
func (ttsc TradeTrendStepCol) GetTrendLevelAmount(trendCount int) float64 {

	if ttsc.CycleSteps {
		trendCount = trendCount % len(ttsc.TradeTrendSteps)
		if trendCount == 0 {
			trendCount = len(ttsc.TradeTrendSteps)
		}
	}

	for _, step := range ttsc.TradeTrendSteps {
		if step.SameTrendLevel == trendCount {
			if step.Liquidate {
				return 1
			}
			return step.AmountPercentage
		}
	}
	return 0
}
