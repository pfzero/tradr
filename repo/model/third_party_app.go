package model

import "bytes"

// ThirdPartyApp is the model that holds
// metadata of the apps that can fetch api data
type ThirdPartyApp struct {
	Identity
	Author         User
	AuthorID       uint
	Name           string
	ConsumerToken  string
	ConsumerSecret string
}

// DisplayName returns the display name of the
// app
func (t *ThirdPartyApp) DisplayName() string {
	var displayName bytes.Buffer
	displayName.WriteString("App::")
	displayName.WriteString(t.Name)
	return displayName.String()
}
