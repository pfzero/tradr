package model

import (
	"time"
)

// TradeCondition represents a single decision step
// that can be used by trade bots
type TradeCondition struct {
	Identity

	Name string `json:"name"`

	// execute after the given duration
	HasWaitDuration bool       `json:"hasWaitDuration"`
	WaitDuration    string     `json:"waitDuration"`
	WaitUntil       *time.Time `json:"waitUntil"`

	// Deadline parameters
	HasDeadline      bool       `json:"hasDeadline"`
	DeadlineDuration string     `json:"deadlineDuration"`
	DeadlineDate     *time.Time `json:"deadlineDate"`

	// DecisionThresholdsUp and DecisionThresholdsDown will
	// add the price variation unit
	HasPriceVariation       bool    `json:"hasPriceVariation"`
	PriceIncreasePercent    float64 `json:"priceIncreasePercent"`
	SymmetricPriceVariation bool    `json:"symmetricPriceVariation"`
	PriceDecreasePercent    float64 `json:"priceDecreasePercent"`

	// Include trend listener as well (execute trade)
	// after the trend finishes
	ListenTrend bool    `json:"listenTrend"`
	NoiseUp     float64 `json:"noiseUp"`
	NoiseDown   float64 `json:"noiseDown"`

	// Listen for specific candlesticks with a specific variation
	// defined by the above noise level
	ListenCandlestick bool    `json:"listenCandlestick"`
	TrendUp           float64 `json:"trendUp"`
	SymmetricTrend    bool    `json:"symmetricTrend"`
	TrendDown         float64 `json:"trendDown"`

	// Info on specific trends that formed
	ListenTrendFormation    bool    `json:"listenTrendFormation"`
	TrendFormationUp        float64 `json:"trendFormationUp"`
	SymmetricTrendFormation bool    `json:"symmetricTrendFormation"`
	TrendFormationDown      float64 `json:"trendFormationDown"`
}

// SetName sets the name of the trade condition
func (tc *TradeCondition) SetName(name string) *TradeCondition {
	tc.Name = name
	return tc
}

// SetWaitDuration sets the wait duration
func (tc *TradeCondition) SetWaitDuration(duration time.Duration) *TradeCondition {
	tc.HasWaitDuration = true
	tc.WaitDuration = duration.String()
	return tc
}

// SetWaitTime sets the wait time of the trade condition
func (tc *TradeCondition) SetWaitTime(t time.Time) *TradeCondition {
	tc.HasWaitDuration = true
	tc.WaitUntil = &t
	return tc
}

// SetDeadlineDuration sets the deadline duration of the trade condition
func (tc *TradeCondition) SetDeadlineDuration(d time.Duration) *TradeCondition {
	tc.HasDeadline = true
	tc.DeadlineDuration = d.String()
	return tc
}

// SetDeadlineTime sets the deadline time fo the trade condition
func (tc *TradeCondition) SetDeadlineTime(t time.Time) *TradeCondition {
	tc.HasDeadline = true
	tc.DeadlineDate = &t
	return tc
}

// SetPriceVariation sets the price variation accordingly; if the given
// value is negative then it sets the PriceDecreasePercent value; otherwise
// PriceIncreaseValue is set;
func (tc *TradeCondition) SetPriceVariation(v float64) *TradeCondition {
	if v < 0 {
		tc.PriceDecreasePercent = v
	} else {
		tc.PriceIncreasePercent = v
	}
	tc.HasPriceVariation = true
	return tc
}

// SetSymmetricPriceVariation sets the given price variation value and listens
// for symmetric price variation
func (tc *TradeCondition) SetSymmetricPriceVariation(variation float64) *TradeCondition {
	tc.SetPriceVariation(variation).SymmetricPriceVariation = true
	return tc
}

// SetNoise sets the noise value of the trade condition
func (tc *TradeCondition) SetNoise(v float64) *TradeCondition {
	if v < 0 {
		tc.NoiseDown = v
	} else {
		tc.NoiseUp = v
	}
	return tc
}

// WaitForCandle sets the candlestick variation that will be listen
// by this trade condition
func (tc *TradeCondition) WaitForCandle(v float64) *TradeCondition {
	if v < 0 {
		tc.TrendDown = v
	} else {
		tc.TrendUp = v
	}
	tc.ListenCandlestick = true
	return tc
}

// WaitForSymmetricCandle configures the trade condition to contain a
// symmetric candle formation
func (tc *TradeCondition) WaitForSymmetricCandle(v float64) *TradeCondition {
	tc.WaitForCandle(v).SymmetricTrend = true
	return tc
}

// WaitTrendFormation configures the trade condition to listen for specific
// trend formation
func (tc *TradeCondition) WaitTrendFormation(v float64) *TradeCondition {
	if v < 0 {
		tc.TrendFormationDown = v
	} else {
		tc.TrendFormationUp = v
	}
	tc.ListenTrendFormation = true
	return tc
}

// WaitSymmetricTrendFormation configures the trade condition to listen for
// symmetric trend formation
func (tc *TradeCondition) WaitSymmetricTrendFormation(v float64) *TradeCondition {
	tc.WaitTrendFormation(v).SymmetricTrendFormation = true
	return tc
}

// WaitTrendChange configures the trade condition to also listen for
// trend changes
func (tc *TradeCondition) WaitTrendChange() *TradeCondition {
	tc.ListenTrend = true
	return tc
}
