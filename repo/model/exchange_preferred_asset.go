package model

// ExchangePreferredAsset represents a preferred asset
// when making cross-trades on that specific exchange
// (or when fetching the price of an asset)
// it implements cryptomarketutil.ExchangePriorityAsset interface
type ExchangePreferredAsset struct {
	Identity
	ExchangeID       uint
	Exchange         CryptoExchange
	TradeableAssetID uint
	TradeableAsset   TradeableAsset
	Priority         int
}

// GetExchange returns the exchange name
func (epa *ExchangePreferredAsset) GetExchange() string {
	return epa.Exchange.Name
}

// GetSymbol returns the asset symbol
func (epa *ExchangePreferredAsset) GetSymbol() string {
	return epa.TradeableAsset.Symbol
}

// GetPriority returns the priority of the asset for the specific
// exchange
func (epa *ExchangePreferredAsset) GetPriority() int {
	return epa.Priority
}
