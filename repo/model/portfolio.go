package model

// Portfolio represents the model
// of a pool of assets
type Portfolio struct {
	Identity

	Name string

	PortfolioAssets []PortfolioAsset
}
