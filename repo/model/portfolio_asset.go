package model

// PortfolioAsset is the model that holds informations
// about an asset within a pool of assets
type PortfolioAsset struct {
	ID uint `json:"id" gorm:"primary_key"`

	TradeableAssetID uint
	TradeableAsset   TradeableAsset

	Distribution    float64
	DistributionMax float64
	DistributionMin float64

	PortfolioID uint
	Portfolio   Portfolio
}
