package model

import (
	"fmt"
)

// CryptoTrackedCoin is the entity that represents
// the settings for tracking a specific crypto coin
// Manually implements Identity interface in order to create
// unique_id on deletedAt field
type CryptoTrackedCoin struct {
	Identity

	// Symbolic name of the tracked coin
	// usually it should be CoinSymbol-Exchange
	Name string `json:"name"`

	// Which coin to track
	CoinID uint           `json:"-" gorm:"unique_index:hash_id"`
	Coin   TradeableAsset `json:"coin"`

	// which currency do we track the price for?
	TrackedExchangeID uint           `json:"-" gorm:"unique_index:hash_id"`
	TrackedExchange   CryptoExchange `gorm:"many2many:tracked_crypto_coins"`
}

// HashID returns the global unique id of this crypto tracked coin
func (ctc *CryptoTrackedCoin) HashID() string {
	return fmt.Sprintf(`%d:%d`, ctc.CoinID, ctc.TrackedExchangeID)
}
