package model

// TradeTrendStep represents the amount percentages
// that are going to be traded when decision pipeline signals
// for a trade
type TradeTrendStep struct {
	ID uint `json:"id" gorm:"primary_key"`

	SameTrendLevel   int
	AmountPercentage float64
	Liquidate        bool

	TradeTrendStepColID uint
	TradeTrendStepCol   TradeTrendStepCol
}
