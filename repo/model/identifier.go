package model

import "time"

// Identifier is the interface of all entities that
// have an ID
type Identifier interface {
	GetID() uint
	SetID(uint)
	ResetID()
	IsZero() bool
}

// Identity represents the base fields of models
type Identity struct {
	ID        uint      `json:"id" gorm:"primary_key"`
	CreatedAt time.Time `json:"-"`
	UpdatedAt time.Time
	DeletedAt *time.Time `json:"-" sql:"index"`
}

// GetID returns the id of the model
func (model *Identity) GetID() uint {
	return model.ID
}

// SetID sets the id of the model
func (model *Identity) SetID(id uint) {
	model.ID = id
}

// ResetID resets the id of the model
func (model *Identity) ResetID() {
	model.ID = 0
}

// IsZero checks wether the model has an identity
func (model *Identity) IsZero() bool {
	return model.GetID() == 0
}
