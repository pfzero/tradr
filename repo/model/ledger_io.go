package model

// LedgerIO is the datastructure that
// represents all the possible deposits and withdrawals
// from either exchanges or traders
type LedgerIO struct {
	Identity

	// one of LedgerIOType(s)
	Type       string
	OwnerID    uint
	Owner      User
	ExchangeID uint
	Exchange   CryptoExchange

	// TradeGUID in cases when the withrawal
	// or deposit action were made to a trader
	TraderGUID string

	AssetID uint
	Asset   TradeableAsset
	Amount  float64
}

// IsExchangeAction checks wether the current action
// was performed for a specific exchange
func (lio *LedgerIO) IsExchangeAction() bool {
	return lio.TraderGUID == "" && lio.ExchangeID != 0
}

// IsTraderAction checks wether the current action was
// performed for a specific trader
func (lio *LedgerIO) IsTraderAction() bool {
	return lio.TraderGUID != ""
}
