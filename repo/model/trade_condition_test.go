package model_test

import (
	"reflect"
	"testing"
	"time"

	"bitbucket.org/pfzero/tradr/repo/model"
)

func TestTradeCondition_SetName(t *testing.T) {
	type args struct {
		name string
	}
	tests := []struct {
		name string
		tc   *model.TradeCondition
		args args
		want *model.TradeCondition
	}{
		{
			name: "Name Setter",
			tc:   &model.TradeCondition{},
			args: args{name: "Test Condition"},
			want: &model.TradeCondition{Name: "Test Condition"},
		},
		{
			name: "Name Setter 2",
			tc:   &model.TradeCondition{},
			args: args{name: "Test Some Other Name"},
			want: &model.TradeCondition{Name: "Test Some Other Name"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tc.SetName(tt.args.name); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TradeCondition.SetName() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTradeCondition_SetWaitDuration(t *testing.T) {
	type args struct {
		duration time.Duration
	}
	tests := []struct {
		name string
		tc   *model.TradeCondition
		args args
		want *model.TradeCondition
	}{
		{
			name: "Test Wait Duration",
			tc:   &model.TradeCondition{},
			args: args{duration: 1 * time.Second},
			want: &model.TradeCondition{HasWaitDuration: true, WaitDuration: time.Second.String()},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tc.SetWaitDuration(tt.args.duration); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TradeCondition.SetWaitDuration() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTradeCondition_SetWaitTime(t *testing.T) {
	type args struct {
		t time.Time
	}
	wantTime := time.Date(2018, 05, 15, 0, 0, 0, 0, time.Local)
	tests := []struct {
		name string
		tc   *model.TradeCondition
		args args
		want *model.TradeCondition
	}{
		{
			name: "Test Wait Time",
			tc:   &model.TradeCondition{},
			args: args{t: wantTime},
			want: &model.TradeCondition{HasWaitDuration: true, WaitUntil: &wantTime},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tc.SetWaitTime(tt.args.t); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TradeCondition.SetWaitTime() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTradeCondition_SetDeadlineDuration(t *testing.T) {
	type args struct {
		d time.Duration
	}
	tests := []struct {
		name string
		tc   *model.TradeCondition
		args args
		want *model.TradeCondition
	}{
		{
			name: "Deadline Duration Setter",
			tc:   &model.TradeCondition{},
			args: args{d: time.Second},
			want: &model.TradeCondition{HasDeadline: true, DeadlineDuration: time.Second.String()},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tc.SetDeadlineDuration(tt.args.d); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TradeCondition.SetDeadlineDuration() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTradeCondition_SetDeadlineTime(t *testing.T) {
	type args struct {
		t time.Time
	}
	wantTime := time.Date(2018, 05, 15, 0, 0, 0, 0, time.Local)
	tests := []struct {
		name string
		tc   *model.TradeCondition
		args args
		want *model.TradeCondition
	}{
		{
			name: "Deadline Time Setter",
			tc:   &model.TradeCondition{},
			args: args{t: wantTime},
			want: &model.TradeCondition{HasDeadline: true, DeadlineDate: &wantTime},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tc.SetDeadlineTime(tt.args.t); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TradeCondition.SetDeadlineTime() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTradeCondition_SetPriceVariation(t *testing.T) {
	type args struct {
		v float64
	}
	tests := []struct {
		name string
		tc   *model.TradeCondition
		args args
		want *model.TradeCondition
	}{
		{
			name: "Price Variation Setter Up",
			tc:   &model.TradeCondition{},
			args: args{v: 0.2},
			want: &model.TradeCondition{HasPriceVariation: true, PriceIncreasePercent: 0.2},
		},
		{
			name: "Price Variation Setter Down",
			tc:   &model.TradeCondition{},
			args: args{v: -0.2},
			want: &model.TradeCondition{HasPriceVariation: true, PriceDecreasePercent: -0.2},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tc.SetPriceVariation(tt.args.v); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TradeCondition.SetPriceVariation() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTradeCondition_SetSymmetricPriceVariation(t *testing.T) {
	type args struct {
		variation float64
	}
	tests := []struct {
		name string
		tc   *model.TradeCondition
		args args
		want *model.TradeCondition
	}{
		{
			name: "Symmetric Price Variation Setter Up",
			tc:   &model.TradeCondition{},
			args: args{variation: 0.25},
			want: &model.TradeCondition{HasPriceVariation: true, SymmetricPriceVariation: true, PriceIncreasePercent: 0.25},
		},
		{
			name: "Symmetric Price Variation Setter Down",
			tc:   &model.TradeCondition{},
			args: args{variation: -0.25},
			want: &model.TradeCondition{HasPriceVariation: true, SymmetricPriceVariation: true, PriceDecreasePercent: -0.25},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tc.SetSymmetricPriceVariation(tt.args.variation); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TradeCondition.SetSymmetricPriceVariation() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTradeCondition_SetNoise(t *testing.T) {
	type args struct {
		v float64
	}
	tests := []struct {
		name string
		tc   *model.TradeCondition
		args args
		want *model.TradeCondition
	}{
		{
			name: "Noise Setter Up",
			tc:   &model.TradeCondition{},
			args: args{v: 0.025},
			want: &model.TradeCondition{NoiseUp: 0.025},
		},
		{
			name: "Noise Setter Down",
			tc:   &model.TradeCondition{},
			args: args{v: -0.02},
			want: &model.TradeCondition{NoiseDown: -0.02},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tc.SetNoise(tt.args.v); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TradeCondition.SetNoise() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTradeCondition_WaitForCandle(t *testing.T) {
	type args struct {
		v float64
	}
	tests := []struct {
		name string
		tc   *model.TradeCondition
		args args
		want *model.TradeCondition
	}{
		{
			name: "Wait For Candle Up",
			tc:   &model.TradeCondition{},
			args: args{v: 0.25},
			want: &model.TradeCondition{TrendUp: 0.25, ListenCandlestick: true},
		},
		{
			name: "Wait For Candle Down",
			tc:   &model.TradeCondition{},
			args: args{v: -0.2},
			want: &model.TradeCondition{TrendDown: -0.2, ListenCandlestick: true},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tc.WaitForCandle(tt.args.v); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TradeCondition.WaitForCandle() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTradeCondition_WaitForSymmetricCandle(t *testing.T) {
	type args struct {
		v float64
	}
	tests := []struct {
		name string
		tc   *model.TradeCondition
		args args
		want *model.TradeCondition
	}{
		{
			name: "Wait For Symmetric Candle Up",
			tc:   &model.TradeCondition{},
			args: args{v: 0.25},
			want: &model.TradeCondition{TrendUp: 0.25, ListenCandlestick: true, SymmetricTrend: true},
		},
		{
			name: "Wait For Symmetric Candle Down",
			tc:   &model.TradeCondition{},
			args: args{v: -0.2},
			want: &model.TradeCondition{TrendDown: -0.2, ListenCandlestick: true, SymmetricTrend: true},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tc.WaitForSymmetricCandle(tt.args.v); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TradeCondition.WaitForSymmetricCandle() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTradeCondition_WaitTrendFormation(t *testing.T) {
	type args struct {
		v float64
	}
	tests := []struct {
		name string
		tc   *model.TradeCondition
		args args
		want *model.TradeCondition
	}{
		{
			name: "Wait For Trend Formation Up",
			tc:   &model.TradeCondition{},
			args: args{v: 0.25},
			want: &model.TradeCondition{TrendFormationUp: 0.25, ListenTrendFormation: true},
		},
		{
			name: "Wait For Trend Formation Down",
			tc:   &model.TradeCondition{},
			args: args{v: -0.2},
			want: &model.TradeCondition{TrendFormationDown: -0.2, ListenTrendFormation: true},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tc.WaitTrendFormation(tt.args.v); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TradeCondition.WaitTrendFormation() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTradeCondition_WaitSymmetricTrendFormation(t *testing.T) {
	type args struct {
		v float64
	}
	tests := []struct {
		name string
		tc   *model.TradeCondition
		args args
		want *model.TradeCondition
	}{
		{
			name: "Wait For Trend Formation Up",
			tc:   &model.TradeCondition{},
			args: args{v: 0.25},
			want: &model.TradeCondition{TrendFormationUp: 0.25, ListenTrendFormation: true, SymmetricTrendFormation: true},
		},
		{
			name: "Wait For Trend Formation Down",
			tc:   &model.TradeCondition{},
			args: args{v: -0.2},
			want: &model.TradeCondition{TrendFormationDown: -0.2, ListenTrendFormation: true, SymmetricTrendFormation: true},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tc.WaitSymmetricTrendFormation(tt.args.v); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TradeCondition.WaitSymmetricTrendFormation() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTradeCondition_WaitTrendChange(t *testing.T) {
	tests := []struct {
		name string
		tc   *model.TradeCondition
		want *model.TradeCondition
	}{
		{
			name: "Wait Trend Change",
			tc:   &model.TradeCondition{},
			want: &model.TradeCondition{ListenTrend: true},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tc.WaitTrendChange(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TradeCondition.WaitTrendChange() = %v, want %v", got, tt.want)
			}
		})
	}
}
