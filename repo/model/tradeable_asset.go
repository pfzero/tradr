package model

// TradeableAsset represents the model of a tradeable
// asset
type TradeableAsset struct {
	Identity
	Name   string
	Symbol string `gorm:"type:varchar(20);unique_index" sql:"index"`
}
