package model

import (
	"bitbucket.org/pfzero/tradr/repo/modelenum"
)

// TradeDecision is the datastructure created by auto traders
type TradeDecision struct {
	Identity

	// the user for which the decision was made
	BeneficiaryID uint
	Beneficiary   User

	// Ledger of transactions
	// creted for this decision
	Trades []Trade

	// trader unique id
	TraderGUID string

	Notify bool

	// NotifyStatus represents the status
	// of the notification. One of
	// modelenum.NotifyState
	NotifyStatus string
	AutoTrade    bool
}

// IsCompletedNotification checks wether the notification
// was successfully sent
func (td *TradeDecision) IsCompletedNotification() bool {
	return td.NotifyStatus == modelenum.CompletedNotif.String()
}

// IsCompleted checks wether the current trade decision
// was completed: ie. status is either complete or noop
func (td *TradeDecision) IsCompleted() bool {
	isCompleted := true
	for _, trade := range td.Trades {
		isCompleted = isCompleted && trade.IsCompleteTrade()
	}

	return isCompleted && td.IsCompletedNotification()
}

// IsInProgress checks wether the current trade decision
// is in progress;
func (td *TradeDecision) IsInProgress() bool {
	for _, trade := range td.Trades {
		if trade.IsProgressTrade() {
			return true
		}
	}
	return false
}

// GetTrades returns the parent trades of this trade decision
func (td *TradeDecision) GetTrades() []Trade {
	return td.Trades
}
