package model

import (
	"time"

	"bitbucket.org/pfzero/tradr/repo/modelenum"
)

// Trade represents the datastructure
// that encodes a trade between 2 assets
type Trade struct {
	Identity

	FromID uint
	From   TradeableAsset
	ToID   uint
	To     TradeableAsset

	// On which exchange should this trade
	// occur
	ExchangeID uint
	Exchange   CryptoExchange

	// trade's owner
	OwnerID uint
	Owner   User

	// One of TradeType(s)
	Type string

	// The amount intent to trade
	FromAmountIntent float64
	ToAmountIntent   float64

	// The intended price
	PriceIntent float64

	// The amount actually traded
	FromAmountTrade float64
	ToAmountTrade   float64

	// Actual trade price
	TradePrice float64

	// Time when the trade completed / failed
	TradeFinishTime *time.Time

	// One of TradeStatus(es)
	Status string

	// Optional reference to a TradeDecision
	// will be set when an auto trader makes
	// a trade decision
	TradeDecisionID uint
	TradeDecision   TradeDecision

	LinkedTradeID uint
	LinkedTrade   *Trade

	// unique id for staging / commiting
	// the trade within a ledger
	LedgerTxID string

	// Trade Error, in cases when the trade wasn't
	// successfull
	TradeErrorID uint
	TradeError   TradeError
}

// IsCompleteTrade checks wether a trade was completed
func (t *Trade) IsCompleteTrade() bool {
	return t.Status == modelenum.CompletedTrade.String()
}

// IsProgressTrade checks wether the current trade is
// in progress
func (t *Trade) IsProgressTrade() bool {
	return t.Status == modelenum.InProgressTrade.String()
}

// IsCanceledTrade checks wether the trade is canceled trade
func (t *Trade) IsCanceledTrade() bool {
	return t.Status == modelenum.CanceledTrade.String()
}

// IsErroredTrade checks if the status of the trade is errored
func (t *Trade) IsErroredTrade() bool {
	return t.Status == modelenum.ErroredTrade.String()
}

// GetTradeRelevantTime returns the relevant time of the trade;
// if trade finish time is not zeroed than it's returned; otherwise
// the created at field is returned
func (t *Trade) GetTradeRelevantTime() time.Time {
	if t.TradeFinishTime == nil || t.TradeFinishTime.IsZero() {
		return t.UpdatedAt
	}

	return *t.TradeFinishTime
}
