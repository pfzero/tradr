package model

import (
	"fmt"
	"reflect"
	"time"

	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"
)

// CryptoPriceEntry represents a point of price
// in time
type CryptoPriceEntry struct {
	TrackedCoinID uint              `json:"-" gorm:"index"`
	TrackedCoin   CryptoTrackedCoin `json:"trackedCoin"`
	Time          time.Time         `json:"time" gorm:"index"`
	Price         float64           `json:"price"`
}

// GetPrice returns the price of the entry
func (cpe *CryptoPriceEntry) GetPrice() float64 {
	return cpe.Price
}

// GetTimestamp returns the timestamp of the price
func (cpe *CryptoPriceEntry) GetTimestamp() time.Time {
	return cpe.Time
}

// ConvertPricesToUtilPrices converts a list of price entries
// to a list of Pricer interface
func ConvertPricesToUtilPrices(prices []CryptoPriceEntry) []cryptomarketutil.Pricer {
	utilPrices := make([]cryptomarketutil.Pricer, len(prices))
	for idx := range prices {
		utilPrices[idx] = &prices[idx]
	}
	return utilPrices
}

// ConvertUtilPricesToPriceEntries converts the list of
// Pricer interface to actual list of CryptoPriceEntry
func ConvertUtilPricesToPriceEntries(utilPrices []cryptomarketutil.Pricer) ([]CryptoPriceEntry, error) {
	prices := make([]CryptoPriceEntry, len(utilPrices))
	errMsg := `Cannot convert type %s to CryptoPriceEntry`

	for idx := range utilPrices {
		c, e := utilPrices[idx].(*CryptoPriceEntry)
		if !e {
			e := fmt.Errorf(errMsg, reflect.TypeOf(utilPrices[idx]).Name())
			return nil, e
		}
		prices[idx] = *c
	}

	return prices, nil
}
