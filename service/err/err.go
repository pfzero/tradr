package err

// IsNotFound checks if the given error was a
// 404 not found type error
func IsNotFound(err error) bool {
	return err != nil && err.Error() == "404 Not Found"
}
