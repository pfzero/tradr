package service

import (
	"bitbucket.org/pfzero/tradr/cfg"
	"bitbucket.org/pfzero/tradr/service/cryptomarket"
	"bitbucket.org/pfzero/tradr/service/exchange"
	"bitbucket.org/pfzero/tradr/service/mailer"
	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"
)

// Initialize fetches all the services
// and provides the needed configurations
func Initialize() {
	err := exchange.Initialize()
	if err != nil {
		panic(err)
	}

	mailerCfg := &mailer.MailServiceCfg{
		FromName:  cfg.GetConfig().GetString("services.mail.from_name"),
		FromEmail: cfg.GetConfig().GetString("services.mail.from_email"),
		FromPwd:   cfg.GetConfig().GetString("services.mail.from_pwd"),
		SMTPAddr:  cfg.GetConfig().GetString("services.mail.smtp_addr"),
		SMTPPort:  cfg.GetConfig().GetString("services.mail.smtp_port"),
		RateLimit: cfg.GetConfig().GetFloat64("services.mail.rate_limit"),
	}

	mailer.Configure(mailerCfg)
}

// Provide provides further the needed dependencies for this module
func Provide(epa *cryptomarketutil.ExchangePreferredAssets, aliases *cryptomarketutil.ExchangeAssetAlias) {
	err := cryptomarket.Initialize(epa, aliases)
	if err != nil {
		panic(err)
	}
}
