package cryptomarket

import (
	"errors"
	"time"

	"bitbucket.org/pfzero/tradr/cfg"

	"bitbucket.org/pfzero/tradr/service/cryptomarket/binancemarket"
	"bitbucket.org/pfzero/tradr/service/cryptomarket/cryptocompareapi"
	"bitbucket.org/pfzero/tradr/service/cryptomarket/market"
	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"
)

var exchangePreferredAssets *cryptomarketutil.ExchangePreferredAssets
var exchangeAliases *cryptomarketutil.ExchangeAssetAlias
var cmCfg *cryptomarketCfg

type cryptomarketCfg struct {
	readRetries          int
	orderRetries         int
	readRealTimeDeadline time.Duration
	readHistoDeadline    time.Duration
	readAllHistoDeadline time.Duration
	orderDeadline        time.Duration
}

// Initialize initializes the package
func Initialize(pa *cryptomarketutil.ExchangePreferredAssets, aliases *cryptomarketutil.ExchangeAssetAlias) error {
	exchangePreferredAssets = pa
	exchangeAliases = aliases

	cryptoCompareapiErr := cryptocompareapi.Initialize()
	if cryptoCompareapiErr != nil {
		return cryptoCompareapiErr
	}

	marketExchangeErr := market.Initialize(exchangeAliases)
	if marketExchangeErr != nil {
		return marketExchangeErr
	}

	binanceMarketErr := binancemarket.Initialize(exchangePreferredAssets, exchangeAliases)

	if binanceMarketErr != nil {
		return binanceMarketErr
	}

	cfgErr := loadcmCfg()

	return cfgErr
}

func loadcmCfg() error {
	readRetries := cfg.GetConfig().GetInt("services.cryptomarket.read_retries")
	orderRetries := cfg.GetConfig().GetInt("services.cryptomarket.order_retries")
	readRealTimeDeadline := cfg.GetConfig().GetDuration("services.cryptomarket.read_rt_deadline")
	readHistoDeadline := cfg.GetConfig().GetDuration("services.cryptomarket.read_histo_deadline")
	readAllHistoDeadline := cfg.GetConfig().GetDuration("services.cryptomarket.read_all_histo_deadline")
	orderDeadline := cfg.GetConfig().GetDuration("services.cryptomarket.order_deadline")

	if readRetries == 0 {
		return errors.New("Please set the `services.cryptomarket` configurations in config file; read_retries is missing")
	}
	if orderRetries == 0 {
		return errors.New("Please set the `services.cryptomarket` configurations in config file; order_retries is missing")
	}
	if readRealTimeDeadline == 0 {
		return errors.New("Please set the `services.cryptomarket` configurations in config file; read_rt_deadline is missing")
	}
	if readHistoDeadline == 0 {
		return errors.New("Please set the `services.cryptomarket` configurations in config file; read_histo_deadline is missing")
	}
	if readAllHistoDeadline == 0 {
		return errors.New("Please set the `services.cryptomarket` configurations in config file; read_all_histo_deadline is missing")
	}

	if orderDeadline == 0 {
		return errors.New("Please set the `services.cryptomarket` configurations in config file; order_deadline is missing")
	}

	cmCfg = &cryptomarketCfg{
		readRetries:          readRetries,
		orderRetries:         orderRetries,
		readRealTimeDeadline: readRealTimeDeadline * time.Millisecond,
		readHistoDeadline:    readHistoDeadline * time.Second,
		readAllHistoDeadline: readAllHistoDeadline * time.Second,
		orderDeadline:        orderDeadline * time.Second,
	}

	return nil
}
