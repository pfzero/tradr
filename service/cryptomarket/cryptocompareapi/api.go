package cryptocompareapi

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/pfzero/tradr/cfg"

	"bitbucket.org/pfzero/tradr/service/request"
	"golang.org/x/time/rate"
	"gopkg.in/fatih/set.v0"
)

var (
	cryptoCompareAddr string
	tickerAddrTpl     string
	histoHourAddrTpl  string
	statusAddrTpl     string

	pageLimit uint

	tickerLimiter *rate.Limiter
	histoLimiter  *rate.Limiter
)

// Initialize initializes the cryptocompare service
// it should be called after the global configuration module was
// started
func Initialize() error {
	cryptoCompareAddr = cfg.GetConfig().GetString("services.cryptocompare.base")
	tickerAddrTpl = cfg.GetConfig().GetString("services.cryptocompare.ticker")
	histoHourAddrTpl = cfg.GetConfig().GetString("services.cryptocompare.histo")
	statusAddrTpl = cfg.GetConfig().GetString("services.cryptocompare.statusAddrTpl")

	pageLimit = uint(cfg.GetConfig().GetInt("services.cryptocompare.page_limit"))

	if cryptoCompareAddr == "" || tickerAddrTpl == "" || histoHourAddrTpl == "" || pageLimit == 0 {
		return errors.New("CryptoCompare API settings are not correct. Please check them in configuration files")
	}

	tickerRL := cfg.GetConfig().GetFloat64("services.cryptocompare.rl_ticker")
	tickerHisto := cfg.GetConfig().GetFloat64("services.cryptocompare.rl_histo")

	if tickerRL == 0 || tickerHisto == 0 {
		return errors.New("CryptoCompare API configs did not set rl_ticker or rl_histo params correctly. Please check them in config files")
	}

	tickerLimiter = rate.NewLimiter(rate.Limit(tickerRL), int(tickerRL))
	histoLimiter = rate.NewLimiter(rate.Limit(tickerHisto), int(tickerHisto))

	return nil
}

// GetTicker returns the ticker info for the given pair and exchange
func GetTicker(ctx context.Context, req *TickReq) (*TickResp, error) {

	fsym := strings.ToUpper(req.From)
	tsym := strings.ToUpper(req.To)
	exchange := req.Exchange
	if exchange == "" || exchange == "Aggregate" {
		exchange = "Market"
	}

	if fsym == "" || tsym == "" {
		return nil, fmt.Errorf("'From' and 'To' params are required. Received From=%s and To=%s", fsym, tsym)
	}

	relative := fmt.Sprintf(tickerAddrTpl, fsym, tsym)

	if exchange != "Market" {
		relative += fmt.Sprintf("&e=%s", exchange)
	}

	addr := fmt.Sprintf("%s%s", cryptoCompareAddr, relative)

	r := tickerLimiter.Reserve()

	time.Sleep(r.Delay())

	rawResp := &tickerResp{}

	err := request.Get(ctx, addr, &rawResp)

	if err != nil {
		return nil, err
	}

	if rawResp.Response != "" {
		return nil, errors.New(rawResp.Message)
	}

	tickerData, ok := rawResp.Raw[fsym][tsym]

	if !ok {
		return nil, fmt.Errorf("The received ticker response was ok but got no data for %s-%s @%s", fsym, tsym, exchange)
	}

	return &TickResp{
		Time:      time.Unix(tickerData.LastUpdate, 0),
		Price:     tickerData.Price,
		Supply:    tickerData.Supply,
		MarketCap: tickerData.Price * tickerData.Supply,
	}, nil
}

// GetHistoPrice returns the historical price based on the given request
func GetHistoPrice(ctx context.Context, req *HistoReq) ([]ParsedHistoricalPrice, error) {

	accum := []HistoricalPrice{}
	lastTimestamp := req.ToTime

	// get all the batches required to cover the requested period
	for {
		batchReq := &historicalReq{
			TickReq: TickReq{
				From:     req.From,
				To:       req.To,
				Exchange: req.Exchange,
			},
			ToTimestamp: lastTimestamp.Unix(),
		}

		batch, batchErr := getCoinHistorical(ctx, batchReq)

		if batchErr != nil {
			return nil, batchErr
		}

		accum = append(batch.Data, accum...)
		lastTimestamp = time.Unix(batch.TimeFrom, 0)

		firstPrice := batch.Data[0]

		if lastTimestamp.Before(req.FromTime) || firstPrice.IsZero() {
			break
		}
	}

	resp := []ParsedHistoricalPrice{}
	tsSet := set.New(set.ThreadSafe)

	// filter the prices that don't fit the requested time period
	for _, elem := range accum {
		priceTimestamp := time.Unix(elem.Time, 0)

		// remove out of range prices
		if req.FromTime.After(priceTimestamp) || elem.IsZero() {
			continue
		}

		// remove duplicates, if any
		if tsSet.Has(elem.Time) {
			continue
		}
		tsSet.Add(elem.Time)

		// also set the price to Close price
		elem.Price = elem.Close

		price := ParsedHistoricalPrice{
			HistoricalPrice: elem,
			Time:            priceTimestamp,
		}
		resp = append(resp, price)
	}

	return resp, nil
}

// GetStatus checks the status of the cryptocompare servers
func GetStatus(ctx context.Context) error {
	addr := getStatusTpl()
	time.Sleep(tickerLimiter.Reserve().Delay())
	err := request.Get(ctx, addr, nil)
	return err
}

// GetAllHistoData is a shorthand to GetHistoPrice without
// specifying the FromTime parameter
func GetAllHistoData(ctx context.Context, req *TickReq) ([]ParsedHistoricalPrice, error) {
	return GetHistoPrice(ctx, &HistoReq{
		TickReq: *req,
		ToTime:  time.Now(),
	})
}

func getCoinHistorical(ctx context.Context, req *historicalReq) (*historicalResp, error) {

	badReq := fmt.Errorf(`The given parameters are incorrect. Received: %v`, req)
	limit := req.Limit
	toTimestamp := req.ToTimestamp

	if req.From == "" || req.To == "" {
		return nil, badReq
	}

	if limit == 0 {
		limit = pageLimit
	}

	if toTimestamp == 0 {
		toTimestamp = time.Now().Unix()
	}

	addr := fmt.Sprintf(getHistoHourTpl(), req.From, req.To, limit, toTimestamp)

	if req.Exchange != "" && req.Exchange != "Aggregate" && req.Exchange != "Market" {
		addr = fmt.Sprintf("%s&e=%s", addr, req.Exchange)
	}

	r := histoLimiter.Reserve()
	time.Sleep(r.Delay())

	resp := &historicalResp{}
	err := request.Get(ctx, addr, &resp)

	if err != nil {
		return nil, err
	}
	if resp.Response == "Error" {
		return nil, errors.New(resp.Message)
	}

	return resp, nil
}

func getHistoHourTpl() string {
	return fmt.Sprintf("%s%s", cryptoCompareAddr, histoHourAddrTpl)
}

func getTickerTpl() string {
	return fmt.Sprintf("%s%s", cryptoCompareAddr, tickerAddrTpl)
}

func getStatusTpl() string {
	return fmt.Sprintf("%s%s", cryptoCompareAddr, statusAddrTpl)
}
