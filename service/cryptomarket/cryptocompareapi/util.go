package cryptocompareapi

type historicalReq struct {
	TickReq
	Limit       uint
	ToTimestamp int64
}

type historicalResp struct {
	Response          string
	Message           string
	Type              uint
	Aggregated        bool
	Data              []HistoricalPrice
	TimeTo            int64
	TimeFrom          int64
	FirstValueInArray bool
}

type tickerResp struct {
	Raw map[string]map[string]tickerInfo `json:"RAW"`

	Response   string
	Message    string
	Type       int
	Warning    string
	HasWarning bool
}

type tickerInfo struct {
	FromSymbol string  `json:"FROMSYMBOL"`
	ToSymbol   string  `json:"TOSYMBOL"`
	Price      float64 `json:"PRICE"`
	LastUpdate int64   `json:"LASTUPDATE"`
	Supply     float64 `json:"SUPPLY"`
	MarketCap  float64 `json:"MKTCAP"`
}
