package cryptocompareapi

import (
	"time"

	"bitbucket.org/pfzero/tradr/repo/model"
)

// TickReq represents the input needed for a tick request
type TickReq struct {
	From     string
	To       string
	Exchange string
}

// TickResp represents the response got from tick response
type TickResp struct {
	Time      time.Time
	Price     float64
	MarketCap float64
	Supply    float64
}

// HistoReq represents a request needed to be done in order to get
// histo pricing data for the given asset
type HistoReq struct {
	TickReq
	FromTime time.Time
	ToTime   time.Time
}

// HistoricalPrice represents a historical price
// response received from cryptocompare
type HistoricalPrice struct {
	Time       int64
	Price      float64
	Open       float64
	Close      float64
	High       float64
	Low        float64
	VolumeFrom float64
	VolumeTo   float64
}

// IsZero checks if the current historical price entry
// is zero
func (p *HistoricalPrice) IsZero() bool {
	return p.Close == 0 && p.High == 0 && p.Low == 0 && p.Open == 0 && p.VolumeFrom == 0 && p.VolumeTo == 0
}

// ParsedHistoricalPrice bla bla... will be refactored
type ParsedHistoricalPrice struct {
	HistoricalPrice
	Time time.Time
}

// AsModel converts the parsed historical price to a model
func (php *ParsedHistoricalPrice) AsModel() *model.CryptoPriceEntry {
	return &model.CryptoPriceEntry{
		Time:  php.Time,
		Price: php.Price,
	}
}

// GetPrice returns the price of the structure
func (php *ParsedHistoricalPrice) GetPrice() float64 {
	return php.Price
}

// GetTimestamp returns the timestamp of the parsed histo price
func (php *ParsedHistoricalPrice) GetTimestamp() time.Time {
	return php.Time
}
