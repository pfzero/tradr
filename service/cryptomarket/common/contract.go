package common

import (
	"context"
	"time"

	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"
)

// TickerReq represents the ticker request
type TickerReq struct {
	FSym string
	TSym string
}

// HistoReq represents the histo request
type HistoReq struct {
	TickerReq
	FromTime time.Time
	ToTime   time.Time
}

// ExchangeInfo represents the interface provided by
// each exchange api to collect real-time and historical
// data for that given exchange
type ExchangeInfo interface {
	GetTicker(ctx context.Context, r *TickerReq) (*AssetData, error)
	GetHistoData(ctx context.Context, r *HistoReq) (HistoResp, error)
	GetAllHistoData(ctx context.Context, r *TickerReq) (HistoResp, error)
	Status(ctx context.Context) error
}

// AssetData represents the ticker response given by exchanges
type AssetData struct {
	Time  time.Time
	Price float64
}

// AsRawModel converts the tick resp to a CryptoPriceEntry model
func (tr *AssetData) AsRawModel() *model.CryptoPriceEntry {
	return &model.CryptoPriceEntry{
		Time:  tr.Time,
		Price: tr.Price,
	}
}

// AsFullModel also adds data regarding the tracked coin
func (tr *AssetData) AsFullModel(trackedCoin *model.CryptoTrackedCoin) *model.CryptoPriceEntry {
	price := tr.AsRawModel()
	price.TrackedCoinID = trackedCoin.GetID()
	price.TrackedCoin = *trackedCoin
	return price
}

// GetPrice returns the price of the asset data
func (tr *AssetData) GetPrice() float64 {
	return tr.Price
}

// GetTimestamp returns the timestamp of the asset data
func (tr *AssetData) GetTimestamp() time.Time {
	return tr.Time
}

// HistoResp represents a list of AssetData
type HistoResp []AssetData

// AsRawModels transforms the histo response to a list of
// crypto price entry models
func (tr HistoResp) AsRawModels() []model.CryptoPriceEntry {
	toRet := make([]model.CryptoPriceEntry, len(tr))
	for i := 0; i < len(tr); i++ {
		toRet[i] = *(tr[i].AsRawModel())
	}
	return toRet
}

// AsFullModels transforms the list of asset data to a list of crypto price entries that also
// have assigned the reference to crypto tracked coin
func (tr HistoResp) AsFullModels(trackedCoin *model.CryptoTrackedCoin) []model.CryptoPriceEntry {
	toRet := make([]model.CryptoPriceEntry, len(tr))
	for i, ad := range tr {
		toRet[i] = *(ad.AsFullModel(trackedCoin))
	}
	return toRet
}

// ExchangeTrader represents a service that is able to
// place orders on an actual exchange
// Currently WIP
type ExchangeTrader interface {
	SuggestTrades(r *TickerReq) []cryptomarketutil.TradeRoute
	PlaceOrder(o *Order)
}

// Order represents an order that can be
// placed on an exchange; Still work in
// progress
type Order struct {
	BaseSymbol  string
	QuoteSymbol string
	PairSymbol  string
	Side        string // Buy, Sell
	Amount      string
	Type        string // Market, Limit, ...
}

// OrderFullResponse is the response got from the exchange
// after placing an order
type OrderFullResponse struct {
	// ... ToDO...
}
