package binancemarket

import (
	"fmt"
	"strings"
	"sync"
	"time"

	"github.com/sirupsen/logrus"

	"bitbucket.org/pfzero/tradr/logs"

	"bitbucket.org/pfzero/tradr/cfg"
	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"
	binance "github.com/pfzero/go-binance"
	"golang.org/x/time/rate"
)

const exchangeName = "Binance"

var (
	baseURL      string
	readsLimiter *rate.Limiter
	readsAPI     binance.Binance

	exchangeAliases           *cryptomarketutil.ExchangeAssetAlias
	exchangePreferredAssets   *cryptomarketutil.ExchangePreferredAssets
	exchangeTradeRoutes       *cryptomarketutil.ExchangeTradeRoutes
	fetchExchangeInfoInterval time.Duration

	lastExchangeInfoL   sync.Mutex
	lastExchangeInfo    *binance.ExchangeInfo
	lastExchangeInfoErr error
)

type binanceLogger struct {
}

func (bl *binanceLogger) Log(keyvals ...interface{}) error {

	fields := logrus.Fields{}

	for i := 0; i < len(keyvals); {
		k := keyvals[i]
		v := keyvals[i+1]
		i += 2
		fields[fmt.Sprintf("%s", k)] = fmt.Sprintf("%v", v)
	}

	logs.GetLogger().WithFields(fields).Println()
	return nil
}

// Initialize initializes the binance market configs
func Initialize(pa *cryptomarketutil.ExchangePreferredAssets, aliases *cryptomarketutil.ExchangeAssetAlias) error {
	readsLimit := cfg.GetConfig().GetFloat64("services.binance.rl_reqs")
	if readsLimit == 0 {
		return fmt.Errorf("Please setup the binance requests rate limit")
	}
	readsLimiter = rate.NewLimiter(rate.Limit(readsLimit), int(readsLimit))

	baseURL = cfg.GetConfig().GetString("services.binance.base")
	if baseURL == "" {
		return fmt.Errorf("Please setup the api base url to binance api")
	}

	svc := binance.NewAPIService(baseURL, "", nil, &binanceLogger{}, nil)
	readsAPI = binance.NewBinance(svc)

	interval := (cfg.GetConfig().GetDuration("services.binance.refresh_exchange_info_interval"))
	if interval == 0 {
		return fmt.Errorf("Please setup the refresh_exchange_info_interval for binance api")
	}

	fetchExchangeInfoInterval = (24 * time.Hour) / interval

	exchangeAliases = aliases
	exchangePreferredAssets = pa
	exchangeTradeRoutes = cryptomarketutil.NewExchangeTradeRoutes(exchangeName, aliases, pa)

	err := setupExchangeInfoRefresher()

	return err
}

func setupExchangeInfoRefresher() error {
	done := make(chan error)
	go func() {
		err := refreshExchangeInfo()
		// can't do nothing if the exchange throws error from the first
		// time
		done <- err
	}()

	err := <-done
	if err != nil {
		return err
	}

	go func() {
		for {
			time.Sleep(fetchExchangeInfoInterval)
			// don't really care about errors;
			// (temporarily)
			refreshExchangeInfo()
		}
	}()

	return err
}

func refreshExchangeInfo() error {
	r := readsLimiter.Reserve()
	time.Sleep(r.Delay())

	lastExchangeInfoL.Lock()
	defer lastExchangeInfoL.Unlock()

	info, err := readsAPI.ExchangeInfo()

	supportedPairs := make(map[string]map[string]string)

	if err != nil {
		exchangeTradeRoutes.SetPairs(supportedPairs)
		lastExchangeInfo = nil
		lastExchangeInfoErr = err
		return err
	}

	for _, sym := range info.Symbols {
		if strings.ToLower(sym.Status) != "trading" {
			continue
		}

		_, ok := supportedPairs[sym.BaseAsset]
		if !ok {
			supportedPairs[sym.BaseAsset] = make(map[string]string)
		}
		supportedPairs[sym.BaseAsset][sym.QuoteAsset] = sym.Symbol
	}

	exchangeTradeRoutes.SetPairs(supportedPairs)

	lastExchangeInfo = info
	lastExchangeInfoErr = nil

	return err
}
