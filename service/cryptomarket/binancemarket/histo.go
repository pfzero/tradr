package binancemarket

import (
	"context"
	"fmt"
	"time"

	"bitbucket.org/pfzero/tradr/service/cryptomarket/common"

	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"
	"github.com/pfzero/go-binance"
)

func convertTimeToBinanceTime(t time.Time) int64 {
	return t.UnixNano() / int64(time.Millisecond)
}

func getHistoData(ctx context.Context, symbol string, fromTime, toTime time.Time) ([]*binance.Kline, error) {
	lastFetchedTime := toTime
	accum := []*binance.Kline{}

	type tuple struct {
		partial []*binance.Kline
		err     error
	}

	doneCh := make(chan tuple, 1)

	for lastFetchedTime.After(fromTime) {
		ms := convertTimeToBinanceTime(lastFetchedTime)

		go func() {
			time.Sleep(readsLimiter.Reserve().Delay())
			partial, err := readsAPI.Klines(binance.KlinesRequest{
				Symbol:   symbol,
				EndTime:  ms,
				Interval: binance.Hour,
			})
			doneCh <- tuple{partial: partial, err: err}
		}()

		var currentTuple tuple
		select {
		case <-ctx.Done():
			// deplete the chan so it doesn't leak
			go func() { <-doneCh }()
			return nil, fmt.Errorf("Time exceeded for symbol %s", symbol)
		case currentTuple = <-doneCh:
		}

		partial, err := currentTuple.partial, currentTuple.err

		if err != nil {
			return nil, err
		}

		if len(partial) == 0 {
			break
		}

		lastFetchedTime = partial[0].OpenTime

		if len(accum) == 0 {
			accum = partial
			continue
		}

		firstStored := accum[0]
		lastFetched := partial[len(partial)-1]

		if lastFetched.OpenTime.Equal(firstStored.OpenTime) {
			partial = partial[:len(partial)-1]
		}

		if len(partial) == 0 {
			break
		}

		accum = append(partial, accum...)
	}

	sliceFrom := -1
	for i := 0; i < len(accum); i++ {
		current := accum[i]
		if current.OpenTime.Before(fromTime) {
			sliceFrom = i
		}
	}

	if sliceFrom > -1 {
		accum = accum[sliceFrom+1:]
	}

	return accum, nil
}

func convertToUtilPrices(histoPrices []*binance.Kline) []cryptomarketutil.Pricer {
	ret := make([]cryptomarketutil.Pricer, len(histoPrices))
	for i := 0; i < len(histoPrices); i++ {
		ret[i] = &common.AssetData{
			Price: histoPrices[i].Open,
			Time:  histoPrices[i].OpenTime,
		}
	}
	return ret
}
