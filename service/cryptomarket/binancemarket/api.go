package binancemarket

import (
	"context"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/pfzero/tradr/service/cryptomarket/common"
	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"
	"github.com/juju/errors"
	"github.com/pfzero/go-binance"
)

// Exchange is a wrapper on top of
// github.com/pfzero/go-binance and also provides
// rate limiting
type Exchange struct {
}

// GetTicker returns ticker data for a given pair; If the pair it's not tradeable
// on binance it tries to do cross-trading
func (bm *Exchange) GetTicker(ctx context.Context, r *common.TickerReq) (*common.AssetData, error) {
	var (
		asset *common.AssetData
		err   error
	)
	doneCh := make(chan struct{}, 1)

	go func() {
		asset, err = bm.getTicker(r)
		doneCh <- struct{}{}
	}()

	select {
	case <-ctx.Done():
		// deplete the request
		go func() { <-doneCh }()

		return nil, errors.Annotatef(ctx.Err(), "binance service: fetch %s%s", r.FSym, r.TSym)
	case <-doneCh:
		return asset, errors.Annotatef(err, "binance service: fetch %s%s", r.FSym, r.TSym)
	}
}

// GetHistoData returns histo data for the given pair
func (bm *Exchange) GetHistoData(ctx context.Context, r *common.HistoReq) (common.HistoResp, error) {
	tradeRoutes, err := exchangeTradeRoutes.GetExchangeTradeRoute(r.FSym, r.TSym)
	annotateErrFmt := fmt.Sprintf("binance service: fetch histo data for %s%s from: %s", r.FSym, r.TSym, r.FromTime.String())

	if err != nil {
		return nil, errors.Annotatef(err, annotateErrFmt)
	}

	pricePairs := make([][]cryptomarketutil.Pricer, len(tradeRoutes))

	for i := 0; i < len(tradeRoutes); i++ {
		route := tradeRoutes[i]

		routePrices, e := getHistoData(ctx, route.ExchangeSymbol, r.FromTime, r.ToTime)
		if e != nil {
			return nil, errors.Annotatef(e, annotateErrFmt)
		}

		pricePairs[i] = convertToUtilPrices(routePrices)
	}

	groupedPrices := cryptomarketutil.SyncPriceGroups(pricePairs)
	histoData := common.HistoResp{}

	for _, group := range groupedPrices {
		if len(group) != len(tradeRoutes) {
			continue
		}
		var lastTime time.Time
		pricedTradeRoute := make([]cryptomarketutil.PricedTradeRoute, len(group))
		for i, groupPrice := range group {
			pricedTradeRoute[i] = cryptomarketutil.PricedTradeRoute{
				TradeRoute: tradeRoutes[i],
				Price:      groupPrice.GetPrice(),
			}
			if groupPrice.GetTimestamp().After(lastTime) {
				lastTime = groupPrice.GetTimestamp()
			}
		}

		price := cryptomarketutil.GetPriceVia(pricedTradeRoute)
		histoData = append(histoData, common.AssetData{
			Price: price,
			Time:  lastTime,
		})
	}

	return histoData, nil
}

// GetAllHistoData is a shorthand for GetHistoData from when the coin appeared on binance to present
func (bm *Exchange) GetAllHistoData(ctx context.Context, r *common.TickerReq) (common.HistoResp, error) {
	return bm.GetHistoData(ctx, &common.HistoReq{
		TickerReq: *r,
		FromTime:  time.Time{},
		ToTime:    time.Now(),
	})
}

// Status returns the status of the binance exchange
func (bm *Exchange) Status(ctx context.Context) error {
	lastExchangeInfoL.Lock()
	defer lastExchangeInfoL.Unlock()

	if lastExchangeInfoErr != nil {
		return lastExchangeInfoErr
	}

	symbols := map[string]struct{}{"ETHBTC": struct{}{}, "BTCUSDT": struct{}{}, "ETHUSDT": struct{}{}, "XRPETH": struct{}{}}

	for _, sym := range lastExchangeInfo.Symbols {
		_, ok := symbols[sym.Symbol]
		if !ok {
			continue
		}
		if strings.ToLower(sym.Status) == "trading" {
			return nil
		}
	}

	return errors.Errorf("Neither of the pairs: %v are trading on binance", symbols)
}

func (bm *Exchange) getTicker(r *common.TickerReq) (*common.AssetData, error) {
	tradeRoutes, err := exchangeTradeRoutes.GetExchangeTradeRoute(r.FSym, r.TSym)
	if err != nil {
		return nil, err
	}

	lim := readsLimiter.ReserveN(time.Now(), len(tradeRoutes))
	d := lim.Delay()

	time.Sleep(d)

	pricedTradeRoutes := make([]cryptomarketutil.PricedTradeRoute, len(tradeRoutes))

	for i := 0; i < len(tradeRoutes); i++ {
		route := tradeRoutes[i]

		tickerData, err := readsAPI.Ticker24(binance.TickerRequest{Symbol: route.ExchangeSymbol})

		if err != nil {
			return nil, err
		}

		pricedTradeRoutes[i] = cryptomarketutil.PricedTradeRoute{
			TradeRoute: route,
			Price:      tickerData.LastPrice,
		}
	}

	price := cryptomarketutil.GetPriceVia(pricedTradeRoutes)

	return &common.AssetData{Price: price, Time: time.Now()}, nil
}
