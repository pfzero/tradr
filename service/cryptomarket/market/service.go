package market

import (
	"context"

	"bitbucket.org/pfzero/tradr/service/cryptomarket/common"
	"bitbucket.org/pfzero/tradr/service/cryptomarket/cryptocompareapi"
	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"
)

const exchange = "Market"

var (
	exchangeAliases *cryptomarketutil.ExchangeAssetAlias
)

// Initialize initializes the market exchange service
func Initialize(aliases *cryptomarketutil.ExchangeAssetAlias) error {
	exchangeAliases = aliases
	return nil
}

// Exchange is the cryptocurrencies market exchange implementation
// It provides ticker and historical informations regarding various coins
// on no specific exchange (ie. Market)
type Exchange struct {
}

// GetTicker returns ticker information between the 2 given coins at market
func (me *Exchange) GetTicker(ctx context.Context, r *common.TickerReq) (*common.AssetData, error) {
	p, e := cryptocompareapi.GetTicker(ctx, &cryptocompareapi.TickReq{
		Exchange: exchange,
		From:     exchangeAliases.GetAliasForAsset(exchange, r.FSym),
		To:       exchangeAliases.GetAliasForAsset(exchange, r.TSym),
	})

	if e != nil {
		return nil, e
	}

	return &common.AssetData{
		Price: p.Price,
		Time:  p.Time,
	}, nil
}

// GetHistoData returns the list of historical data based on the given request
func (me *Exchange) GetHistoData(ctx context.Context, r *common.HistoReq) (common.HistoResp, error) {
	histoReq := &cryptocompareapi.HistoReq{
		TickReq: cryptocompareapi.TickReq{
			Exchange: exchange,
			From:     exchangeAliases.GetAliasForAsset(exchange, r.FSym),
			To:       exchangeAliases.GetAliasForAsset(exchange, r.TSym),
		},
		FromTime: r.FromTime,
		ToTime:   r.ToTime,
	}

	list, e := cryptocompareapi.GetHistoPrice(ctx, histoReq)
	if e != nil {
		return nil, e
	}

	return me.convertToAssetData(list), nil
}

// GetAllHistoData returns the asset data from the beginning
func (me *Exchange) GetAllHistoData(ctx context.Context, r *common.TickerReq) (common.HistoResp, error) {
	req := &cryptocompareapi.TickReq{Exchange: exchange, From: r.FSym, To: r.TSym}
	list, e := cryptocompareapi.GetAllHistoData(ctx, req)
	if e != nil {
		return nil, e
	}
	return me.convertToAssetData(list), nil
}

// Status returns the status of the exchange
func (me *Exchange) Status(ctx context.Context) error {
	return cryptocompareapi.GetStatus(ctx)
}

func (me *Exchange) convertToAssetData(list []cryptocompareapi.ParsedHistoricalPrice) common.HistoResp {
	toRet := make(common.HistoResp, len(list))
	for i, elem := range list {
		toRet[i] = common.AssetData{
			Price: elem.Price,
			Time:  elem.Time,
		}
	}
	return toRet
}
