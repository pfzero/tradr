package cryptomarket

import (
	"context"
	"fmt"
	"sync"
	"time"

	"bitbucket.org/pfzero/tradr/service/cryptomarket/binancemarket"
	"bitbucket.org/pfzero/tradr/service/cryptomarket/common"
	"bitbucket.org/pfzero/tradr/service/cryptomarket/market"
	"bitbucket.org/pfzero/tradr/util/retry"
)

var providerOnce sync.Once
var instance *Provider

func unsupportedExchange(exchange string) error {
	return fmt.Errorf("Exchange %s is not supported", exchange)
}

// GetProvider return the cryptomarket api provider instance
func GetProvider() *Provider {
	providerOnce.Do(func() {

		instance = &Provider{
			readOnlyAPIs: map[string]common.ExchangeInfo{
				"Market":  &market.Exchange{},
				"Binance": &binancemarket.Exchange{},
			},
			tradeCapableAPIs: map[string]common.ExchangeTrader{},
		}
	})
	return instance
}

// Provider represents the central hub of cryptomarket
// apis
type Provider struct {
	readOnlyAPIs     map[string]common.ExchangeInfo
	tradeCapableAPIs map[string]common.ExchangeTrader
}

// GetTicker returns ticker info from the requested exchange
func (p *Provider) GetTicker(exchange string, r *common.TickerReq) (*common.AssetData, error) {
	e, ok := p.readOnlyAPIs[exchange]
	if !ok {
		return nil, unsupportedExchange(exchange)
	}

	var assetData *common.AssetData

	lastErr := retry.DoWithThrottle(func() error {
		ctx, cancel := p.getReadRTCtx()
		defer cancel()

		asset, e := e.GetTicker(ctx, r)

		if e != nil {
			return e
		}
		assetData = asset
		return nil
	}, cmCfg.readRetries, time.Millisecond)

	return assetData, lastErr
}

// GetHistoData returns the requested historical data for the given pair of symbols
// on the requested exchange
func (p *Provider) GetHistoData(exchange string, r *common.HistoReq) (common.HistoResp, error) {
	e, ok := p.readOnlyAPIs[exchange]
	if !ok {
		return nil, unsupportedExchange(exchange)
	}

	var assetHistoData common.HistoResp

	lastErr := retry.DoWithThrottle(func() error {
		ctx, cancel := p.getReadHistoCtx()
		defer cancel()
		resp, e := e.GetHistoData(ctx, r)
		if e != nil {
			return e
		}
		assetHistoData = resp
		return nil
	}, cmCfg.readRetries, time.Millisecond)

	return assetHistoData, lastErr
}

// GetAllHistoData returns all historical data for the given pair of symbols on the
// requested exchange
func (p *Provider) GetAllHistoData(exchange string, r *common.TickerReq) (common.HistoResp, error) {
	e, ok := p.readOnlyAPIs[exchange]
	if !ok {
		return nil, unsupportedExchange(exchange)
	}

	var assetHistoData common.HistoResp

	lastErr := retry.DoWithThrottle(func() error {
		ctx, cancel := p.getReadAllHistoCtx()
		defer cancel()

		resp, e := e.GetAllHistoData(ctx, r)

		if e != nil {
			return e
		}
		assetHistoData = resp

		return nil
	}, cmCfg.readRetries, time.Millisecond)

	return assetHistoData, lastErr
}

// GetExchangeStatus returns an error if the exchange server is unreachable,
// nil if the exchange is up-and-running
func (p *Provider) GetExchangeStatus(exchange string) error {
	e, ok := p.readOnlyAPIs[exchange]
	if !ok {
		return unsupportedExchange(exchange)
	}

	lastErr := retry.DoWithThrottle(func() error {
		ctx, cancel := p.getReadRTCtx()
		defer cancel()
		return e.Status(ctx)
	}, cmCfg.readRetries, time.Millisecond)

	return lastErr
}

// GetTrackableMarkets returns the map of exchanges that have support for
// market tracking
func (p *Provider) GetTrackableMarkets() map[string]common.ExchangeInfo {
	return p.readOnlyAPIs
}

// GetTradeSupportingMarkets returns the map of exchanges that support trading
// via api
func (p *Provider) GetTradeSupportingMarkets() map[string]common.ExchangeTrader {
	return p.tradeCapableAPIs
}

func (p *Provider) getReadRTCtx() (context.Context, context.CancelFunc) {
	return context.WithTimeout(context.Background(), cmCfg.readRealTimeDeadline)
}

func (p *Provider) getReadHistoCtx() (context.Context, context.CancelFunc) {
	return context.WithTimeout(context.Background(), cmCfg.readHistoDeadline)
}

func (p *Provider) getReadAllHistoCtx() (context.Context, context.CancelFunc) {
	return context.WithTimeout(context.Background(), cmCfg.readAllHistoDeadline)
}

func (p *Provider) getOrderCtx() (context.Context, context.CancelFunc) {
	return context.WithTimeout(context.Background(), cmCfg.orderDeadline)
}
