package exchange

// Cfg represents the fiat exchange
// configuration
type Cfg struct {
	Base        string
	BaseAsset   string
	AccessKey   string
	RefreshRate float64
}

// ServerResp represents the response from
// fixxr exchange server
type ServerResp struct {
	Base  string
	Rates map[string]float64
}

// Conversion represents a conversion result
type Conversion struct {
	From   string
	To     string
	Amount float64
	Result float64
}
