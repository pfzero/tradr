package exchange

import (
	"fmt"

	"strings"

	"bitbucket.org/pfzero/tradr/cfg"
)

func getDefaultExchangeRates() *ServerResp {
	var resp ServerResp
	config := cfg.GetConfig()
	defaultBase := config.GetString("exchange.base")
	resp.Base = defaultBase
	resp.Rates = make(map[string]float64)
	rates := config.GetStringMap("exchange.rates")
	for currency := range rates {
		value := config.GetFloat64(fmt.Sprintf("exchange.rates.%s", currency))
		resp.Rates[strings.ToUpper(currency)] = value
	}
	return &resp
}
