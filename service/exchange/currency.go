package exchange

import (
	"strings"
)

// CurrencyList represents a list of currencies
type CurrencyList []string

// Has returns true if the given currency exists in the
// current list of currencies
func (cl CurrencyList) Has(currency string) bool {
	for _, el := range cl {
		if strings.ToLower(el) == strings.ToLower(currency) {
			return true
		}
	}
	return false
}
