package exchange

import (
	"context"
	"errors"
	"fmt"
	"sync"
	"time"

	"bitbucket.org/pfzero/tradr/logs"

	"bitbucket.org/pfzero/tradr/cfg"
	"bitbucket.org/pfzero/tradr/service/request"
	"bitbucket.org/pfzero/tradr/util/retry"
)

var serviceConf *Cfg

var cachedServerRespL = &sync.Mutex{}
var cachedServerResp *ServerResp

var defaultRatesL = &sync.Mutex{}

// defaultRates are loaded from configuration file
var defaultRates *ServerResp

// Initialize initializes the exchange api
func Initialize() error {
	serviceConf = &Cfg{
		Base:        cfg.GetConfig().GetString("services.fiat_exchange.base"),
		BaseAsset:   cfg.GetConfig().GetString("services.fiat_exchange.base_asset"),
		AccessKey:   cfg.GetConfig().GetString("services.fiat_exchange.access_key"),
		RefreshRate: cfg.GetConfig().GetFloat64("services.fiat_exchange.refresh_rate"),
	}

	if serviceConf.Base == "" {
		return errors.New("Please setup the services.fiat_exchange.base key in config file")
	}
	if serviceConf.BaseAsset == "" {
		return errors.New("Please setup the services.fiat_exchange.base_asset key in config file")
	}
	if serviceConf.AccessKey == "" {
		return errors.New("Please setup the services.fiat_exchange.access_key key in config file")
	}
	if serviceConf.RefreshRate == 0 {
		return errors.New("Please setup the services.fiat_exchange.refresh_rate key in config file")
	}

	go startExchangeRatesRefresher()

	return nil
}

func startExchangeRatesRefresher() {
	ratiosBaseTemplate := serviceConf.Base + `/latest?base=%s`

	loadedRates := getDefaultExchangeRates()

	defaultRatesL.Lock()
	defaultRates = &(*loadedRates)
	defaultRatesL.Unlock()

	cachedServerRespL.Lock()
	cachedServerResp = &(*loadedRates)
	cachedServerRespL.Unlock()

	refreshCycle := float64(time.Hour) * serviceConf.RefreshRate

	for {
		time.Sleep(time.Duration(refreshCycle))
		var resp ServerResp
		err := retry.DoWithThrottle(func() error {
			err := request.Get(context.Background(), fmt.Sprintf(ratiosBaseTemplate, serviceConf.BaseAsset), &resp)
			return err
		}, 5, 500*time.Millisecond)
		if err != nil {
			logs.GetLogger().Warnln("cannot connect to live exchange api and will use the latest cached response")
			logs.GetLogger().Warnln("exchange api error was: %s\n", err.Error())
			continue
		}

		cp := resp

		logs.GetLogger().Info("Fiat Exchange Data Refreshed")

		cachedServerRespL.Lock()
		cachedServerResp = &cp
		cachedServerRespL.Unlock()
	}
}
