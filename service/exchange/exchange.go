package exchange

import (
	"fmt"

	"strings"
)

var currencyList CurrencyList

func changeBase(exchangeResp *ServerResp, newBase string) (*ServerResp, error) {
	// if same base, nothing to do
	if strings.ToLower(exchangeResp.Base) == strings.ToLower(newBase) {
		return exchangeResp, nil
	}

	// check if the new base is supported
	found := false
	for currency := range exchangeResp.Rates {
		if strings.ToLower(currency) == strings.ToLower(newBase) {
			found = true
			break
		}
	}

	// invalid base
	if !found {
		return nil, fmt.Errorf("Given base is invalid: %s", newBase)
	}

	// perform conversions
	return convertBase(exchangeResp, newBase), nil
}

func convertBase(exchangeResp *ServerResp, newBase string) *ServerResp {
	newBase = strings.ToLower(newBase)
	var ratio float64
	for currency, val := range exchangeResp.Rates {
		if strings.ToLower(currency) == newBase {
			ratio = 1 / val
			break
		}
	}
	newExchange := new(ServerResp)
	newExchange.Rates = make(map[string]float64)
	newExchange.Base = strings.ToUpper(newBase)
	newExchange.Rates[strings.ToUpper(exchangeResp.Base)] = ratio
	for currency, val := range exchangeResp.Rates {
		newExchange.Rates[strings.ToUpper(currency)] = val * ratio
	}
	return newExchange
}

// GetRates returns the rates of fiat currencies
func GetRates(base string) (*ServerResp, error) {
	var rates ServerResp
	cachedServerRespL.Lock()
	rates = *cachedServerResp
	cachedServerRespL.Unlock()

	return changeBase(&rates, base)
}

// GetCurrencies returns the list of fiat currencies
func GetCurrencies() CurrencyList {

	if len(currencyList) > 0 {
		return currencyList
	}

	defaultRatesL.Lock()
	r := defaultRates
	defaultRatesL.Unlock()

	for key := range r.Rates {
		currencyList = append(currencyList, key)
	}

	currencyList = append(currencyList, r.Base)

	return currencyList
}

// Convert converts between the 2 fiat currencies
func Convert(newConversion *Conversion) (*Conversion, error) {
	if newConversion.From == newConversion.To {
		newConversion.Result = newConversion.Amount
		return newConversion, nil
	}
	rates, err := GetRates(newConversion.From)
	if err != nil {
		return nil, err
	}

	conversionFound := false
	for currency, ratio := range rates.Rates {
		if strings.ToLower(currency) == strings.ToLower(newConversion.To) {
			newConversion.Result = ratio * newConversion.Amount
			conversionFound = true
			break
		}
	}

	if !conversionFound {
		return nil, fmt.Errorf("Currency %s not found", newConversion.To)
	}
	return newConversion, nil
}

// ConvertAmount is same as Convert but also applies the given amount
func ConvertAmount(from string, to string, amount float64) (*Conversion, error) {
	newConversion := &Conversion{From: from, To: to, Amount: amount}
	return Convert(newConversion)
}
