package request

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"time"

	"strings"

	"bitbucket.org/pfzero/tradr/cfg"
	"bitbucket.org/pfzero/tradr/logs"
	"github.com/sirupsen/logrus"
)

type reqDebug struct {
	req   *http.Request
	resp  *http.Response
	start time.Time
}

func (r *reqDebug) Start(req *http.Request) {
	if !cfg.DebugIO() {
		return
	}
	r.req = req
	r.start = time.Now()
	if !cfg.GetConfig().GetBool("record_io") {
		return
	}
	if req.Body != nil {
		req.Body = r.logBuf(req.Body, "Req Body: %s\n")
	}
}

func (r *reqDebug) Stop(resp *http.Response) {
	if !cfg.DebugIO() {
		return
	}

	if resp == nil {
		logs.GetLogger().Printf("Null response; time: %s", time.Now().Sub(r.start).String())
	} else {
		logs.GetLogger().Printf("%s %s :: %d :: %s", resp.Request.Method, resp.Request.URL.String(), resp.StatusCode, time.Now().Sub(r.start).String())
	}

	if !cfg.GetConfig().GetBool("record_io") {
		return
	}

	if resp != nil && resp.Body != nil {
		resp.Body = r.logBuf(resp.Body, "Resp Body: %s\n")
	}
}

func (r *reqDebug) logBuf(src io.Reader, format string, params ...interface{}) io.ReadCloser {
	buf, _ := ioutil.ReadAll(src)
	mime := http.DetectContentType(buf)
	cloneToReturn := ioutil.NopCloser(bytes.NewBuffer(buf))

	// check mime
	if !strings.Contains(mime, "text/plain") {
		buf = []byte(mime)
	}
	newParams := append(params, string(buf))
	logs.GetLogger().WithFields(logrus.Fields{
		"module": "request_helper",
		"action": "logging response buffer",
	}).Printf(format, newParams...)

	return cloneToReturn
}

// Get creates a get request to the given request and
// deserializes the response
func Get(ctx context.Context, url string, deserialize interface{}) error {
	req, err := http.NewRequest(http.MethodGet, url, nil)
	req = req.WithContext(ctx)

	if err != nil {
		return err
	}

	return MakeReq(req, deserialize)
}

// MakeReq initiates the given http request and deserializes the response into
// the given variable
func MakeReq(req *http.Request, deserialize interface{}) error {
	debug := &reqDebug{}
	debug.Start(req)
	client := &http.Client{}

	ctx := req.Context()
	if ctx != nil {
		deadline, ok := ctx.Deadline()
		if ok {
			client.Timeout = deadline.Sub(time.Now())
		}
	}

	resp, err := client.Do(req)

	debug.Stop(resp)

	if resp != nil && resp.Body != nil {
		defer resp.Body.Close()
	}

	if err != nil {
		return err
	}

	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		if resp.StatusCode == http.StatusNotFound {
			return errors.New("404 not found")
		}
		return fmt.Errorf("request error: %s", resp.Status)
	}

	if deserialize == nil {
		return nil
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	err = json.Unmarshal(body, deserialize)
	if err != nil {
		return err
	}
	return nil
}
