package mailer

import (
	"html/template"
)

// GetTradeNotifTpl returns the template needed for sending
// trade decision notifications
func GetTradeNotifTpl() (*template.Template, error) {
	t, err := template.ParseFiles("static/tpl/mail/trade_notif.tmpl.html")
	return t, err
}
