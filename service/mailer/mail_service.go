package mailer

import (
	"fmt"
	"net/mail"
	"net/smtp"
	"sync"
	"time"

	"golang.org/x/time/rate"

	"github.com/scorredoira/email"

	"github.com/pkg/errors"
)

var onceMailService sync.Once
var onceMailCfg sync.Once

var mailService *MailService
var mailCfg *MailServiceCfg
var mailRateLimiter *rate.Limiter

// MailServiceCfg is the configuration object
// needed by the MailService to function
type MailServiceCfg struct {
	FromName  string
	FromEmail string
	FromPwd   string
	SMTPAddr  string
	SMTPPort  string
	RateLimit float64
}

// Configure configures the mail service
func Configure(cfg *MailServiceCfg) {
	onceMailCfg.Do(func() {
		mailCfg = cfg

		burst := int(cfg.RateLimit / 2)
		if burst == 0 {
			burst = 1
		}
		mailRateLimiter = rate.NewLimiter(rate.Limit(cfg.RateLimit/2), burst)
	})
}

// MailService is a wrapper over email package
type MailService struct {
}

// GetInstance returns the instance of the mail service
func GetInstance() *MailService {
	onceMailService.Do(func() {
		mailService = &MailService{}
	})
	return mailService
}

// send sends a new email given parameters
func (ms *MailService) send(subject, body string, isHTML bool, recipients ...string) error {
	if mailCfg == nil {
		return errors.WithStack(errors.New("The MailerSerice isn't configured"))
	}
	if len(recipients) == 0 {
		return errors.New("MailerService: No recipients to send the email to")
	}
	var m *email.Message
	if isHTML {
		m = email.NewHTMLMessage(subject, body)
	} else {
		m = email.NewMessage(subject, body)
	}

	m.From = mail.Address{Name: mailCfg.FromName, Address: mailCfg.FromEmail}
	m.To = recipients

	auth := smtp.PlainAuth("", mailCfg.FromEmail, mailCfg.FromPwd, mailCfg.SMTPAddr)

	time.Sleep(mailRateLimiter.Reserve().Delay())

	err := email.Send(fmt.Sprintf("%s:%s", mailCfg.SMTPAddr, mailCfg.SMTPPort), auth, m)

	return err
}

// SendEmail sends an email
func (ms *MailService) SendEmail(subject, body string, recipients ...string) error {
	return ms.send(subject, body, false, recipients...)
}

// SendEmailHTML sends an email as html
func (ms *MailService) SendEmailHTML(subject, body string, recipients ...string) error {
	return ms.send(subject, body, true, recipients...)
}
