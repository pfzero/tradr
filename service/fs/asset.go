package fs

import (
	"io"
	"net/http"
	"os"
	"path/filepath"
)

// DownloadAsset downloads the asset found at the given url
// to the given destination path
func DownloadAsset(url string, destPath string) error {

	// recursively create path
	dirPath, _ := filepath.Split(destPath)
	pathErr := os.MkdirAll(dirPath, 0755)
	if pathErr != nil {
		if !os.IsExist(pathErr) {
			return pathErr
		}
	}

	// make request
	response, err := http.Get(url)
	if err != nil {
		return err
	}
	defer response.Body.Close()

	destinationf, err := os.Create(destPath)
	if err != nil {
		return err
	}
	defer destinationf.Close()

	_, err = io.Copy(destinationf, response.Body)
	if err == nil {
		err = os.Chmod(destPath, os.ModePerm)
	}
	return err
}
