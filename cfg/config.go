package cfg

import (
	"log"

	"github.com/spf13/viper"
)

var config *viper.Viper

// LoadConfig loads the configuration from config folder
func LoadConfig(configFile string) {

	if config != nil {
		log.Fatalf("config already loaded. Can't reload config from %s", configFile)
	}

	var err error
	v := viper.GetViper()

	v.AddConfigPath("static/cfg") // add static folder to config path
	v.AddConfigPath(".")          // add current folder to config path
	v.SetConfigFile(configFile)   // set config file name
	v.AutomaticEnv()              // read in environment variables that match
	err = v.ReadInConfig()

	// no configuration, then nothing to do;
	if err != nil {
		log.Fatalf("error on parsing configuration file: %s", err.Error())
	}
	config = v
}

// GetConfig returns the config object
func GetConfig() *viper.Viper {
	return config
}

// DebugDB returns wether the debug_db flag
func DebugDB() bool {
	return config.GetBool("debug_db")
}

// DebugIO returns the debug_io flag
func DebugIO() bool {
	return config.GetBool("debug_io")
}

// DebugWebServer returns teh debug_web flag
func DebugWebServer() bool {
	return config.GetBool("debug_web")
}
