// some useful js utils

// fetch histo data for symbol from binance api
async function fillAccum(symbol) {
  let accum = [];
  const getTime = () => {
    return accum.length > 0 ? accum[0][0] : new Date().getTime();
  };
  let batch = await (await fetch(
    `https://api.binance.com/api/v1/klines?symbol=${symbol}&interval=1h&endTime=${getTime()}`
  )).json();

  while (batch.length > 1) {
    batch = batch.slice(0, batch.length - 1);
    accum = batch.concat(accum);
    batch = await (await fetch(
      `https://api.binance.com/api/v1/klines?symbol=${symbol}&interval=1h&endTime=${getTime()}`
    )).json();
  }

  return accum;
}

// fetch fixxer rates for the day
async function getRates() {
  // to eur base
  let r = await fetch(
    "http://data.fixer.io/latest?base=eur&access_key=1dcacb83a566fed7b281c2e13e518253"
  );
  let jsoned = await r.json();
  let rates = jsoned.rates;
  let accum = "";
  Object.keys(jsoned.rates).forEach(fiatCurrency => {
    accum += `${fiatCurrency}: ${jsoned[fiatCurrency]}\n`;
  });
  return accum;
}

function genTrackedCoins(exchanges, tradeableAssets) {
  accum = [];
  for (let i = 0; i < tradeableAssets.length; i++) {
    let ob = tradeableAssets[i];
    if (ob.symbol.toLowerCase() == "usd") {
      continue;
    }

    for (let j = 0; j < exchanges.length; j++) {
      let exchange = exchanges[j];
      accum.push({
        name: `${ob.name} ${exchange.name}`,
        coin: ob,
        trackedExchange: exchange
      });
    }
  }
  return accum;
}
