# Tradr

Tradr is a platform where multiple trade bots can be developed, backtested and run.

# Main Modules

* Admin - admin interface
* Daemons - long-running procs
* Repo - models & various contracts
* Store - database communication services
* Trade - existing traders;
  * Each trader it's a go module;
  * Each module has a TraderBuilder (ie. it's responsible for fetching trader configuration from db and bootstrap the actual trader)
  * Each module contains the trader which implements repo/contracts/Trader interface
* TraderStore - a store that references all other specific TraderBuilder
  * It uses specific TraderBuilder to build the list of available traders for each specific type of trade bot, and then it caches those trade bots;
* Util - contains various tools for either storing trading-related informations or performing computations / analysis on the stream of market data;

# Data-Flow

## Bootstrap

1. Start all price trackers (eg. ETH-USD @Market, BTC-USD @Market) etc.;

* The configs for each tracked coin is stored in database `CryptoTrackedCoin`
* Each crypto tracked coin actually fetches the price at fixed interval (eg. 30s) but also stores the last market price at fixed interval (eg. 1 hour);

2. Start TradeDaemon;

* It takes a market snapshot (ie. all the tracked prices) from `CryptoMarketDaemon` - see point 1.)
* It creates a new context from this market snapshot;
  * A context contains the market snapshot + other tools for analyzing market evolution - eg. CandlestickList;
* It fetches all traders from `TraderStorer` via `GetAvailableTraders`;
* It runs each trader with the newly created context;
* If the trader took a decision, this decision will be a datastructure that will contain informations about the trades needed to be done;
  * If the trader returned a decision, then pass the decision to `TradeDecisionProcessor`
  * `TradeDecisionProcessor` performs the actual trades, notifies the trader's owner etc.
  * returns success / error;
  * After the `TradeDecision` successfully completed, the `TraderManager` informs the `Trader` on the success / error of the `TradeDecision`;
  * If successfull, the `Trader` will update its internal balance or do other stuff;

## Flow

1. `TraderManager` fetches market snapshot via `CryptoMarketDaemon` @fixed time interval - eg. 30s
2. `TraderManager` fetches the list of available `Trader`s via `TraderStorer`.
3. `TraderManager` creates the a new `TradeContext` from the given `MarketSnapshot`;
4. `TraderManager` runs each `Trader` (see pt. 2) and runs it with this `TradeContext`;
5. If a `Trader` returns a non-nil `TradeDecision`
   1. It processes the `TradeDecision` via `TradeDecisionProcessor`
   2. It informs the `Trader` on the success/error of processing the `TradeDecision`;
   3. `Trader` updates its internal balance based on success/error of the `TradeDecision` processing.

Besides the above simple flow, `Trader` can build their own `TradeDecisionPipeline` which is a generic tool for making decisions;

As an example of how can the `TradeDecisionPipeline` be used

```xml
<!-- for example assuming this configuration -->
<!-- Please note that at the moment xml configs aren't supported -->
<pipeline name="SpecialPipeline">
    <step>
        <or>
            <and>
                <price-variation>
                    <value>0.4</value>
                </price-variation>
                <trend-change>
                    <noise>
                        <up>0.07</up>
                        <down>-0.05</down>
                    </noise>
                </trend-change>
            </and>
            <candlestick>
                <threshold>
                    <up>0.9</up>
                </threshold>
                <noise>
                    <up>0.07</up>
                    <down>-0.05</down>
                </noise>
            </candlestick>
        </or>
    </step>
    <step>
        ...
    </step>
</pipeline>
```

In simple words, the above configuration tells: Create a decision pipeline that has 2 steps;

First step can be described as:

* If eitehr of the following is true:
  * Price varies with 0.4 (or increases with 40%) AND trend changes (where trend is defined by {0.07, -0.05} noise)
  * A semnificative candlestick is formed (ie. a candlestick that forms a price increase of 0.9 (90%))
* Then signal true;

```go
func MakePipeline() {
    step1 := CreateNewDecTree()
    fragment1 := tradedec.And(
        tradeDec.PriceVar(0.4),
        tradeDec.TrendChange(&Variation{
            Up: 0.07,
            Down: -0.05,
        }),
    )
    fragment2 := tradedec.CandlestickVar(0.9, &Variation{
        Up: 0.07,
        Down: -0.05,
    })
    step1.Or(fragment1, fragment2)

    pipeline := CreateNewDecPipeline()
    pipeline.AddStep(step1)
    return pipeline
}
```
