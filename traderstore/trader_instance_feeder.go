package traderstore

import (
	"bitbucket.org/pfzero/tradr/repo/contracts"
	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/repo/modelutil"
	"bitbucket.org/pfzero/tradr/store/datastore/cryptomarketstore"
)

// FeedTrader takes a trader and adds historical data to it as it happened
func FeedTrader(trader contracts.Trader) error {

	traderLedger := trader.GetLedger()
	traderStats := trader.GetStats()

	tradeDecisions, err := cryptomarketstore.GetTradeDecisionsByTrader(traderStats.GUID)
	if err != nil {
		return err
	}

	ledgerIOList, err := cryptomarketstore.GetLedgerIOByTrader(traderStats.GUID)
	if err != nil {
		return err
	}

	tradeDecisionIdx := 0
	userTransactionIdx := 0

	for {
		var currentTradeDecision *model.TradeDecision
		var currentLedgerIO *model.LedgerIO
		if tradeDecisionIdx < len(tradeDecisions) {
			currentTradeDecision = &tradeDecisions[tradeDecisionIdx]
		}
		if userTransactionIdx < len(ledgerIOList) {
			currentLedgerIO = &ledgerIOList[userTransactionIdx]
		}

		if currentTradeDecision == nil && currentLedgerIO == nil {
			break
		}

		// case when we have no more trade decisions left
		if currentTradeDecision == nil {
			traderLedger.AddLedgerIO(currentLedgerIO)
			userTransactionIdx++
			continue
		}

		// case when we have no more user transactions left
		if currentLedgerIO == nil {
			modelutil.AddTradeDecisionToLedger(currentTradeDecision, traderLedger)
			tradeDecisionIdx++
			continue
		}

		if currentTradeDecision.CreatedAt.Before(currentLedgerIO.CreatedAt) {
			modelutil.AddTradeDecisionToLedger(currentTradeDecision, traderLedger)
			tradeDecisionIdx++
			continue
		} else {
			traderLedger.AddLedgerIO(currentLedgerIO)
			userTransactionIdx++
			continue
		}
	}
	return nil
}
