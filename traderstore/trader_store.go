package traderstore

import (
	"fmt"
	"sync"
	"time"

	"github.com/sirupsen/logrus"

	"bitbucket.org/pfzero/tradr/logs"
	"bitbucket.org/pfzero/tradr/util/tradeutil"

	"bitbucket.org/pfzero/tradr/repo/contracts"
)

var traderStoreOnce sync.Once
var traderStore *TraderStore

// TraderStore represents the centralized store of all traders
// it also acts as a trader provider
type TraderStore struct {
	providers map[string]contracts.TraderStorer

	activeTradersL sync.Mutex
	activeTraders  []contracts.Trader

	allTradersL sync.Mutex
	allTraders  []contracts.Trader

	traderListenersL sync.Mutex
	traderListeners  map[string]string
}

// GetInstance returns the store singleton
func GetInstance() *TraderStore {
	traderStoreOnce.Do(func() {
		traderStore = &TraderStore{
			providers:       make(map[string]contracts.TraderStorer),
			activeTraders:   []contracts.Trader{},
			allTraders:      []contracts.Trader{},
			traderListeners: make(map[string]string),
		}
	})
	return traderStore
}

// RegisterProvider registers a new trader instance provider
func (ts *TraderStore) RegisterProvider(provider contracts.TraderStorer) error {
	name := provider.Name()
	if _, ok := ts.providers[name]; ok {
		return nil
	}
	ts.providers[name] = provider
	allTraders, err := provider.GetAllTraders()

	if err != nil {
		logs.GetLogger().WithFields(logrus.Fields{
			"module": "trader_store",
		}).Errorf(`Couldn't register traders for: %s. Error was: %s`, provider.Name(), err.Error())
	}

	if allTraders == nil || len(allTraders) == 0 {
		return nil
	}

	for i := 0; i < len(allTraders); i++ {
		err = ts.RegisterTraderInstance(allTraders[i])
	}
	return err
}

// RegisterTraderInstance registers a new trader provider
func (ts *TraderStore) RegisterTraderInstance(t contracts.Trader) error {
	err := FeedTrader(t)
	traderStats := t.GetStats()

	if err != nil {
		return err
	}

	ts.activeTradersL.Lock()
	defer ts.activeTradersL.Unlock()
	ts.traderListenersL.Lock()
	defer ts.traderListenersL.Unlock()
	ts.allTradersL.Lock()
	defer ts.allTradersL.Unlock()

	if traderStats.IsTraderActive() {
		ts.activeTraders = append(ts.activeTraders, t)
		_, ok := ts.traderListeners[traderStats.GUID]
		if !ok {
			ts.traderListeners[traderStats.GUID] = t.SubStateChange(makeTraderListener(ts))
		}
	}

	if traderStats.IsTraderActive() || traderStats.IsTraderIdle() {
		ts.allTraders = append(ts.allTraders, t)
	}

	return nil
}

// RemoveActiveTraderInstance removes the trader from the list
// of active traders
func (ts *TraderStore) RemoveActiveTraderInstance(guid string) {
	ts.activeTradersL.Lock()
	defer ts.activeTradersL.Unlock()
	ts.traderListenersL.Lock()
	defer ts.traderListenersL.Unlock()

	for i := 0; i < len(ts.activeTraders); i++ {
		t := ts.activeTraders[i]
		if t.GetStats().GUID != guid {
			continue
		}

		subID, ok := ts.traderListeners[guid]
		if ok {
			t.UnsubStateChange(subID)
		}

		ts.activeTraders = append(ts.activeTraders[:i], ts.activeTraders[i+1:]...)
		break
	}
}

// RemoveIdleTraderInstance removes the trader from the list
// containing all (idle) traders
func (ts *TraderStore) RemoveIdleTraderInstance(guid string) {
	ts.allTradersL.Lock()
	defer ts.allTradersL.Unlock()

	for i := 0; i < len(ts.allTraders); i++ {
		t := ts.allTraders[i]
		if t.GetStats().GUID != guid {
			continue
		}
		ts.allTraders = append(ts.allTraders[:i], ts.allTraders[i+1:]...)
		break
	}
}

// RemoveTraderInstance removes the given trader instance
func (ts *TraderStore) RemoveTraderInstance(guid string) {
	ts.RemoveActiveTraderInstance(guid)
	ts.RemoveIdleTraderInstance(guid)
}

// RefreshTraderInstance refreshes the given trader instance
func (ts *TraderStore) RefreshTraderInstance(traderGUID string) error {
	traderIDBuilder, err := tradeutil.NewTraderIDFromGUID(traderGUID)
	if err != nil {
		return err
	}

	provider, ok := ts.providers[traderIDBuilder.GetName()]

	if !ok {
		return fmt.Errorf(`The given provider name was not found: "%s"`, traderIDBuilder.GetName())
	}

	ts.RemoveTraderInstance(traderGUID)
	refreshed, err := provider.GetTraderByGUID(traderGUID)
	if err != nil {
		return err
	}
	ts.RegisterTraderInstance(refreshed)
	return nil
}

// GetTraderByGUID returns the given trader by guid
func (ts *TraderStore) GetTraderByGUID(guid string) (contracts.Trader, error) {
	ts.allTradersL.Lock()
	defer ts.allTradersL.Unlock()

	for i := 0; i < len(ts.allTraders); i++ {
		if ts.allTraders[i].GetStats().GUID == guid {
			return ts.allTraders[i], nil
		}
	}

	return nil, fmt.Errorf(`TraderStore: Trader with guid: "%s" was not found`, guid)
}

// GetTraderForBacktesting returns the given trader prepared for backtesting
func (ts *TraderStore) GetTraderForBacktesting(refCtxTime time.Time, guid string) (contracts.Trader, error) {
	traderIDBuilder, err := tradeutil.NewTraderIDFromGUID(guid)
	if err != nil {
		return nil, err
	}

	provider := ts.providers[traderIDBuilder.GetName()]
	if provider == nil {
		available := []string{}
		for name := range ts.providers {
			available = append(available, name)
		}
		return nil, fmt.Errorf("Provider with name: %s, was not found; Available: %v", traderIDBuilder.GetName(), available)
	}

	found, err := provider.GetTraderForBacktesting(refCtxTime, guid)
	if err != nil {
		return nil, err
	}
	err = FeedTrader(found)
	return found, err
}

// GetActiveTraders returns the list of active traders
func (ts *TraderStore) GetActiveTraders() ([]contracts.Trader, error) {
	return ts.activeTraders, nil
}

// GetAllTraders returns the complete list of traders
func (ts *TraderStore) GetAllTraders() ([]contracts.Trader, error) {
	return ts.allTraders, nil
}

// Name returns the name of this trader provider
func (ts *TraderStore) Name() string {
	return `Global Traders Store`
}

// GetAllTradersForExchange returns all the traders existing on the requested exchange
func (ts *TraderStore) GetAllTradersForExchange(eName string) ([]contracts.Trader, error) {
	allTraders, err := ts.GetAllTraders()
	if err != nil {
		return nil, err
	}
	exchangeTraders := []contracts.Trader{}
	for i := 0; i < len(allTraders); i++ {
		trader := allTraders[i]
		if trader.GetStats().Exchange != eName {
			continue
		}
		exchangeTraders = append(exchangeTraders, trader)
	}
	return exchangeTraders, nil
}

// GetActiveTradersForExchange returns the list of active traders for the requested exchange
func (ts *TraderStore) GetActiveTradersForExchange(eName string) ([]contracts.Trader, error) {
	activeTraders, err := ts.GetActiveTraders()
	if err != nil {
		return nil, err
	}
	exchangeActiveTraders := []contracts.Trader{}
	for i := 0; i < len(activeTraders); i++ {
		trader := activeTraders[i]
		if trader.GetStats().Exchange != eName {
			continue
		}
		exchangeActiveTraders = append(exchangeActiveTraders, trader)
	}

	return exchangeActiveTraders, nil
}

func makeTraderListener(ts *TraderStore) func(guid, oldState, newState string) {
	return func(guid, oldState, newState string) {
		// ToDO: Better use ts.RefreshTraderInstance(guid); but this
		// needs a refactoring so that RefreshTraderInstance accepts only
		// guid as parameter

		for i := 0; i < len(ts.activeTraders); i++ {
			t := ts.activeTraders[i]

			traderStats := t.GetStats()

			// check only the changed smart notifier
			if traderStats.GUID != guid {
				continue
			}

			// check if still active
			if traderStats.IsTraderActive() {
				break
			}

			// then the trader is not active anymore
			ts.RemoveActiveTraderInstance(guid)
		}

		for i := 0; i < len(ts.allTraders); i++ {
			t := ts.allTraders[i]

			traderStats := t.GetStats()

			// same. we are interested in the trader
			// with same guid
			if traderStats.GUID != guid {
				continue
			}

			// check if still idle
			if traderStats.IsTraderIdle() {
				break
			}

			ts.RemoveIdleTraderInstance(traderStats.GUID)
		}
	}
}
