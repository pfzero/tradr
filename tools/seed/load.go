package seed

import (
	"encoding/json"
	"io/ioutil"
	"os"

	"bitbucket.org/pfzero/tradr/logs"

	"path/filepath"
)

func readFileContents(file string) ([]byte, error) {
	fpath := filepath.Join(seedDir, file)

	logs.GetLogger().Infoln("loading seed: %s", fpath)

	f, err := os.Open(fpath)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	fbody, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}
	return fbody, nil
}

func load(seedFile string, dst interface{}) error {
	buf, err := readFileContents(seedFile)
	if err != nil {
		return err
	}
	err = json.Unmarshal(buf, &dst)
	return err
}
