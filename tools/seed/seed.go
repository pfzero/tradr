package seed

import (
	"fmt"

	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"

	"strings"

	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/store"
	"bitbucket.org/pfzero/tradr/store/datastore"
)

var seedDir string

// SetSeedDir sets the directory where the
// seed data that will be imported to database can
// be found
func SetSeedDir(dir string) {
	seedDir = dir
}

// AddToDB adds the seed data to database
func AddToDB() error {
	// first load all seed data
	roles := []model.Role{}
	users := []model.User{}
	cryptoExchanges := []model.CryptoExchange{}
	tradeableAssets := []model.TradeableAsset{}
	cryptoTrackedCoins := []model.CryptoTrackedCoin{}
	assetAliases := []model.ExchangeSymbolAlias{}
	preferredAssets := []model.ExchangePreferredAsset{}

	var loadErr error

	loadErr = load("roles.json", &roles)
	if loadErr != nil {
		return loadErr
	}
	loadErr = load("users.json", &users)
	if loadErr != nil {
		return loadErr
	}
	loadErr = load("crypto_exchanges.json", &cryptoExchanges)
	if loadErr != nil {
		return loadErr
	}
	loadErr = load("tradeable_assets.json", &tradeableAssets)
	if loadErr != nil {
		return loadErr
	}

	loadErr = load("crypto_tracked_coins.json", &cryptoTrackedCoins)
	if loadErr != nil {
		return loadErr
	}

	loadErr = load("exchange_asset_aliases.json", &assetAliases)
	if loadErr != nil {
		return loadErr
	}

	loadErr = load("exchange_preferred_assets.json", &preferredAssets)
	if loadErr != nil {
		return loadErr
	}

	// seed everything into a single tranzaction
	tx := store.DB.Begin()

	var saveErr error

	// add roles
	for k := range roles {
		role := &roles[k]
		exists, err := datastore.RecordExistsBy(role, "name", role.Name)

		// check for critical error
		if err != nil {
			tx.Rollback()
			return fmt.Errorf("error creating role: %s, error: %s", role.Name, err.Error())
		}
		if exists {
			err = datastore.PreloadRecordBy(role, "name", role.Name)
			if err != nil {
				tx.Rollback()
				return fmt.Errorf("error loading role %s from db, error: %s", role.Name, err.Error())
			}
			continue
		}

		_, saveErr = datastore.Create(tx, &roles[k])
		if saveErr != nil {
			tx.Rollback()
			return fmt.Errorf("error creating role: %s, error: %s", role.Name, saveErr.Error())
		}
	}

	// set the role for each user
	for k := range users {
		user := &users[k]
		// continue if user has no role
		if user.Role.Name == "" {
			continue
		}

		// search user's role
		for _, role := range roles {
			if strings.ToLower(user.Role.Name) == strings.ToLower(role.Name) {
				user.Role = role
				user.RoleID = role.GetID()
				break
			}
		}

		// rollback everything and return if the role doesn't match
		// our roles
		if user.RoleID == 0 {
			tx.Rollback()
			return fmt.Errorf(`the role %s associated with user %s is not available. Please check if the role exists in roles.json file`,
				user.Role.Name, user.Name)
		}

		bcryptPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
		if err != nil {
			tx.Rollback()
			return err
		}
		user.Password = string(bcryptPassword)
		user.Confirmed = true
	}

	// and now save users
	for k := range users {
		user := &users[k]

		// check for user
		if user.Email == "" {
			tx.Rollback()
			return fmt.Errorf("user has no email, which is mandatory: %v", user)
		}

		// check if exists
		exists, err := datastore.RecordExistsBy(user, "email", user.Email)
		if err != nil {
			tx.Rollback()
			return fmt.Errorf("error creating user: %s, error: %s", user.Email, err.Error())
		}

		if exists {
			continue
		}

		// save user
		saveErr = tx.Save(user).Error
		if saveErr != nil {
			tx.Rollback()
			return saveErr
		}
	}

	// add exchanges
	for idx := range cryptoExchanges {
		exchange := &cryptoExchanges[idx]

		exists, err := datastore.RecordExistsBy(exchange, "name", exchange.Name)

		if err != nil {
			tx.Rollback()
			return errors.Wrapf(err, "couldn't create exchange %s", exchange.Name)
		}

		if exists {
			continue
		}

		_, saveErr = datastore.Create(tx, exchange)
		if saveErr != nil {
			tx.Rollback()
			return errors.Wrapf(saveErr, "error creating exchange %s", exchange.Name)
		}
	}

	// add initial list of tradeable assets
	for idx := range tradeableAssets {
		asset := &tradeableAssets[idx]
		exists, err := datastore.RecordExistsBy(asset, "symbol", asset.Symbol)

		if err != nil {
			tx.Rollback()
			return errors.Wrapf(err, "couldn't check wether asset \"%s\" exists", asset.Symbol)
		}

		if exists {
			continue
		}

		_, saveErr = datastore.Create(tx, asset)
		if saveErr != nil {
			tx.Rollback()
			return errors.Wrapf(saveErr, "error creating asset %s::%s", asset.Name, asset.Symbol)
		}
	}

	for idx := range cryptoTrackedCoins {
		trackedCoin := &cryptoTrackedCoins[idx]
		exists, err := datastore.RecordExistsBy(trackedCoin, "name", trackedCoin.Name)

		if err != nil {
			tx.Rollback()
			return errors.Wrapf(err, "couldn't check if crypto tracked coin with name %s exists", trackedCoin.Name)
		}

		if exists {
			continue
		}

		var asset *model.TradeableAsset
		var exchange *model.CryptoExchange

		for i := range tradeableAssets {
			tradeableAsset := &tradeableAssets[i]
			if tradeableAsset.Symbol == trackedCoin.Coin.Symbol {
				asset = tradeableAsset
				break
			}
		}

		for i := range cryptoExchanges {
			cryptoExchange := &cryptoExchanges[i]
			if cryptoExchange.Name == trackedCoin.TrackedExchange.Name {
				exchange = cryptoExchange
				break
			}
		}

		if asset == nil || exchange == nil {
			tx.Rollback()
			return errors.Errorf("Couldn't find exchange or asset for tracked coin: %s; Asset Found: %v; Exchange Found: %v", trackedCoin.Name, asset, exchange)
		}

		trackedCoin.Coin = *asset
		trackedCoin.CoinID = asset.GetID()

		trackedCoin.TrackedExchange = *exchange
		trackedCoin.TrackedExchangeID = exchange.GetID()

		_, saveErr = datastore.Create(tx, trackedCoin)
		if saveErr != nil {
			tx.Rollback()
			return errors.Wrapf(saveErr, "couldn't create crypto tracked coin %s", trackedCoin.Name)
		}
	}

	for idx := range assetAliases {
		alias := &assetAliases[idx]

		var exchange *model.CryptoExchange

		for i := range cryptoExchanges {
			if cryptoExchanges[i].Name == alias.Exchange.Name {
				exchange = &cryptoExchanges[i]
				break
			}
		}

		if exchange == nil {
			tx.Rollback()
			return errors.Errorf("Couldn't fetch exchange %s from the list of exchanges; Aborting", alias.Exchange.Name)
		}

		alias.Exchange = *exchange
		alias.ExchangeID = exchange.GetID()

		_, saveErr = datastore.Create(tx, alias)
		if saveErr != nil {
			tx.Rollback()
			return errors.Wrapf(saveErr, "error creating alias %s:%s on exchange %s", alias.AssetSymbol, alias.AssetAlias, exchange.Name)
		}
	}

	for idx := range preferredAssets {
		preferredAsset := &preferredAssets[idx]

		var exchange *model.CryptoExchange
		var tradeableAsset *model.TradeableAsset

		for i := range cryptoExchanges {
			if cryptoExchanges[i].Name == preferredAsset.Exchange.Name {
				exchange = &cryptoExchanges[i]
				break
			}
		}

		for i := range tradeableAssets {
			if tradeableAssets[i].Symbol == preferredAsset.TradeableAsset.Symbol {
				tradeableAsset = &tradeableAssets[i]
				break
			}
		}

		if exchange == nil {
			tx.Rollback()
			return errors.Errorf("Couldn't find exchange %s in the list of exchanges", preferredAsset.Exchange.Name)
		}

		if tradeableAsset == nil {
			tx.Rollback()
			return errors.Errorf("Couldn't find tradeable asset %s:%s in the list of tradeable assets",
				preferredAsset.TradeableAsset.Name,
				preferredAsset.TradeableAsset.Symbol,
			)
		}

		preferredAsset.ExchangeID = exchange.GetID()
		preferredAsset.Exchange = *exchange
		preferredAsset.TradeableAssetID = tradeableAsset.GetID()
		preferredAsset.TradeableAsset = *tradeableAsset

		_, saveErr = datastore.Create(tx, preferredAsset)

		if saveErr != nil {
			tx.Rollback()
			return errors.Wrapf(saveErr, "couldn't serialize preferred asset: %s:%s",
				preferredAsset.TradeableAsset.Symbol,
				preferredAsset.Exchange.Name)
		}
	}

	// everything was successfull
	tx.Commit()

	return nil
}
