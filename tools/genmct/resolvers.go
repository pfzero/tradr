package genmct

import (
	"errors"
	"fmt"
	"strings"

	"bitbucket.org/pfzero/tradr/store"

	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/store/datastore"
	"bitbucket.org/pfzero/tradr/store/datastore/cryptomarketstore"
)

func resolveBeneficiary(name string) (*model.User, error) {
	foundUsers := []model.User{}
	err := datastore.SearchRecordsByKV(&foundUsers, "name", name)
	if err != nil {
		return nil, err
	}

	if len(foundUsers) == 0 {
		return nil, fmt.Errorf("No users with name %s found", name)
	}

	return &foundUsers[0], nil
}

func resolveExchange(exchangeName string) (*model.CryptoExchange, error) {
	foundExchanges := []model.CryptoExchange{}
	err := datastore.SearchRecordsByKV(&foundExchanges, "name", exchangeName)
	if err != nil {
		return nil, err
	}
	if len(foundExchanges) == 0 {
		return nil, fmt.Errorf("No exchanges found with name %s", exchangeName)
	}
	return &foundExchanges[0], nil
}

func getPortfolioByAssetName(assetSymbol string) (*model.Portfolio, error) {
	name := fmt.Sprintf("%s portfolio", strings.ToLower(assetSymbol))
	return cryptomarketstore.GetPortfolioByName(name)
}

func resolvePortfolio(assetSymbol string) (*model.Portfolio, error) {
	resolvedPortfolio, err := getPortfolioByAssetName(assetSymbol)
	if err == nil && resolvedPortfolio != nil {
		return resolvedPortfolio, nil
	}

	foundAsset, err := cryptomarketstore.GetTradeableAssetBySymbol(assetSymbol)
	if err != nil {
		return nil, err
	}

	portfolio := &model.Portfolio{Name: fmt.Sprintf("%s Portfolio", strings.ToUpper(assetSymbol)), PortfolioAssets: []model.PortfolioAsset{model.PortfolioAsset{
		TradeableAsset:   *foundAsset,
		TradeableAssetID: foundAsset.GetID(),
	}}}

	err = store.DB.Save(portfolio).Error
	if err != nil {
		return nil, err
	}

	return portfolio, nil
}

const tradeTrendStepsCollectionName = "All In"

func getTrendTradeSteps() (*model.TradeTrendStepCol, error) {
	foundStepCollections := []model.TradeTrendStepCol{}
	err := datastore.SearchRecordsByKV(&foundStepCollections, "name", tradeTrendStepsCollectionName)
	if err != nil {
		return nil, err
	}
	if len(foundStepCollections) == 0 {
		return nil, errors.New("Not found")
	}
	return &foundStepCollections[0], nil
}

func resolveTrendTradeSteps() (*model.TradeTrendStepCol, error) {
	found, err := getTrendTradeSteps()
	if err == nil {
		return found, nil
	}

	toCreate := &model.TradeTrendStepCol{
		Name: tradeTrendStepsCollectionName,
		TradeTrendSteps: []model.TradeTrendStep{
			model.TradeTrendStep{SameTrendLevel: 1, AmountPercentage: 1},
		},
	}

	err = store.DB.Save(toCreate).Error
	if err != nil {
		return nil, err
	}

	return toCreate, nil
}
