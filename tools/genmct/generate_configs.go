package genmct

import (
	"regexp"

	"bitbucket.org/pfzero/tradr/repo/model"
)

var linesReg = regexp.MustCompile(`\n+`)

// GenerateConfigs takes a string of configurations separated by newlines
// and generates multi-coin-traders
func GenerateConfigs(rawConfigLines string) ([]model.MultiCoinTradeCfg, error) {
	strConfigs := linesReg.Split(rawConfigLines, 5000)
	configLines := []string{}

	for _, lineStr := range strConfigs {
		l := mcConfigRegex.ReplaceAllString(lineStr, " ")
		if l == " " {
			continue
		}
		configLines = append(configLines, l)
	}

	createdTraders := []model.MultiCoinTradeCfg{}
	for i := 0; i < len(configLines); i++ {
		configLine := configLines[i]
		parsed, err := makeConfigFromStr(configLine)
		if err != nil {
			return createdTraders, err
		}
		trader, err := parsed.makeTrader()
		if err != nil {
			return createdTraders, err
		}
		createdTraders = append(createdTraders, *trader)
	}

	return createdTraders, nil
}
