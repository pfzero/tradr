package genmct

import (
	"errors"
	"fmt"
	"math"
	"regexp"
	"strconv"
	"strings"

	"bitbucket.org/pfzero/tradr/repo/modelenum"

	"bitbucket.org/pfzero/tradr/trade/strategies/multicointrade"

	"bitbucket.org/pfzero/tradr/store/datastore/cryptomarketstore"

	"bitbucket.org/pfzero/tradr/store"

	"bitbucket.org/pfzero/tradr/repo/model"
)

const valuesCount = 11

const (
	fsym int = iota
	tsym
	beneficiary
	exchange
	invAmt
	invTrendFormation
	invTrendCandle
	invTrendChange
	profitPVar
	profitTrendChange
)

var mcConfigRegex = regexp.MustCompile(`\s+`)

// invSignalCfg represents the investment signal
// configuration
type invSignalCfg struct {
	TrendFormation float64
	TrendCandle    float64
	TrendChange    float64
}

// profitSignalCfg represents the profit signal cfg
type profitSignalCfg struct {
	PriceVariation float64
	TrendChange    float64
}

// mctConfig represents the minimal multi-coin-trader configuration
type mctConfig struct {
	BeneficiaryName string
	FSym            string
	TSym            string
	ExchangeName    string
	Notify          bool
	AutoTrade       bool
	MinFiatAmount   float64
	InvAmount       float64
	InvSignalCfg    invSignalCfg
	ProfitSignalCfg profitSignalCfg
}

func (mc *mctConfig) getPairName() string {
	return fmt.Sprintf("%s-%s", strings.ToUpper(mc.FSym), strings.ToUpper(mc.TSym))
}

func (mc *mctConfig) makeInvCond() *model.TradeCondition {
	cond := (&model.TradeCondition{}).SetName(fmt.Sprintf("%s Inv", mc.getPairName()))

	trendFormation := -math.Abs(mc.InvSignalCfg.TrendFormation)
	trendCandle := -math.Abs(mc.InvSignalCfg.TrendCandle)
	trendChange := math.Abs(mc.InvSignalCfg.TrendChange)

	if trendFormation != 0 {
		cond.WaitTrendFormation(trendFormation)
	}

	if trendCandle != 0 {
		cond.WaitForCandle(trendCandle)
	}

	if trendChange != 0 {
		cond.WaitTrendChange().SetNoise(trendChange)
	}

	return cond
}

func (mc *mctConfig) makeCdCond() *model.TradeCondition {
	cond := (&model.TradeCondition{}).SetName(fmt.Sprintf("%s Cd", mc.getPairName()))

	invTrendFormation := math.Abs(mc.InvSignalCfg.TrendFormation)
	invTrendCandle := math.Abs(mc.InvSignalCfg.TrendCandle)
	profitPVar := math.Abs(mc.ProfitSignalCfg.PriceVariation)

	inv := math.Max(invTrendFormation, invTrendCandle)

	cond.SetSymmetricPriceVariation(math.Max(inv, profitPVar))

	return cond
}

func (mc *mctConfig) makeProfitCond() *model.TradeCondition {
	cond := (&model.TradeCondition{}).SetName(fmt.Sprintf("%s Profit", mc.getPairName()))
	profitPVar := math.Abs(mc.ProfitSignalCfg.PriceVariation)
	cond.SetPriceVariation(profitPVar)
	if mc.ProfitSignalCfg.TrendChange != 0 {
		cond.WaitTrendChange().SetNoise(mc.ProfitSignalCfg.TrendChange)
	}
	return cond
}

func (mc *mctConfig) getValidationError() error {
	if mc.FSym == "" || mc.TSym == "" || mc.ExchangeName == "" || mc.BeneficiaryName == "" {
		return errors.New("FSym, TSym, ExchangeName and BeneficiaryName are mandatory")
	}

	if mc.InvAmount == 0 {
		return errors.New("Investment Amount is mandatory")
	}

	if mc.InvSignalCfg.TrendCandle == 0 && mc.InvSignalCfg.TrendFormation == 0 {
		return errors.New("Investment Signal cannot have both trend candle and trend formation set to 0")
	}

	if mc.ProfitSignalCfg.PriceVariation == 0 && mc.ProfitSignalCfg.TrendChange == 0 {
		return errors.New("Profit signal cannot have both price variation and trend change set to 0")
	}

	return nil
}

// genTrader converts from minimal configuration to actual multi coin trade
// configuration
func (mc *mctConfig) genTrader() (*model.MultiCoinTradeCfg, error) {
	var err error
	basePortfolio, err := resolvePortfolio(mc.FSym)
	if err != nil {
		return nil, err
	}

	targetPortfolio, err := resolvePortfolio(mc.TSym)
	if err != nil {
		return nil, err
	}

	beneficiary, err := resolveBeneficiary(mc.BeneficiaryName)

	if err != nil {
		return nil, err
	}

	exchange, err := resolveExchange(mc.ExchangeName)
	if err != nil {
		return nil, err
	}

	tradeTrendStepsCollection, err := resolveTrendTradeSteps()
	if err != nil {
		return nil, err
	}

	return &model.MultiCoinTradeCfg{
		AutoTrade:            mc.AutoTrade,
		Notify:               mc.Notify,
		BasePortfolio:        *basePortfolio,
		BasePortfolioID:      basePortfolio.GetID(),
		TargetPortfolio:      *targetPortfolio,
		TargetPortfolioID:    targetPortfolio.GetID(),
		Beneficiary:          *beneficiary,
		BeneficiaryID:        beneficiary.GetID(),
		Exchange:             *exchange,
		ExchangeID:           exchange.GetID(),
		InvCondition:         *mc.makeInvCond(),
		InvCooldownCondition: *mc.makeCdCond(),
		ProfitCondition:      *mc.makeProfitCond(),
		TradeTrendStepCol:    *tradeTrendStepsCollection,
		MinimumFiatAmount:    mc.MinFiatAmount,
		State:                modelenum.TradeBotNewState.String(),
	}, nil
}

func (mc *mctConfig) makeTrader() (*model.MultiCoinTradeCfg, error) {

	mainAsset, err := cryptomarketstore.GetTradeableAssetBySymbol(mc.FSym)
	if err != nil {
		return nil, err
	}

	exchange, err := resolveExchange(mc.ExchangeName)
	if err != nil {
		return nil, err
	}

	owner, err := resolveBeneficiary(mc.BeneficiaryName)
	if err != nil {
		return nil, err
	}

	trader, err := mc.genTrader()
	if err != nil {
		return nil, err
	}

	tx := store.DB.Begin()

	err = tx.Save(trader).Error

	if err != nil {
		return nil, err
	}

	traderIdentifier := multicointrade.GetTraderIdentifier()

	ledgerIO := &model.LedgerIO{
		Amount:     mc.InvAmount,
		Asset:      *mainAsset,
		AssetID:    mainAsset.GetID(),
		Exchange:   *exchange,
		ExchangeID: exchange.GetID(),
		Owner:      *owner,
		OwnerID:    owner.GetID(),
		Type:       modelenum.Deposit.String(),
		TraderGUID: traderIdentifier.GetTraderInstanceGUID(trader.GetID()),
	}

	err = tx.Save(ledgerIO).Error

	if err != nil {
		return nil, err
	}

	err = tx.Commit().Error
	if err != nil {
		return nil, err
	}

	return trader, nil
}

// makeConfigFromStr returns the parsed string as configruation
func makeConfigFromStr(stringifiedCfg string) (*mctConfig, error) {
	split := mcConfigRegex.Split(stringifiedCfg, valuesCount)
	config := &mctConfig{}

	config.FSym = split[fsym]
	config.TSym = split[tsym]
	config.AutoTrade = false
	config.Notify = true
	config.BeneficiaryName = split[beneficiary]
	config.ExchangeName = split[exchange]
	config.MinFiatAmount = 20

	invAmount, err := strconv.ParseFloat(split[invAmt], 64)
	if err != nil {
		return nil, err
	}

	config.InvAmount = invAmount

	invSignalTrendFormed, err := strconv.ParseFloat(split[invTrendFormation], 64)
	if err != nil {
		return nil, err
	}
	invSignalTrendCandle, err := strconv.ParseFloat(split[invTrendCandle], 64)
	if err != nil {
		return nil, err
	}
	invSignalTrendChange, err := strconv.ParseFloat(split[invTrendChange], 64)
	if err != nil {
		return nil, err
	}
	profitSignalPriceVar, err := strconv.ParseFloat(split[profitPVar], 64)
	if err != nil {
		return nil, err
	}

	profitSignalTrendChange, err := strconv.ParseFloat(split[profitTrendChange], 64)
	if err != nil {
		return nil, err
	}

	config.InvSignalCfg = invSignalCfg{TrendFormation: invSignalTrendFormed, TrendCandle: invSignalTrendCandle, TrendChange: invSignalTrendChange}
	config.ProfitSignalCfg = profitSignalCfg{PriceVariation: profitSignalPriceVar, TrendChange: profitSignalTrendChange}

	return config, config.getValidationError()
}
