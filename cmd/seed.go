// Copyright © 2017 Erhan Abibula <erhan.abibula@gmail.com>
//

package cmd

import (
	"path/filepath"

	"bitbucket.org/pfzero/tradr/boot"
	"bitbucket.org/pfzero/tradr/logs"
	"bitbucket.org/pfzero/tradr/tools/seed"
	"github.com/spf13/cobra"
)

// seedCmd represents the seed command
var seedCmd = &cobra.Command{
	Use:   "seed",
	Short: "Seed database with initial values",
	Long: `Seeds a database with initial values right
after it was created.

It will add to database resources like: Currency, Estate Types,
Roles, Users etc.`,
	PreRun: func(cmd *cobra.Command, arg []string) {
		boot.InitializeDB()
		boot.InitializeServices()
	},
	Run: func(cmd *cobra.Command, args []string) {
		seed.SetSeedDir(cmd.Flag("dir").Value.String())
		err := seed.AddToDB()
		if err != nil {
			logs.GetLogger().Warnln("Couldn't seed the database. Everything was aborted")
			logs.GetLogger().Fatalf(err.Error())
		}

		logs.GetLogger().Infof("Database seeded successfully. Now you can start the web server as usual.")
		logs.GetLogger().Infof("Run tradr serve --help for more details")
	},

	PostRun: func(cmd *cobra.Command, args []string) {
		boot.CloseDB()
	},
}

func init() {
	// add command to root cmd
	RootCmd.AddCommand(seedCmd)

	// set default seed dir
	seedDir, _ := filepath.Abs("./static/seed")

	// register flag
	seedCmd.Flags().StringP("dir", "d", seedDir, "use this directory to load seed data. Take static/seed as example")
}
