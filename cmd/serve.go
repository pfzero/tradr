// Copyright © 2017 Erhan Abibula <erhan.abibula@gmail.com>
//

package cmd

import (
	"fmt"
	"math"
	"time"

	"bitbucket.org/pfzero/tradr/repo/modelenum"

	"bitbucket.org/pfzero/tradr/repo/model"

	"bitbucket.org/pfzero/tradr/backtest/mcmontecarlo"
	"bitbucket.org/pfzero/tradr/backtest/traderbacktest"

	"bitbucket.org/pfzero/tradr/logs"
	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"

	"bitbucket.org/pfzero/tradr/store/datastore/cryptomarketstore"

	"bitbucket.org/pfzero/tradr/cfg"

	"runtime"

	"bitbucket.org/pfzero/tradr/boot"
	"github.com/pkg/profile"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var procs int

var cpuProf interface{ Stop() }
var memProf interface{ Stop() }

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Start the web server",
	Long: `serve starts the web server by reading the configuration file
from either provided --config flag or default config file.
	`,
	PreRun: func(cmd *cobra.Command, args []string) {

		cpuFlag := cfg.GetConfig().GetBool("cpu_profile")
		memFlag := cfg.GetConfig().GetBool("mem_profile")

		if cpuFlag {
			cpuProf = profile.Start(profile.CPUProfile, profile.ProfilePath("."))
		} else if memFlag {
			memProf = profile.Start(profile.MemProfile, profile.ProfilePath("."))
		}

		boot.InitializeDB()
		boot.InitializeServices()
		boot.InitializeAdmin()
		boot.RunServices()
		boot.RegisterTraders()

		logs.GetLogger().Infoln("All Services initialized")
	},
	Run: func(cmd *cobra.Command, args []string) {
		if cpuProf != nil {
			defer cpuProf.Stop()
		}
		if memProf != nil {
			defer memProf.Stop()
		}

		// compute number of procs to use
		if runtime.NumCPU() < procs {
			procs = runtime.NumCPU()
		}

		// set the number of parallel cpus to use
		runtime.GOMAXPROCS(procs)

		// testMcMonteCarlo()

		// printCandles("ETH", "XEM", "Market", 0, -0.1)
		// testSimpleMCT()

		boot.ServeWeb()
	},
	PostRun: func(cmd *cobra.Command, args []string) {
		boot.CloseDB()
	},
}

func testMcMonteCarlo() {
	cfg := &mcmontecarlo.BuilderConfiguration{
		FromTime:                time.Date(2018, time.January, 1, 0, 0, 0, 0, time.Local),
		ToTime:                  time.Date(2018, time.July, 15, 0, 0, 0, 0, time.Local),
		InitialFiatAmount:       1000,
		UseWindowedTime:         true,
		WindowedTimeMonthsCount: 3,
		BasePortfolio: model.Portfolio{
			PortfolioAssets: []model.PortfolioAsset{
				model.PortfolioAsset{TradeableAsset: model.TradeableAsset{Symbol: "BTC", Name: "Bitcoin"}},
			},
		},
		TargetPortfolio: model.Portfolio{
			PortfolioAssets: []model.PortfolioAsset{
				model.PortfolioAsset{TradeableAsset: model.TradeableAsset{Symbol: "BCH", Name: "Bch"}},
			},
		},
		Exchange: model.CryptoExchange{
			Name: "Market",
		},
		InvestmentSignal: mcmontecarlo.InvSignalCfg{
			TrendFormedMin: -0.5,
			TrendFormedMax: -0.5,
		},
		ProfitSignal: mcmontecarlo.ProfitSignalCfg{
			PriceVarMin:    0.35,
			PriceVarMax:    0.35,
			TrendChangeMin: 0.05,
			TrendChangeMax: 0.05,
		},
		RangeConfigurations: mcmontecarlo.RangeCfg{
			PriceVariationRange: 0.05,
			TrendCandleRange:    0.05,
			TrendChangeRange:    0.05,
			TrendFormedRange:    0.05,
		},
	}

	sim, err := mcmontecarlo.NewMultiCoinMonteCarloSimulator(cfg)
	if err != nil {
		panic(err)
	}

	sim.Next()
}

func testSimpleMCT() {
	fromTime := time.Date(2018, time.April, 1, 0, 0, 0, 0, time.Local)
	toTime := time.Date(2018, time.July, 1, 0, 0, 0, 0, time.Local)

	traderCfg := &traderbacktest.SimpleMultiCoinCfg{
		BasePortfolio: model.Portfolio{
			PortfolioAssets: []model.PortfolioAsset{
				model.PortfolioAsset{TradeableAsset: model.TradeableAsset{Symbol: "XEM"}},
			},
		},
		TargetPortfolio: model.Portfolio{
			PortfolioAssets: []model.PortfolioAsset{
				model.PortfolioAsset{TradeableAsset: model.TradeableAsset{Symbol: "ETH"}},
			},
		},
		Exchange: model.CryptoExchange{Name: "Binance"},
		InvestSignal: traderbacktest.InvestSignalCfg{
			TrendFormed: -0.1,
		},
		ProfitSignal: traderbacktest.ProfitSignalCfg{
			PriceVariation: 0.1,
			TrendChange:    0.1,
		},
		InvestAmount: traderbacktest.InvestmentAmountCfg{
			InvestmentPercent: 1,
			MaxInvestments:    1,
		},
		MinTradeFiatAmount: 20,
	}

	trader, err := traderCfg.BuildMultiCoinTrader()
	if err != nil {
		panic(err)
	}

	trader.GetLedger().AddLedgerIO(&model.LedgerIO{
		Identity: model.Identity{
			CreatedAt: fromTime,
			UpdatedAt: fromTime,
		},
		Amount:   1000,
		Asset:    model.TradeableAsset{Symbol: "XEM"},
		Exchange: model.CryptoExchange{Name: "Binance"},
		Type:     modelenum.Deposit.String(),
	})

	traderbacktest.Run(trader, fromTime, toTime, false, false)
}

func printCandles(fsym, tsym, exchange string, up, down float64) {

	var histoPrices = []cryptomarketutil.Pricer{}

	if fsym == "USD" {
		fsymPrices, e := cryptomarketstore.GetPriceHistoricalBySymbolAndExchange(fsym, exchange, time.Time{}, time.Now())
		if e != nil {
			panic(e)
		}
		for i := range fsymPrices {
			p := &fsymPrices[i]
			histoPrices = append(histoPrices, p)
		}
	}

	if tsym == "USD" {
		tsymPrices, e := cryptomarketstore.GetPriceHistoricalBySymbolAndExchange(tsym, exchange, time.Time{}, time.Now())
		if e != nil {
			panic(e)
		}
		for i := range tsymPrices {
			p := &tsymPrices[i]
			histoPrices = append(histoPrices, &cryptomarketutil.SimplePrice{
				Price:     1 / p.GetPrice(),
				Timestamp: p.GetTimestamp(),
			})
		}
	}

	if fsym != "USD" && tsym != "USD" {
		convertedPrices, err := cryptomarketstore.GetHistoPriceConversion(fsym, tsym, exchange, time.Time{}, time.Now())
		if err != nil {
			panic(err)
		}
		histoPrices = convertedPrices
	}

	cStickList := cryptomarketutil.NewCandlestickList(cryptomarketutil.Variation{
		Up:   up,
		Down: down,
	}, math.MaxInt32)

	for i := range histoPrices {
		histoPrice := histoPrices[i]
		cStickList.AddPrice(histoPrice)
	}

	candles := cStickList.GetCandlesticks()

	for i := range candles {
		candle := candles[i]
		fmt.Printf("Time: %s, Price: %f \n", candle.PriceFrom.GetTimestamp().Format("2006-01-02 15:04"), candle.PriceFrom.GetPrice())
	}
	lastCandle := candles[len(candles)-1]
	fmt.Printf("Time: %s, Price: %f \n", lastCandle.PriceTo.GetTimestamp().Format("2006-01-02 15:04"), lastCandle.PriceTo.GetPrice())
}

func init() {
	RootCmd.AddCommand(serveCmd)

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	serveCmd.PersistentFlags().Bool("debug-io", false, "enable http I/O requests debugging (default false)")
	serveCmd.PersistentFlags().Bool("record-io", false, "enable http I/O requests recording")
	serveCmd.PersistentFlags().Bool("debug-web", false, "enable http server debugging (default false)")
	serveCmd.PersistentFlags().IntVar(&procs, "procs", 1, fmt.Sprintf("number of procs to use - max %d on this machine", runtime.NumCPU()))
	serveCmd.PersistentFlags().Bool("enable-access-log", false, "Enable request access level debugging")
	serveCmd.PersistentFlags().Bool("prod", false, "Enable prod mode. Will use compiled assets")
	serveCmd.PersistentFlags().Bool("cpu-profile", false, "Enable cpu profiling. (default false)")
	serveCmd.PersistentFlags().Bool("mem-profile", false, "Enable cpu profiling. (default false)")

	viper.BindPFlag("debug_io", serveCmd.PersistentFlags().Lookup("debug-io"))
	viper.BindPFlag("record_io", serveCmd.PersistentFlags().Lookup("record-io"))
	viper.BindPFlag("debug_web", serveCmd.PersistentFlags().Lookup("debug-web"))
	viper.BindPFlag("enable_access_log", serveCmd.PersistentFlags().Lookup("enable-access-log"))
	viper.BindPFlag("prod", serveCmd.PersistentFlags().Lookup("prod"))
	viper.BindPFlag("cpu_profile", serveCmd.PersistentFlags().Lookup("cpu-profile"))
	viper.BindPFlag("mem_profile", serveCmd.PersistentFlags().Lookup("mem-profile"))
}
