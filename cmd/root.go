// Copyright © 2017 Erhan Abibula<erhan.abibula@gmail.com>

package cmd

import (
	"fmt"
	"os"
	"runtime"

	"bitbucket.org/pfzero/tradr/boot"
	"bitbucket.org/pfzero/tradr/cfg"
	"bitbucket.org/pfzero/tradr/logs"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string

// RootCmd represents the base command when called without any subcommands
var RootCmd = &cobra.Command{
	Use:   "tradr",
	Short: "Latest version of tradr admin",
	Long: `This program is used to run the latest admin interface
and the api service with go programming language.

This CLI program is used to either start the web server or to 
seed an existing database with core data so that the app can be
usable.

Please note that the importance of configuration variables is:
Environment Variables > CLI Flags > Config File
	`,
	Run: func(cmd *cobra.Command, args []string) {
		qorJobID := cfg.GetConfig().GetInt("qor-job")
		if qorJobID == -1 {
			cmd.Help()
			return
		}

		// allocate 1 cpu only when running jobs
		runtime.GOMAXPROCS(1)

		boot.InitializeDB()
		boot.InitializeServices()
		// boot.RunServices()
		boot.RegisterTraders()
		boot.InitializeAdmin()

	},

	PostRun: func(cmd *cobra.Command, args []string) {
		qorJobID := cfg.GetConfig().GetInt("qor-job")
		if qorJobID == -1 {
			return
		}
		boot.CloseDB()
	},
}

// Execute adds all child commands to the root command sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(-1)
	}
}

func init() {
	// initialization hook
	cobra.OnInitialize(func() {
		if cfgFile == "" {
			err := fmt.Errorf("config file wasn't set. Please specify configuration file via --config flag")
			panic(err)
		}

		// setup configuration
		cfg.LoadConfig(cfgFile)

		err := logs.Initialize()

		if err != nil {
			panic(err)
		}
	})

	RootCmd.PersistentFlags().StringVar(&cfgFile, "config", "static/cfg/development.yml", "config file")
	RootCmd.PersistentFlags().Bool("debug-db", false, "enable database query debugging (default false)")
	RootCmd.PersistentFlags().IntP("qor-job", "j", -1, "qor job id to run")
	viper.BindPFlag("qor-job", RootCmd.PersistentFlags().Lookup("qor-job"))
	viper.BindPFlag("debug_db", RootCmd.PersistentFlags().Lookup("debug-db"))
}
