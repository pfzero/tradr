// Copyright © 2017 Erhan Abibula <erhan.abibula@gmail.com>
//

package cmd

import (
	"net/http"
	"os"

	"fmt"

	"bitbucket.org/pfzero/tradr/admin/bindatafs"
	"bitbucket.org/pfzero/tradr/admin/config"
	"bitbucket.org/pfzero/tradr/boot"
	"github.com/spf13/cobra"
)

// buildfsCmd represents the buildfs command
var buildfsCmd = &cobra.Command{
	Use:   "buildfs",
	Short: "Build application's static files into binary",
	Long:  ``,
	PreRun: func(cmd *cobra.Command, args []string) {
		boot.InitializeDB()
		boot.InitializeAdmin()
		boot.InitializeServices()
	},
	Run: func(cmd *cobra.Command, args []string) {
		mux := http.NewServeMux()
		// dummy mount, just to load templates
		config.Admin.MountTo("/", mux)
		compileErr := bindatafs.AssetFS.Compile()
		if compileErr != nil {
			fmt.Println(compileErr)
			os.Exit(1)
		}
		fmt.Println("build complete")
	},
	PostRun: func(cmd *cobra.Command, args []string) {
		boot.CloseDB()
	},
}

func init() {
	RootCmd.AddCommand(buildfsCmd)
}
