package monitor

import (
	"fmt"
	"sync"
	"time"

	"bitbucket.org/pfzero/tradr/traderstore"

	"bitbucket.org/pfzero/tradr/cfg"

	"bitbucket.org/pfzero/tradr/logs"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"bitbucket.org/pfzero/tradr/daemon/marketdaemon"
)

var (
	monitorOnce            sync.Once
	monitorDaemon          *DaemonMonitor
	storeTelemetryInterval time.Duration
)

// GetInstance returns the instance of daemon
// monitor
func GetInstance() *DaemonMonitor {
	return monitorDaemon
}

// Initialize initializes the daemon monitor
func Initialize() error {
	var err error

	monitorOnce.Do(func() {
		storeTelemetryInterval = cfg.GetConfig().GetDuration("daemons.monitor.store_telemetry_data_interval") * time.Minute
		if storeTelemetryInterval == 0 {
			err = fmt.Errorf("Please consider configuring the daemons.monitor.store_telemetry_data_interval field")
		}
		monitorDaemon = &DaemonMonitor{}
	})

	return err
}

// DaemonMonitor represents daemon monitoring
type DaemonMonitor struct {
}

// Start starts the daemon monitor
func (dm *DaemonMonitor) Start() error {
	dm.monitExchangeStatuses()
	dm.monitMarketChanges()
	dm.monitTraders()

	return nil
}

func (dm *DaemonMonitor) monitMarketChanges() {
	sub := marketdaemon.GetInstance().SubMarketSnapshot()

	go func() {
		for {
			snap := <-sub.C

			for eName, data := range snap {
				if data.HistoDataModified {
					logs.GetLogger().WithFields(logrus.Fields{
						"module":  "monitor_daemon",
						"subject": fmt.Sprintf("Exchange %s", eName),
					}).Infof("Exchange %s has modified historical data", eName)
				}

				if data.PricesSerialized {
					logs.GetLogger().WithFields(logrus.Fields{
						"module":  "monitor_daemon",
						"subject": fmt.Sprintf("Exchange %s", eName),
					}).Infof("Exchange %s has serialized the current snapshot", eName)
				}
			}
		}
	}()
}

func (dm *DaemonMonitor) monitExchangeStatuses() {
	sub := marketdaemon.GetInstance().SubMarketSnapshot()
	ticker := time.NewTicker(storeTelemetryInterval)

	go func() {
		for {
			<-ticker.C

			snap := <-sub.C

			for eName, data := range snap {
				switch data.Telemetry.Status {
				case marketdaemon.ExchangeStatusOnline:
					logs.GetLogger().WithFields(logrus.Fields{
						"module": "daemon_monitor",
						"status": data.Telemetry.Status,
					}).Infof("Exchange %s is online from %s", eName, data.Telemetry.StatusFrom.String())
				case marketdaemon.ExchangeStatusOffline:
					if data.Telemetry.LastErr == nil {
						logs.GetLogger().WithFields(logrus.Fields{
							"module":      "daemon_monitor",
							"status":      data.Telemetry.Status,
							"description": errors.New("Last Err was unknown"),
						}).Errorf("Exchange %s is offline from %s", eName, data.Telemetry.StatusFrom.String())
					} else {
						logs.GetLogger().WithFields(logrus.Fields{
							"module":      "daemon_monitor",
							"status":      data.Telemetry.Status,
							"description": fmt.Errorf("Last Err was: %s", data.Telemetry.LastErr.Error()),
						}).Errorf("Exchange %s is offline from %s", eName, data.Telemetry.StatusFrom.String())
					}
				}
			}
		}
	}()
}

func (dm *DaemonMonitor) monitTraders() {
	ticker := time.NewTicker(10 * time.Minute)
	go func() {
		for {
			<-ticker.C
			traders, err := traderstore.GetInstance().GetAllTraders()
			if err != nil {
				logs.GetLogger().WithFields(logrus.Fields{
					"module":          "daemon_monitor",
					"error":           "Couldn't get list of traders from trader store",
					"err_description": err,
				}).Error("Couldn't take traders from trader store")
				continue
			}
			logs.GetLogger().WithFields(logrus.Fields{
				"module":  "daemon_monitor",
				"type":    "traders_monitor",
				"message": fmt.Sprintf("There are currently %d traders registered to trader store", len(traders)),
			}).Infof("There are %d traders", len(traders))

			activeTraders, err := traderstore.GetInstance().GetActiveTraders()
			if err != nil {
				logs.GetLogger().WithFields(logrus.Fields{
					"module":          "daemon_monitor",
					"error":           "Couldn't get list of active traders from trader store",
					"err_description": err,
				}).Error("Couldn't take active traders from trader store")
				continue
			}
			logs.GetLogger().WithFields(logrus.Fields{
				"module":  "daemon_monitor",
				"type":    "traders_monitor",
				"message": fmt.Sprintf("There are currently %d active traders registered to trader store", len(activeTraders)),
			}).Infof("There are %d active traders", len(activeTraders))
		}
	}()
}
