package tickdaemon

import (
	"errors"
	"fmt"
	"sync"
	"time"

	"bitbucket.org/pfzero/tradr/cfg"
	"bitbucket.org/pfzero/tradr/util/tick"
)

var tickersInitOnce sync.Once
var ticksDaemon *TicksDaemon

var (
	// RealTimeInterval is the configured real-time interval
	// of the system
	RealTimeInterval time.Duration
	// StoreInterval is the configured store-time interval of the
	// system
	StoreInterval time.Duration
)

// Initialize initializes the daemon
func Initialize() error {
	c := cfg.GetConfig()

	RealTimeInterval = c.GetDuration("daemons.tickers.real_time") * time.Second
	StoreInterval = c.GetDuration("daemons.tickers.store_interval") * time.Hour

	if RealTimeInterval == 0 {
		return fmt.Errorf("Please set daemons.tickers.real_time configuration;")
	}
	if StoreInterval == 0 {
		return errors.New("Please set daemons.tickers.store_time configuration")
	}
	if StoreInterval < time.Hour {
		return fmt.Errorf("StoreInterval cannot be smaller than 1 hour; Currently set to %s", StoreInterval)
	}

	return nil
}

// GetInstance returns the ticks_daemon instance
func GetInstance() *TicksDaemon {
	tickersInitOnce.Do(func() {

		delta := GetTimeAtFixedHourAfter(1).Sub(time.Now())

		ticksDaemon = &TicksDaemon{
			RealTime:   tick.NewFanOutTicker(RealTimeInterval),
			StoreTime:  tick.NewScheduledFanOutTicker(delta, StoreInterval),
			framedTime: make(chan time.Time),
		}
	})
	return ticksDaemon
}

// GetStartOfCurrentHourTime returns the time at the beginning of current hour
// if time.now == 2018-05-17 15:49 => 2018-05-17 15:00
func GetStartOfCurrentHourTime() time.Time {
	now := time.Now()
	startHour := time.Date(now.Year(), now.Month(), now.Day(), now.Hour(), 0, 0, 0, time.Local)
	return startHour
}

// GetTimeAtFixedHourAfter returns the time at the beginning of next ${hoursCnt} hours
// if time.now == 2018-05-17 15:49; GetTimeAtFixedHourAfter(2) => 2018-05-17 17:00
func GetTimeAtFixedHourAfter(hoursCnt int) time.Time {
	return GetStartOfCurrentHourTime().Add(time.Duration(hoursCnt) * time.Hour)
}

// TicksDaemon contains the main tickers that daemons use
// in order to fetch new market data / run traders / serialize
// market data / etc...
type TicksDaemon struct {
	RealTime  *tick.FanOutTicker
	StoreTime *tick.FanOutTicker

	framedTime chan time.Time
}

func (td *TicksDaemon) genSameTimeInFrame(frame time.Duration) {
	go func() {
		now := time.Now()

		for {
			select {
			case td.framedTime <- now:
			case <-time.NewTimer(frame).C:
				now = time.Now()
			}
		}
	}()
}

// GetFillHistoTime returns the time until the histo data should
// be filled; It is used to sync a number of separate threads that
// can access this within a frame of time
func (td *TicksDaemon) GetFillHistoTime() time.Time {
	return <-td.framedTime
}

// Start starts the ticks daemon
func (td *TicksDaemon) Start() error {
	// pick a frameSize sufficiently large that supports
	// burst calls - usually this is our case
	// also, not larger than the
	// exchange_status_check_interval flag
	frameSize := time.Minute
	go td.genSameTimeInFrame(frameSize)
	return nil
}
