package daemon

import (
	"fmt"

	"bitbucket.org/pfzero/tradr/daemon/monitor"
	"bitbucket.org/pfzero/tradr/daemon/tickdaemon"

	"bitbucket.org/pfzero/tradr/daemon/marketdaemon"
	"bitbucket.org/pfzero/tradr/daemon/tradedaemon"
	"bitbucket.org/pfzero/tradr/traderstore"
)

// InitDaemons initializes and boots the daemons
func InitDaemons() {
	err := tickdaemon.Initialize()
	if err != nil {
		panic(err)
	}

	err = marketdaemon.Initialize()
	if err != nil {
		panic(err)
	}

	err = tradedaemon.Initialize()
	if err != nil {
		panic(err)
	}

	err = monitor.Initialize()
	if err != nil {
		panic(err)
	}
}

// StartDaemons starts all the daemons
func StartDaemons() {
	err := tickdaemon.GetInstance().Start()
	if err != nil {
		panic(err)
	}

	// run the price tracking daemon
	marketDaemonErr := marketdaemon.GetInstance().Start()
	if marketDaemonErr != nil {
		panic(marketDaemonErr)
	}

	// run the trader daemon
	traderStore := traderstore.GetInstance()
	tradedaemon.GetTradersManager().RegisterTraderStore(traderStore)

	tradeDaemonErr := tradedaemon.GetTradersManager().Start()
	if tradeDaemonErr != nil {
		panic(fmt.Errorf("Couldn't start tradedaemon. Error was: %s", tradeDaemonErr.Error()))
	}

	monitor.GetInstance().Start()
}
