package tradehandler

import (
	"bytes"
	"sync"

	"bitbucket.org/pfzero/tradr/logs"
	"github.com/sirupsen/logrus"

	"bitbucket.org/pfzero/tradr/service/mailer"

	"bitbucket.org/pfzero/tradr/repo/contracts"
	"bitbucket.org/pfzero/tradr/repo/modelenum"

	"bitbucket.org/pfzero/tradr/store"
)

var tradeDecisionOnce sync.Once
var decisionProcInstance *TradeDecisionProcessor

// TradeDecisionProcessor is the datastructure responsible
// for processing trade decisions
// ToDO: Needs refactoring
type TradeDecisionProcessor struct {
}

func newTradeDecisionProcessor() *TradeDecisionProcessor {
	return &TradeDecisionProcessor{}
}

// GetTradeDecisionProcessor returns the trade decision instance
func GetTradeDecisionProcessor() *TradeDecisionProcessor {
	tradeDecisionOnce.Do(func() {
		decisionProcInstance = newTradeDecisionProcessor()
	})
	return decisionProcInstance
}

func (tdp *TradeDecisionProcessor) handleNewTradeDecision(resp *contracts.TraderResponse) (*contracts.TraderResponse, error) {
	if resp.Decision.Notify {
		resp.Decision.NotifyStatus = modelenum.PendingNotif.String()
	} else {
		resp.Decision.NotifyStatus = modelenum.CompletedNotif.String()
	}

	for i := 0; i < len(resp.Decision.Trades); i++ {
		trade := &resp.Decision.Trades[i]
		if resp.Decision.AutoTrade {
			trade.Status = modelenum.InProgressTrade.String()
		} else {
			trade.Status = modelenum.NewTrade.String()
		}
	}

	err := store.DB.Save(resp.Decision).Error
	return resp, err
}

func (tdp *TradeDecisionProcessor) processTradeNotif(resp *contracts.TraderResponse) (*contracts.TraderResponse, error) {
	if !resp.Decision.Notify || resp.Decision.NotifyStatus == modelenum.CompletedNotif.String() {
		return resp, nil
	}

	tpl, e := mailer.GetTradeNotifTpl()
	if e != nil {
		return resp, e
	}

	buf := bytes.NewBufferString("")
	e = tpl.Execute(buf, resp)
	if e != nil {
		return resp, e
	}

	e = mailer.GetInstance().SendEmailHTML("New Trade Decision Occured", buf.String(), resp.Decision.Beneficiary.Email)
	if e != nil {
		resp.Decision.NotifyStatus = modelenum.ErroredNotif.String()
		store.DB.Save(resp.Decision)
		return resp, e
	}

	resp.Decision.NotifyStatus = modelenum.CompletedNotif.String()
	e = store.DB.Save(resp.Decision).Error
	return resp, e
}

func (tdp *TradeDecisionProcessor) processTrades(resp *contracts.TraderResponse) (*contracts.TraderResponse, error) {
	// Not Implemented yet...
	return resp, nil
}

func (tdp *TradeDecisionProcessor) processTradeDecision(resp *contracts.TraderResponse) (*contracts.TraderResponse, error) {
	var err error
	if resp.Decision.Notify {
		resp, err = tdp.processTradeNotif(resp)
		if err != nil {
			logs.GetLogger().WithFields(logrus.Fields{
				"module": "tradedaemon.tradehandler.trade_dec_processor.process_trade_decision",
			}).Error(err)
		}
	}

	if resp.Decision.AutoTrade {
		resp, err = tdp.processTrades(resp)
		if err != nil {
			return resp, err
		}
	}

	return resp, nil
}

func (tdp *TradeDecisionProcessor) postProcessDecision(resp *contracts.TraderResponse) (*contracts.TraderResponse, error) {
	// ToDO: implement this after trade decision notification was sent and
	// after the trades were made
	return resp, nil
}

// HandleTradeDecision must handle the trade decisions
func (tdp *TradeDecisionProcessor) HandleTradeDecision(resp *contracts.TraderResponse) (*contracts.TraderResponse, error) {

	decision, e := tdp.handleNewTradeDecision(resp)

	if e != nil {
		return decision, e
	}

	processedDecision, err := tdp.processTradeDecision(decision)

	if err != nil {
		return processedDecision, err
	}

	postProcessedDecision, err := tdp.postProcessDecision(processedDecision)

	return postProcessedDecision, nil
}
