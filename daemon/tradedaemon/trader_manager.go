package tradedaemon

import (
	"sync"
	"time"

	"github.com/sirupsen/logrus"

	"bitbucket.org/pfzero/tradr/repo/model"

	"bitbucket.org/pfzero/tradr/repo/modelutil"

	"bitbucket.org/pfzero/tradr/logs"

	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"

	"bitbucket.org/pfzero/tradr/daemon/marketdaemon"
	"bitbucket.org/pfzero/tradr/daemon/tradedaemon/tradehandler"
	"bitbucket.org/pfzero/tradr/repo/contracts"
	"bitbucket.org/pfzero/tradr/util/tradeutil"
)

var (
	onceTradeManager       sync.Once
	tradersManagerInstance *TradersManager
)

// GetTradersManager returns the singleton instance of
// the TradeManager
func GetTradersManager() *TradersManager {
	onceTradeManager.Do(func() {
		tradersManagerInstance = newTradersManager()
	})
	return tradersManagerInstance
}

// newTradeManager represents the TraderManger constructor
func newTradersManager() *TradersManager {

	tm := &TradersManager{
		contextManagers: make(map[string]*tradeutil.TradeContextManager),
		stopSignal:      make(chan struct{}),
	}
	return tm
}

// TradersManager represents the datastructure
// responsible for storing the existing traders
// and also processing their decisions
type TradersManager struct {
	traderStore     contracts.TraderStorer
	contextManagers map[string]*tradeutil.TradeContextManager

	stopSignal chan struct{}
}

// RegisterTraderStore registers the given trader store
func (tm *TradersManager) RegisterTraderStore(store contracts.TraderStorer) {
	tm.traderStore = store
}

// Start starts the trade daemon
func (tm *TradersManager) Start() error {

	for _, eName := range tradeDaemonCfg.trackableMarkets {
		contextMgr := tradeutil.NewTradeContextManagerWithCleanup(
			tradeDaemonCfg.candlesticksLim,
			tradeDaemonCfg.candleClaimCycleDuration,
			tradeDaemonCfg.fiatCurrencies...,
		)
		contextMgr.SetSnapsLim(tradeDaemonCfg.marketSnapsHeld)
		tm.contextManagers[eName] = contextMgr
	}

	start := time.Now().Add(-time.Duration(time.Hour * time.Duration(tradeDaemonCfg.marketSnapsHeld)))

	for eName, tradeCtxMgr := range tm.contextManagers {
		histoMarketSnaps, err := marketdaemon.GetInstance().GetHistoMarketSnapshots(eName, start, time.Now())
		if err != nil {
			return err
		}

		tradeCtxMgr.FeedHistoMarketSnaps(histoMarketSnaps)
	}

	// 1. Listen to pricetracking to create pricing buckets
	// 2. Make trader context
	go tm.startTrading()

	return nil
}

// GetContextManager returns the context manager for the given exchange
func (tm *TradersManager) GetContextManager(exchange string) *tradeutil.TradeContextManager {
	return tm.contextManagers[exchange]
}

// startTrading listens to the stream of price buckets
// creates trading context and then passes the context
// to the procedure responsible for managing traders
func (tm *TradersManager) startTrading() {
	s := marketdaemon.GetInstance().SubMarketSnapshot()

	var ms map[string]marketdaemon.ExchangeSnapshotTickData

tradeLoop:
	for {
		select {
		case ms = <-s.C:
		case <-tm.stopSignal:
			marketdaemon.GetInstance().UnsubMarketSnapshot(s)
			break tradeLoop
		}

		tradeContexts := tm.prepareTradingContexts(ms)
		chs := make(chan struct{}, len(tradeContexts))

		for eName := range tradeContexts {
			go func(eName string) {
				tm.handleTraders(eName, tradeContexts[eName])
				chs <- struct{}{}
			}(eName)
		}

		// drain the responses before going to the next tick
		for i := 0; i < len(tradeContexts); i++ {
			<-chs
		}
	}
}

func (tm *TradersManager) prepareTradingContexts(mtd map[string]marketdaemon.ExchangeSnapshotTickData) map[string]*tradeutil.TradeContext {
	contexts := make(map[string]*tradeutil.TradeContext)

	for eName, tickData := range mtd {
		if tickData.Telemetry.Status == marketdaemon.ExchangeStatusOffline {
			continue
		}

		if tickData.HistoDataModified {
			tm.reloadExchangeHistoData(eName)
		}

		ms := cryptomarketutil.NewMarketSnapshot()
		prices := tickData.Prices

		for sym := range prices {
			ms.Set(sym, eName, prices[sym])
		}

		ctxMgr, ok := tm.contextManagers[eName]
		if !ok {
			continue
		}

		var ctx *tradeutil.TradeContext
		if !tickData.HistoDataModified && tickData.PricesSerialized {
			logs.GetLogger().WithFields(logrus.Fields{
				"module":   "trader_manager",
				"exchange": tickData.Exchange,
				"action":   "store_market_snapshot_in_ctx_manager",
			}).Infof("Storing market snapshot for %s at time: %s", tickData.Exchange, tickData.Prices["ETH"].GetTimestamp())
			ctx = ctxMgr.MakeContextAndStore(ms)
		} else {
			ctx = ctxMgr.MakeContext(ms)
		}

		contexts[eName] = ctx
	}

	return contexts
}

func (tm *TradersManager) reloadExchangeHistoData(exchangeName string) {

	logs.GetLogger().WithFields(logrus.Fields{
		"module":   "tradedaemon.trader_manager",
		"exchange": exchangeName,
	}).Infof("Reloading histo data for exchange %s", exchangeName)

	start := time.Now().Add(-time.Duration(time.Hour * time.Duration(tradeDaemonCfg.marketSnapsHeld)))
	histoMarketSnaps, err := marketdaemon.GetInstance().GetHistoMarketSnapshots(exchangeName, start, time.Now())

	if err != nil {
		logs.GetLogger().Panicf("TraderManager: Couldn't reload exchange histo data for %s; Err: %s", exchangeName, err.Error())
	}

	_, ok := tm.contextManagers[exchangeName]
	if !ok {
		return
	}

	tm.contextManagers[exchangeName] = tradeutil.NewTradeContextManagerWithCleanup(
		tradeDaemonCfg.candlesticksLim,
		tradeDaemonCfg.candleClaimCycleDuration,
		tradeDaemonCfg.fiatCurrencies...,
	)
	tm.contextManagers[exchangeName].FeedHistoMarketSnaps(histoMarketSnaps)
}

// handleTraders takes trading context and then handles each trader
func (tm *TradersManager) handleTraders(eName string, ctx *tradeutil.TradeContext) {
	activeTraders, _ := tm.traderStore.GetActiveTradersForExchange(eName)
	c := make(chan struct{}, len(activeTraders))
	for i := 0; i < len(activeTraders); i++ {
		go func(idx int) {
			tm.handleTrader(ctx, activeTraders[idx])
			c <- struct{}{}
		}(i)
	}

	for i := 0; i < len(activeTraders); i++ {
		<-c
	}
}

func (tm *TradersManager) handleTrader(ctx *tradeutil.TradeContext, trader contracts.Trader) {
	resp := trader.MakeDecision(*ctx)

	if resp == nil {
		return
	}

	dec := resp.Decision
	for i := 0; i < len(dec.GetTrades()); i++ {
		trade := &dec.Trades[i]
		trade.CreatedAt = ctx.GetTimestamp()
		trade.UpdatedAt = ctx.GetTimestamp()
	}

	l := trader.GetLedger()
	modelutil.AddTradeDecisionToLedger(resp.Decision, l)

	processor := tradehandler.GetTradeDecisionProcessor()
	processedDecision, e := processor.HandleTradeDecision(resp)

	if e != nil {
		logs.GetLogger().WithField("module", "daemons.tradedaemon.handle_trade_decision").
			Errorf("Err processing decision: %d. Err was: %s", processedDecision.Decision.GetID(), e.Error())
		if processedDecision.Decision != nil {
			modelutil.AddTradeDecisionToLedger(processedDecision.Decision, l)
			tm.notifyTrader(trader, processedDecision.Decision)
		}
		return
	}

	modelutil.AddTradeDecisionToLedger(processedDecision.Decision, l)
	tm.notifyTrader(trader, processedDecision.Decision)
}

func (tm *TradersManager) notifyTrader(t contracts.Trader, dec *model.TradeDecision) {
	trades := dec.GetTrades()
	for i := 0; i < len(trades); i++ {
		trade := &trades[i]
		e := t.HandleProcessedTrade(trade)
		if e != nil {
			stats := t.GetStats()
			logs.GetLogger().WithFields(logrus.Fields{
				"module":          "tradedaemon.trade_manager.notifyTrader",
				"trader_guid":     stats.GUID,
				"trader_status":   stats.State,
				"trader_exchange": stats.Exchange,
			}).Errorf("Got error when notifying trader about a processed trade. Err: %s", e.Error())
		}
	}
}
