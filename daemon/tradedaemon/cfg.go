package tradedaemon

import (
	"errors"
	"time"

	"bitbucket.org/pfzero/tradr/service/cryptomarket"

	"bitbucket.org/pfzero/tradr/cfg"
	"bitbucket.org/pfzero/tradr/service/exchange"
)

var (
	tradeDaemonCfg *tradeDaemonConfig
)

type tradeDaemonConfig struct {
	candlesticksLim          int
	candleClaimCycleDuration time.Duration
	marketSnapsHeld          int
	fiatCurrencies           []string

	trackableMarkets []string
	tradeableMarkets []string
}

func initCfg() error {
	hoursPerMonth := 24 * 30
	tradeDaemonCfg = &tradeDaemonConfig{
		candlesticksLim:          cfg.GetConfig().GetInt("daemons.trader.candlesticks_held"),
		candleClaimCycleDuration: cfg.GetConfig().GetDuration("daemons.trader.candlesticks_claim_cycle_duration") * time.Minute,
		marketSnapsHeld:          cfg.GetConfig().GetInt("daemons.trader.histo_data_held") * hoursPerMonth,

		trackableMarkets: []string{},
		tradeableMarkets: []string{},
	}

	if tradeDaemonCfg.marketSnapsHeld == 0 {
		return errors.New("Please set the daemons.trader.histo_data_held limit;")
	}
	if tradeDaemonCfg.candlesticksLim == 0 {
		return errors.New("Please set the daemons.trader.candlesticks_held config value;")
	}
	if tradeDaemonCfg.candleClaimCycleDuration == 0 {
		return errors.New("Please set the daemons.trader.candlesticks_claim_cycle_duration config value;")
	}

	resp := exchange.GetCurrencies()

	tradeDaemonCfg.fiatCurrencies = resp

	trackableMarkets := cryptomarket.GetProvider().GetTrackableMarkets()
	tradeableMarkets := cryptomarket.GetProvider().GetTradeSupportingMarkets()
	for marketName := range trackableMarkets {
		tradeDaemonCfg.trackableMarkets = append(tradeDaemonCfg.trackableMarkets, marketName)
	}

	for marketName := range tradeableMarkets {
		tradeDaemonCfg.tradeableMarkets = append(tradeDaemonCfg.tradeableMarkets, marketName)
	}

	return nil
}

// Initialize is used to initialize the configuration
// object for the tader manager
func Initialize() error {
	return initCfg()
}
