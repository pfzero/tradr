package marketdaemon

import (
	"fmt"
	"sync"
	"time"

	"bitbucket.org/pfzero/tradr/logs"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"bitbucket.org/pfzero/tradr/store/datastore/cryptomarketstore"

	"bitbucket.org/pfzero/tradr/daemon/tickdaemon"

	"bitbucket.org/pfzero/tradr/util/str"

	"bitbucket.org/pfzero/tradr/repo/model"
)

type exchangeSub struct {
	k string
	C <-chan ExchangeSnapshotTickData
}

// exchangeTracker is responsible
// Error: currently stop/start are leaking
// goroutines
type exchangeTracker struct {

	// exchange name
	name string

	isRunningL sync.Mutex
	isRunning  bool

	coinPriceDaemonsL sync.Mutex
	// list of price trackers
	coinPriceDaemons []*coinPriceDaemon

	coinPriceDaemonSubsL sync.Mutex
	// list of subscriptions per each tracked coin
	// on this exchange
	coinPriceDaemonSubs []*coinPriceDaemonSub

	etSubscribersL sync.Mutex
	// list of subscribers to this exchange
	etSubscribers map[string]chan ExchangeSnapshotTickData

	exchangeStatusSubscription *exchangeStatusSub
	exchangeStatusChecker      *exchangeStatusChecker

	lastSerializedTimeL sync.Mutex
	lastSerializedTime  time.Time
}

// newExchangeTracker creates a new exchange tracker
func newExchangeTracker(ename string) *exchangeTracker {
	est := newExchangeStatusChecker(ename, mdCfg.ExchangeStatusCheckInterval)
	sub := est.sub()

	em := &exchangeTracker{
		name:                       ename,
		coinPriceDaemonSubs:        []*coinPriceDaemonSub{},
		etSubscribers:              make(map[string]chan ExchangeSnapshotTickData),
		exchangeStatusChecker:      est,
		exchangeStatusSubscription: sub,
	}
	return em
}

// Start starts the current exchange tracker
func (et *exchangeTracker) Start() error {

	e := et.start()

	if e != nil {
		panicErr := et.Stop()
		if panicErr != nil {
			logs.GetLogger().WithFields(logrus.Fields{
				"module":   "daemons.exchange_tracker",
				"startErr": e.Error(),
				"stopErr":  panicErr.Error(),
			}).Panicf("Couldn't start daemons and also couldn't correctly stop the daemons;")
		}

		return errors.Wrapf(e, "Couldn't start daemons; Stopped the exchange tracker")
	}

	err := et.loadLastSerialized()

	if err != nil {
		panicErr := et.Stop()
		if panicErr != nil {
			logs.GetLogger().WithFields(logrus.Fields{
				"module":   "daemons.exchange_tracker",
				"startErr": err.Error(),
				"stopErr":  panicErr.Error(),
			}).Panicf("Couldn't start daemons and also couldn't correctly stop the daemons;")
		}

		return errors.Wrapf(err, "Couldn't start daemons because latest market snapshot couldn't be loaded; Stopped the exchange tracker;")
	}

	et.setRunning(true)

	return e
}

// Stop stops the coin daemons
func (et *exchangeTracker) Stop() error {
	et.stopDaemons()
	et.exchangeStatusChecker.stop()
	et.setRunning(false)

	return nil
}

// AddCoin adds / tracks a new coin
func (et *exchangeTracker) AddCoin(coin *model.CryptoTrackedCoin) error {

	if et.exists(coin) {
		return fmt.Errorf("Coin %s already exists", coin.Name)
	}

	daemon := newcoinPriceDaemon(coin)

	if et.getRunningStatus() {
		err := daemon.Start()
		if err != nil {
			return err
		}
	}

	et.coinPriceDaemonsL.Lock()
	et.coinPriceDaemons = append(et.coinPriceDaemons, daemon)
	et.coinPriceDaemonsL.Unlock()
	et.coinPriceDaemonSubsL.Lock()
	et.coinPriceDaemonSubs = append(et.coinPriceDaemonSubs, daemon.Sub())
	et.coinPriceDaemonSubsL.Unlock()

	return nil
}

// Sub creates a new subscriber to market snapshot
func (et *exchangeTracker) Sub() *exchangeSub {
	c := make(chan ExchangeSnapshotTickData)
	s := &exchangeSub{k: str.GenerateToken(), C: c}
	et.etSubscribersL.Lock()
	et.etSubscribers[s.k] = c
	et.etSubscribersL.Unlock()
	return s
}

// Unsub unsubscribes an existing subscriber
func (et *exchangeTracker) Unsub(s *exchangeSub) {
	if s == nil {
		return
	}

	et.etSubscribersL.Lock()
	defer et.etSubscribersL.Unlock()

	c, ok := et.etSubscribers[s.k]
	if !ok {
		return
	}

	close(c)
	delete(et.etSubscribers, s.k)
	return
}

func (et *exchangeTracker) start() error {
	err := et.startDaemons()
	if err != nil {
		return err
	}

	tickStreamCh := et.listenTickStream()

	go func() {
		for {
			var data ExchangeSnapshotTickData

			data = <-tickStreamCh

			go et.stream(data)

			if data.Telemetry.Status == ExchangeStatusOnline {
				continue
			}

			et.stopDaemons()

			et.exchangeStatusChecker.start()

			// run the exchange status checker
			// this function will terminate when the
			// exchange becomes online, right after
			// the next real-time tick
			et.startESStream(data.Telemetry)

			et.exchangeStatusChecker.stop()
			et.startDaemons()
			et.loadLastSerialized()

		}
	}()

	return nil
}

func (et *exchangeTracker) loadLastSerialized() error {
	lastMS, err := cryptomarketstore.GetMarketSnapClosestTo(et.name, time.Now())
	if err != nil {
		return err
	}

	if lastMS == nil {
		et.lastSerializedTime = time.Time{}
	} else {
		et.lastSerializedTime = lastMS.GetTimestamp()
	}

	return nil
}

func (et *exchangeTracker) listenTickStream() <-chan ExchangeSnapshotTickData {
	internalChan := make(chan ExchangeSnapshotTickData)
	ticker := tickdaemon.GetInstance().RealTime.Sub()

	go func() {
		for {
			<-ticker.C

			et.coinPriceDaemonSubsL.Lock()
			subs := make([]*coinPriceDaemonSub, len(et.coinPriceDaemonSubs))
			copy(subs, et.coinPriceDaemonSubs)
			et.coinPriceDaemonSubsL.Unlock()

			c := make(chan coinPriceTickData, len(subs))

			for i := 0; i < len(subs); i++ {
				go func(sub *coinPriceDaemonSub) {
					c <- <-sub.C
				}(
					subs[i],
				)
			}

			exchangeTickData := ExchangeSnapshotTickData{
				Exchange: et.name,
				Prices:   make(map[string]*model.CryptoPriceEntry),
			}

			var err error
			for i := 0; i < len(subs); i++ {
				coinTickData := <-c

				exchangeTickData.HistoDataModified = exchangeTickData.HistoDataModified || coinTickData.HistoDataModified

				err = coinTickData.Err

				if err != nil {
					continue
				}

				exchangeTickData.Prices[coinTickData.Price.TrackedCoin.Coin.Symbol] = coinTickData.Price
			}

			if err != nil {
				exchangeTickData.Prices = nil
				exchangeTickData.Telemetry = ExchangeTelemetry{
					Status:  ExchangeStatusOffline,
					LastErr: err,
				}
			} else {
				exchangeTickData.Telemetry = ExchangeTelemetry{
					Status: ExchangeStatusOnline,
				}
			}

			potentiallySerialized := et.serializeExchangeTickData(exchangeTickData)

			select {
			case internalChan <- potentiallySerialized:
			default:
			}
		}
	}()

	return internalChan
}

func (et *exchangeTracker) startESStream(initialTelemetry ExchangeTelemetry) {

	subscription := tickdaemon.GetInstance().RealTime.Sub()
	exchangeRecovered := false

	for {
		lastTelemetry := initialTelemetry

		select {
		case curTelemetry := <-et.exchangeStatusSubscription.C:
			if curTelemetry.Status == ExchangeStatusOnline {
				exchangeRecovered = true
			} else {
				lastTelemetry = curTelemetry
			}
			continue
		case <-subscription.C:
		}

		go et.stream(ExchangeSnapshotTickData{Exchange: et.name, Telemetry: lastTelemetry})

		if exchangeRecovered {
			break
		}
	}

	tickdaemon.GetInstance().RealTime.Unsub(subscription)

}

func (et *exchangeTracker) stream(tickData ExchangeSnapshotTickData) {
	et.etSubscribersL.Lock()
	defer et.etSubscribersL.Unlock()

	for _, c := range et.etSubscribers {
		select {
		case c <- tickData:
		default:
		}
	}
}

func (et *exchangeTracker) startDaemons() error {
	et.coinPriceDaemonsL.Lock()
	defer et.coinPriceDaemonsL.Unlock()

	for _, daemon := range et.coinPriceDaemons {
		err := daemon.Start()
		if err != nil {
			return err
		}
	}
	return nil
}

func (et *exchangeTracker) stopDaemons() {
	et.coinPriceDaemonsL.Lock()
	defer et.coinPriceDaemonsL.Unlock()

	for idx := range et.coinPriceDaemons {
		daemon := et.coinPriceDaemons[idx]
		go func(d *coinPriceDaemon) {
			d.Stop()
		}(daemon)
	}
}

func (et *exchangeTracker) serializeExchangeTickData(exchangeTickData ExchangeSnapshotTickData) ExchangeSnapshotTickData {
	et.lastSerializedTimeL.Lock()
	defer et.lastSerializedTimeL.Unlock()

	if exchangeTickData.Telemetry.Status.IsOff() {
		return exchangeTickData
	}

	if exchangeTickData.HistoDataModified {
		err := et.loadLastSerialized()
		if err != nil {
			logs.GetLogger().WithFields(logrus.Fields{
				"module":   "exchange_tracker",
				"exchange": et.name,
			}).Errorln(errors.Wrap(err, "couldn't load the latest serialized time"))

			et.lastSerializedTime = tickdaemon.GetInstance().GetFillHistoTime()
		}
	}

	var tickTime time.Time

	for _, c := range exchangeTickData.Prices {
		tickTime = c.GetTimestamp()
	}

	serializationDate := time.Date(
		et.lastSerializedTime.Year(),
		et.lastSerializedTime.Month(),
		et.lastSerializedTime.Day(),
		et.lastSerializedTime.Hour()+1,
		0, 0, 0, time.Local,
	)

	if tickTime.Before(serializationDate) {
		return exchangeTickData
	}

	err := et.serializePrices(exchangeTickData.Prices, serializationDate)

	if err != nil {
		logs.GetLogger().WithFields(logrus.Fields{
			"module":   "exchange_tracker",
			"exchange": et.name,
		}).Errorf("Error while serializing exchange tick data: %s", err.Error())
		panic(err)
	}

	exchangeTickData.PricesSerialized = true
	et.lastSerializedTime = serializationDate

	return exchangeTickData
}

func (et *exchangeTracker) serializePrices(prices map[string]*model.CryptoPriceEntry, t time.Time) error {
	toSave := []model.CryptoPriceEntry{}
	for _, price := range prices {
		price.Time = tickdaemon.GetStartOfCurrentHourTime()
		p := *price
		toSave = append(toSave, p)
	}

	_, err := cryptomarketstore.AddPriceEntries(toSave)

	return err
}

func (et *exchangeTracker) exists(coin *model.CryptoTrackedCoin) bool {
	for _, el := range et.coinPriceDaemons {
		if el.GetHash() == coin.HashID() {
			return true
		}
	}
	return false
}

func (et *exchangeTracker) setRunning(flag bool) *exchangeTracker {
	et.isRunningL.Lock()
	defer et.isRunningL.Unlock()
	et.isRunning = flag
	return et
}

func (et *exchangeTracker) getRunningStatus() bool {
	et.isRunningL.Lock()
	defer et.isRunningL.Unlock()
	return et.isRunning
}
