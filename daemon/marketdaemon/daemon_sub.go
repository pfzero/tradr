package marketdaemon

// DaemonSub represents a marketdaemon
// subscriber
type DaemonSub struct {
	k string
	C <-chan map[string]ExchangeSnapshotTickData
}
