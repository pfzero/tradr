package marketdaemon

import (
	"time"

	"bitbucket.org/pfzero/tradr/repo/model"
)

// ExchangeStatus expresses the status of an exchange
type ExchangeStatus string

// IsOn returns true if the exchange is online
func (es ExchangeStatus) IsOn() bool {
	return es == ExchangeStatusOnline
}

// IsOff returns true if the exchange is offline
func (es ExchangeStatus) IsOff() bool {
	return es == ExchangeStatusOffline
}

const (
	// ExchangeStatusOnline represents the online status
	// of an exchange
	ExchangeStatusOnline ExchangeStatus = "Online"
	// ExchangeStatusOffline represents the offline status
	// of an exchange
	ExchangeStatusOffline ExchangeStatus = "Offline"
)

// ExchangeSnapshotTickData represents the data included in
// each tick
type ExchangeSnapshotTickData struct {
	Exchange  string
	Telemetry ExchangeTelemetry
	Prices    map[string]*model.CryptoPriceEntry

	// in cases when this exchange switched from offline
	// to online, there might've been historical data to be
	// filled for each coin; This flag indicates that there is
	// a coin that had to be filled with missing historical data
	HistoDataModified bool

	// at each interval the prices are also serialized in database
	// This flag indicates wether the prices were serialized or not
	PricesSerialized bool
}

// ExchangeTelemetry represents metrics of an exchange
type ExchangeTelemetry struct {
	Status     ExchangeStatus
	LastErr    error     // in case status is offline, what was the latest err received
	StatusFrom time.Time // time from when this current status is active
}
