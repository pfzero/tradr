package marketdaemon

import (
	"fmt"
	"strings"
	"sync"
	"time"

	"bitbucket.org/pfzero/tradr/service/cryptomarket"
	"bitbucket.org/pfzero/tradr/store/datastore/cryptomarketstore"

	"bitbucket.org/pfzero/tradr/util/str"

	"bitbucket.org/pfzero/tradr/repo/model"

	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"
)

const marketExchange = "Market"

var marketDaemonOnce sync.Once
var marketDaemon *MarketDaemon

// Middleware represents a mdidleware function that can be mounted
// for the incoming bundle of exchange snapshot tick data
type Middleware func(map[string]ExchangeSnapshotTickData) map[string]ExchangeSnapshotTickData

// GetInstance returns the MarketDaemon instance
func GetInstance() *MarketDaemon {
	marketDaemonOnce.Do(func() {
		availableETrackers := cryptomarket.GetProvider().GetTrackableMarkets()
		exchangeTrackers := make(map[string]*exchangeTracker)
		initialTelemetry := make(map[string]ExchangeTelemetry)
		n := time.Now()

		for eName := range availableETrackers {
			exchangeTrackers[eName] = newExchangeTracker(eName)
			initialTelemetry[eName] = ExchangeTelemetry{
				Status:     ExchangeStatusOnline,
				LastErr:    nil,
				StatusFrom: n,
			}
		}

		marketDaemon = &MarketDaemon{
			exchangeTrackers: exchangeTrackers,
			subs:             make(map[string]chan map[string]ExchangeSnapshotTickData),
			mids:             []Middleware{statusFromMid(initialTelemetry)},
			stopSignal:       make(chan struct{}),
			lastTelemetry:    initialTelemetry,
		}
	})
	return marketDaemon
}

// MarketDaemon represents the public
// interface of this module
type MarketDaemon struct {
	exchangeTrackers map[string]*exchangeTracker

	subsL sync.Mutex
	subs  map[string]chan map[string]ExchangeSnapshotTickData

	midsL sync.Mutex
	mids  []Middleware

	stopSignal chan struct{}

	lastTelemetryL sync.Mutex
	lastTelemetry  map[string]ExchangeTelemetry

	stateL        sync.Mutex
	isRunning     bool
	isInitialized bool
}

// Start starts the daemon
func (md *MarketDaemon) Start() error {
	err := md.registerTrackedCoins()
	if err != nil {
		return err
	}
	md.setInitialized(true)

	err = md.startExchangeTrackers()
	if err != nil {
		return err
	}

	go md.start()

	md.setRunning(true)
	return nil
}

// Stop stops the daemon
func (md *MarketDaemon) Stop() error {
	// send the stop listening signal first
	md.stopSignal <- struct{}{}
	// stop exchange trackers
	e := md.stopExchangeTrackers()

	if e != nil {
		return e
	}

	return nil
}

// GetHistoMarketSnapshots returns the list of historical marekt snapshots for the given exchange
func (md *MarketDaemon) GetHistoMarketSnapshots(exchange string, fromTime, toTime time.Time) ([]cryptomarketutil.MarketSnapshot, error) {
	allSnaps, err := cryptomarketstore.GetHistoMarketSnapshots(fromTime, toTime)
	if err != nil {
		return nil, err
	}
	if strings.ToLower(exchange) == strings.ToLower(marketExchange) {
		return allSnaps[marketExchange], nil
	}

	// otherwise, fill histo data with market data
	marketSnaps := allSnaps[marketExchange]
	exchangeSnaps := allSnaps[exchange]

	filled := exchangeSnaps.FillSnaps(marketSnaps, exchange, marketExchange)

	return filled, nil
}

// GetMarketSnapClosestTo for the given exchange, returns the closest exchange snapshot (list of all prices
// tracked on that exchange) to the given timestmap
func (md *MarketDaemon) GetMarketSnapClosestTo(exchange string, time time.Time) (*cryptomarketutil.MarketSnapshot, error) {
	return cryptomarketstore.GetMarketSnapClosestTo(exchange, time)
}

// AddCoin adds a new coin to be tracked
func (md *MarketDaemon) AddCoin(coin *model.CryptoTrackedCoin) error {
	exchange := coin.TrackedExchange.Name
	_, ok := md.exchangeTrackers[exchange]
	if !ok {
		return fmt.Errorf("Cannot track coin on exchange %s. The exchange is not supported for tracking", exchange)
	}

	return md.exchangeTrackers[exchange].AddCoin(coin)
}

// SubMarketSnapshot creates a new subscriber to the stream of
// available market snapshots (all)
func (md *MarketDaemon) SubMarketSnapshot() *DaemonSub {
	md.subsL.Lock()
	defer md.subsL.Unlock()

	ch := make(chan map[string]ExchangeSnapshotTickData)

	s := &DaemonSub{k: str.GenerateToken(), C: ch}
	md.subs[s.k] = ch

	return s
}

// UnsubMarketSnapshot unsubscribes from market snapshots
func (md *MarketDaemon) UnsubMarketSnapshot(s *DaemonSub) {
	if s == nil || s.k == "" {
		return
	}

	md.subsL.Lock()
	defer md.subsL.Unlock()
	ch, ok := md.subs[s.k]
	if !ok {
		return
	}
	close(ch)
	delete(md.subs, s.k)
}

// GetStatus returns the status of all exchanges
func (md *MarketDaemon) GetStatus() map[string]ExchangeTelemetry {
	md.lastTelemetryL.Lock()
	defer md.lastTelemetryL.Unlock()
	cp := make(map[string]ExchangeTelemetry)
	for eName, tel := range md.lastTelemetry {
		cp[eName] = tel
	}
	return cp
}

// Use registers a new middleware
func (md *MarketDaemon) Use(m Middleware) *MarketDaemon {
	if m == nil {
		return md
	}
	md.midsL.Lock()
	defer md.midsL.Unlock()

	md.mids = append(md.mids, m)
	return md
}

func (md *MarketDaemon) start() {
	subs := make(map[string]*exchangeSub)
	for eName, eTracker := range md.exchangeTrackers {
		subs[eName] = eTracker.Sub()
	}

	unsub := func() {
		for eName, sub := range subs {
			md.exchangeTrackers[eName].Unsub(sub)
		}
	}

	exchangeTicks := make(chan ExchangeSnapshotTickData, len(subs))

mainL:
	for {
		// fw all exchange snaps to the above channel
		for eName := range subs {
			go func(eName string) {
				sub := subs[eName]
				exchangeTicks <- <-sub.C
			}(eName)
		}

		// find out if there's a stop signal
		select {
		case <-md.stopSignal:
			unsub()
			break mainL
		default:
		}

		data := make(map[string]ExchangeSnapshotTickData)

		// drain the exchangeTicks channel
		for i := 0; i < len(subs); i++ {
			tickData := <-exchangeTicks
			data[tickData.Exchange] = tickData
		}

		afterMid := md.applyMiddleware(data)
		md.storeLastTelemetry(afterMid)

		go md.stream(afterMid)
	}
}

func (md *MarketDaemon) startExchangeTrackers() error {
	for _, tracker := range md.exchangeTrackers {
		e := tracker.Start()
		if e != nil {
			return e
		}
	}
	return nil
}

func (md *MarketDaemon) stopExchangeTrackers() error {
	for _, tracker := range md.exchangeTrackers {
		e := tracker.Stop()
		if e != nil {
			return e
		}
	}
	return nil
}

func (md *MarketDaemon) applyMiddleware(sn map[string]ExchangeSnapshotTickData) map[string]ExchangeSnapshotTickData {
	applied := sn
	md.midsL.Lock()
	defer md.midsL.Unlock()

	for _, mid := range md.mids {
		applied = mid(applied)
	}

	return applied
}

func (md *MarketDaemon) storeLastTelemetry(sn map[string]ExchangeSnapshotTickData) {
	statuses := make(map[string]ExchangeTelemetry)
	for ename, eTickData := range sn {
		statuses[ename] = eTickData.Telemetry
	}
	md.lastTelemetryL.Lock()
	defer md.lastTelemetryL.Unlock()
	md.lastTelemetry = statuses
}

func (md *MarketDaemon) stream(sn map[string]ExchangeSnapshotTickData) {
	md.subsL.Lock()
	defer md.subsL.Unlock()

	for _, ch := range md.subs {
		select {
		case ch <- sn:
		default:
		}
	}
}

func (md *MarketDaemon) registerTrackedCoins() error {
	if md.getInitialized() {
		return nil
	}

	coins, err := cryptomarketstore.GetTrackedCoins()
	if err != nil {
		return err
	}

	// also register the list of coins
	for i := 0; i < len(coins); i++ {
		coin := &coins[i]
		tracker, ok := md.exchangeTrackers[coin.TrackedExchange.Name]
		if !ok {
			continue
		}
		tracker.AddCoin(coin)
	}

	return nil
}

func (md *MarketDaemon) setRunning(flag bool) *MarketDaemon {
	md.stateL.Lock()
	defer md.stateL.Unlock()
	md.isRunning = flag
	return md
}

func (md *MarketDaemon) setInitialized(flag bool) *MarketDaemon {
	md.stateL.Lock()
	defer md.stateL.Unlock()
	md.isInitialized = flag
	return md
}

func (md *MarketDaemon) getRunning() bool {
	md.stateL.Lock()
	defer md.stateL.Unlock()
	return md.isRunning
}

func (md *MarketDaemon) getInitialized() bool {
	md.stateL.Lock()
	defer md.stateL.Unlock()
	return md.isInitialized
}
