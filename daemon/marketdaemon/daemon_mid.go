package marketdaemon

import (
	"time"
)

// statusFromMid represents the middleware that assigns
// the StatusFrom field for each exchange telemetry struct
func statusFromMid(initialTelemetry map[string]ExchangeTelemetry) Middleware {
	lastTelemetry := make(map[string]ExchangeTelemetry)

	// copy initial telemetry data
	for eName, telemetry := range initialTelemetry {
		lastTelemetry[eName] = telemetry
	}

	return func(snap map[string]ExchangeSnapshotTickData) map[string]ExchangeSnapshotTickData {
		ret := make(map[string]ExchangeSnapshotTickData)

		for eName, eTickData := range snap {
			telemetry, ok := lastTelemetry[eName]
			if ok && telemetry.Status == eTickData.Telemetry.Status {
				eTickData.Telemetry.StatusFrom = telemetry.StatusFrom
				ret[eName] = eTickData
				continue
			}

			eTickData.Telemetry.StatusFrom = time.Now()
			ret[eName] = eTickData
			lastTelemetry[eName] = eTickData.Telemetry
		}

		return ret
	}
}
