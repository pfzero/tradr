package marketdaemon

import (
	"bitbucket.org/pfzero/tradr/repo/model"
)

// coinPriceTickData represents the data emitted
// per each tick from the coin price daemon
type coinPriceTickData struct {
	Err               error
	Price             *model.CryptoPriceEntry
	HistoDataModified bool
}

// coinPriceDaemonSub represents a subscriber
// of the coin price daemon. Used to open a stream
// of prices for a specific coin
type coinPriceDaemonSub struct {
	id string
	C  <-chan coinPriceTickData
}
