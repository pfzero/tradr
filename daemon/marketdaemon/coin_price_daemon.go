package marketdaemon

import (
	"errors"
	"math"
	"sync"
	"time"

	"bitbucket.org/pfzero/tradr/util/str"

	"bitbucket.org/pfzero/tradr/util/tick"

	"bitbucket.org/pfzero/tradr/daemon/tickdaemon"
	"bitbucket.org/pfzero/tradr/service/cryptomarket"
	"bitbucket.org/pfzero/tradr/service/cryptomarket/common"
	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"

	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/store/datastore/cryptomarketstore"
	"github.com/jinzhu/gorm"
)

// coinPriceDaemon is responsible for tracking the price
// of a single coin
type coinPriceDaemon struct {
	coinSettings        *model.CryptoTrackedCoin
	priceTickerSub      *tick.S
	stopSignal          chan struct{}
	histoModifiedSignal chan bool
	coinListenersL      sync.Mutex
	coinListeners       map[string]chan coinPriceTickData
}

// newcoinPriceDaemon creates a new coin price daemon
func newcoinPriceDaemon(cfg *model.CryptoTrackedCoin) *coinPriceDaemon {
	return &coinPriceDaemon{
		coinSettings:        cfg,
		priceTickerSub:      tickdaemon.GetInstance().RealTime.Sub(),
		stopSignal:          make(chan struct{}, 1),
		histoModifiedSignal: make(chan bool, 1),
		coinListeners:       make(map[string]chan coinPriceTickData),
	}
}

// GetHash returns the coin's UUID
func (cpd *coinPriceDaemon) GetHash() string {
	return cpd.coinSettings.HashID()
}

// Start starts the daemon
func (cpd *coinPriceDaemon) Start() error {
	err := cpd.fillHistoData()
	if err != nil {
		return err
	}

	go cpd.startTracking()

	return nil
}

// Stop stops the daemon
func (cpd *coinPriceDaemon) Stop() error {
	cpd.stopSignal <- struct{}{}
	return nil
}

// Sub creates a new subscriber to this daemon
func (cpd *coinPriceDaemon) Sub() *coinPriceDaemonSub {
	c := make(chan coinPriceTickData)
	s := &coinPriceDaemonSub{id: str.GenerateToken(), C: c}
	cpd.coinListenersL.Lock()
	cpd.coinListeners[s.id] = c
	cpd.coinListenersL.Unlock()
	return s
}

// Unsub deletes subscriber
func (cpd *coinPriceDaemon) Unsub(s *coinPriceDaemonSub) {
	if s == nil {
		return
	}

	cpd.coinListenersL.Lock()
	defer cpd.coinListenersL.Unlock()
	c, ok := cpd.coinListeners[s.id]
	if !ok {
		return
	}
	close(c)
	delete(cpd.coinListeners, s.id)
	return
}

func (cpd *coinPriceDaemon) startTracking() {
	cm := cryptomarket.GetProvider()

streamerLoop:
	for {
		var t time.Time

		select {
		case <-cpd.stopSignal:
			break streamerLoop
		case t = <-cpd.priceTickerSub.C:
		}

		latestPrice, err := cm.GetTicker(cpd.coinSettings.TrackedExchange.Name, &common.TickerReq{
			FSym: cpd.coinSettings.Coin.Symbol,
			TSym: mdCfg.BaseCoinSym,
		})

		if err != nil {
			cpd.stream(nil, err)
			continue
		}

		if latestPrice == nil {
			// maybe log this somehow?
			cpd.stream(nil, errors.New("Unknown error; Both the error and the latest price is nil"))
			continue
		}

		price := latestPrice.AsFullModel(cpd.coinSettings)
		price.Time = t
		cpd.stream(price, nil)
	}
}

func (cpd *coinPriceDaemon) stream(p *model.CryptoPriceEntry, err error) {
	cpd.coinListenersL.Lock()
	chans := make([]chan coinPriceTickData, len(cpd.coinListeners))
	cnt := 0
	for _, c := range cpd.coinListeners {
		chans[cnt] = c
		cnt++
	}
	cpd.coinListenersL.Unlock()

	var histoModified bool

	select {
	case histoModified = <-cpd.histoModifiedSignal:
	default:
		histoModified = false
	}

	for _, c := range chans {
		select {
		case c <- coinPriceTickData{
			Err:               err,
			Price:             p,
			HistoDataModified: histoModified,
		}:
		default:
		}
	}
}

func (cpd *coinPriceDaemon) getLastDBPrice() (*model.CryptoPriceEntry, error) {
	price, err := cryptomarketstore.GetPriceByTrackedCoin(cpd.coinSettings)
	if err != nil && err == gorm.ErrRecordNotFound {
		return nil, nil
	} else if err != nil {
		return nil, err
	}
	return price, nil
}

func (cpd *coinPriceDaemon) fillHistoData() error {
	bounds := &cryptomarketutil.Variation{
		Up:   mdCfg.PriceListDistillUpperBound,
		Down: mdCfg.PriceListDistillLowerBound,
	}

	pList := cryptomarketutil.NewPriceList(math.MaxInt32, mdCfg.PriceListHistoDistilledBufLimit, bounds)

	lastPrice, err := cpd.getLastDBPrice()
	if err != nil {
		return err
	}

	if lastPrice != nil {

		// nothing to do
		if time.Now().Sub(lastPrice.GetTimestamp()) < tickdaemon.StoreInterval {
			return nil
		}

		remaining, err := cryptomarket.GetProvider().GetHistoData(cpd.coinSettings.TrackedExchange.Name, &common.HistoReq{
			FromTime: lastPrice.GetTimestamp(),
			ToTime:   tickdaemon.GetInstance().GetFillHistoTime(),
			TickerReq: common.TickerReq{
				FSym: cpd.coinSettings.Coin.Symbol,
				TSym: mdCfg.BaseCoinSym,
			},
		})

		if err != nil {
			return err
		}

		prices := remaining.AsFullModels(cpd.coinSettings)
		pList.Add(lastPrice)
		for i := 0; i < len(remaining); i++ {
			pList.AddDistilled(&prices[i])
		}

		addedPrices := pList.GetAll()[1:]

		if len(addedPrices) == 0 {
			return nil
		}

		_, err = cpd.storePrices(addedPrices)

		if err == nil {
			cpd.histoModifiedSignal <- true
		}

		return err
	}

	histoData, err := cpd.fetchAPIHistoricalData()

	if err != nil {
		return err
	}

	if len(histoData) == 0 {
		return nil
	}

	prices := make([]cryptomarketutil.Pricer, len(histoData))
	for i := 0; i < len(histoData); i++ {
		prices[i] = &histoData[i]
	}

	pList.Seed(prices)
	distilled := pList.Distill().GetAll()
	_, err = cpd.storePrices(distilled)
	if err == nil {
		cpd.histoModifiedSignal <- true
	}
	return err
}

func (cpd *coinPriceDaemon) storePrices(prices []cryptomarketutil.Pricer) ([]model.CryptoPriceEntry, error) {
	toStore := make([]model.CryptoPriceEntry, len(prices))
	for i := 0; i < len(prices); i++ {
		price := prices[i].(*model.CryptoPriceEntry)
		toStore[i] = *price
	}
	return cryptomarketstore.AddPriceEntries(toStore)
}

// fetchAPIHistoricalData fetches the pricing history of this coin from
// cryptomarket API layer
func (cpd *coinPriceDaemon) fetchAPIHistoricalData() ([]model.CryptoPriceEntry, error) {

	resp, err := cryptomarket.GetProvider().GetAllHistoData(cpd.coinSettings.TrackedExchange.Name, &common.TickerReq{
		FSym: cpd.coinSettings.Coin.Symbol,
		TSym: mdCfg.BaseCoinSym,
	})

	if err != nil {
		return nil, err
	}

	return resp.AsFullModels(cpd.coinSettings), nil
}
