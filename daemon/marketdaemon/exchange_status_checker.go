package marketdaemon

import (
	"sync"
	"time"

	"bitbucket.org/pfzero/tradr/service/cryptomarket"
	"bitbucket.org/pfzero/tradr/util/str"
)

type exchangeStatusSub struct {
	k string
	C <-chan ExchangeTelemetry
}

// exchangeStatusChecker checks the status of the
// exchange
type exchangeStatusChecker struct {
	name          string
	checkInterval time.Duration
	stopSignal    chan struct{}

	subsL sync.Mutex
	subs  map[string]chan ExchangeTelemetry
}

func newExchangeStatusChecker(exchangeName string, interval time.Duration) *exchangeStatusChecker {
	return &exchangeStatusChecker{
		name:          exchangeName,
		checkInterval: interval,
		stopSignal:    make(chan struct{}, 1),

		subs: make(map[string]chan ExchangeTelemetry),
	}
}

func (esc *exchangeStatusChecker) start() error {
	go func() {
		ticker := time.NewTicker(esc.checkInterval)

	checkerLoop:
		for {
			select {
			case <-esc.stopSignal:
				break checkerLoop
			case <-ticker.C:
			}

			err := cryptomarket.GetProvider().GetExchangeStatus(esc.name)

			if err == nil {
				esc.streamStatus(ExchangeTelemetry{
					Status:  ExchangeStatusOnline,
					LastErr: nil,
				})
			} else {
				esc.streamStatus(ExchangeTelemetry{
					Status:  ExchangeStatusOffline,
					LastErr: err,
				})
			}
		}

	}()
	return nil
}

func (esc *exchangeStatusChecker) stop() {
	esc.stopSignal <- struct{}{}
}

func (esc *exchangeStatusChecker) streamStatus(status ExchangeTelemetry) {
	esc.subsL.Lock()
	defer esc.subsL.Unlock()

	for _, c := range esc.subs {
		select {
		case c <- status:
		default:
		}
	}
}

func (esc *exchangeStatusChecker) sub() *exchangeStatusSub {
	c := make(chan ExchangeTelemetry)

	s := &exchangeStatusSub{k: str.GenerateToken(), C: c}

	esc.subsL.Lock()
	esc.subs[s.k] = c
	esc.subsL.Unlock()

	return s
}

func (esc *exchangeStatusChecker) unsub(subscriber *exchangeStatusSub) {
	esc.subsL.Lock()
	defer esc.subsL.Unlock()

	if subscriber == nil {
		return
	}

	if _, ok := esc.subs[subscriber.k]; !ok {
		return
	}
	close(esc.subs[subscriber.k])
	delete(esc.subs, subscriber.k)
}
