package marketdaemon

import (
	"errors"
	"time"

	"bitbucket.org/pfzero/tradr/cfg"
)

// marketDaemonCfg represents the list of configuration
// parameters needed by the CoinPriceDaemon
type marketDaemonCfg struct {
	// PriceListHistoDistilledBufLimit represents the max number of prices
	// that don't fit within the price list because of the out-of-bounds
	// variation of the price. After this number is reached, the cumulated
	// buffer of prices are distilled and merged within the price list
	PriceListHistoDistilledBufLimit int

	// PriceListDistillUpperBound and PriceListDistillLowerBound sets the
	// needed bounds when distilling the historical prices
	PriceListDistillUpperBound float64
	PriceListDistillLowerBound float64

	// BaseCoinSym represents the base coin which will be used to fetch the
	// price of the tracked crypto currency
	BaseCoinSym string

	// ExchangeStatusCheckInterval specifies the interval at which an offline
	// exchange should be checked
	ExchangeStatusCheckInterval time.Duration
}

var mdCfg *marketDaemonCfg

func initConf() error {
	c := cfg.GetConfig()
	mdCfg = &marketDaemonCfg{}

	mdCfg.PriceListHistoDistilledBufLimit = c.GetInt("daemons.market_tracker.price_list_histo_distilled_buf_limit")
	mdCfg.PriceListDistillUpperBound = c.GetFloat64("daemons.market_tracker.price_list_perc_variation_upper_bound")
	mdCfg.PriceListDistillLowerBound = c.GetFloat64("daemons.market_tracker.price_list_perc_variation_lower_bound")
	mdCfg.BaseCoinSym = c.GetString("daemons.market_tracker.price_tracking_base_coin")
	mdCfg.ExchangeStatusCheckInterval = c.GetDuration("daemons.market_tracker.exchange_status_check_interval") * time.Minute

	var err error
	if mdCfg.PriceListHistoDistilledBufLimit == 0 {
		err = errors.New("Please set the daemons.market_tracker.price_list_histo_distilled_buf_limit in config file")
	}
	if mdCfg.PriceListDistillUpperBound == 0 && mdCfg.PriceListDistillLowerBound == 0 {
		err = errors.New(
			"Please set the daemons.market_tracker.price_list_perc_variation_upper_bound or daemons.market_tracker.price_list_perc_variation_lower_bound in config file",
		)
	}

	if mdCfg.BaseCoinSym == "" {
		err = errors.New("Please set the daemons.market_tracker.price_tracking_base_coin in config file")
	}

	if mdCfg.ExchangeStatusCheckInterval == 0 {
		err = errors.New("Please set the daemons.market_tracker.exchange_status_check_interval in config file")
	}

	return err
}

// Initialize is used for initializing the market daemon
// configurations
func Initialize() error {
	return initConf()
}
