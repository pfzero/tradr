package backtestcfg

import (
	"bitbucket.org/pfzero/tradr/cfg"
	"github.com/pkg/errors"
)

// BacktestingConfig encapsulates the config
// parameters for backtesting
type BacktestingConfig struct {
	MinTradeAmount        float64
	DefaultTransactionFee float64
	GeneratedSnapsFactor  int
}

// MultiCoinMonteCarloConfig represents the configuration
// needed for multi coin monte-carlo simulator
type MultiCoinMonteCarloConfig struct {
	BacktestingConfig
	MaxConfigsPerPair int
	BatchSize         int
	ParallelBatches   int
}

// GetBacktestingConfig loads the backtesting configuration
func GetBacktestingConfig() (*BacktestingConfig, error) {
	minTradeAmtKey := "workers.trader_backtest.min_trade_amount"
	defaultFeeKey := "workers.trader_backtest.default_fee"
	generatedSnapsFactorKey := "workers.trader_backtest.generated_snaps_factor"

	minTradeAmt := cfg.GetConfig().GetFloat64(minTradeAmtKey)
	defaultFee := cfg.GetConfig().GetFloat64(defaultFeeKey)
	generatedSnapsFactor := cfg.GetConfig().GetInt(generatedSnapsFactorKey)

	if minTradeAmt == 0 {
		return nil, errors.Errorf(
			"minimum trade amount not set in configuration file; Please set %s", minTradeAmtKey,
		)
	}

	if defaultFee == 0 {
		return nil, errors.Errorf(
			"default fee for transaction not set in config file; Please set %s", defaultFeeKey,
		)
	}

	if generatedSnapsFactor == 0 {
		return nil, errors.Errorf(
			"the generated_snaps_factor was not set in config file; Please set %s", generatedSnapsFactorKey,
		)
	}

	return &BacktestingConfig{
		MinTradeAmount:        minTradeAmt,
		DefaultTransactionFee: defaultFee,
		GeneratedSnapsFactor:  generatedSnapsFactor,
	}, nil
}

// GetMultiCoinMonteCarloConfig returns the configuration needed for
// multi-coin strategy monte-carlo simulator
func GetMultiCoinMonteCarloConfig() (*MultiCoinMonteCarloConfig, error) {
	backtestCfg, err := GetBacktestingConfig()
	if err != nil {
		return nil, err
	}

	maxConfigsPerPairKey := "workers.multi_coin_monte_carlo.max_configs_per_pair"
	batchSizeKey := "workers.multi_coin_monte_carlo.batch_size"
	parallelBatchesKey := "workers.multi_coin_monte_carlo.parallel_batches"

	maxConfigsPerPair := cfg.GetConfig().GetInt(maxConfigsPerPairKey)
	batchSize := cfg.GetConfig().GetInt(batchSizeKey)
	parallelBatches := cfg.GetConfig().GetInt(parallelBatchesKey)

	if maxConfigsPerPair == 0 {
		return nil, errors.Errorf(
			"the maximum number of configurations per pair is missing: Please set %s", maxConfigsPerPairKey,
		)
	}

	if batchSize == 0 {
		return nil, errors.Errorf(
			"the batch size is missing from config file; Please set %s", batchSizeKey,
		)
	}

	if parallelBatches == 0 {
		parallelBatches = 1
	}

	return &MultiCoinMonteCarloConfig{
		BacktestingConfig: *backtestCfg,
		MaxConfigsPerPair: maxConfigsPerPair,
		BatchSize:         batchSize,
		ParallelBatches:   parallelBatches,
	}, nil
}
