package traderbacktest

import (
	"sync"
	"time"

	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"
	"github.com/pkg/errors"

	"bitbucket.org/pfzero/tradr/backtest/backtestcfg"
	"bitbucket.org/pfzero/tradr/repo/modelutil"

	"bitbucket.org/pfzero/tradr/repo/contracts"
	"bitbucket.org/pfzero/tradr/repo/modelenum"
	"bitbucket.org/pfzero/tradr/service/exchange"
	"bitbucket.org/pfzero/tradr/traderstore"

	"bitbucket.org/pfzero/tradr/cfg"
	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/store/datastore/cryptomarketstore"
	"bitbucket.org/pfzero/tradr/util/tradeutil"
)

var fiatCurrencies = []string{}
var backtestConf *backtestcfg.BacktestingConfig
var configOnce sync.Once

func loadCfg() error {
	if backtestConf != nil {
		return nil
	}

	c, err := backtestcfg.GetBacktestingConfig()
	if err != nil {
		return err
	}

	backtestConf = c
	return nil
}

func isFiatCurrency(sym string) bool {
	for _, existingFiatSym := range fiatCurrencies {
		if existingFiatSym == sym {
			return true
		}
	}
	return false
}

func loadSnaps(exchangeName string, fromTime, toTime time.Time) (cryptomarketutil.MarketSnapshotList, error) {
	snaps, err := cryptomarketstore.GetHistoMarketSnapshots(fromTime, toTime)
	if err != nil {
		return nil, err
	}

	exchangeSnaps, ok := snaps[exchangeName]
	if !ok {
		return nil, errors.Errorf("Couldn't fetch market snaps for exchange %s; Exchange doesn't exist", exchangeName)
	}

	genSnapsFactor := backtestConf.GeneratedSnapsFactor
	filled := exchangeSnaps.FillSnaps(snaps["Market"], exchangeName, "Market")
	return filled.InjectFakeSnaps(genSnapsFactor), nil
}

func getCtxManager(seed cryptomarketutil.MarketSnapshotList) *tradeutil.TradeContextManager {
	candlesticksLim := cfg.GetConfig().GetInt("daemons.trader.candlesticks_held")

	resp := exchange.GetCurrencies()
	fiatCurrencies = resp

	contextMgr := tradeutil.NewTradeContextManager(candlesticksLim, fiatCurrencies...)

	contextMgr.FeedHistoMarketSnaps(seed)

	return contextMgr
}

func emulateCompleteDecision(decision *model.TradeDecision, completedTime time.Time) {

	decision.NotifyStatus = modelenum.CompletedNotif.String()
	for i := 0; i < len(decision.Trades); i++ {
		trade := &decision.Trades[i]
		trade.FromAmountTrade = trade.FromAmountIntent
		trade.ToAmountTrade = trade.ToAmountIntent - backtestConf.DefaultTransactionFee*trade.ToAmountIntent
		trade.TradePrice = trade.PriceIntent
		trade.Status = modelenum.CompletedTrade.String()
		trade.TradeFinishTime = &completedTime
		trade.CreatedAt = completedTime
		trade.UpdatedAt = completedTime
	}
}

// Run takes a trader and runs it with histo data
func Run(trader contracts.Trader, fromTime, toTime time.Time, checkSnapData, checkDBData bool) (*Report, contracts.Trader, error) {
	var err error
	traderStats := trader.GetStats()

	err = loadCfg()
	if err != nil {
		return nil, nil, err
	}

	year := time.Hour * 24 * 365
	seedFromTime := fromTime.Add(-1 * year)

	snaps, err := loadSnaps(traderStats.Exchange, seedFromTime, toTime)
	if err != nil {
		return nil, nil, err
	}
	timeStamped := snaps.GetAsTimestampedEntries()
	firstSnapIdx := cryptomarketutil.GetClosestEntryIndexChronologically(timeStamped, fromTime)

	seedData := snaps[:firstSnapIdx]
	backtestData := snaps[firstSnapIdx:]

	ctxManager := getCtxManager(seedData)

	report := &Report{
		CheckSnapData: checkSnapData,
		CheckDB:       checkDBData,
	}

	trader.SetBackTestMode(true)
	var idx uint = 1

	for i := 0; i < len(backtestData); i++ {
		bucket := &backtestData[i]
		ctx := ctxManager.MakeContextAndStore(bucket)
		resp := trader.MakeDecision(*ctx)

		if resp == nil {
			continue
		}

		// emulate distinct ids
		for i := 0; i < len(resp.Decision.Trades); i++ {
			resp.Decision.Trades[i].SetID(idx)
			idx++
		}

		modelutil.AddTradeDecisionToLedger(resp.Decision, trader.GetLedger())
		emulateCompleteDecision(resp.Decision, bucket.GetTimestamp())
		modelutil.AddTradeDecisionToLedger(resp.Decision, trader.GetLedger())
		contracts.NotifyTraderWithProcessedDecision(trader, resp.Decision)
		report.AddDecision(resp.Decision, *ctx)
	}

	return report, trader, err
}

// RunTraderWithGUID runs the trader with the given trader guid
func RunTraderWithGUID(traderGUID string, fromTime, toTime time.Time, checkSnapData, checkDBData bool) (*Report, contracts.Trader, error) {
	trader, err := traderstore.GetInstance().GetTraderForBacktesting(fromTime, traderGUID)

	if err != nil {
		return nil, nil, err
	}

	return Run(trader, fromTime, toTime, checkSnapData, checkDBData)
}
