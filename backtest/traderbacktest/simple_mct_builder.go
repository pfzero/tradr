package traderbacktest

import (
	"math"

	"bitbucket.org/pfzero/tradr/repo/modelenum"
	"bitbucket.org/pfzero/tradr/trade/strategies/multicointrade"

	"bitbucket.org/pfzero/tradr/repo/contracts"
	"bitbucket.org/pfzero/tradr/repo/model"
)

// InvestSignalCfg contains the minimum and most relevant
// configuration to setup the investment signal for multi-coin trader
type InvestSignalCfg struct {
	TrendFormed float64
	TrendCandle float64
	TrendChange float64
}

// ProfitSignalCfg contains the minimum and most relevant
// configs needed to setup a profit signal for multi-coin-trader
type ProfitSignalCfg struct {
	PriceVariation float64
	TrendChange    float64
}

// InvestmentAmountCfg contains the needed configurations
// for simple multi-coin trader investment amount
type InvestmentAmountCfg struct {
	InvestmentPercent float64
	MaxInvestments    int
}

// SimpleMultiCoinCfg represents the datastructure that
// covers the majority of configuration needed to create
// a multi coin trader
type SimpleMultiCoinCfg struct {
	BasePortfolio   model.Portfolio
	TargetPortfolio model.Portfolio
	Exchange        model.CryptoExchange

	InvestSignal InvestSignalCfg
	ProfitSignal ProfitSignalCfg
	InvestAmount InvestmentAmountCfg

	MinTradeFiatAmount float64
}

// BuildMultiCoinTrader builds the multi coin trader
func (smcc *SimpleMultiCoinCfg) BuildMultiCoinTrader() (contracts.Trader, error) {
	mod := &model.MultiCoinTradeCfg{
		ExchangeID:           smcc.Exchange.GetID(),
		Exchange:             smcc.Exchange,
		MinimumFiatAmount:    smcc.MinTradeFiatAmount,
		BasePortfolio:        smcc.BasePortfolio,
		TargetPortfolio:      smcc.TargetPortfolio,
		InvCondition:         smcc.getInvestCond(),
		ProfitCondition:      smcc.getProfitCond(),
		InvCooldownCondition: smcc.getCdCondition(),
		TradeTrendStepCol:    smcc.getTrendTradeSteps(),
		State:                modelenum.TradeBotRunningState.String(),
	}

	trader, err := multicointrade.NewMultiCoinTrader(mod)
	if err != nil {
		return nil, err
	}

	return trader, nil
}

func (smcc *SimpleMultiCoinCfg) getInvestCond() model.TradeCondition {
	cond := &model.TradeCondition{}
	if smcc.InvestSignal.TrendFormed != 0 {
		cond.WaitTrendFormation(smcc.InvestSignal.TrendFormed)
	}

	if smcc.InvestSignal.TrendChange != 0 {
		cond.
			SetNoise(smcc.InvestSignal.TrendChange).
			WaitTrendChange()
	}

	if smcc.InvestSignal.TrendCandle != 0 {
		cond.WaitForCandle(smcc.InvestSignal.TrendCandle)
	}

	return *cond
}

func (smcc *SimpleMultiCoinCfg) getProfitCond() model.TradeCondition {
	cond := &model.TradeCondition{}

	if smcc.ProfitSignal.TrendChange != 0 {
		cond.SetNoise(smcc.ProfitSignal.TrendChange).
			WaitTrendChange()
	}

	if smcc.ProfitSignal.PriceVariation != 0 {
		cond.SetPriceVariation(smcc.ProfitSignal.PriceVariation)
	}

	return *cond
}

func (smcc *SimpleMultiCoinCfg) getCdCondition() model.TradeCondition {
	cond := &model.TradeCondition{}

	relevantInv := smcc.InvestSignal.TrendFormed
	if smcc.InvestSignal.TrendCandle != 0 {
		relevantInv = smcc.InvestSignal.TrendCandle
	}
	relevantProfitInv := smcc.ProfitSignal.PriceVariation

	relevantInv = math.Abs(relevantInv) / 2
	relevantProfitInv = math.Abs(relevantProfitInv)

	cond.SetPriceVariation(-relevantInv).SetPriceVariation(relevantProfitInv)

	return *cond
}

func (smcc *SimpleMultiCoinCfg) getTrendTradeSteps() model.TradeTrendStepCol {
	steps := []model.TradeTrendStep{}

	steps = append(steps, model.TradeTrendStep{
		AmountPercentage: smcc.InvestAmount.InvestmentPercent,
		SameTrendLevel:   1,
	})

	for i := 1; i < smcc.InvestAmount.MaxInvestments; i++ {
		steps = append(steps, model.TradeTrendStep{
			AmountPercentage: smcc.InvestAmount.InvestmentPercent,
			SameTrendLevel:   1,
		})
	}

	col := &model.TradeTrendStepCol{
		TradeTrendSteps: steps,
	}

	if smcc.InvestAmount.MaxInvestments <= 0 {
		col.CycleSteps = true
	}

	return *col
}
