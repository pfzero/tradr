package traderbacktest

import (
	"fmt"

	"bitbucket.org/pfzero/tradr/store/datastore/cryptomarketstore"

	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/util/tradeutil"
)

// make sure we don't get too strict
// when working with floats
const margin = 0.001

// TradeError represents the data structure
// that pairs a trade with a potential trade error
type TradeError struct {
	Trade model.Trade
	Error error
}

// DecisionReport encapsulates a trade decision which then
// checks it to be correct
type DecisionReport struct {
	TradeDecision model.TradeDecision
	TradeContext  tradeutil.TradeContext

	LiveDataProblems   []TradeError
	StoredDataProblems []TradeError
}

func (dr *DecisionReport) hasLiveDataProblems() bool {
	return len(dr.LiveDataProblems) > 0
}

func (dr *DecisionReport) hasStoredDataProblems() bool {
	return len(dr.StoredDataProblems) > 0
}

func (dr *DecisionReport) check(checkSnapData, checkDB bool) {
	var liveDataProblems = []TradeError{}
	var storedDataProblems = []TradeError{}

	if checkDB {
		storedDataProblems = dr.getAgainstDB()
	}

	if checkSnapData {
		liveDataProblems = dr.getAgainstSnapshot()
	}

	dr.LiveDataProblems = liveDataProblems
	dr.StoredDataProblems = storedDataProblems
}

func (dr *DecisionReport) getAgainstSnapshot() []TradeError {
	problems := []TradeError{}

	for _, trade := range dr.TradeDecision.Trades {
		fsym := trade.From.Symbol
		tsym := trade.To.Symbol
		exchange := trade.Exchange.Name
		price, err := dr.TradeContext.GetPrice(fsym, tsym, exchange)
		if err != nil {
			problems = append(problems, TradeError{
				Trade: trade,
				Error: fmt.Errorf("Couldn't fetched the price of %s-%s@%s; Err was: %s", fsym, tsym, exchange, err.Error()),
			})
			continue
		}

		p := price.GetPrice()
		maxAccepted := p + margin*p
		minAccepted := p - margin*p

		if trade.PriceIntent > maxAccepted || trade.PriceIntent < minAccepted {
			problems = append(problems, TradeError{
				Trade: trade,
				Error: fmt.Errorf("LiveData: Price of %f for %s-%s@%s @%s is incorrect. The correct price is: %f",
					trade.PriceIntent,
					fsym,
					tsym,
					exchange,
					dr.TradeContext.GetTimestamp(),
					p,
				),
			})
		}
	}

	return problems
}

func (dr *DecisionReport) getAgainstDB() []TradeError {
	problems := []TradeError{}

	for _, trade := range dr.TradeDecision.Trades {
		fsym := trade.From.Symbol
		tsym := trade.To.Symbol
		exchange := trade.Exchange.Name
		t := dr.TradeContext.GetTimestamp()
		fprice := 0.00
		tprice := 0.00
		if isFiatCurrency(fsym) {
			fprice = 1
		}
		if isFiatCurrency(tsym) {
			tprice = 1
		}
		if fprice == 0 {
			dbFPrice, err := cryptomarketstore.GetPriceClosestToBySymAndExchange(fsym, exchange, t)
			if err != nil {
				problems = append(problems, TradeError{
					Trade: trade,
					Error: fmt.Errorf("Couldn't fetch price for %s@%s at time: %s; Err was: %s", fsym, exchange, t, err.Error()),
				})
				continue
			}
			fprice = dbFPrice.GetPrice()
		}
		if tprice == 0 {
			dbTPrice, err := cryptomarketstore.GetPriceClosestToBySymAndExchange(tsym, exchange, t)
			if err != nil {
				problems = append(problems, TradeError{
					Trade: trade,
					Error: fmt.Errorf("Couldn't fetch price for %s@%s at time: %s; Err was: %s", tsym, exchange, t, err.Error()),
				})
				continue
			}
			tprice = dbTPrice.GetPrice()
		}

		conversionResult := fprice / tprice

		maxAccepted := conversionResult + margin*conversionResult
		minAccepted := conversionResult - margin*conversionResult

		if trade.PriceIntent > maxAccepted || trade.PriceIntent < minAccepted {
			problems = append(problems, TradeError{
				Trade: trade,
				Error: fmt.Errorf(
					"StoredData: Price of %f for %s-%s@%s @%s is incorrect. The correct price is: %f",
					trade.PriceIntent,
					fsym,
					tsym,
					exchange,
					t,
					conversionResult,
				),
			})
		}
	}

	return problems
}

// Report evaluates all the decisions made by a trader
// and returns the problems encountered
type Report struct {
	CheckSnapData bool
	CheckDB       bool

	decisionReports []DecisionReport

	problematicDecisions []int
}

// GetDecisionReports returns the list of decisions
func (cr *Report) GetDecisionReports() []DecisionReport {
	return cr.decisionReports
}

// HasProblems checks wether there's any decision that has problems
func (cr *Report) HasProblems() bool {
	return len(cr.problematicDecisions) > 0
}

// GetProblematicDecisions returns the list of decisions that have problems
func (cr *Report) GetProblematicDecisions() []DecisionReport {
	problematicDecisions := []DecisionReport{}
	for i := 0; i < len(cr.problematicDecisions); i++ {
		problematicIdx := cr.problematicDecisions[i]
		problematicDecisions = append(problematicDecisions, cr.decisionReports[problematicIdx])
	}
	return problematicDecisions
}

// AddDecision adds a new decision to the report
func (cr *Report) AddDecision(decision *model.TradeDecision, ctx tradeutil.TradeContext) {
	dr := DecisionReport{
		TradeContext:  ctx,
		TradeDecision: *decision,
	}

	dr.check(cr.CheckSnapData, cr.CheckDB)

	cr.decisionReports = append(cr.decisionReports, dr)
	if dr.hasLiveDataProblems() || dr.hasStoredDataProblems() {
		cr.problematicDecisions = append(cr.problematicDecisions, len(cr.decisionReports)-1)
	}
}
