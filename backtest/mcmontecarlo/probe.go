package mcmontecarlo

import (
	"math"
	"time"

	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"

	"bitbucket.org/pfzero/tradr/repo/contracts"
	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/repo/modelenum"
	"bitbucket.org/pfzero/tradr/trade/strategies/multicointrade"
)

// InvestCfg represents the investment configuration
// for the trader probe
type InvestCfg struct {
	TrendFormed float64
	TrendCandle float64
	TrendChange float64
}

// ProfitCfg represents the profit condition config
type ProfitCfg struct {
	PriceVariation float64
	TrendChange    float64
}

// ProbeCfg represents the trader's main configuration
type ProbeCfg struct {
	Invest InvestCfg
	Profit ProfitCfg
}

// GetCooldownVariation returns the price variation up / down for
// this specific probe
func (pc ProbeCfg) GetCooldownVariation() *cryptomarketutil.Variation {
	relevantInvestmentSignal := pc.Invest.TrendFormed
	if relevantInvestmentSignal == 0 {
		relevantInvestmentSignal = pc.Invest.TrendCandle
	}

	relevantProfitSignal := pc.Profit.PriceVariation
	relevantInvestmentSignal = math.Abs(relevantInvestmentSignal) / 2

	return &cryptomarketutil.Variation{
		Up:   math.Abs(relevantProfitSignal),
		Down: -relevantInvestmentSignal,
	}
}

// TraderProbe represents a trader that will be used for monte-carlo
// symmulation
type TraderProbe struct {
	FSym              model.PortfolioAsset
	TSym              model.PortfolioAsset
	Exchange          model.CryptoExchange
	InitialInvestment float64

	TraderConfig ProbeCfg
	Trader       contracts.Trader
}

// BuildTrader builds the trader
func (tp *TraderProbe) BuildTrader() error {
	mod := &model.MultiCoinTradeCfg{
		ExchangeID:           tp.Exchange.GetID(),
		Exchange:             tp.Exchange,
		MinimumFiatAmount:    mcMonteCarloConfig.MinTradeAmount,
		TradeTrendStepCol:    tp.getTradeTrendStepsCol(),
		InvCooldownCondition: tp.getCdCond(),
		InvCondition:         tp.getInvestCond(),
		ProfitCondition:      tp.getProfitCond(),
		BasePortfolio: model.Portfolio{
			PortfolioAssets: []model.PortfolioAsset{
				tp.FSym,
			},
		},
		TargetPortfolio: model.Portfolio{
			PortfolioAssets: []model.PortfolioAsset{tp.TSym},
		},
		State: modelenum.TradeBotRunningState.String(),
	}

	trader, err := multicointrade.NewMultiCoinTrader(mod)
	if err != nil {
		return err
	}
	l := trader.GetLedger()
	l.AddLedgerIO(&model.LedgerIO{
		Identity: model.Identity{
			CreatedAt: time.Time{},
			UpdatedAt: time.Time{},
		},
		Type:       modelenum.Deposit.String(),
		Asset:      tp.FSym.TradeableAsset,
		Exchange:   tp.Exchange,
		ExchangeID: tp.Exchange.GetID(),
		Amount:     tp.InitialInvestment,
	})
	tp.Trader = trader

	return nil
}

func (tp *TraderProbe) getTradeTrendStepsCol() model.TradeTrendStepCol {
	return model.TradeTrendStepCol{
		CycleSteps: false,
		TradeTrendSteps: []model.TradeTrendStep{
			model.TradeTrendStep{
				SameTrendLevel:   1,
				AmountPercentage: 1,
			},
		},
	}
}

func (tp *TraderProbe) getInvestCond() model.TradeCondition {
	cond := &model.TradeCondition{}
	if tp.TraderConfig.Invest.TrendFormed != 0 {
		cond.WaitTrendFormation(tp.TraderConfig.Invest.TrendFormed)
	}
	if tp.TraderConfig.Invest.TrendChange != 0 {
		cond.
			SetNoise(tp.TraderConfig.Invest.TrendChange).
			WaitTrendChange()
	}

	if tp.TraderConfig.Invest.TrendCandle != 0 {
		cond.WaitForCandle(tp.TraderConfig.Invest.TrendCandle)
	}

	return *cond
}

func (tp *TraderProbe) getCdCond() model.TradeCondition {
	cond := &model.TradeCondition{}

	variation := tp.TraderConfig.GetCooldownVariation()

	cond.SetPriceVariation(variation.Down).SetPriceVariation(variation.Up)

	return *cond
}

func (tp *TraderProbe) getProfitCond() model.TradeCondition {
	cond := &model.TradeCondition{}

	if tp.TraderConfig.Profit.PriceVariation != 0 {
		cond.SetPriceVariation(tp.TraderConfig.Profit.PriceVariation)
	}

	if tp.TraderConfig.Profit.TrendChange != 0 {
		cond.SetNoise(tp.TraderConfig.Profit.TrendChange).
			WaitTrendChange()
	}

	return *cond
}
