package mcmontecarlo

import (
	"math"
	"sort"
)

// ProbeResultsComparator is used as a function to sort the list of
// probe results;
// it should respond to question: Is a < b?
type ProbeResultsComparator func(a, b *ProbeResult) bool

// Iterator is the function needed to iterate through
// the elements of ProbeResultList
type Iterator func(a *ProbeResult)

// Predicate represents the iterator function that is used
// to filter / find elements within the probe result list
type Predicate func(a *ProbeResult) bool

// ProbeResultList represents a list of TraderStats
type ProbeResultList struct {
	FSym        string
	TSym        string
	Exchange    string
	InitialFiat float64

	comparator ProbeResultsComparator
	results    []ProbeResult
}

// Len is used to determine the length of the list
func (prl *ProbeResultList) Len() int {
	return len(prl.results)
}

// Swap is used to swap 2 items in the list of probe results
func (prl *ProbeResultList) Swap(i, j int) {
	prl.results[i], prl.results[j] = prl.results[j], prl.results[i]
}

// Less is used to compare the probe results at the given indexes
func (prl *ProbeResultList) Less(i, j int) bool {
	a := &prl.results[i]
	b := &prl.results[j]
	return prl.comparator(a, b)
}

// Add adds a new probe result to the list
func (prl *ProbeResultList) Add(res *ProbeResult) *ProbeResultList {
	prl.results = append(prl.results, *res)
	return prl
}

// ForEach is used to iterate over the elements from the list
func (prl *ProbeResultList) ForEach(fn Iterator) *ProbeResultList {
	for i := 0; i < len(prl.results); i++ {
		el := &prl.results[i]
		fn(el)
	}
	return prl
}

// Merge merges the given chunk of probe results in the current probe result list
func (prl *ProbeResultList) Merge(chunk *ProbeResultList) *ProbeResultList {
	prl.results = append(prl.results, chunk.results...)
	return prl
}

// SortDescByWeightedEfficiency sorts the list by weighted efficiency score
// in desc order
func (prl *ProbeResultList) SortDescByWeightedEfficiency() *ProbeResultList {
	return prl.SortBy(func(a, b *ProbeResult) bool {
		// reverse sort
		return b.GetWeightedEfficiencyScore() < a.GetWeightedEfficiencyScore()
	})
}

// SortDescByWeightedProfit sorts the list by weighted profit score in descending
// order
func (prl *ProbeResultList) SortDescByWeightedProfit() *ProbeResultList {
	return prl.SortBy(func(a, b *ProbeResult) bool {
		return b.GetWeightedProfit() < a.GetWeightedProfit()
	})
}

// SortDescByLastWindowProfit sorts the results list by profit made on the last window
// of time
func (prl *ProbeResultList) SortDescByLastWindowProfit() *ProbeResultList {
	return prl.SortBy(func(a, b *ProbeResult) bool {
		lastWindowForA := len(a.WindowedResults) - 1
		lastWindowForB := len(b.WindowedResults) - 1
		return b.GetWindowProfit(lastWindowForB) < a.GetWindowProfit(lastWindowForA)
	})
}

// SortBy sorts the list given comparator function
func (prl *ProbeResultList) SortBy(fn ProbeResultsComparator) *ProbeResultList {
	prl.comparator = fn
	sort.Stable(prl)
	prl.comparator = nil
	return prl
}

// BatchCount returns the nunmber of batches that result
// after slicing the current list in lists of given size `s`
func (prl *ProbeResultList) BatchCount(s int) int {
	batches := float64(prl.Len()) / float64(s)
	ceiled := math.Ceil(batches)
	return int(ceiled)
}

// Batch slices the current list of probe results and returns the batch with
// index `batchIndex` of size `s`
// batch of given size `s`
func (prl *ProbeResultList) Batch(s, batchIndex int) *ProbeResultList {
	if s == 0 {
		s = 100 // default batch size
	}

	if batchIndex == 0 || batchIndex > prl.BatchCount(s) {
		return nil
	}

	var slice []ProbeResult

	if batchIndex*s > prl.Len() {
		slice = prl.results[(batchIndex-1)*s:]
	} else {
		slice = prl.results[(batchIndex-1)*s : batchIndex*s]
	}

	return &ProbeResultList{
		FSym:        prl.FSym,
		TSym:        prl.TSym,
		Exchange:    prl.Exchange,
		InitialFiat: prl.InitialFiat,
		results:     slice,
	}
}

// Filter returns a subset of the list containing all the elements
// that satisfy the given predicate function
func (prl *ProbeResultList) Filter(fn Predicate) *ProbeResultList {
	cp := *prl
	subSet := &cp
	subSet.results = []ProbeResult{}

	prl.ForEach(func(pr *ProbeResult) {
		if fn(pr) {
			subSet.Add(pr)
		}
	})

	return subSet
}

// Find finds the record that satisfies the predicate function
func (prl *ProbeResultList) Find(fn Predicate) *ProbeResult {
	for i := range prl.results {
		pr := &prl.results[i]
		if fn(pr) {
			return pr
		}
	}
	return nil
}
