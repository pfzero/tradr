package mcmontecarlo

import (
	"time"
)

// WindowedResult represents the results of a trader
// per a window of time
type WindowedResult struct {
	FromTime        time.Time
	ToTime          time.Time
	InvestmentsMade int
	ProfitsMade     int
	InitialAmt      float64
	FinalAmount     float64
}

// GetProfit returns the profit made within the current window of time
func (wr *WindowedResult) GetProfit() float64 {
	return (wr.FinalAmount - wr.InitialAmt) / wr.InitialAmt
}

// GetEfficiency returns the efficiency score obtained within
// the current window of time
func (wr *WindowedResult) GetEfficiency() float64 {
	return wr.GetProfit() / float64(wr.InvestmentsMade)
}

// ProbeResult represents the results of a trader per the
// whole backtested timespan
type ProbeResult struct {
	ProbeCfg        ProbeCfg
	WindowedResults []WindowedResult
}

// GetTotalInvestments returns the number of investments made
// on the whole timespan
func (pr *ProbeResult) GetTotalInvestments() int {
	accum := 0
	for i := range pr.WindowedResults {
		res := pr.WindowedResults[i]
		accum += res.InvestmentsMade
	}
	return accum
}

// GetTotalProfits returns the number of total profits made within
// the whole timespan
func (pr *ProbeResult) GetTotalProfits() int {
	accum := 0
	for i := range pr.WindowedResults {
		res := pr.WindowedResults[i]
		accum += res.ProfitsMade
	}
	return accum
}

// GetAvgScore computes the avg score for all the existing windowed results given
// a function that indicates a score per window
func (pr *ProbeResult) GetAvgScore(fn func(wr *WindowedResult) float64) float64 {
	sum := 0.00
	for i := range pr.WindowedResults {
		wr := &pr.WindowedResults[i]
		sum += fn(wr)
	}

	return sum / float64(len(pr.WindowedResults))
}

// GetWeightedScore computes the weighted score for all the existing windowed results
func (pr *ProbeResult) GetWeightedScore(fn func(wr *WindowedResult) float64) float64 {
	sum := 0.00
	cnt := 0.00
	for i := range pr.WindowedResults {
		wr := &pr.WindowedResults[i]
		score := fn(wr)
		multiplier := float64(i + 1)
		sum += multiplier * score
		cnt += multiplier
	}
	return sum / cnt
}

// GetAvgProfit returns the average profit made per each window of time
func (pr *ProbeResult) GetAvgProfit() float64 {
	return pr.GetAvgScore(func(wr *WindowedResult) float64 {
		return wr.GetProfit()
	})
}

// GetWeightedProfit returns the weighted profit made by the trader within the whole
// timespan; The weight starts from 1 and is incremented per each window;
func (pr *ProbeResult) GetWeightedProfit() float64 {
	return pr.GetWeightedScore(func(wr *WindowedResult) float64 {
		return wr.GetProfit()
	})
}

// GetAvgEfficiencyScore returns the average efficiency score per all
// the existing windows of time
func (pr *ProbeResult) GetAvgEfficiencyScore() float64 {
	return pr.GetAvgScore(func(wr *WindowedResult) float64 {
		return wr.GetEfficiency()
	})
}

// GetWeightedEfficiencyScore returns the weighted efficiency score per all
// the existing windows of time
func (pr *ProbeResult) GetWeightedEfficiencyScore() float64 {
	return pr.GetWeightedScore(func(wr *WindowedResult) float64 {
		return wr.GetEfficiency()
	})
}

// GetWindowProfit returns the profit made for the requested window
// index
func (pr *ProbeResult) GetWindowProfit(windowIdx int) float64 {
	if windowIdx < 0 || windowIdx >= len(pr.WindowedResults) {
		return 0
	}

	wr := &pr.WindowedResults[windowIdx]
	return wr.GetProfit()
}

// GetTotalFinalAmt returns the final amount accumulated if the trader were to be run
// in the same manner as in the monte-carlo simulation (ie. at windowed intervals of time
// and restart at the beginning of each interval)
func (pr *ProbeResult) GetTotalFinalAmt() float64 {
	finalAmt := pr.WindowedResults[0].FinalAmount
	for i := 1; i < len(pr.WindowedResults); i++ {
		res := pr.WindowedResults[i]
		windowProfit := (res.FinalAmount - res.InitialAmt) / res.InitialAmt
		finalAmt += (finalAmt * windowProfit)
	}
	return finalAmt
}
