package mcmontecarlo

import "math"

// ProbeListFilterFn represents the function used
// for filtering / finding elements in probelist
type ProbeListFilterFn func(tp *TraderProbe) bool

// ProbeListIteratorFn is the function signature used to iterate
// over the elements from probelist
type ProbeListIteratorFn func(tp *TraderProbe)

// ProbeList represents a list of trade probes
type ProbeList struct {
	list []TraderProbe
}

// Add adds a new trader probe to the list
func (pl *ProbeList) Add(probe TraderProbe) *ProbeList {
	pl.list = append(pl.list, probe)
	return pl
}

// GetAll returns the underlying list of trader probes
func (pl *ProbeList) GetAll() []TraderProbe {
	return pl.list
}

// Len returns the length of the probelist
func (pl *ProbeList) Len() int {
	return len(pl.list)
}

// Filter filters the list based on the given filter fn
func (pl *ProbeList) Filter(fn ProbeListFilterFn) *ProbeList {
	accum := []TraderProbe{}
	for i := range pl.list {
		item := &pl.list[i]
		if fn(item) {
			accum = append(accum, *item)
		}
	}

	return &ProbeList{list: accum}
}

// Find finds the first tradeprobe that satisfies the filter
// fn
func (pl *ProbeList) Find(fn ProbeListFilterFn) *TraderProbe {
	for i := range pl.list {
		item := &pl.list[i]
		if fn(item) {
			return item
		}
	}
	return nil
}

// ForEach is used to iterate over the all trade probes in the list
func (pl *ProbeList) ForEach(fn ProbeListIteratorFn) *ProbeList {
	for i := range pl.list {
		item := &pl.list[i]
		fn(item)
	}
	return pl
}

// BatchCount returns the nunmber of batches that result
// after slicing the current list in lists of given size
func (pl *ProbeList) BatchCount(size int) int {
	batches := float64(pl.Len()) / float64(size)
	ceiled := math.Ceil(batches)
	return int(ceiled)
}

// Batch is used to slice the list into batches of fixed size and
// returns the batch
func (pl *ProbeList) Batch(size, batchNumber int) *ProbeList {
	if batchNumber == 0 || batchNumber > pl.BatchCount(size) {
		return nil
	}

	slice := []TraderProbe{}

	if batchNumber*size > pl.Len() {
		slice = pl.list[(batchNumber-1)*size:]
	} else {
		slice = pl.list[(batchNumber-1)*size : batchNumber*size]
	}

	cp := make([]TraderProbe, len(slice))
	copy(cp, slice)

	return &ProbeList{list: cp}
}
