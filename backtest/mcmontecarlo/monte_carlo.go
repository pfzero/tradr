package mcmontecarlo

import (
	"fmt"
	"runtime"
	"strings"
	"time"

	"bitbucket.org/pfzero/tradr/backtest/backtestcfg"

	"bitbucket.org/pfzero/tradr/cfg"
	"bitbucket.org/pfzero/tradr/util/tradeutil"

	"bitbucket.org/pfzero/tradr/store/datastore/cryptomarketstore"
	"github.com/pkg/errors"

	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"

	"bitbucket.org/pfzero/tradr/repo/contracts"
	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/repo/modelenum"
	"bitbucket.org/pfzero/tradr/repo/modelutil"
)

const (
	usd            = "USD" // USD fiat currency, used to inject in trade context manager;
	marketExchange = "Market"
)

var mcMonteCarloConfig *backtestcfg.MultiCoinMonteCarloConfig

func loadConfig() error {
	if mcMonteCarloConfig != nil {
		return nil
	}

	c, err := backtestcfg.GetMultiCoinMonteCarloConfig()
	if err != nil {
		return err
	}
	mcMonteCarloConfig = c
	return nil
}

func ensureRuntimeForParalleBatches(parallelBatches int) {
	currentMaxProcs := runtime.GOMAXPROCS(0)
	if currentMaxProcs < parallelBatches {
		runtime.GOMAXPROCS(parallelBatches)
	}
}

// RangeCfg provides range configurations
// for all the signals
type RangeCfg struct {
	TrendFormedRange    float64
	TrendChangeRange    float64
	TrendCandleRange    float64
	PriceVariationRange float64
}

// InvSignalCfg represents the investment signal
// configuration range
type InvSignalCfg struct {
	TrendFormedMin float64
	TrendFormedMax float64
	TrendCandleMin float64
	TrendCandleMax float64
	TrendChangeMin float64
	TrendChangeMax float64
}

// ProfitSignalCfg represents the configuration
// for profit signal
type ProfitSignalCfg struct {
	PriceVarMin    float64
	PriceVarMax    float64
	TrendChangeMin float64
	TrendChangeMax float64
}

// BuilderConfiguration represents the configuration needed
// to generate monte-carlo simulations for multi coin trader
type BuilderConfiguration struct {
	FromTime                time.Time
	ToTime                  time.Time
	InitialFiatAmount       float64
	UseWindowedTime         bool
	WindowedTimeMonthsCount int
	BasePortfolio           model.Portfolio
	TargetPortfolio         model.Portfolio
	Exchange                model.CryptoExchange

	InvestmentSignal    InvSignalCfg
	ProfitSignal        ProfitSignalCfg
	RangeConfigurations RangeCfg
}

// MultiCoinMonteCarlo is used to generate multi-coin trader
// configurations and run them accross a range of configurations
type MultiCoinMonteCarlo struct {
	cfg *BuilderConfiguration

	pairsToRunLeft  map[string]struct{}
	totalPairsToRun int

	seed       cryptomarketutil.MarketSnapshotList
	snapSlices []cryptomarketutil.MarketSnapshotList
}

// NewMultiCoinMonteCarloSimulator returns a new MultiCoinMonteCarlo data-structure that is
// capable of running monte-carlo simulations on multi-coin trader
func NewMultiCoinMonteCarloSimulator(cfg *BuilderConfiguration) (*MultiCoinMonteCarlo, error) {

	e := loadConfig()
	if e != nil {
		return nil, e
	}

	ensureRuntimeForParalleBatches(mcMonteCarloConfig.ParallelBatches)

	pairsToRun := map[string]struct{}{}

	for i := range cfg.BasePortfolio.PortfolioAssets {
		baseAsset := cfg.BasePortfolio.PortfolioAssets[i].TradeableAsset
		for j := range cfg.TargetPortfolio.PortfolioAssets {
			targetAsset := cfg.TargetPortfolio.PortfolioAssets[j].TradeableAsset
			if baseAsset.Symbol == targetAsset.Symbol {
				continue
			}

			pairsToRun[fmt.Sprintf("%s::%s", baseAsset.Symbol, targetAsset.Symbol)] = struct{}{}
		}
	}

	mcmc := &MultiCoinMonteCarlo{
		cfg:             cfg,
		pairsToRunLeft:  pairsToRun,
		totalPairsToRun: len(pairsToRun),
	}

	var err error
	err = mcmc.loadBacktestData()
	if err != nil {
		return nil, err
	}

	return mcmc, nil
}

// GetProgress returns the progress of the simulator so far
func (mcmc *MultiCoinMonteCarlo) GetProgress() float64 {
	remaining := len(mcmc.pairsToRunLeft)
	total := mcmc.totalPairsToRun

	finished := total - remaining
	progress := float64(finished) / float64(total)
	return progress
}

// GetTotalPairsCount returns the total number of pairs
func (mcmc *MultiCoinMonteCarlo) GetTotalPairsCount() int {
	return mcmc.totalPairsToRun
}

// GetRemainingPairsCount returns the remaining pairs count
func (mcmc *MultiCoinMonteCarlo) GetRemainingPairsCount() int {
	return len(mcmc.pairsToRunLeft)
}

// Next loads a new pair to run, runs all the configurations per that pair
// and returns the results
func (mcmc *MultiCoinMonteCarlo) Next() (*ProbeResultList, error) {

	var currentPair string
	for p := range mcmc.pairsToRunLeft {
		currentPair = p
		break
	}
	if currentPair == "" {
		return nil, nil
	}

	cpy := *mcmc.cfg
	cfgCopy := &cpy

	symbols := strings.Split(currentPair, "::")
	fsym, tsym := symbols[0], symbols[1]

	var fromAsset, toAsset model.PortfolioAsset

	for i := range mcmc.cfg.BasePortfolio.PortfolioAssets {
		asset := mcmc.cfg.BasePortfolio.PortfolioAssets[i].TradeableAsset
		if asset.Symbol == fsym {
			fromAsset = mcmc.cfg.BasePortfolio.PortfolioAssets[i]
			break
		}
	}

	for i := range mcmc.cfg.TargetPortfolio.PortfolioAssets {
		asset := mcmc.cfg.TargetPortfolio.PortfolioAssets[i].TradeableAsset
		if asset.Symbol == tsym {
			toAsset = mcmc.cfg.TargetPortfolio.PortfolioAssets[i]
			break
		}
	}

	cfgCopy.BasePortfolio = model.Portfolio{
		PortfolioAssets: []model.PortfolioAsset{
			fromAsset,
		},
	}

	cfgCopy.TargetPortfolio = model.Portfolio{
		PortfolioAssets: []model.PortfolioAsset{toAsset},
	}

	probeList, err := BuildProbes(cfgCopy)
	if err != nil {
		return nil, err
	}

	backTestData := mcmc.getBacktestDataForSymbols(fsym, tsym)

	type tuple struct {
		e error
		r *ProbeResultList
	}

	take := make(chan struct{}, mcMonteCarloConfig.ParallelBatches)
	put := make(chan tuple, mcMonteCarloConfig.ParallelBatches)

	go func() {
		currentBatchCount := 1
		for {
			<-take
			batch := probeList.Probes.Batch(mcMonteCarloConfig.BatchSize, currentBatchCount)
			currentBatchCount++

			if batch == nil {
				put <- tuple{e: nil, r: nil}
				continue
			}

			go func(b *ProbeList) {
				results, err := mcmc.runProbeList(b, backTestData.seed, backTestData.snapSlices)
				t := tuple{
					e: err,
					r: results,
				}
				put <- t
			}(batch)
		}
	}()

	putCnt := mcMonteCarloConfig.ParallelBatches
	for i := 0; i < putCnt; i++ {
		take <- struct{}{}
	}

	res := &ProbeResultList{
		Exchange:    mcmc.cfg.Exchange.Name,
		FSym:        fsym,
		TSym:        tsym,
		InitialFiat: mcmc.cfg.InitialFiatAmount,
		results:     []ProbeResult{},
	}

	var batchErr error
	for putCnt > 0 {
		took := <-put
		putCnt--

		// skip
		if took.r == nil && took.e == nil {
			continue
		}

		// if an err was encountered, wait until
		// everything is drained and return the error
		if batchErr != nil {
			continue
		}

		if took.e != nil {
			// there should be one more tuple to drain,
			// so wait for it
			batchErr = took.e
			continue
		}

		res = res.Merge(took.r)
		take <- struct{}{}
		putCnt++
	}

	delete(mcmc.pairsToRunLeft, currentPair)

	return res, nil
}

func (mcmc *MultiCoinMonteCarlo) getBacktestDataForSymbols(symbols ...string) struct {
	seed       cryptomarketutil.MarketSnapshotList
	snapSlices []cryptomarketutil.MarketSnapshotList
} {
	ret := &struct {
		seed       cryptomarketutil.MarketSnapshotList
		snapSlices []cryptomarketutil.MarketSnapshotList
	}{}

	idx := mcmc.seed.GetFirstSnapIndexForSymbols(mcmc.cfg.Exchange.Name, symbols...)
	if idx == -1 {
		ret.seed = cryptomarketutil.MarketSnapshotList{}
	} else {
		ret.seed = mcmc.seed[idx:]
	}

	snapSlices := []cryptomarketutil.MarketSnapshotList{}
	nextIdx := 0
	for i := range mcmc.snapSlices {
		slice := mcmc.snapSlices[i]
		idx := slice.GetFirstSnapIndexForSymbols(mcmc.cfg.Exchange.Name, symbols...)
		if idx == -1 {
			continue
		}

		snapSlices = append(snapSlices, slice[idx:])
		nextIdx = i + 1
		break
	}

	for ; nextIdx < len(mcmc.snapSlices); nextIdx++ {
		slice := mcmc.snapSlices[nextIdx]
		snapSlices = append(snapSlices, slice)
	}

	ret.snapSlices = snapSlices

	return *ret
}

func (mcmc *MultiCoinMonteCarlo) loadBacktestData() error {

	var fromTime, toTime = mcmc.cfg.FromTime, mcmc.cfg.ToTime

	if toTime.IsZero() {
		toTime = time.Now()
	}

	seedStartTime := fromTime.AddDate(-1, 0, 0)

	allSnaps, err := mcmc.getSnaps(seedStartTime, toTime, mcmc.cfg.Exchange.Name)
	if err != nil {
		return err
	}

	timestampedList := allSnaps.GetAsTimestampedEntries()
	firstSnapIdx := cryptomarketutil.GetClosestEntryIndexChronologically(timestampedList, fromTime)

	seedSnaps := allSnaps[:firstSnapIdx]
	backTestData := allSnaps[firstSnapIdx:]

	mcmc.seed = seedSnaps

	windowSize := mcmc.cfg.WindowedTimeMonthsCount
	if !mcmc.cfg.UseWindowedTime {
		windowSize = 0
	}

	slices := mcmc.getSnapSlices(backTestData, windowSize)

	mcmc.snapSlices = slices

	return nil
}

func (mcmc *MultiCoinMonteCarlo) getSnaps(fromTime, toTime time.Time, exchange string) (cryptomarketutil.MarketSnapshotList, error) {
	if toTime.IsZero() {
		toTime = time.Now()
	}

	marketSnaps, err := cryptomarketstore.GetHistoMarketSnapshots(fromTime, toTime)
	if err != nil {
		return nil, err
	}

	snaps, ok := marketSnaps[exchange]
	if !ok {
		return nil, errors.Errorf("No histo snaps found for period: %s - %s for %s", fromTime, toTime, mcmc.cfg.Exchange.Name)
	}

	genSnapsFactor := mcMonteCarloConfig.GeneratedSnapsFactor
	return snaps.FillSnaps(marketSnaps["Market"], exchange, "Market").InjectFakeSnaps(genSnapsFactor), nil
}

func (mcmc *MultiCoinMonteCarlo) getSnapSlices(snaps []cryptomarketutil.MarketSnapshot, windowSize int) []cryptomarketutil.MarketSnapshotList {
	if windowSize <= 0 {
		return []cryptomarketutil.MarketSnapshotList{snaps}
	}

	list := cryptomarketutil.MarketSnapshotList(snaps)
	timestampedList := list.GetAsTimestampedEntries()

	firstSnap := snaps[0]
	lastSnap := snaps[len(snaps)-1]
	firstTimeStamp := firstSnap.GetTimestamp()
	lastTimeStamp := lastSnap.GetTimestamp()

	results := []cryptomarketutil.MarketSnapshotList{}

	currentTimestamp := firstTimeStamp.AddDate(0, windowSize, 0)
	prevIdx := 0
	for !currentTimestamp.After(lastTimeStamp) {
		currentIdx := cryptomarketutil.GetClosestEntryIndexChronologically(timestampedList, currentTimestamp)
		slice := snaps[prevIdx : currentIdx+1]
		results = append(results, slice)
		prevIdx = currentIdx + 1
		currentTimestamp = currentTimestamp.AddDate(0, windowSize, 0)
	}

	if prevIdx == len(snaps)-1 {
		return results
	}

	nextToAdd := snaps[prevIdx]

	// if the remaining snapshots make a window of less than 2 weeks (14 days)
	// then we can safely return the already created windows
	if nextToAdd.GetTimestamp().AddDate(0, 0, 14).After(lastTimeStamp) {
		return results
	}

	results = append(results, snaps[prevIdx:])

	return results
}

func (mcmc *MultiCoinMonteCarlo) runProbeList(
	probeList *ProbeList, seed cryptomarketutil.MarketSnapshotList, snapSlices []cryptomarketutil.MarketSnapshotList,
) (*ProbeResultList, error) {
	candlesticksLim := cfg.GetConfig().GetInt("daemons.trader.candlesticks_held")

	ctxMgr := tradeutil.NewTradeContextManager(candlesticksLim, usd) // don't inject more fiat currencies; they're useless in this scenario
	ctxMgr.FeedHistoMarketSnaps(seed)

	probeResults := make([]ProbeResult, probeList.Len())

	probes := probeList.GetAll()
	for i := range probes {
		probe := probes[i]
		probeResults[i] = ProbeResult{
			ProbeCfg:        probe.TraderConfig,
			WindowedResults: make([]WindowedResult, len(snapSlices)),
		}
	}

	for snapSliceIndex := range snapSlices {
		slice := snapSlices[snapSliceIndex]
		firstSnap := &slice[0]
		// fmt.Println("First snap time for current snap slice:", firstSnap.GetTimestamp())
		firstCtx := ctxMgr.MakeContextAndStore(firstSnap)

		tradeProbes := probeList.GetAll()
		cachedAmt := map[string]float64{}

		// compute the initial amount for each trader
		for j := range tradeProbes {
			tp := &tradeProbes[j]
			fsym := tp.FSym.TradeableAsset.Symbol
			var initialAmt float64
			if amt, ok := cachedAmt[fsym]; ok {
				tp.InitialInvestment = amt
			} else {
				if fsym == usd {
					initialAmt = mcmc.cfg.InitialFiatAmount
				} else {
					p, e := firstCtx.GetPrice(fsym, usd, mcmc.cfg.Exchange.Name)
					if e != nil {
						return nil, e
					}
					initialAmt = mcmc.cfg.InitialFiatAmount / p.GetPrice()
				}
				tp.InitialInvestment = initialAmt
				cachedAmt[fsym] = initialAmt
			}

			e := tp.BuildTrader()
			if e != nil {
				return nil, e
			}

			probeResults[j].WindowedResults[snapSliceIndex] = WindowedResult{
				InitialAmt: tp.InitialInvestment,
				FromTime:   slice[0].GetTimestamp(),
				ToTime:     slice[len(slice)-1].GetTimestamp(),
			}
		}

		traders := make([]contracts.Trader, len(tradeProbes))

		for traderIDx := range tradeProbes {
			probe := &tradeProbes[traderIDx]
			traders[traderIDx] = probe.Trader
		}

		var lastID uint
		for sliceIDx := 1; sliceIDx < len(slice); sliceIDx++ {
			ms := &slice[sliceIDx]
			ctx := ctxMgr.MakeContextAndStore(ms)
			lastID = mcmc.execTraders(traders, ctx, lastID)
		}

		lastCtx := ctxMgr.GetLastContext()
		for traderIDx := range tradeProbes {
			probe := tradeProbes[traderIDx]
			trader := traders[traderIDx]

			l := trader.GetLedger()
			tradeList := l.GetTradeList()
			investmentsCnt := tradeList.Filter(func(t *model.Trade) bool { return t.LinkedTradeID == 0 }).Len()
			profitsCnt := tradeList.Filter(func(t *model.Trade) bool { return t.LinkedTradeID > 0 }).Len()
			totalBalance := l.GetTotalBalance().GetRaw()

			// trades := tradeList.GetAll()
			// for tradeIDx := range trades {
			// 	t := trades[tradeIDx]
			// 	if t.LinkedTradeID == 0 {
			// 		fmt.Printf("Trade %f %s for %f %s @price %f and time %s\n",
			// 			t.FromAmountTrade,
			// 			t.From.Symbol,
			// 			t.ToAmountTrade,
			// 			t.To.Symbol,
			// 			t.TradePrice,
			// 			t.TradeFinishTime)
			// 	}

			// 	if t.LinkedTradeID > 0 {
			// 		fmt.Printf("Profit %f %s for %f %s @price %f and time %s\n",
			// 			t.FromAmountTrade,
			// 			t.From.Symbol,
			// 			t.ToAmountTrade,
			// 			t.To.Symbol,
			// 			t.TradePrice,
			// 			t.TradeFinishTime)
			// 	}
			// }

			fsym := probe.FSym.TradeableAsset.Symbol
			finalAmount := totalBalance[fsym]

			for sym, amount := range totalBalance {
				if sym == fsym {
					continue
				}

				p, e := lastCtx.GetPrice(sym, fsym, probe.Exchange.Name)
				if e != nil {
					return nil, e
				}
				finalAmount += amount * p.GetPrice()
			}

			windowedResult := &probeResults[traderIDx].WindowedResults[snapSliceIndex]
			windowedResult.InvestmentsMade = investmentsCnt
			windowedResult.ProfitsMade = profitsCnt
			windowedResult.FinalAmount = finalAmount
		}
	}

	return &ProbeResultList{results: probeResults}, nil
}

func (mcmc *MultiCoinMonteCarlo) execTraders(traders []contracts.Trader, ctx *tradeutil.TradeContext, lastID uint) uint {

	for traderIDx := range traders {
		trader := traders[traderIDx]

		resp := trader.MakeDecision(*ctx)
		if resp == nil {
			continue
		}

		// emulate distinct ids
		for i := 0; i < len(resp.Decision.Trades); i++ {
			lastID++
			resp.Decision.Trades[i].SetID(lastID)
		}
		lastID++
		resp.Decision.SetID(lastID)

		modelutil.AddTradeDecisionToLedger(resp.Decision, trader.GetLedger())
		mcmc.emulateCompleteDecision(resp.Decision, ctx.GetTimestamp())
		modelutil.AddTradeDecisionToLedger(resp.Decision, trader.GetLedger())
		contracts.NotifyTraderWithProcessedDecision(trader, resp.Decision)
	}

	return lastID
}

func (mcmc *MultiCoinMonteCarlo) emulateCompleteDecision(decision *model.TradeDecision, completedTime time.Time) {
	decision.NotifyStatus = modelenum.CompletedNotif.String()
	for i := 0; i < len(decision.Trades); i++ {
		trade := &decision.Trades[i]
		trade.FromAmountTrade = trade.FromAmountIntent
		trade.ToAmountTrade = trade.ToAmountIntent - mcMonteCarloConfig.DefaultTransactionFee*trade.ToAmountIntent
		trade.TradePrice = trade.PriceIntent
		trade.Status = modelenum.CompletedTrade.String()
		trade.TradeFinishTime = &completedTime
		trade.CreatedAt = completedTime
		trade.UpdatedAt = completedTime
	}
}
