package mcmontecarlo

import (
	"fmt"
	"strings"
	"time"

	"github.com/360EntSecGroup-Skylar/excelize"
)

// ExcelOutput is a helper data structure
// that is able to output the monte carlo simulations
// to a excel spreadsheet
type ExcelOutput struct {
	fName string
	fPath string

	Config *BuilderConfiguration

	SpreadSheet *excelize.File
}

// NewExcelOutput creates a new excel file which will hold all the results file
// for the simulation results
func NewExcelOutput(outputDir string, cfg *BuilderConfiguration) *ExcelOutput {
	fName := fmt.Sprintf(
		"Sim.%s.xlsx",
		time.Now().Format("02-01-2006 15 04"),
	)

	eo := &ExcelOutput{
		fName:       fName,
		fPath:       fmt.Sprintf("%s/%s", outputDir, fName),
		Config:      cfg,
		SpreadSheet: excelize.NewFile(),
	}
	eo.init()

	return eo
}

// AddResults adds a new result list to the spreadsheet file
func (eo *ExcelOutput) AddResults(prl *ProbeResultList) {

	sheetName := fmt.Sprintf("%s_%s", prl.FSym, prl.TSym)
	idx := eo.SpreadSheet.NewSheet(sheetName)
	eo.SpreadSheet.SetActiveSheet(idx)

	eo.SpreadSheet.SetCellStr(sheetName, "A1", "Cnt")
	eo.SpreadSheet.SetCellStr(sheetName, "B1", "Investment Signal")
	eo.SpreadSheet.MergeCell(sheetName, "B1", "D1")
	eo.SpreadSheet.SetCellStr(sheetName, "E1", "Profit Signal")
	eo.SpreadSheet.MergeCell(sheetName, "E1", "F1")
	// eo.SpreadSheet.SetCellStr(sheetName, "G1", "Cooldown Signal")
	// eo.SpreadSheet.MergeCell(sheetName, "G1", "H1")

	eo.SpreadSheet.SetCellStr(sheetName, "G1", "Weighted Profit")
	eo.SpreadSheet.SetCellStr(sheetName, "H1", "Avg Profit")
	eo.SpreadSheet.SetCellStr(sheetName, "I1", "Final Virtual Amt")

	eo.SpreadSheet.SetCellStr(sheetName, "A2", "")
	eo.SpreadSheet.SetCellStr(sheetName, "B2", "Trend Formation")
	eo.SpreadSheet.SetCellStr(sheetName, "C2", "Trend Candle")
	eo.SpreadSheet.SetCellStr(sheetName, "D2", "Trend Change")

	eo.SpreadSheet.SetCellStr(sheetName, "E2", "Price Variation")
	eo.SpreadSheet.SetCellStr(sheetName, "F2", "Trend Change")

	eo.SpreadSheet.SetCellStr(sheetName, "G2", "")
	eo.SpreadSheet.SetCellStr(sheetName, "H2", "")
	eo.SpreadSheet.SetCellStr(sheetName, "I2", "")

	firstResult := prl.results[0]
	windowsCount := len(firstResult.WindowedResults)

	currentWindowColStart := "I"
	windowHeaderRow := 1
	windowColumnsRow := 2
	for i := 0; i < windowsCount; i++ {
		currentWindowColStart = eo.getNextColLabel(currentWindowColStart, 1)
		currentWindowColEnd := eo.getNextColLabel(currentWindowColStart, 7)

		eo.SpreadSheet.SetCellStr(sheetName, fmt.Sprintf("%s%d", currentWindowColStart, windowHeaderRow), fmt.Sprintf("Window #%d", i+1))
		eo.SpreadSheet.MergeCell(sheetName, fmt.Sprintf("%s%d", currentWindowColStart, windowHeaderRow), fmt.Sprintf("%s%d", currentWindowColEnd, windowHeaderRow))

		cols := []string{
			"From Time",
			"To Time",
			"Investments Made",
			"Investments Returned",
			fmt.Sprintf("Initial %s", prl.FSym),
			fmt.Sprintf("Final %s", prl.FSym),
			"Efficiency",
			fmt.Sprintf("%s Profit", prl.FSym),
		}

		next := currentWindowColStart
		for _, colName := range cols {
			eo.SpreadSheet.SetCellStr(sheetName, fmt.Sprintf("%s%d", next, windowColumnsRow), colName)
			next = eo.getNextColLabel(next, 1)
		}

		currentWindowColStart = currentWindowColEnd
	}

	currentRow := 2
	cnt := 0
	prl.ForEach(func(result *ProbeResult) {
		cnt++
		currentRow++

		eo.SpreadSheet.SetCellValue(sheetName, fmt.Sprintf("A%d", currentRow), cnt)
		eo.SpreadSheet.SetCellValue(sheetName, fmt.Sprintf("B%d", currentRow), result.ProbeCfg.Invest.TrendFormed)
		eo.SpreadSheet.SetCellValue(sheetName, fmt.Sprintf("C%d", currentRow), result.ProbeCfg.Invest.TrendCandle)
		eo.SpreadSheet.SetCellValue(sheetName, fmt.Sprintf("D%d", currentRow), result.ProbeCfg.Invest.TrendChange)

		eo.SpreadSheet.SetCellValue(sheetName, fmt.Sprintf("E%d", currentRow), result.ProbeCfg.Profit.PriceVariation)
		eo.SpreadSheet.SetCellValue(sheetName, fmt.Sprintf("F%d", currentRow), result.ProbeCfg.Profit.TrendChange)

		eo.SpreadSheet.SetCellValue(sheetName, fmt.Sprintf("G%d", currentRow), result.GetWeightedProfit())
		eo.SpreadSheet.SetCellValue(sheetName, fmt.Sprintf("H%d", currentRow), result.GetAvgProfit())
		eo.SpreadSheet.SetCellValue(sheetName, fmt.Sprintf("I%d", currentRow), result.GetTotalFinalAmt())

		winCount := len(result.WindowedResults)
		currentCol := "I"

		for i := 0; i < winCount; i++ {
			wr := &result.WindowedResults[i]

			// FromTime
			currentCol = eo.getNextColLabel(currentCol, 1)
			eo.SpreadSheet.SetCellValue(sheetName, eo.getCellAxis(currentCol, currentRow), wr.FromTime)

			// ToTime
			currentCol = eo.getNextColLabel(currentCol, 1)
			eo.SpreadSheet.SetCellValue(sheetName, eo.getCellAxis(currentCol, currentRow), wr.ToTime)

			// Investments Made
			currentCol = eo.getNextColLabel(currentCol, 1)
			eo.SpreadSheet.SetCellValue(sheetName, eo.getCellAxis(currentCol, currentRow), wr.InvestmentsMade)

			// Investments Returned
			currentCol = eo.getNextColLabel(currentCol, 1)
			eo.SpreadSheet.SetCellValue(sheetName, eo.getCellAxis(currentCol, currentRow), wr.ProfitsMade)

			// Initial AMT
			currentCol = eo.getNextColLabel(currentCol, 1)
			eo.SpreadSheet.SetCellValue(sheetName, eo.getCellAxis(currentCol, currentRow), wr.InitialAmt)

			// Final AMT
			currentCol = eo.getNextColLabel(currentCol, 1)
			eo.SpreadSheet.SetCellValue(sheetName, eo.getCellAxis(currentCol, currentRow), wr.FinalAmount)

			// Efficiency
			currentCol = eo.getNextColLabel(currentCol, 1)
			eo.SpreadSheet.SetCellValue(sheetName, eo.getCellAxis(currentCol, currentRow), wr.GetEfficiency())

			// Profit
			currentCol = eo.getNextColLabel(currentCol, 1)
			eo.SpreadSheet.SetCellValue(sheetName, eo.getCellAxis(currentCol, currentRow), wr.GetProfit())
		}
	})

}

// Save generates a file name and saves it to the specified output directory
func (eo *ExcelOutput) Save() error {
	err := eo.SpreadSheet.SaveAs(eo.fPath)
	return err
}

// GetFilePath returns the filepath to the generated
// excel file
func (eo *ExcelOutput) GetFilePath() string {
	return eo.fPath
}

// GetFileName returns the filename of the generated excel
// file
func (eo *ExcelOutput) GetFileName() string {
	return eo.fName
}

func (eo *ExcelOutput) init() {
	const genDataSheet = "GenData"
	eo.SpreadSheet.NewSheet(genDataSheet)

	eo.SpreadSheet.SetCellStr(genDataSheet, "A1", "Invest Signal Ranges")
	eo.SpreadSheet.MergeCell(genDataSheet, "A1", "F1")

	eo.SpreadSheet.SetCellStr(genDataSheet, "G1", "Profit Signal Ranges")
	eo.SpreadSheet.MergeCell(genDataSheet, "G1", "J1")

	eo.SpreadSheet.SetCellStr(genDataSheet, "K1", "Ranges Configurations")
	eo.SpreadSheet.MergeCell(genDataSheet, "K1", "O1")

	eo.SpreadSheet.SetCellStr(genDataSheet, "A10", "Base Assets")
	eo.SpreadSheet.SetCellStr(genDataSheet, "B10", "Target Assets")

	eo.SpreadSheet.SetCellStr(genDataSheet, "A2", "Trend Formed Min")
	eo.SpreadSheet.SetCellStr(genDataSheet, "B2", "Trend Formed Max")
	eo.SpreadSheet.SetCellStr(genDataSheet, "C2", "Trend Candle Min")
	eo.SpreadSheet.SetCellStr(genDataSheet, "D2", "Trend Candle Max")
	eo.SpreadSheet.SetCellStr(genDataSheet, "E2", "Trend Change Min")
	eo.SpreadSheet.SetCellStr(genDataSheet, "F2", "Trend Change Max")
	eo.SpreadSheet.SetCellStr(genDataSheet, "G2", "Price Var Min")
	eo.SpreadSheet.SetCellStr(genDataSheet, "H2", "Price Var Max")
	eo.SpreadSheet.SetCellStr(genDataSheet, "I2", "Trend Change Min")
	eo.SpreadSheet.SetCellStr(genDataSheet, "J2", "Trend Change Max")
	eo.SpreadSheet.SetCellStr(genDataSheet, "K2", "Trend Formed Range")
	eo.SpreadSheet.SetCellStr(genDataSheet, "L2", "Trend Candle Range")
	eo.SpreadSheet.SetCellStr(genDataSheet, "M2", "Trend Change Range")
	eo.SpreadSheet.SetCellStr(genDataSheet, "N2", "Price Var Range")

	eo.SpreadSheet.SetCellValue(genDataSheet, "A3", eo.Config.InvestmentSignal.TrendFormedMin)
	eo.SpreadSheet.SetCellValue(genDataSheet, "B3", eo.Config.InvestmentSignal.TrendFormedMax)
	eo.SpreadSheet.SetCellValue(genDataSheet, "C3", eo.Config.InvestmentSignal.TrendCandleMin)
	eo.SpreadSheet.SetCellValue(genDataSheet, "D3", eo.Config.InvestmentSignal.TrendCandleMax)
	eo.SpreadSheet.SetCellValue(genDataSheet, "E3", eo.Config.InvestmentSignal.TrendChangeMin)
	eo.SpreadSheet.SetCellValue(genDataSheet, "F3", eo.Config.InvestmentSignal.TrendChangeMax)
	eo.SpreadSheet.SetCellValue(genDataSheet, "G3", eo.Config.ProfitSignal.PriceVarMin)
	eo.SpreadSheet.SetCellValue(genDataSheet, "H3", eo.Config.ProfitSignal.PriceVarMax)
	eo.SpreadSheet.SetCellValue(genDataSheet, "I3", eo.Config.ProfitSignal.TrendChangeMin)
	eo.SpreadSheet.SetCellValue(genDataSheet, "J3", eo.Config.ProfitSignal.TrendChangeMax)
	eo.SpreadSheet.SetCellValue(genDataSheet, "K3", eo.Config.RangeConfigurations.TrendFormedRange)
	eo.SpreadSheet.SetCellValue(genDataSheet, "L3", eo.Config.RangeConfigurations.TrendCandleRange)
	eo.SpreadSheet.SetCellValue(genDataSheet, "M3", eo.Config.RangeConfigurations.TrendChangeRange)
	eo.SpreadSheet.SetCellValue(genDataSheet, "N3", eo.Config.RangeConfigurations.PriceVariationRange)

	cellCnt := 11 // start to put base assets
	for _, asset := range eo.Config.BasePortfolio.PortfolioAssets {
		tradeableAsset := asset.TradeableAsset
		eo.SpreadSheet.SetCellStr(genDataSheet, fmt.Sprintf("A%d", cellCnt), tradeableAsset.Symbol)
		cellCnt++
	}

	cellCnt = 11
	for _, asset := range eo.Config.TargetPortfolio.PortfolioAssets {
		tradeableAsset := asset.TradeableAsset
		eo.SpreadSheet.SetCellStr(genDataSheet, fmt.Sprintf("B%d", cellCnt), tradeableAsset.Symbol)
		cellCnt++
	}

	eo.SpreadSheet.SetCellStr(genDataSheet, "D10", "Invested Fiat")
	eo.SpreadSheet.SetCellValue(genDataSheet, "D11", eo.Config.InitialFiatAmount)

}

// getNextColLabel returns the column name that needs to be used
// by giving the current column name and the distance to the desired
// column
func (eo *ExcelOutput) getNextColLabel(crt string, positions int) string {
	alphabet := "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	b := []byte(crt)
	l := len(b)
	lastChar := b[l-1]
	lastCharIndex := strings.IndexByte(alphabet, lastChar)
	nextIndex := lastCharIndex + positions
	rotations := nextIndex / len(alphabet)
	rotatedIndex := nextIndex - rotations*len(alphabet)

	if l > 1 {
		prefix := eo.getNextColLabel(string(b[:l-1]), rotations)
		suf := alphabet[rotatedIndex]
		return prefix + string(suf)
	}

	rotations--
	if rotations >= 0 {
		prefix := alphabet[rotations]
		suf := alphabet[rotatedIndex]
		r := []byte{prefix, suf}
		return string(r)
	}

	return string(alphabet[rotatedIndex])
}

func (eo *ExcelOutput) getCellAxis(col string, row int) string {
	return fmt.Sprintf("%s%d", col, row)
}
