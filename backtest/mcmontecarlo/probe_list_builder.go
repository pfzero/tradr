package mcmontecarlo

import (
	"math"

	"github.com/pkg/errors"
)

// BuiltProbe represents stats of the signals
// to be built and also the traders generated
type BuiltProbe struct {
	InvSignalsCnt    int
	ProfitSignalsCnt int
	InvestmensCnt    int
	PairsCnt         int
	TotalProbes      int
	Probes           ProbeList
}

// BuildProbes instantiates probes of multi-coin traders
func BuildProbes(cfg *BuilderConfiguration) (*BuiltProbe, error) {

	if err := validateConfiguration(cfg); err != nil {
		return nil, err
	}

	invSignals := genInvestmentSignals(cfg.InvestmentSignal, cfg.RangeConfigurations)
	profitSignals := genProfitSignals(cfg.ProfitSignal, cfg.RangeConfigurations)
	baseAssets := cfg.BasePortfolio.PortfolioAssets
	targetAssets := cfg.TargetPortfolio.PortfolioAssets

	traderProbesConfigs := []TraderProbe{}

	templates := []TraderProbe{}

	for i := range baseAssets {
		baseAsset := baseAssets[i]

		for j := range targetAssets {
			targetAsset := targetAssets[j]
			if targetAsset.TradeableAsset.Symbol == baseAsset.TradeableAsset.Symbol {
				continue
			}
			templates = append(templates, TraderProbe{
				Exchange: cfg.Exchange,
				FSym:     baseAsset,
				TSym:     targetAsset,
			})
		}
	}

	builtProbe := &BuiltProbe{
		InvSignalsCnt:    len(invSignals),
		ProfitSignalsCnt: len(profitSignals),
		PairsCnt:         len(templates),
	}

	builtProbe.TotalProbes = builtProbe.InvSignalsCnt * builtProbe.ProfitSignalsCnt * builtProbe.InvestmensCnt * builtProbe.PairsCnt

	if builtProbe.TotalProbes > mcMonteCarloConfig.MaxConfigsPerPair {
		return builtProbe, errors.Errorf("Refusing to build that many trade instances: %d; the limit is: %d", builtProbe.TotalProbes, mcMonteCarloConfig.MaxConfigsPerPair)
	}

	for invIdx := range invSignals {
		invSignal := invSignals[invIdx]
		for profitIdx := range profitSignals {
			profitSignal := profitSignals[profitIdx]

			for tplIdx := range templates {
				template := templates[tplIdx]

				traderProbe := TraderProbe{
					FSym:     template.FSym,
					TSym:     template.TSym,
					Exchange: template.Exchange,
					TraderConfig: ProbeCfg{
						Invest: invSignal,
						Profit: profitSignal,
					},
				}

				traderProbesConfigs = append(traderProbesConfigs, traderProbe)
			}
		}
	}

	builtProbe.Probes = ProbeList{
		list: traderProbesConfigs,
	}

	return builtProbe, nil
}

func validateConfiguration(cfg *BuilderConfiguration) error {
	ranges := cfg.RangeConfigurations

	if ranges.PriceVariationRange == 0 {
		return errors.New("Err: Price Variation Range is 0")
	}

	if ranges.TrendCandleRange == 0 {
		return errors.New("Err: Trend Candle Range is 0")
	}

	if ranges.TrendChangeRange == 0 {
		return errors.New("Err: Trend Change Range is 0")
	}

	if ranges.TrendFormedRange == 0 {
		return errors.New("Err: Trend Formed Range is 0")
	}

	ranges.PriceVariationRange = math.Abs(ranges.PriceVariationRange)
	ranges.TrendCandleRange = math.Abs(ranges.TrendCandleRange)
	ranges.TrendChangeRange = math.Abs(ranges.TrendChangeRange)
	ranges.TrendFormedRange = math.Abs(ranges.TrendFormedRange)

	return nil
}

func genInvestmentSignals(cfg InvSignalCfg, rangeCfg RangeCfg) []InvestCfg {
	investSignals := []InvestCfg{}

	currentTrendFormed := -math.Abs(cfg.TrendFormedMin)
	trendFormedMax := -math.Abs(cfg.TrendFormedMax) - (0.5 * rangeCfg.TrendFormedRange)
	trendChangeMax := math.Abs(cfg.TrendChangeMax) + (0.5 * rangeCfg.TrendChangeRange)

	for currentTrendFormed >= trendFormedMax {
		currentTrend := math.Abs(cfg.TrendChangeMin)

		for currentTrend <= trendChangeMax {
			signal := InvestCfg{
				TrendFormed: currentTrendFormed,
				TrendChange: currentTrend,
			}
			investSignals = append(investSignals, signal)
			currentTrend += rangeCfg.TrendChangeRange
		}

		currentTrendFormed -= rangeCfg.TrendFormedRange
	}

	var trendCandleNoise = math.Max(0.05, math.Abs(cfg.TrendChangeMin))

	for trendCandleNoise <= trendChangeMax {
		trendCandle := math.Max(trendCandleNoise+rangeCfg.TrendCandleRange, math.Abs(cfg.TrendCandleMin))

		for trendCandle <= math.Abs(cfg.TrendCandleMax) {
			investSignals = append(investSignals, InvestCfg{
				TrendChange: trendCandleNoise,
				TrendCandle: -math.Abs(trendCandle),
			})
			trendCandle += rangeCfg.TrendCandleRange
		}

		trendCandleNoise += rangeCfg.TrendChangeRange
	}

	return investSignals
}

func genProfitSignals(cfg ProfitSignalCfg, rangeCfg RangeCfg) []ProfitCfg {
	profitSignals := []ProfitCfg{}

	currentPriceVar := math.Abs(cfg.PriceVarMin)
	priceVarMax := math.Abs(cfg.PriceVarMax) + (0.5 * rangeCfg.PriceVariationRange)
	trendChangeMax := math.Abs(cfg.TrendChangeMax) + (0.5 * rangeCfg.TrendChangeRange)

	for currentPriceVar <= priceVarMax {
		trend := math.Abs(cfg.TrendChangeMin)

		for trend <= trendChangeMax {
			profitSignals = append(profitSignals, ProfitCfg{
				PriceVariation: currentPriceVar,
				TrendChange:    trend,
			})

			trend += rangeCfg.TrendChangeRange
		}

		currentPriceVar += rangeCfg.PriceVariationRange
	}

	return profitSignals
}
