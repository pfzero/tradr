package mcmontecarlo

// ResultsAnalyser is a helper struct that
// provides some analysis based on the results
// obtained from monte-carlo simulator
type ResultsAnalyser struct {
	lists []ProbeResultList
}
