package logs

import (
	"fmt"
	"io/ioutil"
	"time"

	"github.com/evalphobia/logrus_sentry"
	colorable "github.com/mattn/go-colorable"

	"github.com/rifflock/lfshook"

	"github.com/juju/errors"

	"github.com/lestrrat-go/file-rotatelogs"

	"bitbucket.org/pfzero/tradr/cfg"
	"github.com/sirupsen/logrus"
)

var log *logrus.Logger

func logPath(path string) string {
	const suffix = "%d%h%Y"
	return fmt.Sprintf("%s.%s.log", path, suffix)
}

func configureLogOutput(l *logrus.Logger) (bool, error) {
	infoPath := cfg.GetConfig().GetString("logs.info_path")
	errorPath := cfg.GetConfig().GetString("logs.error_path")
	rotationInterval := cfg.GetConfig().GetDuration("logs.rotation_interval")
	purgeOlderThan := cfg.GetConfig().GetDuration("logs.purge_older_than")

	if infoPath == "" || errorPath == "" {
		return false, nil
	}

	if rotationInterval == 0 || purgeOlderThan == 0 {
		return false, errors.New("Please provide both rotation_interval and purge_older_than flags in configuration file")
	}

	infoPathPattern := logPath(infoPath)
	errorPathPattern := logPath(errorPath)

	infoWriter, err := rotatelogs.New(
		infoPathPattern,
		rotatelogs.WithLinkName(infoPath),
		rotatelogs.WithRotationTime(time.Hour*rotationInterval),
		rotatelogs.WithMaxAge(24*time.Hour*purgeOlderThan),
	)
	if err != nil {
		return false, errors.Annotate(err, "logs")
	}

	errorWriter, err := rotatelogs.New(
		errorPathPattern,
		rotatelogs.WithLinkName(errorPath),
		rotatelogs.WithRotationTime(time.Hour*rotationInterval),
		rotatelogs.WithMaxAge(24*time.Hour*purgeOlderThan),
	)
	if err != nil {
		return false, err
	}

	hook := lfshook.NewHook(
		lfshook.WriterMap{
			logrus.DebugLevel: infoWriter,
			logrus.InfoLevel:  infoWriter,
			logrus.WarnLevel:  infoWriter,
			logrus.ErrorLevel: errorWriter,
			logrus.FatalLevel: errorWriter,
			logrus.PanicLevel: errorWriter,
		},
		&logrus.TextFormatter{},
	)

	l.AddHook(hook)

	return true, nil
}

func configureSentryOutput(l *logrus.Logger) (bool, error) {
	dsn := cfg.GetConfig().GetString("services.sentry.dsn")
	if dsn == "" {
		return false, errors.New("Please specify the services.sentry.dsn address")
	}

	hook, err := logrus_sentry.NewSentryHook(dsn, []logrus.Level{
		logrus.ErrorLevel,
		logrus.FatalLevel,
		logrus.PanicLevel,
	})
	if err != nil {
		return false, err
	}
	hook.Timeout = 20 * time.Second

	l.Hooks.Add(hook)
	return true, nil
}

// Initialize initializes the logging
// module
func Initialize() error {
	if log != nil {
		panic("Logging module already initialized")
	}

	logrus.SetFormatter(&logrus.TextFormatter{})

	log = logrus.New()

	level := cfg.GetConfig().GetString("logs.level")
	parsedLevel, err := logrus.ParseLevel(level)
	if err != nil {
		return err
	}

	log.SetLevel(parsedLevel)

	addedFs, err := configureLogOutput(log)

	if err != nil {
		return err
	}

	if !addedFs {
		log.Formatter = &logrus.TextFormatter{
			ForceColors:      true,
			DisableTimestamp: false,
			FullTimestamp:    true,
			QuoteEmptyFields: true,
		}
		log.Out = colorable.NewColorableStdout()
		log.Info("Will use standard output for logging since filesystem logging wasn't configured")
	} else {
		// since all logs are going to file we don't have to display them to stdout as well
		log.Out = ioutil.Discard
	}

	if cfg.GetConfig().GetBool("logs.use_sentry") {
		_, err = configureSentryOutput(log)
	}
	return err
}

// GetLogger returns the logger instance
func GetLogger() *logrus.Logger {

	if log == nil {
		l := logrus.New()
		l.Warn("Tried to use logger without initializing it...")
		return l
	}

	return log
}
