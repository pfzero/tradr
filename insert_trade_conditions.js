const getParsedArgs = valuesStr => {
  const parsed = valuesStr.replace(/\s+/g, ",").split(",");
  const [
    name,
    __private,
    invTrendFormation,
    invTrendCandle,
    invTrendChange,
    profitPVar,
    profitTrendChange
  ] = parsed;

  return {
    name,
    invest: {
      trendFormation: parseFloat(invTrendFormation),
      trendCandle: parseFloat(invTrendCandle),
      trendChange: parseFloat(invTrendChange)
    },
    profit: {
      priceVar: parseFloat(profitPVar),
      trendChange: parseFloat(profitTrendChange)
    }
  };
};

function signalValsToSqlQuery(valuesStr) {
  const parsedVals = getParsedArgs(valuesStr);

  const cdFields = [
    "created_at",
    "updated_at",
    "name",
    "has_price_variation",
    "price_increase_percent",
    "price_decrease_percent"
  ];
  const cdValues = [
    "now()",
    "now()",
    `"${parsedVals.name} Cooldown"`,
    1,
    parsedVals.profit.priceVar,
    parsedVals.invest.trendFormation == 0
      ? parsedVals.invest.trendCandle / 2
      : parsedVals.invest.trendFormation / 2
  ];

  const invFields = ["created_at", "updated_at", "name"];
  const invValues = ["now()", "now()", `"${parsedVals.name} Invest"`];

  if (parsedVals.invest.trendFormation != 0) {
    invFields.push("listen_trend_formation", "trend_formation_down");
    invValues.push(1, parsedVals.invest.trendFormation);
  } else if (parsedVals.invest.trendCandle != 0) {
    invFields.push("listen_candlestick", "trend_down");
    invValues.push(1, parsedVals.invest.trendCandle);
  } else {
    throw new Error(
      "At least one of trend formation or trend candle should be present for investment signal"
    );
  }

  if (parsedVals.invest.trendChange != 0) {
    invFields.push("listen_trend", "noise_up");
    invValues.push(1, parsedVals.invest.trendChange);
  }

  const profitFields = [
    "created_at",
    "updated_at",
    "name",
    "has_price_variation",
    "price_increase_percent"
  ];
  const profitValues = [
    "now()",
    "now()",
    `"${parsedVals.name} Profit"`,
    1,
    parsedVals.profit.priceVar
  ];

  if (parsedVals.profit.priceVar == 0) {
    throw new Error(
      "Expecting profit signal to have price variation, but got 0"
    );
  }

  if (parsedVals.profit.trendChange != 0) {
    profitFields.push("listen_trend", "noise_up");
    profitValues.push(1, parsedVals.profit.trendChange);
  }

  return `
    INSERT INTO tradr.trade_conditions (${cdFields})
    VALUES (${cdValues});

    INSERT INTO tradr.trade_conditions (${invFields})
    values (${invValues});

    INSERT INTO tradr.trade_conditions (${profitFields})
    values (${profitValues})
  `;
}
