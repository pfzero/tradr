#!/bin/bash

proj_root="bitbucket.org/pfzero/tradr" # relative

if [ "$1" = "-gen" ]; then
    echo "generating code for $proj_root";
    go generate "$proj_root/repo/modelenum";
    echo "generate complete";
    go run main.go "${@:2}"
else
    go run main.go "$@"
fi
