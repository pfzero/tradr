// Copyright © 2017 Erhan Abibula<erhan.abibula@gmail.com>

package main

import "bitbucket.org/pfzero/tradr/cmd"

func main() {
	cmd.Execute()
}
