<section class="hero is-fullheight is-dark is-bold">
    <div class="hero-body">
      <div class="container">
        <div class="columns is-vcentered">
          <div class="column is-4 is-offset-4">
            <h1 class="title">
              Login
            </h1>
            <div class="box">
              <form action="{{mountpathed "login"}}?redir=/admin" method="POST">
                <label class="label">Email</label>
                <p class="control">
                  <input class="input" type="text" name="{{.primaryID}}" placeholder="{{title .primaryID}}" value="{{.primaryIDValue}}">
                </p>
                <label class="label">Password</label>
                <p class="control">
                  <input class="input" type="password" name="password" placeholder="●●●●●●●">
                </p>
                {{if .error}}
                    <span class="help is-danger">{{.error}}</span>
                {{end}}
                <hr>
                <p class="control">
                  <button class="button is-primary">Login</button>
                </p>
                <input type="hidden" name="{{.xsrfName}}" value="{{.xsrfToken}}" />
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
</section>
