package ctrl

import (
	"fmt"
	"math"
	"net/http"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/pfzero/tradr/service/exchange"
	"bitbucket.org/pfzero/tradr/store/datastore/cryptomarketstore"
	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"
	"bitbucket.org/pfzero/tradr/util/tradeutil"
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
)

func loadSnaps(exchangeName string, fromTime, toTime time.Time) (cryptomarketutil.MarketSnapshotList, error) {
	snaps, err := cryptomarketstore.GetHistoMarketSnapshots(fromTime, toTime)
	if err != nil {
		return nil, err
	}

	exchangeSnaps, ok := snaps[exchangeName]
	if !ok {
		return nil, errors.Errorf("Couldn't fetch market snaps for exchange %s; Exchange doesn't exist", exchangeName)
	}

	filled := exchangeSnaps.FillSnaps(snaps["Market"], exchangeName, "Market")
	return filled, nil
}

func getCtxManager(seed cryptomarketutil.MarketSnapshotList) *tradeutil.TradeContextManager {
	contextMgr := tradeutil.NewTradeContextManager(-1, exchange.GetCurrencies()...)
	contextMgr.FeedHistoMarketSnaps(seed)
	return contextMgr
}

// GetTrends computes the trends and returns the response
func GetTrends(c *gin.Context) {

	fSymQ := strings.ToUpper(c.Query("fsym"))
	tSymQ := strings.ToUpper(c.Query("tsym"))
	noiseUpQ := c.Query("noiseup")
	noiseDownQ := c.Query("noisedown")
	exchangeQ := strings.ToUpper(c.Query("echange"))

	if fSymQ == "" {
		c.String(http.StatusBadRequest, fmt.Sprintf(`fsym "query" parameter is required; Given empty string`))
		return
	}

	if noiseUpQ == "" && noiseDownQ == "" {
		c.String(http.StatusBadRequest, fmt.Sprintf(`either "noiseUp" or "noiseDown" query parameters must be present; both are empty`))
		return
	}

	var fSym, tSym, exchange string
	var noiseUp, noiseDown float64
	var fromTime, toTime time.Time
	var err error

	fSym = fSymQ

	if tSymQ == "" {
		tSym = "USD"
	} else {
		tSym = tSymQ
	}

	if noiseUpQ == "" {
		noiseUp = 0
	} else {
		noiseUp, err = strconv.ParseFloat(noiseUpQ, 64)
		if err != nil {
			c.String(http.StatusBadRequest, fmt.Sprintf(`Got error when trying to convert string to float; Err: %s`, err.Error()))
			return
		}
	}

	if noiseDownQ == "" {
		noiseDown = 0
	} else {
		noiseDown, err = strconv.ParseFloat(noiseDownQ, 64)
		if err != nil {
			c.String(http.StatusBadRequest, fmt.Sprintf(`Got error when trying to convert string to float; Err: %s`, err.Error()))
			return
		}
		noiseDown = -math.Abs(noiseDown)
	}

	if exchangeQ == "" {
		exchange = "Market"
	} else {
		exchange = exchangeQ
	}

	if c.Query("totime") == "" {
		toTime = time.Now()
	} else {
		toTime, err = time.Parse(time.RFC3339, c.Query("totime"))
		if err != nil {
			c.String(http.StatusBadRequest, fmt.Sprintf(`Got error when parsing "totime" parameter; Err: %s`, err.Error()))
			return
		}
	}

	if c.Query("fromtime") != "" {
		fromTime, err = time.Parse(time.RFC3339, c.Query("fromtime"))
		if err != nil {
			c.String(http.StatusBadRequest, fmt.Sprintf(`Got error when parsing "fromtime" parameter; Err: %s`, err.Error()))
			return
		}
	} else {
		fromTime = time.Now().AddDate(-1, 0, 0)
	}

	snaps, err := loadSnaps(exchange, fromTime, toTime)

	if err != nil {
		c.String(http.StatusBadRequest, fmt.Sprintf(`Got err when trying to fetch snaps from db; Err: %s`, err.Error()))
	}

	ctxManager := getCtxManager(snaps)
	_ = ctxManager.MakeContext(&snaps[len(snaps)-1])
	trendList, err := ctxManager.GetCandlestickList(tradeutil.CandlestickListID{
		FSym:     fSym,
		TSym:     tSym,
		Exchange: exchange,
		Variation: *cryptomarketutil.FillSymmetricVariation(cryptomarketutil.Variation{
			Up:   noiseUp,
			Down: noiseDown,
		}),
	})

	if err != nil {
		c.String(http.StatusBadRequest, fmt.Sprintf(`Got error when trying to create trend list; Err: %s`, err.Error()))
		return
	}

	c.JSON(http.StatusOK, trendList.GetCandlesticks())
}
