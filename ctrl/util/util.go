package util

import (
	"strconv"

	"github.com/gin-gonic/gin"
)

// GetParam returns the string parameter from query string
func GetParam(c *gin.Context, param string) string {
	p := c.Query(param)
	if p != "" {
		return p
	}
	return c.Param(param)
}

// GetIDParam returns the id from query string
func GetIDParam(c *gin.Context) (int, error) {
	// check id parameter
	return GetIntParam(c, "id")
}

// GetIntParam returns the query paramater as an integer
func GetIntParam(c *gin.Context, param string) (int, error) {
	resultParam, err := strconv.Atoi(GetParam(c, param))
	return resultParam, err
}

// GetInt64Param returns the query parameter as an int64
func GetInt64Param(c *gin.Context, param string) (int64, error) {
	return strconv.ParseInt(GetParam(c, param), 10, 0)
}

// GetUintParam returns the query parameter as an uint
func GetUintParam(c *gin.Context, param string) (uint, error) {
	res, err := strconv.ParseUint(GetParam(c, param), 10, 0)
	if err != nil {
		return 0, err
	}
	return uint(res), nil
}
