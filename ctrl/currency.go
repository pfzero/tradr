package ctrl

import (
	"net/http"
	"strconv"

	"strings"

	"bitbucket.org/pfzero/tradr/service/exchange"
	"github.com/gin-gonic/gin"
)

const defaultExchangeBase = "EUR"

// GetCurrencyRates sends back the list of currency rates
// relative to either the default base asset (EUR) or the
// given base query param
func GetCurrencyRates(c *gin.Context) {
	base := strings.ToUpper(c.Query("base"))
	if base == "" {
		base = defaultExchangeBase
	}
	rates, err := exchange.GetRates(base)
	if err != nil {
		c.String(http.StatusNotFound, "error fetching exchange rates: %s", err.Error())
		return
	}
	c.JSON(http.StatusOK, rates)
}

// ConvertCurrency sends back the conversion result between the
// given from and to and also uses the given amount
func ConvertCurrency(c *gin.Context) {
	from := strings.ToUpper(c.Query("from"))
	to := strings.ToUpper(c.Query("to"))
	amount := c.Query("amount")
	if from == "" || to == "" || amount == "" {
		c.String(http.StatusBadRequest, "from, to and amount query parameters are mandatory. Received: %s, %s and %s", from, to, amount)
		return
	}

	amountf, err := strconv.ParseFloat(amount, 64)
	if err != nil {
		c.String(http.StatusBadRequest, "cannot convert amount to float64: %s", err.Error())
		return
	}
	result, err := exchange.ConvertAmount(from, to, amountf)
	if err != nil {
		c.String(http.StatusNotFound, "error fetching exchange rates: %s", err.Error())
		return
	}
	c.JSON(http.StatusOK, result)
}
