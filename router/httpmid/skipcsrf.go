package httpmid

import (
	"net/http"
	"strings"

	"github.com/gorilla/csrf"
)

// SkipAdminCSRFCheck skips the csrf check for admin module
func SkipAdminCSRFCheck(h http.Handler) http.Handler {
	middleware := func(w http.ResponseWriter, r *http.Request) {
		if !strings.HasPrefix(r.URL.Path, "/auth") {
			r = csrf.UnsafeSkipCheck(r)
		}
		h.ServeHTTP(w, r)
	}
	return http.HandlerFunc(middleware)
}
