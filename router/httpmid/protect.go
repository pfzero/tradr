package httpmid

import (
	"net/http"

	"bitbucket.org/pfzero/tradr/cfg"
	"github.com/gorilla/csrf"
)

// CSRFProtect adds csrf protection
func CSRFProtect(h http.Handler) http.Handler {
	authKey := cfg.GetConfig().GetString("web.csrf.key")
	secure := cfg.GetConfig().GetBool("web.csrf.secure")
	return csrf.Protect([]byte(authKey), csrf.Secure(secure))(h)
}
