package httpmid

import "net/http"

// HandlerMiddleware is the middleware interface that needs to be
// implemented in order to chain it to a router
type HandlerMiddleware func(http.Handler) http.Handler

// Chain takes a handler and wraps it with the given list of middleware
func Chain(handler http.Handler, middlewares ...HandlerMiddleware) http.Handler {
	for i := len(middlewares) - 1; i >= 0; i-- {
		mid := middlewares[i]
		handler = mid(handler)
	}
	return handler
}
