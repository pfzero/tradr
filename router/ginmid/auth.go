package ginmid

import (
	"net/http"

	"bitbucket.org/pfzero/tradr/store/datastore/thirdappstore"
	"github.com/gin-gonic/gin"
)

func getNoTokenError() map[string]string {
	var err = map[string]string{
		"error":    "no token sent",
		"err_code": "err_token_missing",
	}
	return err
}

func getInvalidTokenError() map[string]string {
	var err = map[string]string{
		"error":    "no app with that token was found",
		"err_code": "err_token_invalid",
	}
	return err
}

// Token checks for token parameter and authenticates
// the app corresponding to the token
func Token() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		token := ctx.Query("token")
		if token == "" {
			ctx.JSON(http.StatusForbidden, getNoTokenError())
			ctx.Abort()
			return
		}
		app, err := thirdappstore.GetAppByToken(token)
		if err != nil {
			ctx.JSON(http.StatusForbidden, getInvalidTokenError())
			ctx.Abort()
			return
		}
		ctx.Set("app", app)
		ctx.Next()
	}
}
