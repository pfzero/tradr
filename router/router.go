package router

import (
	"net/http"

	"bitbucket.org/pfzero/tradr/cfg"
	ctrl "bitbucket.org/pfzero/tradr/ctrl"
	"bitbucket.org/pfzero/tradr/logs"
	"bitbucket.org/pfzero/tradr/router/ginmid"
	"bitbucket.org/pfzero/tradr/router/ginmid/header"
	"github.com/gin-gonic/gin"
)

// Load loads the api router
func Load(middlewares ...gin.HandlerFunc) http.Handler {

	// enable debugging
	if cfg.DebugWebServer() {
		gin.SetMode(gin.DebugMode)
	} else {
		gin.SetMode(gin.ReleaseMode)
	}

	e := gin.New()
	e.Use(gin.Recovery())
	e.Use(gin.LoggerWithWriter(logs.GetLogger().Writer()))

	// api group
	api := e.Group("/api")

	// heartbeat check endpoint
	api.GET("/heartbeat", func(c *gin.Context) { c.Status(http.StatusOK) })

	// install middlewares
	api.Use(header.NoCache)
	api.Use(header.Options)
	api.Use(header.Secure)
	api.Use(middlewares...)
	api.Use(ginmid.Token())

	exchange := api.Group("/exchange")
	{
		exchange.GET("/rates", ctrl.GetCurrencyRates)
		exchange.GET("/convert", ctrl.ConvertCurrency)
	}

	analytics := api.Group("/analytics")
	{
		analytics.GET("/trends", ctrl.GetTrends)
	}

	return e
}
