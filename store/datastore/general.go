package datastore

import (
	"fmt"
	"strings"

	"bitbucket.org/pfzero/tradr/repo/model"
	"github.com/jinzhu/gorm"
)

// RecordExists returns true if the given record exists or false
// otherwise
func RecordExists(record model.Identifier) (bool, error) {
	count := 0
	dbAction := db.Model(record).Where("id=?", record.GetID()).Count(&count)
	if dbAction.Error != nil && dbAction.Error != gorm.ErrRecordNotFound {
		return false, dbAction.Error
	}
	return count > 0, nil
}

// RecordExistsBy checks wether the given model exists by the given field and value
func RecordExistsBy(model model.Identifier, field, value string) (bool, error) {
	count := 0
	dbAction := db.Model(model).Where(fmt.Sprintf("%s=?", strings.ToLower(field)), strings.ToLower(value)).Count(&count)
	if dbAction.Error != nil && dbAction.Error != gorm.ErrRecordNotFound {
		return false, dbAction.Error
	}
	return count > 0, nil
}

// PreloadRecord preloads record's relationship fields
func PreloadRecord(record model.Identifier, relations ...string) (model.Identifier, error) {
	dbAction := db.Where("id = ?", record.GetID())
	for _, relation := range relations {
		dbAction = dbAction.Preload(relation)
	}
	err := dbAction.Find(record).Error
	return record, err
}

// PreloadRecordBy queries the given record by the given k-v and also preloads its relations
func PreloadRecordBy(record model.Identifier, field, value string, relations ...string) error {
	dbAction := db.Where(fmt.Sprintf("%s=?", strings.ToLower(field)), strings.ToLower(value))
	for _, relation := range relations {
		dbAction = dbAction.Preload(relation)
	}
	err := dbAction.Find(record).Error
	return err
}

// SearchRecordsByKV performs a search in database by given value;
// It will return all the found values preloaded with given relations
func SearchRecordsByKV(records interface{}, key, value string, relations ...string) error {
	likeOp := fmt.Sprintf(`%s like "%s"`, key, fmt.Sprintf("%%%s%%", value))
	dbAction := db.Where(likeOp)
	for _, relation := range relations {
		dbAction = dbAction.Preload(relation)
	}
	err := dbAction.Find(records).Error
	return err
}

// Create creates a new entry to db
func Create(db *gorm.DB, record model.Identifier) (model.Identifier, error) {
	err := db.Create(record).Error
	return record, err
}

// Save inserts or updates an existing record
func Save(db *gorm.DB, record model.Identifier) error {
	return db.Save(record).Error
}

// UpdatePartial updates the given record using the provided partial update data
func UpdatePartial(db *gorm.DB, record model.Identifier, toUpdate map[string]interface{}) error {
	err := db.Model(record).Updates(toUpdate).Error
	return err
}

// UpdateUnsafe updates the whole model
func UpdateUnsafe(db *gorm.DB, record model.Identifier) error {
	err := db.Model(record).Updates(record).Error
	return err
}
