package datastore

import (
	"bitbucket.org/pfzero/tradr/store/datastore/cryptomarketstore"
	"bitbucket.org/pfzero/tradr/store/datastore/thirdappstore"
	"github.com/jinzhu/gorm"
)

var db *gorm.DB

// Provide stores a copy of the initialized
// orm instance
func Provide(initialized *gorm.DB) {
	db = initialized
	cryptomarketstore.Provide(db)
	thirdappstore.Provide(db)
}

// Initialize represents the initialize routine
func Initialize() error {
	err := cryptomarketstore.Initialize()
	return err
}
