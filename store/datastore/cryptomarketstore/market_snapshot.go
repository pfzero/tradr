package cryptomarketstore

import (
	"fmt"
	"sync"
	"time"

	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"
)

var cacheL sync.Mutex
var cache = make(map[string]map[time.Time]cryptomarketutil.MarketSnapshot)

const cacheLim = 100

// GetHistoMarketSnapshots returns all the historical prices stored as
// market snapshots (grouped by coin symbol and exchange); The prices
// included are synced by their timestamp
func GetHistoMarketSnapshots(fromTime, toTime time.Time) (map[string]cryptomarketutil.MarketSnapshotList, error) {
	prices := []model.CryptoPriceEntry{}
	if toTime.IsZero() {
		toTime = time.Now()
	}

	dbAction := db
	if fromTime.IsZero() {
		dbAction = dbAction.Where("time <= ?", toTime)
	} else {
		dbAction = dbAction.Where("time BETWEEN ? AND ?", fromTime, toTime)
	}
	dbAction = dbAction.Order("time asc")
	err := dbAction.Find(&prices).Error

	if err != nil {
		return nil, err
	}

	if len(prices) == 0 {
		return nil, nil
	}

	mappedPrices := [][]cryptomarketutil.Pricer{}
	groupMapping := make(map[uint]int)

	for idx, price := range prices {
		if _, ok := groupMapping[price.TrackedCoinID]; !ok {
			mappedPrices = append(mappedPrices, []cryptomarketutil.Pricer{})
			groupMapping[price.TrackedCoinID] = len(mappedPrices) - 1
		}
		groupIDX := groupMapping[price.TrackedCoinID]
		mappedPrices[groupIDX] = append(mappedPrices[groupIDX], &prices[idx])
	}

	syncedMarketSnaps := cryptomarketutil.SyncPriceGroups(mappedPrices)

	trackedCoins := make(map[int]*model.CryptoTrackedCoin)
	for idx := range groupMapping {
		trackedcoin, err := GetTrackedCoinByID(idx)
		if err != nil {
			return nil, err
		}
		trackedCoins[groupMapping[idx]] = trackedcoin
	}

	bucketsCount := len(syncedMarketSnaps)
	marketSnapshots := make(map[string]cryptomarketutil.MarketSnapshotList)

	// now create the buckets
	for i := 0; i < bucketsCount; i++ {
		snap := make(map[string]*cryptomarketutil.MarketSnapshot)

		for idx := range syncedMarketSnaps[i] {
			price := syncedMarketSnaps[i][idx]
			trackedCoin := trackedCoins[idx]
			if _, ok := snap[trackedCoin.TrackedExchange.Name]; !ok {
				snap[trackedCoin.TrackedExchange.Name] = cryptomarketutil.NewMarketSnapshot()
			}
			ms := snap[trackedCoin.TrackedExchange.Name]
			ms.Set(trackedCoin.Coin.Symbol, trackedCoin.TrackedExchange.Name, price)
		}
		for exchangeName, ms := range snap {
			if _, ok := marketSnapshots[exchangeName]; !ok {
				marketSnapshots[exchangeName] = []cryptomarketutil.MarketSnapshot{}
			}
			marketSnapshots[exchangeName] = append(marketSnapshots[exchangeName], *ms)
		}
	}

	return marketSnapshots, nil
}

// GetMarketSnapClosestTo returns the market snapshot for the given exchange closest to (but not exceeding)
// the given time
func GetMarketSnapClosestTo(exchangeName string, t time.Time) (*cryptomarketutil.MarketSnapshot, error) {

	cacheL.Lock()
	defer cacheL.Unlock()

	if ms, ok := cache[exchangeName][t]; ok {
		return &ms, nil
	}

	ms := cryptomarketutil.NewMarketSnapshot()

	trackedCoins, err := GetTrackedCoinsByExchange(exchangeName)

	if err != nil {
		return nil, err
	}

	if len(trackedCoins) == 0 {
		return nil, nil
	}

	trackedCoinIDS := make([]uint, len(trackedCoins))

	for i := 0; i < len(trackedCoins); i++ {
		trackedCoinIDS[i] = trackedCoins[i].GetID()
	}

	const maxAllowedDelta = 48 // hours

	subQ := fmt.Sprintf("SELECT * FROM `crypto_price_entries` WHERE tracked_coin_id IN (?) AND time <= ? ORDER BY `time` DESC LIMIT %d",
		len(trackedCoinIDS)*maxAllowedDelta)

	q := fmt.Sprintf(`
		SELECT cpe1.* FROM crypto_price_entries cpe1
		INNER JOIN 
		(SELECT cpetmp.tracked_coin_id, max(cpetmp.time) as time
		FROM (%s) as cpetmp
		GROUP BY cpetmp.tracked_coin_id) AS cpe2 
		ON (
			cpe1.tracked_coin_id = cpe2.tracked_coin_id AND
			cpe1.time = cpe2.time
		)
		`, subQ)

	prices := []model.CryptoPriceEntry{}
	err = db.Raw(q, trackedCoinIDS, t).
		Scan(&prices).
		Error

	if err != nil {
		return nil, err
	}

	if len(prices) == 0 {
		return ms, nil
	}

	tracked := make(map[uint]model.CryptoTrackedCoin)
	for _, coin := range trackedCoins {
		tracked[coin.GetID()] = coin
	}
	for i := 0; i < len(prices); i++ {
		price := &prices[i]
		price.TrackedCoin = tracked[price.TrackedCoinID]
	}

	for i := 0; i < len(prices); i++ {
		price := prices[i]
		ms.Set(price.TrackedCoin.Coin.Symbol, price.TrackedCoin.TrackedExchange.Name, &price)
	}

	cnt := len(cache[exchangeName])
	if cnt > cacheLim {
		for t := range cache[exchangeName] {
			delete(cache[exchangeName], t)
			break
		}

		cache[exchangeName][t] = *ms
	} else {
		if cnt == 0 {
			cache[exchangeName] = make(map[time.Time]cryptomarketutil.MarketSnapshot)
		}

		cache[exchangeName][t] = *ms
	}

	return ms, nil
}
