package cryptomarketstore

import "github.com/jinzhu/gorm"

var db *gorm.DB

// Provide allows for injecting the orm instance
// within this package
func Provide(initializedDB *gorm.DB) {
	db = initializedDB
}

// Initialize initializes the module
func Initialize() error {
	err := LoadPreferredAssets()
	if err != nil {
		return err
	}
	errAliases := LoadAliases()
	return errAliases
}
