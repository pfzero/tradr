package cryptomarketstore

import (
	"strings"

	"bitbucket.org/pfzero/tradr/repo/model"
)

// GetTradeableAssetByID returns the tradeable asset by the given id
func GetTradeableAssetByID(coinID uint) (*model.TradeableAsset, error) {
	record := &model.TradeableAsset{}
	dbAction := db.Where("id=?", coinID)
	err := dbAction.First(record).Error
	return record, err
}

// GetTradeableAssetBySymbol returns the tradeable asset model based on the
// given symbol
func GetTradeableAssetBySymbol(symbol string) (*model.TradeableAsset, error) {
	record := &model.TradeableAsset{}
	dbAction := db.Where("LOWER(symbol)=?", strings.ToLower(symbol))
	err := dbAction.First(record).Error
	return record, err
}

// GetTradeableAssetByIDs returns the list of tradeable assets givne a list
// of ids
func GetTradeableAssetByIDs(coinIDs ...uint) ([]model.TradeableAsset, error) {
	records := []model.TradeableAsset{}
	dbAction := db.Where("id IN (?)", coinIDs)
	err := dbAction.Find(&records).Error
	if err != nil {
		return nil, err
	}
	return records, nil
}
