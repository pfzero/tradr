package cryptomarketstore

import (
	"fmt"
	"reflect"
	"time"

	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"
)

// ConversionResult is the data structure
// that results after converting a crypto coin's
// price to another price
type ConversionResult struct {
	Time      time.Time
	FromPrice float64
	ToPrice   float64
}

// GetPrice returns the price of the conversion result
func (cr *ConversionResult) GetPrice() float64 {
	return cr.FromPrice / cr.ToPrice
}

// GetTimestamp returns the timestamp of the prices
func (cr *ConversionResult) GetTimestamp() time.Time {
	return cr.Time
}

// ConversionResultsToUtilPrices converts a list of
// *ConversionResult* structs to a list of Pricer interfaces
func ConversionResultsToUtilPrices(results []ConversionResult) []cryptomarketutil.Pricer {
	utilPrices := make([]cryptomarketutil.Pricer, len(results))
	for idx := range results {
		utilPrices[idx] = &results[idx]
	}
	return utilPrices
}

// UtilPricesToConversionResults converts a list of Pricer
// interface to a list of ConversionResult struct
func UtilPricesToConversionResults(utilPrices []cryptomarketutil.Pricer) ([]ConversionResult, error) {
	results := make([]ConversionResult, len(utilPrices))
	errMsg := `Cannot convert type %s to ConversionResult`

	for idx := range utilPrices {
		c, e := utilPrices[idx].(*ConversionResult)
		if !e {
			e := fmt.Errorf(errMsg, reflect.TypeOf(utilPrices[idx]).Name())
			return nil, e
		}
		results[idx] = *c
	}
	return results, nil
}
