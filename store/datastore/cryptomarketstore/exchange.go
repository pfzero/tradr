package cryptomarketstore

import (
	"strings"

	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"

	"bitbucket.org/pfzero/tradr/repo/model"
)

// GetExchangeByID returns the exchange model by the given id
func GetExchangeByID(exchangeID uint) (*model.CryptoExchange, error) {
	record := &model.CryptoExchange{}
	dbAction := db.Where("id=?", exchangeID)
	err := dbAction.Find(record).Error
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, errors.Wrapf(err, "Exchange with id %d not found", exchangeID)
		}

		return nil, errors.WithStack(errors.Wrapf(err, "Couldn't retrieve exchange with id %d. Unknown error", exchangeID))
	}
	return record, nil
}

// GetExchangeByName returns the exchange model by the given name
func GetExchangeByName(exchange string) (*model.CryptoExchange, error) {
	record := &model.CryptoExchange{}
	dbAction := db.Where("Lower(name)=?", strings.ToLower(exchange))
	err := dbAction.Find(record).Error

	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, errors.Wrapf(err, "Exchange %s not found", exchange)
		}

		return nil, errors.WithStack(errors.Wrapf(err, "Exchange %s couldn't be retreived. Unknown error", exchange))
	}

	return record, nil
}
