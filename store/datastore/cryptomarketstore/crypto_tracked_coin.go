package cryptomarketstore

import (
	"fmt"

	"bitbucket.org/pfzero/tradr/repo/model"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
)

func getTrackedCoinHashIDTemplate() string {
	return `CONCAT_WS(':', coin_id, tracked_exchange_id)`
}

func preloadTrackedCoinFields(dbAction *gorm.DB) *gorm.DB {
	return dbAction.Preload("TrackedExchange").Preload("Coin")
}

// GetTrackedCoinByID returns the crypto tracked coin model by id
func GetTrackedCoinByID(id uint) (*model.CryptoTrackedCoin, error) {
	record := &model.CryptoTrackedCoin{}
	dbAction := preloadTrackedCoinFields(db.Where("id=?", id))
	err := dbAction.Find(record).Error
	return record, err
}

// GetTrackedCoinByHashID returns the crypto tracked coin model by
// the given hash id
func GetTrackedCoinByHashID(hashID string) (*model.CryptoTrackedCoin, error) {
	record := &model.CryptoTrackedCoin{}
	dbAction := db.Where(fmt.Sprintf(`%s=?`, getTrackedCoinHashIDTemplate()), hashID)
	dbAction = preloadTrackedCoinFields(dbAction)
	err := dbAction.Find(record).Error
	return record, err
}

// GetTrackedCoin returns the given crypto tracked coin model by the given
// tradeable asset id and exchange id
func GetTrackedCoin(coinID uint, exchangeID uint) (*model.CryptoTrackedCoin, error) {
	record := &model.CryptoTrackedCoin{}
	dbAction := db.Where("coin_id=? AND tracked_exchange_id=?", coinID, exchangeID)
	dbAction = preloadTrackedCoinFields(dbAction)
	err := dbAction.Find(record).Error
	return record, err
}

// GetTrackedCoinBySymbolAndExchange returns the tracked coin model given the tradeable asset symbol
// and exchange
func GetTrackedCoinBySymbolAndExchange(coinSymbol string, exchange string) (*model.CryptoTrackedCoin, error) {
	coinRecord, err := GetTradeableAssetBySymbol(coinSymbol)
	if err != nil {
		return nil, fmt.Errorf("Couldn't fetch coin with symbol '%s'. The error was: '%s'", coinSymbol, err.Error())
	}
	exchangeRecord, err := GetExchangeByName(exchange)
	if err != nil {
		return nil, fmt.Errorf("Couldn't fetch exchange with name '%s'. The error was: '%s'", exchange, err.Error())
	}

	return GetTrackedCoin(coinRecord.GetID(), exchangeRecord.GetID())
}

// GetTrackedCoins returns the list of tracked coins
func GetTrackedCoins() ([]model.CryptoTrackedCoin, error) {
	coinRecords := []model.CryptoTrackedCoin{}
	dbAction := preloadTrackedCoinFields(db)
	err := dbAction.Find(&coinRecords).Error

	if err != nil && !gorm.IsRecordNotFoundError(err) {
		return nil, err
	}

	return coinRecords, nil
}

// GetTrackedCoinsByExchange returns the list of tracked coins by exchange
func GetTrackedCoinsByExchange(exchange string) ([]model.CryptoTrackedCoin, error) {
	exchangeRecord, err := GetExchangeByName(exchange)
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return nil, errors.Wrapf(err, "Exchange %s not found", exchange)
		}

		return nil, err
	}

	coinRecords := []model.CryptoTrackedCoin{}
	dbAction := preloadTrackedCoinFields(db)
	err = dbAction.Where("tracked_exchange_id=?", exchangeRecord.GetID()).Find(&coinRecords).Error

	if err != nil && !gorm.IsRecordNotFoundError(err) {
		return nil, errors.Wrap(err, "Unknown Error")
	}

	return coinRecords, nil
}
