package cryptomarketstore

import (
	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"
)

var preferredAssets = cryptomarketutil.NewExchangePreferredAssets()

// SavePreferredAsset saves the asset to db and also to cache
func SavePreferredAsset(asset model.ExchangePreferredAsset) error {
	toSave := &asset
	err := db.Save(toSave).Error
	if err != nil {
		return err
	}
	SavePreferredAssetToCache(asset)
	return nil
}

// SavePreferredAssetToCache saves the asset to cache
func SavePreferredAssetToCache(asset model.ExchangePreferredAsset) {
	preferredAssets.Save(&asset)
}

// DeletePreferredAssetFromCache deletes the asset from cache
func DeletePreferredAssetFromCache(asset model.ExchangePreferredAsset) {
	preferredAssets.Del(&asset)
}

// GetPreferredAssetsFromDB returns the list of preferred assets for all exchanges
// from db
func GetPreferredAssetsFromDB() ([]model.ExchangePreferredAsset, error) {
	ret := []model.ExchangePreferredAsset{}
	err := db.Preload("Exchange").Preload("TradeableAsset").Find(&ret).Error
	if err != nil {
		return nil, err
	}
	return ret, nil
}

// GetPreferredAssetByIDFromDB returns the exchange preferred asset by id from db
func GetPreferredAssetByIDFromDB(id uint) (*model.ExchangePreferredAsset, error) {
	ret := &model.ExchangePreferredAsset{}
	err := db.Preload("Exchange").Preload("TradeableAsset").Where("id=?", id).First(ret).Error
	if err != nil {
		return nil, err
	}
	return ret, nil
}

// GetPreferredAssets returns the list of cached exchange preferred assets
func GetPreferredAssets() *cryptomarketutil.ExchangePreferredAssets {
	return preferredAssets
}

// LoadPreferredAssets loads the list of exchange preferred assets and
// adds them to cache
func LoadPreferredAssets() error {
	fromDB, err := GetPreferredAssetsFromDB()
	if err != nil {
		return err
	}
	for i := 0; i < len(fromDB); i++ {
		asset := fromDB[i]
		preferredAssets.Save(&asset)
	}
	return nil
}
