package cryptomarketstore

import (
	"bitbucket.org/pfzero/tradr/repo/model"
)

// GetLedgerIOByTrader returns the user transactions performed for a specific trader
func GetLedgerIOByTrader(traderGUID string) ([]model.LedgerIO, error) {
	ledgerIOs := []model.LedgerIO{}
	dbAction := db.Where("trader_guid=?", traderGUID)
	dbAction = dbAction.Preload("Owner").Preload("Exchange").Preload("Asset")
	err := dbAction.Find(&ledgerIOs).Error
	if err != nil {
		return nil, err
	}
	return ledgerIOs, nil
}
