package cryptomarketstore

import (
	"fmt"
	"time"

	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"
)

// GetPriceHistoricalByTrackedCoin returns the historical price given the tracked coin
// model instance
func GetPriceHistoricalByTrackedCoin(
	trackedCoin *model.CryptoTrackedCoin, fromTime, toTime time.Time,
) (
	[]model.CryptoPriceEntry, error,
) {
	records := []model.CryptoPriceEntry{}

	if toTime.IsZero() {
		toTime = time.Now()
	}

	dbAction := db.Where("tracked_coin_id=?", trackedCoin.GetID())
	if fromTime.IsZero() {
		dbAction = dbAction.Where("time <= ?", toTime)
	} else {
		dbAction = dbAction.Where("time BETWEEN ? AND ?", fromTime, toTime)
	}

	dbAction = dbAction.Order("time asc")

	err := dbAction.Find(&records).Error

	if err != nil {
		return nil, err
	}

	for _, el := range records {
		el.TrackedCoinID = trackedCoin.GetID()
		el.TrackedCoin = *trackedCoin
	}

	return records, nil
}

// GetPriceHistoricalBySymbolAndExchange returns the historical price given
// the asset symbol and exchange
func GetPriceHistoricalBySymbolAndExchange(
	coinSymbol, exchangeName string, fromTime, toTime time.Time,
) ([]model.CryptoPriceEntry, error) {
	trackedCoin, err := GetTrackedCoinBySymbolAndExchange(coinSymbol, exchangeName)
	if err != nil {
		return nil, fmt.Errorf(`Error fetching tracked coin with sym '%s' for exchange '%s'. Error was: %s`,
			coinSymbol, exchangeName, err.Error())
	}

	return GetPriceHistoricalByTrackedCoin(trackedCoin, fromTime, toTime)
}

// GetHistoPriceConversion returns the histo price conversion between the 2 given conins
func GetHistoPriceConversion(fsym, tsym, exchange string, fromTime, toTime time.Time) ([]cryptomarketutil.Pricer, error) {

	fsymPrices, err := GetPriceHistoricalBySymbolAndExchange(fsym, exchange, fromTime, toTime)
	if err != nil {
		return nil, err
	}

	tsymPrices, err := GetPriceHistoricalBySymbolAndExchange(tsym, exchange, fromTime, toTime)
	if err != nil {
		return nil, err
	}

	fsymUtilPrices := make([]cryptomarketutil.Pricer, len(fsymPrices))
	tsymUtilPrices := make([]cryptomarketutil.Pricer, len(tsymPrices))

	for i := range fsymPrices {
		p := &fsymPrices[i]
		fsymUtilPrices[i] = p
	}
	for i := range tsymPrices {
		p := &tsymPrices[i]
		tsymUtilPrices[i] = p
	}

	groupedPrices := cryptomarketutil.SyncPriceGroups([][]cryptomarketutil.Pricer{fsymUtilPrices, tsymUtilPrices})

	ret := []cryptomarketutil.Pricer{}

	for _, prices := range groupedPrices {
		if len(prices) < 2 {
			continue
		}

		fsymPrice := prices[0]
		tsymPrice := prices[1]

		timestamp := fsymPrice.GetTimestamp()
		if tsymPrice.GetTimestamp().After(timestamp) {
			timestamp = tsymPrice.GetTimestamp()
		}

		ret = append(ret, &cryptomarketutil.PriceConversion{
			FSym: fsym,
			TSym: tsym, Exchange: exchange,
			FromPrice: fsymPrice.GetPrice(),
			ToPrice:   tsymPrice.GetPrice(),
			Timestamp: timestamp,
		})
	}

	return ret, nil
}
