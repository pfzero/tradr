package cryptomarketstore

import (
	"bitbucket.org/pfzero/tradr/repo/model"
)

// GetTradesByTradeDecisionID returns all the trades associated with a trade
// decision
func GetTradesByTradeDecisionID(tradeDecisionID uint) ([]model.Trade, error) {
	trades := []model.Trade{}
	dbAction := db.Where("trade_decision_id=?", tradeDecisionID)
	dbAction = dbAction.
		Preload("From").
		Preload("To").
		Preload("Exchange").
		Preload("Owner").
		Preload("TradeError")

	err := dbAction.Find(&trades).Error
	if err != nil {
		return nil, err
	}
	return trades, nil
}

// GetTradeByID returns the populated trade by given id
func GetTradeByID(id uint) (*model.Trade, error) {
	trade := &model.Trade{}
	dbAction := db.Where("id=?", id)
	dbAction = dbAction.
		Preload("From").
		Preload("To").
		Preload("Exchange").
		Preload("Owner").
		Preload("TradeError")

	err := dbAction.First(trade).Error
	if err != nil {
		return nil, err
	}

	return trade, nil
}
