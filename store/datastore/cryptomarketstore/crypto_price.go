package cryptomarketstore

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"bitbucket.org/pfzero/tradr/repo/model"
)

const (
	defaultExchange = "Market"
	insertBatchSize = 1000
)

func getInsertMultiQuery(prices []model.CryptoPriceEntry) string {
	buf := &strings.Builder{}
	buf.WriteString("INSERT INTO `crypto_price_entries` VALUES")

	for i := 0; i < len(prices)-1; i++ {
		p := prices[i]
		buf.WriteString(fmt.Sprintf(`(%d, "%s", %f),`, p.TrackedCoinID, p.GetTimestamp().Format("2006-01-02 15:04:05"), p.GetPrice()))
	}

	lastPrice := prices[len(prices)-1]
	buf.WriteString(fmt.Sprintf(`(%d, "%s", %f)`, lastPrice.TrackedCoinID, lastPrice.GetTimestamp().Format("2006-01-02 15:04:05"), lastPrice.GetPrice()))

	return buf.String()
}

// GetPriceByTrackedCoin returns the last price of the given tracked coin
func GetPriceByTrackedCoin(trackedCoin *model.CryptoTrackedCoin) (*model.CryptoPriceEntry, error) {

	record := &model.CryptoPriceEntry{}

	dbAction := db.Where("tracked_coin_id=?", trackedCoin.GetID())
	dbAction = dbAction.Order("time desc")

	err := dbAction.Last(record).Error

	if err != nil {
		return nil, err
	}

	record.TrackedCoin = *trackedCoin
	record.TrackedCoinID = trackedCoin.GetID()

	return record, nil
}

// GetPriceByTrackedCoinID returns the latest price of the given tracked coin id
func GetPriceByTrackedCoinID(trackedCoinID uint) (*model.CryptoPriceEntry, error) {
	tok, err := GetTrackedCoinByID(trackedCoinID)
	if err != nil {
		return nil, err
	}
	return GetPriceByTrackedCoin(tok)
}

// GetPriceBySymbolAndExchange returns the latest price of the requested asset for the given
// exchange
func GetPriceBySymbolAndExchange(sym, exchange string) (*model.CryptoPriceEntry, error) {
	trackedCoin, err := GetTrackedCoinBySymbolAndExchange(sym, exchange)
	if err != nil {
		return nil, fmt.Errorf(`Couldn't fetch tracked coin with sym: %s and exchange %s. Error was: %s`, sym, exchange, err.Error())
	}

	return GetPriceByTrackedCoin(trackedCoin)
}

// GetPriceClosestTo returns the price of the crypto tracked coin that is closest to the
// given timestamp
func GetPriceClosestTo(trackedCoinID uint, t time.Time) (*model.CryptoPriceEntry, error) {
	q := `
		SELECT * FROM crypto_price_entries
		WHERE tracked_coin_id=?
		AND time <= ?
		ORDER BY time DESC
	`

	result := &model.CryptoPriceEntry{}
	dbAction := db.Raw(q, trackedCoinID, t)
	err := dbAction.First(result).Error
	if err != nil {
		return nil, err
	}
	return result, nil
}

// GetPriceClosestToBySymAndExchange returns the price of the given fsym@exchange
// closest to the given t time
func GetPriceClosestToBySymAndExchange(fsym, exchange string, t time.Time) (*model.CryptoPriceEntry, error) {
	fromTrackedCoin, err := GetTrackedCoinBySymbolAndExchange(fsym, exchange)
	if err != nil {
		return nil, err
	}
	return GetPriceClosestTo(fromTrackedCoin.GetID(), t)
}

// ConvertPriceClosestToBySymAndExchange returns the conversion result of the closest price to the given time;
// The prices are fetched by the given fsym, tsym and exchange strings
func ConvertPriceClosestToBySymAndExchange(fsym, tsym, exchange string, t time.Time) (*ConversionResult, error) {
	fromPrice, err := GetPriceClosestToBySymAndExchange(fsym, exchange, t)
	if err != nil {
		return nil, err
	}

	toPrice, err := GetPriceClosestToBySymAndExchange(tsym, exchange, t)
	if err != nil {
		return nil, err
	}

	return &ConversionResult{
		Time:      t,
		FromPrice: fromPrice.GetPrice(),
		ToPrice:   toPrice.GetPrice(),
	}, nil
}

// ConvertByTrackedCoins returns the conversiomn result between the 2 crypto tracked coins
func ConvertByTrackedCoins(fromTrackedCoin, toTrackedCoin *model.CryptoTrackedCoin) (*ConversionResult, error) {
	fromCoinPrice, err := GetPriceByTrackedCoin(fromTrackedCoin)
	toCoinPrice, err := GetPriceByTrackedCoin(toTrackedCoin)

	if err != nil {
		return nil, err
	}

	conversion := &ConversionResult{
		Time:      fromCoinPrice.Time,
		FromPrice: fromCoinPrice.Price,
		ToPrice:   toCoinPrice.Price,
	}
	return conversion, nil
}

// ConvertBySymbolsAndExchange returns the conversion result fromSym:toSym on the given exchange
func ConvertBySymbolsAndExchange(fromSym, toSym, exchange string) (*ConversionResult, error) {
	fromTrackedCoin, err := GetTrackedCoinBySymbolAndExchange(fromSym, exchange)
	toTrackedCoin, err := GetTrackedCoinBySymbolAndExchange(toSym, exchange)
	if err != nil {
		return nil, fmt.Errorf(`Error fetching tracked coins. Error was: %s`, err.Error())
	}
	return ConvertByTrackedCoins(fromTrackedCoin, toTrackedCoin)
}

// AddPriceEntry adds the given price entry to the database
func AddPriceEntry(priceEntry *model.CryptoPriceEntry) (*model.CryptoPriceEntry, error) {
	if priceEntry == nil {
		return nil, errors.New("The given price entry was nil")
	}

	if priceEntry.Time.IsZero() {
		priceEntry.Time = time.Now()
	}

	err := db.Create(priceEntry).Error

	if err != nil {
		return nil, err
	}
	return priceEntry, nil
}

// AddPriceEntries adds the given list of price entries to the database
func AddPriceEntries(priceEntries []model.CryptoPriceEntry) ([]model.CryptoPriceEntry, error) {
	var err error
	tx := db.Begin()

	batches := len(priceEntries)/insertBatchSize + 1

	for i := 0; i < batches; i++ {
		start := i * insertBatchSize
		end := start + insertBatchSize
		if end > len(priceEntries) {
			end = len(priceEntries)
		}
		query := getInsertMultiQuery(priceEntries[start:end])

		err := tx.Exec(query).Error
		if err != nil {
			tx.Rollback()
			return nil, err
		}
	}

	err = tx.Commit().Error

	if err != nil {
		return nil, err
	}
	return priceEntries, nil
}

// DelTrackedCoinPricesByTrackedCoinID deletes all the historical prices
// for the given tracked coin id;
func DelTrackedCoinPricesByTrackedCoinID(trackedCoinID uint) error {
	err := db.Exec(`DELETE FROM crypto_price_entries WHERE tracked_coin_id=?`, trackedCoinID).Error
	return err
}

// DelTrackedCoinPricesByTrackedCoin deletes all the historical prices
// for the given tracked coin id;
func DelTrackedCoinPricesByTrackedCoin(trackedCoin *model.CryptoTrackedCoin) error {
	return DelTrackedCoinPricesByTrackedCoinID(trackedCoin.GetID())
}
