package cryptomarketstore

import (
	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/util/cryptomarketutil"
)

var aliases = cryptomarketutil.NewExchangeAssetAlias()

// SaveAlias updates an existing alias or inserts it if it doesn't
// exist
func SaveAlias(alias model.ExchangeSymbolAlias) error {
	toSave := &alias
	err := db.Save(toSave).Error
	if err != nil {
		return err
	}
	SaveAliasToCache(alias)
	return nil
}

// SaveAliasToCache saves the alias to cache
func SaveAliasToCache(alias model.ExchangeSymbolAlias) {
	exchange := alias.Exchange.Name
	asset := alias.AssetSymbol
	strAlias := alias.AssetAlias
	aliases.Save(exchange, asset, strAlias)
}

// DeleteAliasFromCache removes the alias from the cache
func DeleteAliasFromCache(alias model.ExchangeSymbolAlias) {
	exchange := alias.Exchange.Name
	asset := alias.AssetSymbol
	aliases.Del(exchange, asset)
}

// DeleteAlias deletes an existing alias
func DeleteAlias(alias model.ExchangeSymbolAlias) error {
	er := db.Delete(&alias).Error
	if er != nil {
		return er
	}
	DeleteAliasFromCache(alias)
	return nil
}

// GetAliasesFromDB returns the list of aliases from database
func GetAliasesFromDB() ([]model.ExchangeSymbolAlias, error) {
	ret := []model.ExchangeSymbolAlias{}
	err := db.Preload("Exchange").Find(&ret).Error
	if err != nil {
		return nil, err
	}
	return ret, nil
}

// GetAliasByIDFromDB returns the alias by id from db
func GetAliasByIDFromDB(id uint) (*model.ExchangeSymbolAlias, error) {
	ret := &model.ExchangeSymbolAlias{}
	err := db.Preload("Exchange").Where("id=?", id).First(ret).Error
	if err != nil {
		return nil, err
	}
	return ret, nil
}

// GetAliases returns the cached aliases object that will allways
// remain in-sync with the database
func GetAliases() *cryptomarketutil.ExchangeAssetAlias {
	return aliases
}

// LoadAliases loads the initial list of aliases
func LoadAliases() error {
	dbAliases, err := GetAliasesFromDB()
	if err != nil {
		return err
	}
	for i := 0; i < len(dbAliases); i++ {
		dbAlias := dbAliases[i]
		SaveAliasToCache(dbAlias)
	}

	return nil
}
