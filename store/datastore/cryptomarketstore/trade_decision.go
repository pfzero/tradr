package cryptomarketstore

import (
	"bitbucket.org/pfzero/tradr/repo/model"
)

// GetTradeDecisionsByTrader returns all the trade decisions taken by a specific trader
func GetTradeDecisionsByTrader(traderGUID string) ([]model.TradeDecision, error) {
	results := []model.TradeDecision{}
	dbAction := db.Where("trader_guid=?", traderGUID).Preload("Beneficiary")
	err := dbAction.Find(&results).Error
	if err != nil {
		return nil, err
	}

	for i := 0; i < len(results); i++ {
		tradeDecision := &results[i]
		trades, err := GetTradesByTradeDecisionID(tradeDecision.GetID())
		if err != nil {
			return nil, err
		}
		tradeDecision.Trades = trades
	}

	return results, nil
}

// GetTradeDecisionByID returns the trade decision by id
func GetTradeDecisionByID(id uint) (*model.TradeDecision, error) {
	result := &model.TradeDecision{}
	dbAction := db.Where("id=?", id).Preload("Beneficiary")
	err := dbAction.First(result).Error
	if err != nil {
		return nil, err
	}
	trades, err := GetTradesByTradeDecisionID(id)
	if err != nil {
		return nil, err
	}
	result.Trades = trades
	return result, nil
}
