package cryptomarketstore

import (
	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/repo/modelenum"
	"github.com/jinzhu/gorm"
)

func preloadSMFields(dbAction *gorm.DB) *gorm.DB {
	preload := []string{"Beneficiary", "Exchange", "PairFrom", "PairTo", "TradeCondition"}
	for _, col := range preload {
		dbAction = dbAction.Preload(col)
	}
	return dbAction
}

// GetActiveSmartNotifiers returns the active smart notifiers
func GetActiveSmartNotifiers() ([]model.SmartNotification, error) {
	records := []model.SmartNotification{}
	dbAction := db.Where(`state=?`, modelenum.TradeBotRunningState.String())
	dbAction = preloadSMFields(dbAction)
	err := dbAction.Find(&records).Error
	if err != nil {
		return nil, err
	}
	return records, nil
}

// GetAllSmartNotifiers returns all the smart notifiers
func GetAllSmartNotifiers() ([]model.SmartNotification, error) {
	records := []model.SmartNotification{}
	dbAction := db.Where(`state IN (?, ?, ?)`,
		modelenum.TradeBotNewState.String(),
		modelenum.TradeBotStoppedState.String(),
		modelenum.TradeBotRunningState.String(),
	)
	dbAction = preloadSMFields(dbAction)
	err := dbAction.Find(&records).Error
	if err != nil {
		return nil, err
	}
	return records, nil
}

// GetSmartNotifierByID returns the smart notification record from database
func GetSmartNotifierByID(id uint) (*model.SmartNotification, error) {
	record := &model.SmartNotification{}
	dbAction := db.Where(`id=?`, id)
	dbAction = preloadSMFields(dbAction)

	err := dbAction.First(record).Error
	if err != nil {
		return nil, err
	}

	return record, err
}

// UpdateSmartNotifierState updates the state of the smart notification
func UpdateSmartNotifierState(id uint, state string) error {
	return db.Table(`smart_notifications`).Where(`id=?`, id).Update("state", state).Error
}
