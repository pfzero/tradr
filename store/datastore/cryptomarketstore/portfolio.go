package cryptomarketstore

import (
	"strings"

	"bitbucket.org/pfzero/tradr/repo/model"
)

// GetPortfolioAssetsByPortfolioID returns the portfolio assets linked to the
// given portfolio id
func GetPortfolioAssetsByPortfolioID(pID uint) ([]model.PortfolioAsset, error) {
	assets := []model.PortfolioAsset{}
	dbAction := db.Where("portfolio_id=?", pID).Preload("TradeableAsset")
	err := dbAction.Find(&assets).Error
	if err != nil {
		return nil, err
	}

	return assets, nil
}

// GetPortfolioByID returns the portfolio of assets given a portfolio
// id
func GetPortfolioByID(id uint) (*model.Portfolio, error) {
	record := &model.Portfolio{}
	dbAction := db.Where("id=?", id)
	err := dbAction.First(record).Error

	if err != nil {
		return nil, err
	}
	populatedErr := populatePortfolio(record)
	if populatedErr != nil {
		return nil, err
	}

	return record, nil
}

// GetPortfolioByName returns the portfolio with the given name
func GetPortfolioByName(name string) (*model.Portfolio, error) {
	record := &model.Portfolio{}
	dbAction := db.Where("lower(name)=?", strings.ToLower(name))
	err := dbAction.First(record).Error
	if err != nil {
		return nil, err
	}
	populatedErr := populatePortfolio(record)
	if populatedErr != nil {
		return nil, err
	}
	return record, nil
}

func populatePortfolio(p *model.Portfolio) error {
	portfolioAssets, err := GetPortfolioAssetsByPortfolioID(p.GetID())
	if err != nil {
		return err
	}
	p.PortfolioAssets = portfolioAssets
	return nil
}
