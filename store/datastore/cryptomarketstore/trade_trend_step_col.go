package cryptomarketstore

import (
	"bitbucket.org/pfzero/tradr/repo/model"
)

// GetTradeTrendStepColByID returns the same-trend step configuration by the given id
func GetTradeTrendStepColByID(id uint) (*model.TradeTrendStepCol, error) {
	record := &model.TradeTrendStepCol{}
	dbAction := db.Where("id=?", id).Preload("TradeTrendSteps")
	err := dbAction.First(record).Error

	if err != nil {
		return nil, err
	}
	return record, nil
}
