package cryptomarketstore

import (
	"bitbucket.org/pfzero/tradr/repo/model"
	"bitbucket.org/pfzero/tradr/repo/modelenum"
	"github.com/jinzhu/gorm"
)

func preloadMCTFields(dbAction *gorm.DB) *gorm.DB {
	preload := []string{
		"Beneficiary",
		"Exchange",
		"InvCooldownCondition",
		"InvCondition",
		"ProfitCondition",
	}

	for _, col := range preload {
		dbAction = dbAction.Preload(col)
	}
	return dbAction
}

func populateMultiCoinTraderConfig(record *model.MultiCoinTradeCfg) error {
	sameTrendStepCol, err := GetTradeTrendStepColByID(record.TradeTrendStepColID)
	basePortfolio, err := GetPortfolioByID(record.BasePortfolioID)
	targetPortfolio, err := GetPortfolioByID(record.TargetPortfolioID)
	if err != nil {
		return err
	}
	record.TradeTrendStepCol = *sameTrendStepCol
	record.BasePortfolio = *basePortfolio
	record.TargetPortfolio = *targetPortfolio
	return nil
}

func populateMultiCoinTraderConfigs(records []model.MultiCoinTradeCfg) error {
	for i := 0; i < len(records); i++ {
		e := populateMultiCoinTraderConfig(&records[i])
		if e != nil {
			return e
		}
	}
	return nil
}

// GetActiveMultiCoinTraders returns the list of active multi coin traders
func GetActiveMultiCoinTraders() ([]model.MultiCoinTradeCfg, error) {
	records := []model.MultiCoinTradeCfg{}
	dbAction := db.Where(`state=?`, modelenum.TradeBotRunningState.String())
	dbAction = preloadMCTFields(dbAction)
	err := dbAction.Find(&records).Error
	if err != nil {
		return nil, err
	}
	err = populateMultiCoinTraderConfigs(records)
	if err != nil {
		return nil, err
	}
	return records, nil
}

// GetAllMultiCoinTraders returns the list of all multi coin traders
func GetAllMultiCoinTraders() ([]model.MultiCoinTradeCfg, error) {
	records := []model.MultiCoinTradeCfg{}
	dbAction := db.Where(`state IN (?, ?, ?)`,
		modelenum.TradeBotNewState.String(),
		modelenum.TradeBotStoppedState.String(),
		modelenum.TradeBotRunningState.String(),
	)
	dbAction = preloadMCTFields(dbAction)
	err := dbAction.Find(&records).Error
	if err != nil {
		return nil, err
	}
	err = populateMultiCoinTraderConfigs(records)
	if err != nil {
		return nil, err
	}
	return records, nil
}

// GetMultiCoinTraderByID returns the multi coin trader configuration object
// by the given id
func GetMultiCoinTraderByID(id uint) (*model.MultiCoinTradeCfg, error) {
	record := &model.MultiCoinTradeCfg{}
	dbAction := db.Where(`id=?`, id)
	dbAction = preloadMCTFields(dbAction)

	err := dbAction.First(record).Error
	if err != nil {
		return nil, err
	}
	err = populateMultiCoinTraderConfig(record)
	if err != nil {
		return nil, err
	}
	return record, nil
}

// UpdateMultiCoinTraderState updates the state of the multi coin trader
func UpdateMultiCoinTraderState(id uint, newState string) error {
	return db.Table(`multi_coin_trade_cfgs`).Where(`id=?`, id).Update("state", newState).Error
}
