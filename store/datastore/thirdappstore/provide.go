package thirdappstore

import (
	"github.com/jinzhu/gorm"
)

var db *gorm.DB

// Provide stores the initialized orm
func Provide(initializedDB *gorm.DB) {
	db = initializedDB
}
