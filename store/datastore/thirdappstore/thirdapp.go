package thirdappstore

import (
	"bitbucket.org/pfzero/tradr/repo/model"
)

// GetAppByToken returns the thirt party app by the given token
func GetAppByToken(token string) (*model.ThirdPartyApp, error) {
	var thirdApp model.ThirdPartyApp
	dbQuery := db.Preload("Author").Preload("Author.Role")
	if err := dbQuery.Where("consumer_token=?", token).First(&thirdApp).Error; err != nil {
		return nil, err
	}
	return &thirdApp, nil
}
