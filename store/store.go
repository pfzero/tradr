package store

import (
	"errors"
	"fmt"

	"bitbucket.org/pfzero/tradr/store/datastore"

	staticConfig "bitbucket.org/pfzero/tradr/cfg"
	"bitbucket.org/pfzero/tradr/store/plugin"
	"github.com/jinzhu/gorm"
	// import dialects
	_ "github.com/jinzhu/gorm/dialects/mysql"
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

var (
	// DB is the initialized db orm
	DB *gorm.DB
)

// InitializeDB creates a connection to database
// this function is used when the app is booting
func InitializeDB() {
	if DB != nil {
		return
	}

	var err error
	c := staticConfig.GetConfig()
	dbConnection := fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?charset=utf8&parseTime=true&loc=Local",
		c.GetString("maindb.user"),
		c.GetString("maindb.pass"),
		c.GetString("maindb.host"),
		c.GetString("maindb.port"),
		c.GetString("maindb.name"),
	)

	if c.GetString("maindb.adapter") == "mysql" {
		DB, err = gorm.Open("mysql", dbConnection)
	} else if c.GetString("maindb.adapter") == "postgres" {
		DB, err = gorm.Open("postgres", dbConnection)
	} else {
		panic(errors.New("not supported database adapter::" + c.GetString("maindb.adapter")))
	}

	if err != nil {
		panic(err)
	}

	if staticConfig.DebugDB() {
		DB.LogMode(true)
	}

	datastore.Provide(DB)
	MigrateResources()
	err = datastore.Initialize()
	if err != nil {
		panic(err)
	}
}

// RegisterPlugins registers the various
// database plugins
func RegisterPlugins() {
	plugin.Initialize(DB)
}
