package plugin

import (
	"github.com/jinzhu/gorm"

	"github.com/qor/audited"
	"github.com/qor/l10n"
	"github.com/qor/media_library"
	"github.com/qor/publish"
	"github.com/qor/sorting"
	"github.com/qor/validations"
)

var (
	// Publish represents the publish module from qor
	Publish *publish.Publish
)

// Initialize boots up the publish plugin from qor ecosystem
// and also registers hooks for the other plugins
func Initialize(db *gorm.DB) {
	Publish = publish.New(db.Set("l10n:mode", "unscoped"))
	audited.RegisterCallbacks(db)
	l10n.RegisterCallbacks(db)
	sorting.RegisterCallbacks(db)
	validations.RegisterCallbacks(db)
	media_library.RegisterCallbacks(db)
}
