package store

import "bitbucket.org/pfzero/tradr/repo/model"
import "sync"

var migrationOnce sync.Once

// MigrateResources creates all the tables in database with the
// corresponding columns and relationships between models
func MigrateResources() {
	migrationOnce.Do(func() {
		// register all models here
		DB.AutoMigrate(&model.CryptoTrackedCoin{})
		DB.AutoMigrate(&model.CryptoExchange{})
		DB.AutoMigrate(&model.CryptoPriceEntry{})
		DB.AutoMigrate(&model.ExchangeSymbolAlias{})
		DB.AutoMigrate(&model.ExchangePreferredAsset{})
		DB.AutoMigrate(&model.MultiCoinTradeCfg{})
		DB.AutoMigrate(&model.PortfolioAsset{})
		DB.AutoMigrate(&model.Portfolio{})
		DB.AutoMigrate(&model.Role{})
		DB.AutoMigrate(&model.SmartNotification{})
		DB.AutoMigrate(&model.ThirdPartyApp{})
		DB.AutoMigrate(&model.TradeableAsset{})
		DB.AutoMigrate(&model.TradeDecision{})
		DB.AutoMigrate(&model.Trade{})
		DB.AutoMigrate(&model.TradeError{})
		DB.AutoMigrate(&model.TradeCondition{})
		DB.AutoMigrate(&model.TradeTrendStepCol{})
		DB.AutoMigrate(&model.TradeTrendStep{})
		DB.AutoMigrate(&model.UserCryptoExchange{})
		DB.AutoMigrate(&model.LedgerIO{})
		DB.AutoMigrate(&model.User{})
	})
}
