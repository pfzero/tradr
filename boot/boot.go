package boot

import (
	"sync"

	"bitbucket.org/pfzero/tradr/logs"

	"bitbucket.org/pfzero/tradr/store/datastore/cryptomarketstore"

	"bitbucket.org/pfzero/tradr/admin"
	"bitbucket.org/pfzero/tradr/daemon"
	"bitbucket.org/pfzero/tradr/service"
	"bitbucket.org/pfzero/tradr/store"
	"bitbucket.org/pfzero/tradr/trade"
	"bitbucket.org/pfzero/tradr/traderstore"
)

var initializeDBOnce sync.Once
var bootOnce sync.Once
var initializeAdminOnce sync.Once
var initializeServicesOnce sync.Once
var registerTradersOnce sync.Once
var runServicesOnce sync.Once

// InitializeDB initializes the DB connection to database
func InitializeDB() {
	initializeDBOnce.Do(func() {
		logger := logs.GetLogger()
		logger.Info("Starting DB service")
		// initialize database connection
		store.InitializeDB()

		// register db plugins
		store.RegisterPlugins()

		logger.Info("DB service completed")
	})
}

// InitializeAdmin initializes the admin interface
func InitializeAdmin() {
	initializeAdminOnce.Do(func() {
		logger := logs.GetLogger()
		logger.Info("Initializing Admin")
		// load admin module
		admin.Initialize()
		logger.Info("Admin module initialized")
	})
}

// InitializeServices prepares services
// for running
func InitializeServices() {
	initializeServicesOnce.Do(func() {
		logger := logs.GetLogger()
		// initialize the services
		logger.Info("Initializing services")
		service.Initialize()

		epa := cryptomarketstore.GetPreferredAssets()
		ea := cryptomarketstore.GetAliases()

		service.Provide(epa, ea)

		daemon.InitDaemons()

		logger.Info("Services initialized")
	})
}

// RegisterTraders registers all the type of traders
//  to the central traders repository
func RegisterTraders() {
	registerTradersOnce.Do(func() {
		logger := logs.GetLogger()
		logger.Info("Registering traders")
		// register traders
		providers := trade.GetTraderProviders()
		for i := 0; i < len(providers); i++ {
			err := traderstore.GetInstance().RegisterProvider(providers[i])
			if err != nil {
				panic(err)
			}
		}

		logger.Info("Traders registered")
	})
}

// RunServices starts the services
func RunServices() {
	runServicesOnce.Do(func() {
		logger := logs.GetLogger()

		logger.Info("Starting daemons")
		// start the daemons
		daemon.StartDaemons()
		logger.Info("Daemons Started")
	})
}

// CloseDB cleans database connections
func CloseDB() {
	logger := logs.GetLogger()

	logger.Info("closing db...")
	defer store.DB.Close()
}
